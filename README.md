# Gitlab Repository for the HLT1 Bandwidth Division Upgrade. 

## Prerequisites

- Source root and CMake. CMakeList.txt should activate ROOT and OMP (included in repository)
- If on lxplus this should work:
`source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_105 x86_64-el9-gcc13-opt`
- Or with an older LCG on lxplus7 (this may become deprecated):
`source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_101 x86_64-centos7-gcc10-opt`

## Compile the Project:
- Execute `mkdir /path/to/repository/build`. Navigate to the build folder:
- `cd build/`

- This command adds new source files to the project:
- `cmake ..`

- This command compiles all source and include (header) files:
-  `make`

## Config File:
- The configuration file that lets you customise many of the master function parameters is stored at `input_output/input_files/bw_config.txt`. This can be replaced with another file in the same directory with a different configuration and specified at commmand line (see `Run the Project'). 

- Change the input sample directories (`sample_dir`) in `bw_config.txt` to those desired, e.g, `sample_dir,/eos/lhcb/user/t/tevans/HLT1/hlt1_pp_matching_no_ut_tuning_data/300016/*.root,/eos/lhcb/user/t/tevans/BWDIV/July5/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_pp_matching_no_ut_tuning/`

- As shown, multiple directories are allowed, which can be used for separation of minbias/nobias and signal samples. This could allow, for example, the regeneration of the minbias file under different conditions while keeping the signal MC files the same before running a new division.

- A description of each input and which operation that input corresponds to is included in the commented (#) lines in the config file.

- The config file is read by `src/read_bwconfig(...)`, which is called by the master file `apps/bw_division.cpp`.

## Run the Project:
- Run the shell file output from CMake compilation:
- `./build/bin/bw_division`

- or if you're already in build just modify the path:
- `./bin/bw_division`

- For a full list of command line options: `./bin/bw_division help`

- One can specify the config file being read by the program as `./bin/bw_division bw_config_modified.txt`, as long as said config file is stored within `path/to/bw_division/input_output/input_files/`

- If the config file is specified, then the operation option, ratelimit percentage, tuple, run number and hyperparameter values will be read from the config file (all of these settings are explained in `./bin/bw_division help`)

## Threshold Smoothing

- Run divisions between 700 and 1300 kHz ratelimits
- `./bin/smooth_thresholds` will process all division output files: `path/to/bw_division/smoothing_output/thresholds_{rate_in_khz}_spreadsheet` and smooth them for monotonic behaviour between divisions at different ratelimits.
- These spreadsheet files may need to be moved from `/path/to/bw_division/input_output/output_files/` to the above directory

## Fixing Lines and Parameters and Switching Off lines
- Parameters can be fixed by writing `{param_name},fixed,{value_to_fix}` instead of the usual configuration (see `Add New Lines' below for the usual configuration).
- The lines can be fixed by writing on a new line `{line}_fix,{param_name};{value},{second_param_name};value` in the specified config file.
- If all lines involving a certain parameter are fixed, then the parameter itself automatically becomes fixed
- Lines may also be switched off in the config, by writing `{line}_turnoff` in the specified config file. This automatically forces the signal efficiency/rate of this line to zero.
- If all lines are switched off for a parameter, naturally this parameter will be essentially switched off
- The lines for each parameter can be shown by calling `./bin/bw_division 13` or setting `operation_option` in the config to 13

## Add New Samples
- Add new samples by adding entries to `input_output/input_files/samples/{working_group}.json` with event type, label and associated lines. Program will use only the lines implemented in the line source files, and tell you which are not being used.
- Samples (root files) must also be located in the designated directory indicated by `sample_dir` in configuration file `bw_config.txt` or the alternative specified config file.

## Add New Lines
- If they do not exist already, one must also add the `{input_parameter(s)}` in the config file as `{param_name},{initial_value},{minimum_value},{maximum_value},{number_of_steps_in_grid},{number_of_steps_for_gradient_calculation}`. The grid resolution determines the discrete set of values this parameter can take on (resolution), and the number of steps for the gradient calculation determines the smallest width of the gradient calculation. Use the same units as use in the sample root files. Make sure the minimum is the loosest cut and the maximum is the tightest cut

- Next, append the parameter name(s) to the array `_paramnames` in `include/line_parameters.h` and increase `_num_parameters` in the same file by the number of new parameters being added.

- If the line source and header file, e.g, `charm_lines.{cpp,h}` in `src/`, `include/` respectively does not exist for the type of line being added,
create new source and header files and `#include` the header file in `include/line_tools.h`.

- Additionally, `#include line_parameters.h` and `#include line.h` in the header file for the line, and then `#include line_tools.h` and `#include {new_line_header_name.h}` in the source file.

- Generate a new struct for the line in the same style as the other lines, e.g:

```cpp
struct d2pipi_line : public line_template<d2pipi_line, 3> {
  static constexpr const char* _name = "Hlt1D2PiPi";
  static constexpr std::array<const char*, 3> _quantities =
  {"min_pt", "min_ip", "D0_ct"};
  proxy _charm_track_ip{this, "charm_track_ip"};
  proxy _charm_track_pt{this, "charm_track_pt"};
  bool operator()(const float* monitored_quantities) const;
};
```

where the operator() member function override is declared exactly the same way, and defined almost the same way. The definition depends on four things:

- The decision boolean for the line (see below).
- The number of different quantities fed into the line. This affects how many variables are read from `const float* monitored_quantities` and used in the decision
- The monitored quantities being sourced from `const float* monitored_quantities`. These must be read in the same order as written in the `_quantities` array in the line struct definition in the header files (here it would be `include/charm.h`), e.g, if `static constexpr std::array<const char*, 3> _quantities = {"min_pt", "min_ip", "D0_ct"}` then the associated variables should be read as below:

### Example of operator() Override: 
```cpp
bool d2pipi_line::operator()(
  const float* monitored_quantities
  ) const {

  float track_pt =
  monitored_quantities[0];

  float track_ip =
  monitored_quantities[1];
  
  float d0_ct =
  monitored_quantities[2];

  return track_pt > _charm_track_pt && track_ip > _charm_track_ip 
  && d0_ct > _charm_track_ip; 
}
```

- Once the struct(s) have been authored and included correctly, one must append them to end of the type definition of `line_tuple` in `include/line_tools.h`

- Some fiddling with the parameter ranges and resolution may be needed, but after that, the new lines and parameters are implemented.

## Project Workflow

During Run III, before full event reconstruction, the rate must be reduced to the limit of the buffer between the HLT1 and HLT2. This means the readout for the entire LHCb physics program must be reduced to this rate, and then equitably divided between all physics channels. One can do this by tuning the thresholds on a number of trigger lines representative of each physics channel. For how this is done, see the backup slides of this [RTA DPA General meeting presentation](https://indico.cern.ch/event/1289622/#4-updates-on-bandwidth-divisio). The following is the program workflow that obtains the optimal HLT1 thresholds:

- Start from the initial values for each threshold chosen in the config file 
- Generate the first and second moment vectors, filled with a number of zero-values equal to the number of active parameters. These values vary each epoch and are dependent on the previous iteration moments, the hyper-parameters, the gradient values at a certain selection and the total number of epochs already processed by the minimiser.
- The first-order gradient values in each parameter axis are calculated at this initial selection, which is used for the calculation of the new values of the first and second moments in each axis.
- Since the system is discrete and the gradient is only an approximation due to statistical limitations and no analytical function to minimise, the gradient is estimated via a weighted smear using a number of gradient widths to calculate a more accurate value. The user can choose the number of widths to calculate over using the `accuracy` configuration option, although the lowest accuracy is not recommended due to non-monotonic behaviour of the gradient at smaller gradient widths.
- The direction and size of the step that the minimiser takes depend on the bias-corrected first and second moments, the learning rate and the resolution of the parameter axis.
- This system enables movement against positive gradients if the moment is large enough, which is beneficial for escaping local minima.
- During the first epoch, the moments are multiplied artificially. If the random or fixed initial selection begins within or near a local minimum, this acts to accelerate the path of the minimiser so that it is more likely to escape and fall into the global minimum.
- The stopping condition mechanism finds the smallest figure of merit value calculated so far, and if this occurred a certain threshold of epochs previously to the current epoch, and the current value figure of merit is not smaller than the last 5 values calculated, the minimisation terminates.
- After the first minimisation, a `warm reset' occurs, where another minimisation occurs, starting from the optimal selection chosen from the first one.
- The first moment in this warm reset is accelerated and directed in the opposite direction of the average of all calculated selections, which ensures the search of a larger total range of the parameter space, increasing the chance of global minimum convergence.
- Once the first and warm reset minimisations have completed, the program finds the nearest grid-coordinate selection of each epoch position and calculates the figure of merit at each of those grid points. From this, the program obtains the grid coordinates yielding the smallest figure of merit, and compares this to the figure of merit at the grid coordinates nearest to the smallest continuous figure of merit, choosing the optimal selection of the two.
- This is where the grid-search begins. All orthogonal and diagonal grid points within three resolution steps of the optimal selection in each parameter axis are calculated. If the program finds a grid point smaller than the current solution, it starts again from there and calculates the figure of merit all of the non-searched grid coordinates within three parameter resolution steps of the new set of coordinates. This continues recursively until all surrounding grid points yield a larger figure of merit.
- This solution is chosen, and any transformed variables are de-transformed and rounded to the nearest discrete value.
