// This file is deprecated (old genetic algorithm functions. Should be still
// usable but very slow/inefficient).

#include "minimise_fom.h"
#include "misc_tools.h"
#include "line_parameters.h"


double init_fom_minimiser::SpecificFunction(
  std::vector<double> input_parameters, const unsigned lineindex
  ) {
  // Performs a cut only on specified param
  // with input parameters on a tuple (for line-specific efficiencies)

  double initial_count = _tuple->_unique_precut_events;
  line_parameters inputs;
  int param_index{0};
  for (unsigned i{0}; i < inputs.size(); i++) {
    if (_tuple->_activate_params[i]) {
      // std::cout << "Param index: " << param_index << "\n";
      inputs[i] = input_parameters[param_index];
      param_index++;
    }
  }
  // If calculating the individual tuple FoM, call the non global postcut
  // event counter
  _final_counts.push_back(
  _tuple->specline_unique_events(inputs, lineindex));
  _background_efficiencies.push_back(
  1.0 * _tuple->_background_io->specline_unique_events(inputs,
  lineindex) / _tuple->_background_io->_unique_precut_events);

  // SEffs
  _signal_efficiencies.push_back((initial_count == 0) ? 0 :
  1.0 * _final_counts.back() / initial_count);
  _signal_efficiency_errs.push_back(sqrt(fabs(_signal_efficiencies.back()
  * (1 - _signal_efficiencies.back())) / initial_count));

  // BEffs
  _background_efficiency_errs.push_back(
  sqrt(fabs(_background_efficiencies.back()
  * (1 - _background_efficiencies.back())) /
  _tuple->_background_io->_unique_precut_events));
  
  // BRates
  _background_rates.push_back(
  _collision_rate * _background_efficiencies.back());
  _background_rate_errs.push_back(
  _collision_rate * _background_efficiency_errs.back());

  // Ratelimited SEffs
  _ratelim_signal_efficiencies.push_back(
  _signal_efficiencies.back() * ratelimiter(_background_efficiencies.back(),
  _ratelimit));
  _ratelim_signal_efficiency_errs.push_back(_signal_efficiency_errs.back()
  * ratelimiter(_background_efficiencies.back(), _ratelimit));
  // We can plot the minimiser path using these
  _inputs_used.push_back(input_parameters);
  _all_foms.push_back((1.0 - _ratelim_signal_efficiencies.back())
  * (1.0 - _ratelim_signal_efficiencies.back())
  + 0.0001 * _background_efficiencies.back());
  return _all_foms.back();
}


Double_t init_fom_minimiser::EstimatorFunction(
  std::vector<Double_t>& input_parameters
  ) {
  // Figure of merit function for individual sample FoM minimisation

  // TStopwatch t;
  // t.Start();
  _calc_counter++; 
  // Calculated beforehand
  double initial_count = _tuple->_unique_precut_events;
  line_parameters params;
  int param_index{0};
  for (unsigned i{0}; i < params.size(); i++) {
    if (_tuple->_activate_params[i]) {
      // std::cout << "Param index: " << param_index << "\n";
      params[i] = input_parameters[param_index];
      param_index++;
    }
  }
  // Cut on all lines in the signal sample
  _final_counts.push_back(_tuple->calc_unique_events(
  params, _tuple->_activate_lines));

  // SEffs
  _signal_efficiencies.push_back((initial_count == 0) ? 0 :
  1.0 * _final_counts.back() / initial_count);
  _signal_efficiency_errs.push_back(sqrt(fabs(_signal_efficiencies.back()
  * (1 - _signal_efficiencies.back())) / initial_count));

  // Background cut only on the same lines as the sample
  // BEffs
  // True indicates background cut.
  _background_efficiencies.push_back(
  1.0 * _tuple->_background_io->calc_unique_events(
  params, _tuple->_activate_lines)
  / _tuple->_background_io->_unique_precut_events);
  _background_efficiency_errs.push_back(
  sqrt(fabs(_background_efficiencies.back()
  * (1 - _background_efficiencies.back())) /
  _tuple->_background_io->_unique_precut_events));

  // BRates
  _background_rates.push_back(
  _collision_rate * _background_efficiencies.back());
  _background_rate_errs.push_back(
  _collision_rate * _background_efficiency_errs.back());

  // Ratelimited SEffs
  _ratelim_signal_efficiencies.push_back(
  _signal_efficiencies.back() * ratelimiter(_background_efficiencies.back(),
  _ratelimit));
  _ratelim_signal_efficiency_errs.push_back(_signal_efficiency_errs.back()
  * ratelimiter(_background_efficiencies.back(), _ratelimit));
  // We can plot the minimiser path using these
  _inputs_used.push_back(input_parameters);
  _all_foms.push_back((1.0 - _ratelim_signal_efficiencies.back())
  * (1.0 - _ratelim_signal_efficiencies.back())
  + 0.0001 * _background_efficiencies.back());
 
  // t.Stop();
  // std::cout << "FoM calculation " << _calc_counter << " ";
  // << " for " << _tuple->_filesafe_name << " and inputs ";
  // for (double input: input_parameters) {
  //   std::cout << input << " ";
  // }
  // std::cout << _all_foms.back() << ":\n";
  // t.Print();

  return _all_foms.back();
}


void init_fom_minimiser::plot_min_path(std::string plot_dir) {
  // Plotting all FoMs calculated by the minimiser and the corresponding
  // inputs as a top-down TGraph2D object (technically 1D)
    
  if (_tuple->_parameters.size() > 2) {
    std::cout << "Too many cut variables for 2D plot\n";
    return;
  }

  std::string tuplename = _tuple->_filesafe_name;
  std::vector<std::string> cut_variables = _tuple->_axes;
  tuplename[0] = toupper(tuplename[0]);
  for (std::string var: cut_variables) {
    var[0] = toupper(var[0]);
  }
  std::string plot_name = _tuple->_filesafe_name + "_fom_path";
  std::string plot_title = "Figure of Merit for " + tuplename +
  + " vs. Cuts on: ";
  for (std::string cut: cut_variables) {
    plot_title += cut + ",";
  }
  plot_title.pop_back();
  
  std::string canvas_dir = plot_dir + "minpath_plots/";
  std::filesystem::create_directories(canvas_dir.c_str());
  std::vector<std::string> plot_strings =
  {tuplename, cut_variables[0], cut_variables[1], "FoM", plot_dir,
  plot_title, canvas_dir};
  std::vector<double> first_axis_values, second_axis_values;
  for (unsigned i{0}; i < _inputs_used.size(); i++) {
    first_axis_values.push_back(_inputs_used[i][0]);
    second_axis_values.push_back(_inputs_used[i][1]);
  }
  // Topdown view
  plot_results_vs_lineor_cut(plot_strings, first_axis_values,
  second_axis_values, _all_foms, -90, 270, "pcolz line");

}


double apply_individual_minimiser(
  io_manager* tuple, double collision_rate, double ratelimit,
  std::string options, bool plot_min_paths,
  std::string plot_dir 
  ) {
  // Use the initial minimising algorithm to minimise the FoM for
  // individual samples and return the real time taken to do so

  TStopwatch t;
  t.Start();
      
  std::vector<Interval*> ranges;
  std::vector<double> mins;
  
  // Obtaining parameter axes
  line_parameters params;
  // std::cout << "Signal channel min, max, nbins, step size:\n";
  for (unsigned i{0}; i < params._paramnames.size(); i++) {
    if (tuple->_activate_params[i]) {
      for (auto& [k,v] : tuple->_background_io->_parameters) {
        if (params._paramnames[i].compare(k) == 0) {
          ranges.push_back(new Interval(v._min, v._max, v._steps + 1));
          mins.push_back(v._min);
          // std::cout << k << ": " << v._min << ", " << v._max << ", "
          // << v._steps + 1 << ", " << ranges.back()->GetStepSize() << "\n";
          break;
        }
      }
    }
  }
  std::cout << ":\n";

  // tuple->_parallelise_events = true;
  // tuple->_background_io->_parallelise_events = true;


  std::vector<Double_t> results;

  tuple->_maxeff_minimiser =
  new init_fom_minimiser(tuple, collision_rate, ratelimit);
  IFitterTarget* initial_minimiser = tuple->_maxeff_minimiser;

  bool no_tunable_params = tuple->_parameters.size() == 0;
  double optimal_fom{0};

  if (no_tunable_params) {
    std::cout << "No tunable parameters for "
    << tuple->_filesafe_name << ". Skipping minimisation...\n";
    for (unsigned i{0}; i < params.size(); i++) {
      results.push_back(
      tuple->_background_io->_parameters[params._paramnames[i]]._min);
    }
    optimal_fom = initial_minimiser->EstimatorFunction(results);
  } else {
    if (tuple->_loosest_selection) {
      std::cout << "Loosest selection produces rate < "
      << (ratelimit * collision_rate)/1e6
      << " MHz. Choosing loosest selection...\n";
      for (unsigned i{0}; i < params.size(); i++) {
        if (tuple->_activate_params[i]) {
          results.push_back(
          tuple->_background_io->_parameters[params._paramnames[i]]._min);
        }
      }
      optimal_fom = initial_minimiser->EstimatorFunction(results);
    } else {
      // Obtaining and running algorithm
      FitterBase* fitter = new GeneticFitter(*initial_minimiser, "FitterGA",
      ranges, options.c_str());

      optimal_fom = fitter->Run(results);
    } 
  }

  // Recording and printing the optimal results
  std::cout << "\nResults for " << tuple->_filesafe_name << " tuple:\n";
  tuple->_optimal_fom = optimal_fom;
  std::cout << "Optimal figure of merit: " << tuple->_optimal_fom << "\n";
  tuple->_optimal_minbias_rate =
  tuple->_maxeff_minimiser->_background_rates.back();
  std::cout << "Optimal minbias rate: " << tuple->_optimal_minbias_rate
  << "\n";
  tuple->_optimal_minbias_efficiency =
  tuple->_maxeff_minimiser->_background_efficiencies.back();
  std::cout << "Optimal minbias efficiency: "
  << tuple->_optimal_minbias_efficiency << "\n";
  tuple->_optimal_minbias_efficiency_err =
  tuple->_maxeff_minimiser->_background_efficiency_errs.back();
  std::cout << "Optimal minbias efficiency error: "
  << tuple->_optimal_minbias_efficiency_err << "\n";
  tuple->_optimal_signal_eff =
  tuple->_maxeff_minimiser->_signal_efficiencies.back();
  std::cout << "Optimal signal efficiency: " << tuple->_optimal_signal_eff
  << "\n";
  tuple->_optimal_signal_eff_err =
  tuple->_maxeff_minimiser->_signal_efficiency_errs.back();
  std::cout << "Optimal signal efficiency error: "
  << tuple->_optimal_signal_eff_err << "\n";
  tuple->_optimal_ratelimited_signal_eff =
  tuple->_maxeff_minimiser->_ratelim_signal_efficiencies.back();
  std::cout << "Optimal ratelimited signal efficiency: "
  << tuple->_optimal_ratelimited_signal_eff << "\n";
  tuple->_optimal_ratelimited_signal_eff_err =
  tuple->_maxeff_minimiser->_ratelim_signal_efficiency_errs.back();
  std::cout << "Optimal ratelimited signal efficiency error: "
  << tuple->_optimal_ratelimited_signal_eff_err << "\n";
  
  for (unsigned i{0}; i < tuple->_axes.size(); i++) {
    std::cout << "Optimal value for parameter "
    << tuple->_axes[i] << ": " << results[i] << "\n";
    tuple->_optimal_parameter_inputs.push_back(results[i]);
  }

  t.Stop();
  std::cout << "Time taken for individual minimisation: ";
  t.Print();
  std::cout << "\n";

  std::cout << "Calculating max efficiency per line...\n";
  line_store linestore;
  tuple->_specline_minimiser =
  new init_fom_minimiser(tuple, collision_rate, ratelimit);
  for (unsigned i{0}; i < NUM_LINES; i++) {
    if (tuple->_activate_lines[i]) {
      std::cout << linestore._linenames[i] << ": "
      << tuple->_specline_minimiser->SpecificFunction(results, i)
      << "\n";
    }
  }

  // Plot the minimiser path if desired
  if (plot_min_paths) {
    tuple->_maxeff_minimiser->plot_min_path(plot_dir);
  }

  return t.RealTime();
}



void minimise_individual_ntuples(
  std::vector<io_manager*> tuples, std::string plot_dir,
  double collision_rate, double ratelimit, std::string options,
  bool plot_min_paths
  ) {
  // Minimising the figure of merit for each trigger line if they are
  // allocated the full 1MHz of minimium bias. Obtaining max efficiencies
  // from these optimal inputs to be used in opt_fom_minimiser

  int counter{0};
#pragma omp parallel
  {
#pragma omp for schedule (dynamic) nowait
    for (io_manager* tuple: tuples) {
      TStopwatch tempt;
      tempt.Start();

      apply_individual_minimiser(tuple, collision_rate, ratelimit, options,
      false, // plot_min_paths = false
      plot_dir);

      tempt.Stop();

      std::cout << "\nTime taken for " << tuple->_filesafe_name
      << " FoM to be minimised by GA:\n";

      tempt.Print();
      std::cout << "\n";
      counter++;
      std::cout << "Number of samples minimised: " << counter << "\n";
    }
  }
} 


Double_t opt_fom_minimiser::EstimatorFunction(
  std::vector<Double_t>& input_parameters
  ) {
  // Figure of merit function for global minimisation
   
  // TStopwatch t;
  // t.Start();
  _inputs_used.push_back(input_parameters);
  _calc_counter++; 
  double global_fom{0};
  line_parameters inputs(input_parameters);

  // Calculate the background efficiency for all minbias lines
  _background_efficiency =
  1.0 * _tuples[0]->_background_io->calc_unique_events(
  inputs, _tuples[0]->_background_io->_activate_lines)
  / _tuples[0]->_background_io->_unique_precut_events;
  // Binomial errors
  _background_efficiency_err =
  sqrt(fabs(_background_efficiency * (1.0 - _background_efficiency)) /
  (1.0 * _tuples[0]->_background_io->_unique_precut_events));

  _background_rate = _background_efficiency * _collision_rate;
  _background_rate_err = _background_efficiency_err * _collision_rate;
  double ratelimit_factor = ratelimiter(_background_efficiency, _ratelimit);

  _ratelim_signal_efficiencies.clear();
  _ratelim_signal_efficiency_errs.clear();
  _signal_efficiencies.clear();
  _signal_efficiency_errs.clear();
  // Adding to figure of merit from current-input efficiency and max
  // efficiency from all tuples
#pragma omp parallel for
  for (unsigned i = 0; i < _tuples.size(); i++) {
    io_manager* tuple = _tuples[i];
    if (tuple->_special > 0.0) {
      // Calculated beforehand
      double initial_count = tuple->_unique_precut_events;
      double final_count =
      tuple->calc_unique_events(inputs, tuple->_activate_lines);

      _signal_efficiencies.push_back(
      (initial_count == 0) ? 0 : (final_count / initial_count));
      _ratelim_signal_efficiencies.push_back(
      _signal_efficiencies.back() * ratelimit_factor);
  
      _signal_efficiency_errs.push_back(
      sqrt(fabs(_signal_efficiencies.back()
      * (1.0 - _signal_efficiencies.back())) / initial_count));
      _ratelim_signal_efficiency_errs.push_back(
      sqrt(fabs(_ratelim_signal_efficiencies.back()
      * (1.0 - _ratelim_signal_efficiencies.back())) / initial_count));
      double max_eff = tuple->_optimal_ratelimited_signal_eff;

      // Add to figure of merit
      double fomadd =
      tuple->_special * ((1.0 - (_ratelim_signal_efficiencies.back()
      / max_eff)) * (1.0 - (_ratelim_signal_efficiencies.back()
      / max_eff)));
      // std::cout << "Fom add for " << tuple->_filesafe_name
      // << fomadd << "\n";
      global_fom += fomadd;
    }
  }

  // Adding gradient
  global_fom += 0.0001 * _background_efficiency;
  _all_foms.push_back(global_fom);
  // t.Stop();
  std::cout << "FoM calculation " << _calc_counter << ": ";
  // << " with inputs ";
  // for (double input: input_parameters) {
  //   std::cout << input << " ";
  // }
  std::cout << _all_foms.back() << "\n";
  // t.Print();

  return global_fom;
}


void plot_global_statistics(
  std::string plot_dir, std::vector<io_manager*> tuples,
  double collision_rate, double ratelimit, std::vector<double> results,
  opt_fom_minimiser* global_minimiser
  ) {

  std::string canvas_dir = plot_dir + "global_minplots/";
  std::filesystem::create_directories(canvas_dir.c_str());

  // Calc individual line efficiencies
  line_store linestore;
  line_parameters params;
  for (unsigned i{0}; i < tuples.size(); i++) {
    std::cout << "Calc individual line efficiencies for "
    << tuples[i]->_filesafe_name << "\n";
    // Calc statistics with new optimal cut values (done for
    // individual tuple optimal values in minimise_individual_tuples)
    init_fom_minimiser* specline_minimiser =
    new init_fom_minimiser(tuples[i], collision_rate, ratelimit);
    
    std::vector<double> spec_results;
    for (unsigned j{0}; j < params.size(); j++) {
      if (tuples[i]->_activate_params[j]) {
        spec_results.push_back(results[i]);
      }
    }

    for (unsigned j{0}; j < NUM_LINES; j++) {
      specline_minimiser->SpecificFunction(spec_results, j); 
    }

    TH1D* maxeff_per_line =
    new TH1D((tuples[i]->_filesafe_name + "_maxeff_per_line").c_str(),
    ("Efficiencies per line for " + tuples[i]->_filesafe_name).c_str(),
    NUM_LINES, 0, NUM_LINES);
    maxeff_per_line->SetYTitle("Efficiency");
    maxeff_per_line->SetStats(kFALSE);
    maxeff_per_line->SetFillStyle(3345);
    maxeff_per_line->SetLineWidth(2);
    maxeff_per_line->SetLineColor(kRed);
    maxeff_per_line->SetFillColor(kRed);

    TH1D* finaleff_per_line =
    new TH1D((tuples[i]->_filesafe_name + "_finaleff_per_line").c_str(),
    ("Efficiencies per line for " + tuples[i]->_filesafe_name).c_str(),
    NUM_LINES, 0, NUM_LINES);
    finaleff_per_line->SetYTitle("Efficiency");
    finaleff_per_line->SetStats(kFALSE);
    finaleff_per_line->SetFillStyle(3354);
    finaleff_per_line->SetLineWidth(2);
    finaleff_per_line->SetLineColor(kBlue);
    finaleff_per_line->SetFillColor(kBlue);

    // std::cout << "Max and final effs for:\n";
    for (unsigned j{1}; j <= NUM_LINES;  j++) {
      if (tuples[i]->_activate_lines[j-1]) {
        // std::cout << linestore._linenames[j-1] << ": "
        // << tuples[i]->_specline_minimiser->_signal_efficiencies[j-1]
        // << ", " << specline_minimiser->_signal_efficiencies[j-1] << "\n";
        maxeff_per_line->SetBinContent(j,
        tuples[i]->_specline_minimiser->_signal_efficiencies[j-1]);
        maxeff_per_line->SetBinError(j,
        tuples[i]->_specline_minimiser->_signal_efficiency_errs[j-1]);
        maxeff_per_line->GetXaxis()->SetBinLabel(j,
        linestore._linenames[j-1].c_str());
        finaleff_per_line->SetBinContent(j,
        specline_minimiser->_signal_efficiencies[j-1]);
        finaleff_per_line->SetBinError(j,
        specline_minimiser->_signal_efficiency_errs[j-1]);
        finaleff_per_line->GetXaxis()->SetBinLabel(j,
        linestore._linenames[j-1].c_str());
      }
    }
    
    TCanvas* totaleff_perline_canvas =
    new TCanvas((tuples[i]->_filesafe_name + "totaleff_perline").c_str(),
    ("Total Efficiency per Line for " + tuples[i]->_name).c_str(),
    0, 0, 900, 450);
    totaleff_perline_canvas->cd();

    maxeff_per_line->GetXaxis()->LabelsOption("v");
    maxeff_per_line->GetYaxis()->LabelsOption("v");
    maxeff_per_line->SetMinimum(0.0);
    maxeff_per_line->Draw("Hist, E1");

    finaleff_per_line->GetXaxis()->LabelsOption("v");
    finaleff_per_line->GetYaxis()->LabelsOption("v");
    finaleff_per_line->SetMinimum(0.0);
    finaleff_per_line->Draw("Hist, E1, SAME");
    
    totaleff_perline_canvas->SaveAs((canvas_dir
    + tuples[i]->_filesafe_name + "totaleffs_perline.tex").c_str());
    totaleff_perline_canvas->SaveAs((canvas_dir
    + tuples[i]->_filesafe_name + "totaleffs_perline.C").c_str());
    totaleff_perline_canvas->SaveAs((canvas_dir
    + tuples[i]->_filesafe_name + "totaleffs_perline.svg").c_str());

    std::cout << "Optimal signal statistics for tuple "
    << tuples[i]->_filesafe_name << ":\n";
    std::cout << "Optimal signal efficiency: "
    << global_minimiser->_signal_efficiencies[i] << "\n";
    std::cout << "Optimal ratelimited signal efficiency: "
    << global_minimiser->_ratelim_signal_efficiencies[i] << "\n";
    std::cout << "Optimal signal efficiency error: "
    << global_minimiser->_signal_efficiency_errs[i] << "\n";
    std::cout << "Optimal ratelimited signal efficiency error: "
    << global_minimiser->_ratelim_signal_efficiency_errs[i] << "\n";
    delete specline_minimiser;
  }

  TH1D* maxeff_bars =
  new TH1D("maxeff_bars", "Total Efficiencies per Channel", tuples.size(),
  0, tuples.size());
  maxeff_bars->SetXTitle("Samples");
  maxeff_bars->SetYTitle("Efficiency");
  maxeff_bars->SetStats(kFALSE);
  maxeff_bars->SetFillStyle(3345);
  maxeff_bars->SetLineWidth(2);
  maxeff_bars->SetLineColor(kRed);
  maxeff_bars->SetFillColor(kRed);

  TH1D* finaleff_bars =
  new TH1D("finaleff_bars", "Total Efficiencies per Channel", tuples.size(),
  0, tuples.size());
  finaleff_bars->SetXTitle("Samples");
  finaleff_bars->SetYTitle("Efficiency");
  finaleff_bars->SetStats(kFALSE);
  finaleff_bars->SetFillStyle(3354);
  finaleff_bars->SetLineWidth(2);
  finaleff_bars->SetLineColor(kBlue);
  finaleff_bars->SetFillColor(kBlue);

  for (unsigned i{1}; i <= tuples.size(); i++) {
    std::cout << "Max and final effs (global) for "
    << tuples[i-1]->_filesafe_name << ": "
    << tuples[i-1]->_optimal_signal_eff <<
    ", " << global_minimiser->_signal_efficiencies[i-1] << "\n";
    maxeff_bars->SetBinContent(i, tuples[i-1]->_optimal_signal_eff);
    maxeff_bars->SetBinError(
    i, tuples[i-1]->_optimal_signal_eff_err);
    maxeff_bars->GetXaxis()->SetBinLabel(i, tuples[i-1]->_name.c_str());
    
    finaleff_bars->SetBinContent(
    i, global_minimiser->_signal_efficiencies[i-1]);
    finaleff_bars->SetBinError(
    i, global_minimiser->_signal_efficiency_errs[i-1]);
    finaleff_bars->GetXaxis()->SetBinLabel(i, tuples[i-1]->_name.c_str());
  }

  TCanvas* totaleff_canvas = new TCanvas("totaleff_canvas",
  "Total Efficiency Plot", 0, 0, 1000, 600);
  totaleff_canvas->cd();

  maxeff_bars->GetXaxis()->LabelsOption("v");
  maxeff_bars->GetYaxis()->LabelsOption("v");
  maxeff_bars->SetMinimum(0.0);
  maxeff_bars->Draw("Hist, E1");

  finaleff_bars->GetXaxis()->LabelsOption("v");
  finaleff_bars->GetYaxis()->LabelsOption("v");
  finaleff_bars->SetMinimum(0.0);
  finaleff_bars->Draw("Hist, E1, SAME");
  
  gPad->BuildLegend(0.8,0.5,0.97,0.7,"");
  totaleff_canvas->SaveAs((canvas_dir + "totaleffs.tex").c_str());
  totaleff_canvas->SaveAs((canvas_dir + "totaleffs.C").c_str());
  totaleff_canvas->SaveAs((canvas_dir + "totaleffs.svg").c_str());

}


double minimise_all_ntuples(
  std::vector<io_manager*> tuples, std::string plot_dir,
  double collision_rate, double ratelimit,
  std::string init_options, std::string opt_options
  ) {
  // Function to minimise each individual ntuple using init_fom_minimiser
  // and then minimise all of them globally, before plotting the results

  TStopwatch t;
  t.Start();
  minimise_individual_ntuples(tuples, plot_dir,
  collision_rate, ratelimit, init_options, false);

  std::vector<Interval*> ranges;
  
  line_parameters params;
  io_manager* minbias = tuples[0]->_background_io;
  // Obtaining parameter axes
  // std::cout << "Ranges for all parameters: ";
  for (unsigned i{0}; i < params._paramnames.size(); i++) {
    for (auto& [k,v] : minbias->_parameters) {
      if (params._paramnames[i].compare(k) == 0) {
        ranges.push_back(new Interval(v._min, v._max, v._steps + 1));
        // std::cout << k << ": " << v._min << ", " << v._max << ", "
        // << v._steps + 1 << ", " << ranges.back()->GetStepSize() << "\n";
        break;
      }
    }
  }

  tuples[0]->_background_io->_parallelise_events = true;
  // for (io_manager* tuple: tuples) {
  //   tuple->_parallelise_events = true;
  // }

  std::cout << "\nMinimising global FoM...\n";
  // Obtaining and running algorithm
  opt_fom_minimiser* global_minimiser =
  new opt_fom_minimiser(tuples, collision_rate, ratelimit);
  IFitterTarget* opt_minimiser = global_minimiser;
  FitterBase* fitter = new GeneticFitter(*opt_minimiser, "FitterGA",
  ranges, opt_options.c_str());

  std::vector<Double_t> results;
  double optimal_fom = fitter->Run(results);

  // Recording the optimal results
  std::cout << "\n\nGlobal results:\n";
  for (unsigned i{0}; i < results.size(); i++) {
    std::cout << "Optimal value for parameter "
    << minbias->_axes[i] << ": " << results[i] << "\n";
    minbias->_optimal_parameter_inputs.push_back(results[i]);
  }
  std::cout << "Optimal figure of merit: " << optimal_fom << "\n";
  std::cout << "Optimal minbias rate: "
  << global_minimiser->_background_rate << "\n";
  std::cout << "Optimal minbias rate error: "
  << global_minimiser->_background_rate_err << "\n";
  
  // std::string canvas_dir = plot_dir + "global_minplots/";
  // std::filesystem::create_directories(canvas_dir.c_str());

  // plot_global_statistics(plot_dir, tuples, collision_rate, ratelimit,
  // results, global_minimiser);

  return t.RealTime();
}


void manual_optparam_entry(
  std::vector<double> manual_inputs, std::vector<io_manager*> tuples,
  std::string plot_dir, double collision_rate,
  double ratelimit, std::string init_options
  ) {
  // Fastpass function you know the optimal global parameter configuration
  // and want to replot the results

  minimise_individual_ntuples(tuples, plot_dir,
  collision_rate, ratelimit, init_options, false);

  opt_fom_minimiser* global_minimiser =
  new opt_fom_minimiser(tuples, collision_rate, ratelimit);
  double optimal_fom = global_minimiser->EstimatorFunction(manual_inputs);
  io_manager* minbias = tuples[0]->_background_io;
  // Recording the optimal results
  std::cout << "\n\nGlobal results:\n";
  for (unsigned i{0}; i < manual_inputs.size(); i++) {
    std::cout << "Optimal value for parameter "
    << minbias->_axes[i] << ": " << manual_inputs[i] << "\n";
    minbias->_optimal_parameter_inputs.push_back(manual_inputs[i]);
  }
  std::cout << "Optimal figure of merit: " << optimal_fom << "\n";
  std::cout << "Optimal minbias rate: "
  << global_minimiser->_background_rate << "\n";
  std::cout << "Optimal minbias rate error: "
  << global_minimiser->_background_rate_err << "\n";
  
  plot_global_statistics(plot_dir, tuples, collision_rate, ratelimit,
  manual_inputs, global_minimiser);
}
