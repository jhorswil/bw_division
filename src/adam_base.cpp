#include "adam_base.h"
#include "io_manager.h"
#include "opt_discrete_gradient.h"
#include "init_discrete_gradient.h"



adam_base::adam_base(
  bool glob_or_indiv, std::vector<io_manager*> tuples,
  double collision_rate, double ratelimit, double scalewidth,
  int accuracy, bool include_prints
  ) {
  // Parameterised constructor for adam minimiser base class

  _glob_or_indiv = glob_or_indiv;
  _tuples = tuples;
  _collision_rate = collision_rate;
  _ratelimit = ratelimit;
  _scalewidth = scalewidth;
  _accuracy = accuracy;
  _include_prints = include_prints;
  if (_tuples[0]->_background_io->_parameter_values.empty()) {
    _tuples[0]->_background_io->fill_parameter_values();
  }
  for (io_manager* tuple: _tuples) {
    if (tuple->_parameter_values.empty()) {
      tuple->fill_parameter_values();
    }
  }
  // Minbias dictates inputs
  auto& min_bias = _tuples[0]->_background_io; 
  auto lp = line_parameters(); 

  bool some_nonfixed_params{false};
  // In the line_parameters order
  for(unsigned i{0}; i != lp.size(); ++i){
    _parameters.emplace_back(min_bias->_parameters[lp._paramnames[i]]);
    if (!(min_bias->_parameters[lp._paramnames[i]]._fixed)) {
      some_nonfixed_params = true;
      // _parameters[i]._value =
      // min_bias->_parameters[lp._paramnames[i]]._value;
    }
  }
  if (!some_nonfixed_params) {
    std::cout << "None of the parameters are floating. "
    "Change parameter/line settings in config to run division.\n";
    exit(0);
  }
}


line_parameters adam_base::random_coord_generator() const {
  // Does what is says on the tin. Generates random set of coordinates
  // with all parameters represented

  // Generate coord from minbias coord range. Inactive coords will not be
  // used anyway
  line_parameters random_coords;
  for (unsigned i{0}; i < random_coords.size(); i++) {
    if ((_glob_or_indiv || _tuples[0]->_activate_params[i])
    && !(_parameters[i]._fixed || _parameters[i]._off)) {
      // Choose random element of the parameter vector
      std::random_device rd;
      std::mt19937 eng(rd());

      line_parameters param_mins;
      param_mins[i] = _tuples[0]->_background_io->_cutoff_params[i];

      line_parameters nearest =
      _tuples[0]->find_nearest_point(param_mins);
      for (unsigned i{0}; i < nearest.size(); i++) {
        if (_parameters[i]._fixed) nearest[i] = _parameters[i]._value;
      }

      std::vector<double> possible_values =
      _tuples[0]->_background_io->_parameter_values[i];

      int index = std::distance(possible_values.begin(),
      std::find(possible_values.begin(), possible_values.end(),
      nearest[i]));

      std::uniform_int_distribution<> distr(index, 
      _tuples[0]->_background_io->_parameter_values[i].size() - 1);

      int random_index = distr(eng);
      random_coords[i] =
      _tuples[0]->_background_io->_parameter_values[i][random_index];

      if (_glob_or_indiv || _include_prints) {
        std::cout << "Random index chosen: " << random_index << "\n";
        std::cout << "Random coord: " << random_coords[i] << "\n";
      }
    } else {
      random_coords[i] = 0;
      // std::cout << "Param not active, setting to zero.\n";
    }
  }
  return random_coords;
}


std::vector<line_parameters> adam_base::list_orthogonal_coords(
  const line_parameters& input_parameters
  ) const {
  // List all orthogonal neighbouring gradient coordinates
  // (total 2 * number of input
  // parameters). Using gradient resolutions, so not on grid.

  std::vector<line_parameters> neighbouring_coordinates;
  for (unsigned i{0}; i < input_parameters.size(); i++) {
    const auto& config = _parameters[i];

    if ((_glob_or_indiv || _tuples[0]->_activate_params[i])
    && !_parameters[i]._fixed) {
      line_parameters coords = input_parameters;
      coords[i] = input_parameters.access(i) + _scalewidth
      * config._grad_resolution; 
      
      neighbouring_coordinates.push_back(coords);
      coords[i] = input_parameters.access(i)
      - _scalewidth * config._grad_resolution; 
      neighbouring_coordinates.push_back(coords);
    }
  }

  return neighbouring_coordinates;
}


std::vector<line_parameters> adam_base::list_orthogonal_diagonal_coords(
  const line_parameters& input_parameters, const int depth
  ) const {
  // List all orthogonal grid coordinates and their pair diagonals
  // Total = 4N(N-1) where N is number of input parameters

  std::vector<line_parameters> neighbouring_coordinates;
  // std::vector<int> nsteps = _tuples[0]->_background_io->_unique_nsteps;
  // int minsteps = *min_element(nsteps.begin(), nsteps.end());

  // Orthogonal coords
  // Diagonal pair coordinates
  std::vector<int> signs = {1, -1, -1, 1, 1, 1, -1, -1};
  for (int depth_count{1}; depth_count < depth + 1; depth_count++) {
    for (unsigned i{0}; i < input_parameters.size(); i++) {
      if ((_glob_or_indiv || _tuples[0]->_activate_params[i])
      && !_parameters[i]._fixed) {
        // resolutions conserve sign
        double standard_step =
        depth_count * _parameters[i].resolution();
        const auto& config = _parameters[i]; 

        bool outside_min = sign(config._max - config._min) >= 0.0
        ? input_parameters.access(i) - standard_step
        <= config._min
        : input_parameters.access(i) - standard_step
        >= config._min;

        bool outside_max =
        sign(config._max - config._min) >= 0.0
        ? input_parameters.access(i) + standard_step
        >= config._max 
        : input_parameters.access(i) + standard_step
        <= config._max; 

        double subtract = outside_min ? config._min 
        : -1.0 * standard_step;
        double add = outside_max ? config._max
        : standard_step;

        line_parameters coords = input_parameters;
        coords[i] = input_parameters.access(i) + add;
        neighbouring_coordinates.push_back(coords);
        coords[i] = input_parameters.access(i) + subtract;
        neighbouring_coordinates.push_back(coords);
      }
    }

    std::vector<std::vector<int>> param_combs;
    for (unsigned i{0}; i < input_parameters.size(); i++) {
      for (unsigned j{0}; j < input_parameters.size(); j++) {
        if (i != j && (_glob_or_indiv || (_tuples[0]->_activate_params[i]
          && !_parameters[i]._fixed && _tuples[0]->_activate_params[j]
          && !_parameters[j]._fixed))) {

          bool proceed = true;
          for (unsigned k{0}; k < param_combs.size(); k++) {
            if ((std::find(param_combs[k].begin(),
              param_combs[k].end(), (int)i) != param_combs[k].end())
              && (std::find(param_combs[k].begin(),
              param_combs[k].end(), (int)j) != param_combs[k].end())) {
              proceed = false;
              break;
            }
          }

          if (proceed) {
            double standard_stepi;
            double standard_stepj;
            if (depth_count > 1.0) {
              standard_stepi = depth_count * _parameters[i].resolution();
              standard_stepj = depth_count * _parameters[j].resolution(); 
            } else {
              standard_stepi = _parameters[i].resolution();
              standard_stepj = _parameters[j].resolution();
            }

            for (unsigned k{0}; k < signs.size()/2; k++) {
              line_parameters coords = input_parameters;        
              coords[i] = input_parameters.access(i)
              + (signs[k] * standard_stepi);
              coords[j] = input_parameters.access(j)
              + (signs[k+1] * standard_stepj);

              neighbouring_coordinates.push_back(coords); 
            }
            
            param_combs.push_back(std::vector<int>({(int)i, (int)j}));
          }  
        }
      }
    }
  }

  // if (_glob_or_indiv || _include_prints) {
  //     std::cout << "Number of orthog diag coordinates: "
  //     << neighbouring_coordinates.size() << "\n";
  // }

  return neighbouring_coordinates;
}


void adam_base::gradient_loop(
  double& scale_multiple, std::vector<bool>& zero_gradient_params,
  bool& zero_gradients, std::vector<line_parameters>& coords_to_calculate,
  std::vector<std::vector<double>>& all_stats,
  unsigned weight_size, std::vector<double>& gradient_vector,
  line_parameters input_parameters,
  const std::vector<std::vector<int>> weights,
  const std::vector<double> divisions,
  std::vector<std::vector<line_parameters>>& inputs_used,
  std::vector<std::vector<double>>& foms,
  std::vector<std::vector<double>>& background_efficiencies,
  std::vector<std::vector<double>>& background_efficiency_errs,
  std::vector<std::vector<double>>& background_rates,
  std::vector<std::vector<double>>& background_rate_errs,
  std::vector<std::vector<double>>& signal_efficiencies,
  std::vector<std::vector<double>>& signal_efficiency_errs,
  std::vector<std::vector<double>>& ratelim_signal_efficiencies,
  std::vector<std::vector<double>>& ratelim_signal_efficiency_errs,
  std::vector<std::vector<int>>& final_counts
  ) {

  if (scale_multiple > 1.0 && (_glob_or_indiv || _include_prints)) {
    std::cout << "Multiplying gradient width scale for: ";
    for (unsigned i{0}; i < zero_gradient_params.size(); i++) {
      if (zero_gradient_params[i]) {
        std::cout << input_parameters._paramnames[i] << " ";
      }
    }
    std::cout << "\n";
  }

  coords_to_calculate.clear();
  all_stats.clear();
  int grad_size = (int)gradient_vector.size();

  // Generating inputs and result vectors in order loosest to tightest
  // selection
  // std::cout << "Gradient vector size: " << grad_size << "\n";
  // std::cout << "Weight vector size: " << weight_size << "\n";
  // std::cout << "Generating coordinate and result vectors...\n";
  for (unsigned i{0}; i < weight_size; i++) {
    for (int j{0}; j < grad_size; j++) {
      line_parameters modified_params = input_parameters;
      if (!_parameters.at(j)._fixed) {
        double standard_width =
        scale_multiple * _scalewidth * _parameters[j]._grad_resolution;

        double multiplier = i < (weight_size / 2) ?
        (weight_size / 2) - i : i - (weight_size / 2) + 1;

        double halfwidth = multiplier * standard_width;
        modified_params[j] = input_parameters.access(j)
        // + 1.0 * (_gradient_signs[j] * ...
        + (1.0 * (sign(weights[_accuracy][i]) * halfwidth));
      }
      coords_to_calculate.push_back(modified_params);
      all_stats.push_back({});
    }
  }
  // std::cout << "Coords to calculate size: "
  // << coords_to_calculate.size() << "\n";

  // Don't have to calculate minbias efficiency if a looser cut produces
  // rate less than the cap
  std::vector<bool> monotonic_constraint(input_parameters.size(), false);

  // std::cout << "Performing parallel gradient loop...\n";
  // Gradient calculation loop, loosest thresholds first
#pragma omp parallel if ((_glob_or_indiv || _include_prints))
  {
#pragma omp for schedule (dynamic) nowait
    for (unsigned i = 0; i < coords_to_calculate.size(); i++) {
      int param_index = (int)i % grad_size;
      int weight_index = floor((1.0 * i) / ((1.0 * grad_size)));
      // std::cout << "Param index, weight index: " << param_index << ", "
      // << weight_index << "\n";

      // If param is active and not fixed and currently has zero gradient
      // or the corresponding gradient has not yet been calculated.
      if (((_glob_or_indiv || _tuples[0]->_activate_params[param_index]))
      && zero_gradient_params[param_index]) {
        // Don't calculate rate if looser selection produced
        // less than ratelimit
        _loose_thresh_lt_cap = monotonic_constraint[param_index];

        all_stats[i] = minimised_function(coords_to_calculate[i]);

        // If looser than input threshold and
        // rate is less than
        // ratelimit, no need to calculate rate anymore for this
        // param. If loop is not parallelised and
        // threshold is looser than input threshold, set monotonic
        // constraint. If loop is parallelised and the threshold is
        // the loosest for the parameter in this gradient calculation,
        // set monotonic constraint (threadsafe).

        if (!monotonic_constraint[param_index]
        && all_stats[i][1] < _ratelimit
        && (!(_glob_or_indiv || _include_prints)
        || ((_glob_or_indiv || _include_prints)
        && weight_index == 0))) {
          monotonic_constraint[param_index] = true;
        }
      }
    }
  }

  // Summing weighted gradients and then dividing by the weights at the
  // end
  // std::cout << "all_stats size: " << all_stats.size() << "\n";
  // std::cout << "Assigning and weighting results...\n";
  for (unsigned i{0}; i < weight_size; i++) {
    for (unsigned j{0}; j < gradient_vector.size(); j++) {
      if (((_glob_or_indiv || _tuples[0]->_activate_params[j]))
      && zero_gradient_params[j]) {
        std::vector<double> stats = all_stats[i*grad_size+j];
        // std::cout << "All stats index: " << i*grad_size+j << "\n";
        // stats[0] is the FoM value
        gradient_vector[j] +=
        weights[_accuracy][i] * stats[0];

        // Weight index * number of parameters + parameter index
        inputs_used[i][j] =
        coords_to_calculate[i*grad_size + j];

        foms[i][j] = stats[0];
        
        background_efficiencies[i][j] = stats[1];
        background_efficiency_errs[i][j] = stats[2];
        background_rates[i][j] = stats[3];
        background_rate_errs[i][j] = stats[4];
        
        signal_efficiencies[i][j] = stats[5];
        signal_efficiency_errs[i][j] = stats[6];
        ratelim_signal_efficiencies[i][j] = stats[7];
        ratelim_signal_efficiency_errs[i][j] =
        stats[8];
        final_counts[i][j] = round(stats[9]);    

        // Divide weighted gradients at the end
        if (i == weight_size-1) {
          double standard_width =
          scale_multiple * _scalewidth * _parameters[j]._grad_resolution;

          gradient_vector[j] /= (divisions[_accuracy]
          * standard_width);
        }
      }
    }
  }

  // std::cout << "Checking if parameters have zero gradient...\n";
  // Determining if while loop should continue
  zero_gradients = false;
  for (unsigned i{0}; i < input_parameters.size(); i++) {
    auto& config = _parameters[i];
    if (((_glob_or_indiv || _tuples[0]->_activate_params[i]))
    && zero_gradient_params[i]) {
      // Zero gradients at max gradient width
      if (fabs(scale_multiple * _scalewidth * config._grad_resolution)
      > fabs(config._max - config._min)) {
        zero_gradient_params[i] = false;
      } else {
        if (fabs(gradient_vector[i]) < 1e-15) {
          zero_gradients = true;
        } else {
          zero_gradient_params[i] = false;
        }
      }
    }
  }

  // Multiplying gradient width term
  double scale_term = weight_size/2 > 1 ? weight_size/2 : 2.0;
  scale_multiple *= (1.0 * scale_term);
}


std::vector<double> adam_base::accurate_gradient_vector(
  const line_parameters& input_parameters
  ) {

  std::vector<double> gradient_vector;
  const std::vector<int> weights_zero{-1, 1};
  const std::vector<int> weights_one{-5, -4, -1, 1, 4, 5};
  const std::vector<int> weights_two{-14, -14, -6, -1, 1, 6, 14, 14};
  const std::vector<int> weights_three{-42, -48, -27, -8, -1, 1, 8, 27,
  48, 42};
  const std::vector<std::vector<int>> weights =
  {weights_zero, weights_one, weights_two, weights_three};
  const std::vector<double> divisions = {1, 32.0, 128.0, 512.0};

  if (_test_setting) _accuracy = 0;
  const unsigned weight_size = weights[_accuracy].size();

  // Vectors for saving statistics from parallel loop ----------
  std::vector<double> gradient_return;
  std::vector<std::vector<line_parameters>> inputs_used;

  std::vector<std::vector<double>> foms, background_efficiencies,
  background_efficiency_errs, background_rates, background_rate_errs,
  signal_efficiencies, ratelim_signal_efficiencies,
  signal_efficiency_errs, ratelim_signal_efficiency_errs;

  std::vector<std::vector<int>> final_counts;
  // -----------

  // Calculating loosest cuts first for all params to enact monotonic
  // constraints
  for (unsigned i{0}; i < weight_size; i++) {
    inputs_used.push_back({});
    foms.push_back({});
    background_efficiencies.push_back({});
    background_efficiency_errs.push_back({});
    background_rates.push_back({});
    background_rate_errs.push_back({});
    signal_efficiencies.push_back({});
    signal_efficiency_errs.push_back({});
    ratelim_signal_efficiencies.push_back({});
    ratelim_signal_efficiency_errs.push_back({});
    final_counts.push_back({});

    for (unsigned j{0}; j < input_parameters.size(); j++) {
      if (i == 0) {
        gradient_vector.push_back(0);
      }
      inputs_used[i].push_back(line_parameters());
      foms[i].push_back(1.0*_tuples.size());
      background_efficiencies[i].push_back(1.0);
      background_efficiency_errs[i].push_back(0.0);
      background_rates[i].push_back(1.0 * _collision_rate * _ratelimit);
      background_rate_errs[i].push_back(0.0);
      signal_efficiencies[i].push_back(1.0);
      signal_efficiency_errs[i].push_back(0.0);
      ratelim_signal_efficiencies[i].push_back(1.0);
      ratelim_signal_efficiency_errs[i].push_back(0.0);
      final_counts[i].push_back(0);
    }
  }

  if (_accuracy > 3 || _accuracy < 0) {
    std::cout << "Invalid gradient accuracy chosen. "
    "Choose inclusively between 0 and 3. Exiting..\n";
    exit(0);
  }

  if (_glob_or_indiv || _include_prints) {
    std::cout << "\nBeginning gradient calculation...\n";
  }

  // Inputs and outputs from gradient for loop
  std::vector<line_parameters> coords_to_calculate;
  std::vector<std::vector<double>> all_stats;

  // Run gradient calculations for all paramaters at least once, and then
  // repeat for those with initially calculated zero gradient with a larger
  // width.
  bool zero_gradients = true;
  std::vector<bool> zero_gradient_params(input_parameters.size(), true);
  if (!_glob_or_indiv) {
    for (unsigned i{0}; i < zero_gradient_params.size(); i++) {
      if (!_tuples[0]->_activate_params[i] || _parameters[i]._fixed) {
        zero_gradient_params[i] = false;
      }
    }
  } else {
    for (unsigned i{0}; i < zero_gradient_params.size(); i++) {
      if (_parameters[i]._fixed) {
        zero_gradient_params[i] = false;
      }
    }

  }

  TStopwatch t;
  t.Start();
  double scale_multiple{1.0};

  // While all gradients are set to zero
  while (zero_gradients) {
    gradient_loop(scale_multiple, zero_gradient_params,
    zero_gradients, coords_to_calculate,
    all_stats, weight_size, gradient_vector, input_parameters,
    weights, divisions, inputs_used, foms,
    background_efficiencies, background_efficiency_errs,
    background_rates, background_rate_errs,
    signal_efficiencies, signal_efficiency_errs,
    ratelim_signal_efficiencies, ratelim_signal_efficiency_errs,
    final_counts);
  }

  for (unsigned i{0}; i < weight_size; i++) {
    for (unsigned j{0}; j < gradient_vector.size(); j++) {
      if ((_glob_or_indiv || _tuples[0]->_activate_params[j])) {
        if (i == 0) {
          gradient_return.push_back(gradient_vector[j]);
        }
        _inputs_used.push_back(inputs_used[i][j]);
        _all_foms.push_back(foms[i][j]);

        _background_efficiencies.push_back(
        background_efficiencies[i][j]);
        _background_efficiency_errs.push_back(
        background_efficiency_errs[i][j]);
        _background_rates.push_back(background_rates[i][j]);
        _background_rate_errs.push_back(background_rate_errs[i][j]);

        _signal_efficiencies.push_back(signal_efficiencies[i][j]);
        _signal_efficiency_errs.push_back(
        signal_efficiency_errs[i][j]);
        _ratelim_signal_efficiencies.push_back(
        ratelim_signal_efficiencies[i][j]);
        _ratelim_signal_efficiency_errs.push_back(
        ratelim_signal_efficiency_errs[i][j]);

        _final_counts.push_back(final_counts[i][j]);
      }
    }
  }

  t.Stop();

  _loose_thresh_lt_cap = false;

  std::vector<double> central_stats = minimised_function(input_parameters);
  if (_save_path) {
    _averaged_foms.push_back(central_stats[0]);
    _minpath_coords.push_back(input_parameters);
    _averaged_rates.push_back(central_stats[3]);
  }

  if (_glob_or_indiv || _include_prints) {
    std::cout << "\nTime taken for gradient calculation: ";
    t.Print();
    std::cout << "\n";
    std::cout << "Central FoM Value: "
    << _averaged_foms.back() << "\n";
    std::cout << "Central rate: "
    << _averaged_rates.back() << "\n";
    std::cout << "Last FoM difference: "
    << _averaged_foms.back() - *(_averaged_foms.end() - 2) << "\n";
  }

  return gradient_return;

} 


void remove_intersection(
  std::vector<line_parameters>& a, std::vector<line_parameters>& b
  ) {
  // Remove previously calculated points

  std::unordered_multiset<line_parameters, line_parameters_hash> st;
  st.insert(a.begin(), a.end());
  st.insert(b.begin(), b.end());
  auto predicate = [&st](const line_parameters& k){
    return st.count(k) > 1;
  };
  a.erase(std::remove_if(a.begin(), a.end(), predicate), a.end());
  b.erase(std::remove_if(b.begin(), b.end(), predicate), b.end());
}


line_parameters adam_base::grid_search_neighbourhood(
  const line_parameters input_parameters, double& optimal_fom,
  std::vector<line_parameters>& previous_neighbouring_coords,
  const int depth, const bool repeated_foms
  ) {
  // Find smallest FoM for all orthogonal and diagonal pair neighbouring
  // coordinates and return the corresponding coordinate.

  double fom_copy = optimal_fom;
  // All coords including diagonals, +- resolution
  std::vector<line_parameters> all_neighbouring_coords =
  list_orthogonal_diagonal_coords(input_parameters, depth);

  std::vector<line_parameters> all_neighbouring_coords_copy =
  all_neighbouring_coords;

  if (!previous_neighbouring_coords.empty()) {
    remove_intersection(previous_neighbouring_coords,
    all_neighbouring_coords);

    // Add back in the base point if it has been removed
    all_neighbouring_coords.push_back(input_parameters);
    
    // Current neighbouring coords appended to previous coords
    // for next cycle
    previous_neighbouring_coords.insert(previous_neighbouring_coords.end(),
    all_neighbouring_coords_copy.begin(), all_neighbouring_coords_copy.end());
  }

  std::vector<bool> active_params = _glob_or_indiv ?
  _tuples[0]->_background_io->_activate_params :
  _tuples[0]->_activate_params;

  if (_glob_or_indiv || _include_prints) {
    std::cout << "\nNumber of neighbouring points calculated: "
      << all_neighbouring_coords.size() << "\n";

    // std::cout << "\nNeighbouring coords: ";
    // for (line_parameters coords: all_neighbouring_coords) {
    //     coords.print(active_params);
    // }
    std::cout << "Searching FoM minimum neighbourhood...\n";
  }

  std::vector<double> neighbouring_foms;
  std::vector<line_parameters> inputs_used;

  for (line_parameters lp: all_neighbouring_coords) {
    bool within_bounds{true};

    for (unsigned j{0}; j < lp.size(); j++) {
      if (_parameters[j]._fixed) continue;
      const auto& config = _parameters[j]; 
      bool outside_min =
      sign(config._max - config._min) >= 0.0
      ? lp.access(j) <= config._min - 1e-12
      : lp.access(j) >= config._min + 1e-12;

      bool outside_max =
      sign(config._max - config._min) >= 0.0
      ? lp.access(j) >= config._max + 1e-12
      : lp.access(j) <= config._max - 1e-12;

      if ((_glob_or_indiv || _tuples[0]->_activate_params[j])
      && (outside_min || outside_max)) {
        within_bounds = false;
        break;
      }
    }
    if (within_bounds) {
      inputs_used.push_back(lp);
      _minpath_coords.push_back(lp);
      neighbouring_foms.push_back(0);
    }
  }

  if (inputs_used.empty()) {
    std::cout << "None of the neighbouring coords are within bounds. "
    "Exiting...\n";
    exit(0);
  }

  TStopwatch t;
  t.Start();
#pragma omp parallel for if (_glob_or_indiv || _include_prints)
  for (unsigned i = 0; i < inputs_used.size(); i++) {
    line_parameters rounded = inputs_used.at(i);
    double neighbouring_fom =
    minimised_function(rounded)[0];
    neighbouring_foms[i] = neighbouring_fom;
  }
  t.Stop();

  for (double neighbouring_fom: neighbouring_foms) {
    _averaged_foms.push_back(neighbouring_fom);
  }


  auto it = std::min_element(std::begin(neighbouring_foms),
  std::end(neighbouring_foms));

  int minindex = std::distance(std::begin(neighbouring_foms), it);
  fom_copy = neighbouring_foms.at(minindex);

  if (_glob_or_indiv || _include_prints) {
    std::cout << "Finished searching FoM minimum neighbourhood.\n";
    std::cout << "Time taken for neighbourhood search: ";
    t.Print();
    std::cout << "\n";
    std::cout << "Minimum FoM found in neighbourhood: " << fom_copy
    << "\n";
    std::cout << "\nOptimal coordinates found in neighbourhood:\n";

    std::cout << std::setw(3) << "#" << " " << std::left << std::setw(35)
    << "Parameter" << " " << std::setw(15) << "Threshold\n"
    << std::setw(83) <<
    " -----------------------------------------------------------------------"
    "----------- " << std::endl;
    for (unsigned i{0}; i < inputs_used.at(minindex).size(); i++) {
      if (_glob_or_indiv || active_params[i]) {
        if (_parameters[i]._fixed) {
          std::cout << std::setw(3) << i << " " << std::left
          << std::setw(35) << inputs_used.at(minindex)._paramnames.at(i)
          << " " << std::setw(15) << " fixed " << std::endl;
        } else {
          std::cout << std::setw(3) << i << " " << std::left
          << std::setw(35) << inputs_used.at(i)._paramnames.at(i)  << " " 
          << std::setw(15) << inputs_used.at(minindex).access(i) << std::endl;
        }
      }
    }

    std::cout << "\nOld neighbourhood fom: " << optimal_fom << "\n";
  }

  if (fom_copy < optimal_fom) {
    if (_glob_or_indiv || _include_prints) {
      std::cout << "Choosing new FoM (smaller than old).\n";
    }
    optimal_fom = fom_copy;
    return inputs_used.at(minindex);
  } 
  /*
  else if (fom_copy == optimal_fom && !repeated_foms) {
    if (_glob_or_indiv || _include_prints) {
      std::cout << "Choosing new FoM (same as old).\n";
    }
    optimal_fom = fom_copy;
    return inputs_used.at(minindex);
  } 
  */
  else {
    if (_glob_or_indiv || _include_prints) {
      std::cout << "Choosing old FoM.\n";
    }
    return input_parameters;
  }
}



line_parameters adam_base::recursive_grid_search(
    const line_parameters& input_parameters, double& optimal_fom,
    int& num_grid_searches, const int max_grid_searches
    ) {
  // After Adam minimisation, grid search to find smallest neighbourhood
  // point, then call recursively on new smallest point to find smallest
  // FoM, until all surrounding neighbourhood FoM points are smaller. Only
  // calculate new points

  if (_test_setting) return input_parameters;

  line_parameters new_params;
  line_parameters old_params;
  std::vector<line_parameters> previous_neighbouring_coords = {};
  int grid_search_depth{2};
  std::vector<double> foms;

  // Final solution will be alpha-rounded
  line_parameters rounded = input_parameters;
  foms.push_back(minimised_function(rounded)[0]);
  _minpath_coords.push_back(input_parameters);
  _averaged_foms.push_back(foms[0]);
  double optimal_fom_copy = foms[0];
  std::vector<line_parameters> params;
  params.push_back(input_parameters);

  new_params = grid_search_neighbourhood(input_parameters, optimal_fom_copy,
  previous_neighbouring_coords, grid_search_depth);

  if (_glob_or_indiv || _include_prints) {
    std::cout << "Initial optimal fom from nearest point: "
    << optimal_fom_copy << "\n";
    std::cout << "Second (neighbourhood) fom: " << optimal_fom_copy
    << "\n";
  }

  foms.push_back(optimal_fom_copy);
  params.push_back(new_params);
  previous_neighbouring_coords =
  list_orthogonal_diagonal_coords(input_parameters, grid_search_depth);

  num_grid_searches = 0;
  bool repeated_foms = false;
  std::vector<bool> active_params = _glob_or_indiv ?
  _tuples[0]->_background_io->_activate_params
  : _tuples[0]->_activate_params;

  int maximum_searches = max_grid_searches; 
  if (_test_setting) {
    maximum_searches = -5;
    num_grid_searches = 1;
  }

  while ((new_params != old_params
  && num_grid_searches < maximum_searches + 1) || num_grid_searches < 1) {
    old_params = new_params;
    // This writes to previous neighbouring coords for next cycle
    if (foms.size() > 5) {
      bool repeated_foms_temp = true;
      for (int i{0}; i < 5; i++) {
        if (foms[foms.size()-i-1] != foms[foms.size()-i-2]) {
          repeated_foms_temp = false;
          break;
        }
      }
      repeated_foms = repeated_foms_temp;
    } 
    new_params = grid_search_neighbourhood(old_params, optimal_fom_copy,
    previous_neighbouring_coords, grid_search_depth, repeated_foms);

    foms.push_back(optimal_fom_copy);
    params.push_back(new_params);

    if (_glob_or_indiv || _include_prints) {
      std::cout << "Performing grid search " << num_grid_searches
        << "\n";
      std::cout << "Associated fom: " << optimal_fom_copy << "\n";
      std::cout << "Associated params: \n";

      std::cout << std::setw(3) << "#" << " " << std::left << std::setw(35)
      << "Parameter" << " " << std::setw(15) << "Threshold\n"
      << std::setw(83) <<
      " -----------------------------------------------------------------------"
      "----------- " << std::endl;
      for (unsigned i{0}; i < new_params.size(); i++) {
        if (_glob_or_indiv || active_params[i]) {
          if (_parameters[i]._fixed) {
            std::cout << std::setw(3) << i << " " << std::left
            << std::setw(35) << new_params._paramnames.at(i)  << " " 
            << std::setw(15) << " fixed " << std::endl;
          } else {
            std::cout << std::setw(3) << i << " " << std::left
            << std::setw(35) << new_params._paramnames.at(i)  << " " 
            << std::setw(15) << new_params.access(i) << std::endl; 
          }
        }
      }

      std::cout << "\n";
    }
    num_grid_searches++;
  }
  
  int minindex = std::distance(std::begin(foms),
  std::min_element(std::begin(foms), std::end(foms)));
  
  optimal_fom = foms[minindex];
  _minpath_coords.push_back(params[minindex]);

  if (_glob_or_indiv || _include_prints) {
    std::cout << "Grid searches required: " << num_grid_searches << "\n";
    std::cout << "Final FoM chosen: " << optimal_fom << "\n";
    std::cout << "Final params chosen: \n";
    std::cout << std::setw(3) << "#" << " " << std::left << std::setw(35)
    << "Parameter" << " " << std::setw(15) << "Threshold\n"
    << std::setw(83) <<
    " -----------------------------------------------------------------------"
    "----------- " << std::endl;
    for (unsigned i{0}; i < params[minindex].size(); i++) {
      if (_glob_or_indiv || active_params[i]) {
        if (_parameters[i]._fixed) {
          std::cout << std::setw(3) << i << " " << std::left
          << std::setw(35) << params[minindex]._paramnames.at(i)  << " " 
          << std::setw(15) << " fixed " << std::endl;
        } else {
          std::cout << std::setw(3) << i << " " << std::left
          << std::setw(35) << params[minindex]._paramnames.at(i)  << " " 
          << std::setw(15) << params[minindex].access(i) << std::endl; 
        }
      }
    }

    std::cout << "\n";
  }
  return params[minindex];
}


void adam_base::smart_kick(
  const line_parameters current_coords, std::vector<double>& first_moment,
  std::vector<double>& second_moment, const double alpha, const double beta1,
  const double beta2, const double epsilon
  ) {
  // Kick with equal magnitude in opposite direction to average point

  line_parameters averaged_coordinates;
  int stop_index = (int)_minpath_coords.size() < 10
  ? (int)_minpath_coords.size() : 10;
  
  for (int i{0}; i < stop_index; i++) {
    line_parameters central_coords = _minpath_coords[i];
    for (unsigned j{0}; j < central_coords.size(); j++) {
      if (i==0) averaged_coordinates[j] = 0.0;
      averaged_coordinates[j] += central_coords.access(j);
    }
  }

  for (unsigned i{0}; i < averaged_coordinates.size(); i++) {
    averaged_coordinates[i] /= 1.0 * stop_index;
  }

  line_parameters inverse_averaged_coordinates;
  for (unsigned i{0}; i < current_coords.size(); i++) {
    if ((_glob_or_indiv || _tuples[0]->_activate_params[i])
    && !_parameters[i]._fixed) {
      double relative_distance =
      fabs(current_coords.access(i) - averaged_coordinates[i]);

      if (current_coords.access(i) >= averaged_coordinates[i]) {
      inverse_averaged_coordinates[i] =
        current_coords.access(i) + relative_distance;
        _parameters[i].clip_max(inverse_averaged_coordinates[i]);
      } else if (current_coords.access(i) < averaged_coordinates[i]) {
        inverse_averaged_coordinates[i] =
        current_coords.access(i) - relative_distance;
        _parameters[i].clip_min(inverse_averaged_coordinates[i]); 
      }
    }
  }

  std::vector<bool> active_params = _glob_or_indiv ?
  _tuples[0]->_background_io->_activate_params :
  _tuples[0]->_activate_params;

  if (_glob_or_indiv || _include_prints) {
    std::cout << "Current coords for kick (origin): \n";
    current_coords.print(active_params);
    std::cout << "\nAveraged minpath coordinates: \n";
    averaged_coordinates.print(active_params);
    std::cout << "Inverted averaged coordinates (inverted from origin): \n";
    inverse_averaged_coordinates.print(active_params);
    std::cout << "\n";
    std::cout << "Calculating gradient for warm reset...\n";
  }

  _save_path = false;
  std::vector<double> gradient_vector =
  accurate_gradient_vector(inverse_averaged_coordinates);
  _save_path = true;

  int index_count{0};
  for (unsigned i{0}; i < inverse_averaged_coordinates.size(); i++) {
    if ((_glob_or_indiv || _tuples[0]->_activate_params[i])
    && !_parameters[i]._fixed) {
      if (inverse_averaged_coordinates[i] > current_coords.access(i)) {
        gradient_vector[index_count] =
        -1.0 * fabs(gradient_vector[index_count]);
      } else {
        gradient_vector[index_count] =
        fabs(gradient_vector[index_count]);
      }

      // Kick in reverse direction (away from previous minimum)
      first_moment[index_count] =
      beta1 * first_moment[index_count] + ((1.0 - beta1)
      * gradient_vector[index_count]);
      
      second_moment[index_count] =
      (beta2 * second_moment[index_count]) + ((1.0 - beta2)
      * std::pow(gradient_vector[index_count], 2.0));
      // increase is subtracted in adam so it is the opposite of
      // the intuitive sign

      first_moment[index_count] *= 25.0;
      second_moment[index_count] *= 25.0;
      
      /*
      if (_glob_or_indiv || _include_prints) {
        std::cout << "\nADAM STATS FOR COORDINATE "
          << current_coords._paramnames[i] << ":\n";
        std::cout << "Gradient element: "
          << gradient_vector[index_count] << "\n";
        std::cout << "First and second moments: "
          << first_moment[index_count] << ", "
          << second_moment[index_count] << "\n";
      }
      */

      index_count++;
    }
  } 
}
