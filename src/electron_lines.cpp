#include "electron_lines.h"
#include "line_tools.h"

bool trackelectronmva::operator()(
  const float* monitored_quantities
  ) const {

  float pt =
  monitored_quantities[0];
  float ipchi2 =
  monitored_quantities[1];
  float pt_shift =
  pt - _alpha; // alpha_transform(_alpha); 

  return (pt_shift > _max_pt && ipchi2 > _min_ipchi2) ||
  (pt_shift > _min_pt && pt_shift < _max_pt && (logf(ipchi2) >
  ((1.0*_param1/((pt_shift-1.0*_param2)*(pt_shift-1.0*_param2))) +
  (_param3/_max_pt)*(_max_pt - pt_shift) + logf(_min_ipchi2))));
}


bool singlehighptelectron::operator()(
  const float* monitored_quantities
  ) const {   

  return _fixed_threshold < monitored_quantities[0]; 
}


bool displaced_dielectron::operator()(
  const float* monitored_quantities
  ) const {
  
  return _min_pt < monitored_quantities[0]
    && _min_ipchi2 < monitored_quantities[1]; 
}

bool dielectron_lowmass::operator()(
  const float* monitored_quantities
  ) const {
  
  return true; 
}
