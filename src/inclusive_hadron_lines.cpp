#include "inclusive_hadron_lines.h"
#include "line_tools.h"

// Previous number of elements: 9
// Divisor: 8.0
// steps: 
// _twotrack_ks_steps = {1.0, 1.0, 7.0/8.0, 6.0/8.0,
// 5.0/8.0, 4.0/8.0, 3.0/8.0, 2.0/8.0, 1.0/8.0, 0.0};
// ^^ nsteps 10
// num_grid = num_gradient = 40
// Therefore num_divisor = num_elements - 1, nstep = num_elements + 1,
// ngrid = 5 * num_elements

// New numbers: num_elements = 16, num_divisor = 15, nstep = 17, ngrid = 75


void twotrackks::prepare_twotrackks() {

  float twotrack_ks = twotrack_ks_transform(_param);
  _thresholds.clear();
  std::vector<float> thresholds;
  int loop_size = _twotrack_ks_steps.size() - 1;
  if (twotrack_ks >= 1.0) {
    for (unsigned j{0}; j < _threshold_collection.size(); j++) {
      thresholds.push_back(_threshold_collection.at(j).at(0));
    }
  } else if (twotrack_ks < 0) {
    for (unsigned j{0}; j < _threshold_collection.size(); j++) {
      thresholds.push_back(_threshold_collection.at(j).back());
    }
  } else {
    for (int i{0}; i < loop_size; i++) {
      float fractional_distance = abs((twotrack_ks -
      _twotrack_ks_steps.at(i+1))/
      (_twotrack_ks_steps.at(i) - _twotrack_ks_steps.at(i+1)));
      if (twotrack_ks < _twotrack_ks_steps.at(i) &&
      twotrack_ks >= _twotrack_ks_steps.at(i+1)) {
        for (unsigned j{0}; j < _threshold_collection.size();
        j++) {
          float interpolation =
          _threshold_collection.at(j).at(i) +
          _inc_or_dec.at(j) * fractional_distance
          * abs(_threshold_collection.at(j).at(i-1) -
          _threshold_collection.at(j).at(i));
          thresholds.push_back(interpolation);
        }
      }
    }
  }
  _thresholds = thresholds;
}


void twotrackks::print_thresholds(const line_parameters& parameters) {
  
  std::vector<float> original_thresholds = _thresholds;
  auto original_param = _param; 
  set_parameters(parameters); 
  prepare_twotrackks();
  std::cout << "TwoTrackKs thresholds for input " << _param
  << " (pt_pi, ipchi2_pi, pt_ks, eta_ks, combined_ip): ";
  for (float thresh: _thresholds) {
    std::cout << thresh << " ";
  }
  std::cout << " (pt measured in MeV).\n";
  _thresholds = original_thresholds;
  _param._value = original_param._value;
}


void twotrackks::print_allenconf_thresholds(
  std::ostream& os, const line_parameters& parameters, bool spreadsheet_print
  ) {
  
  std::vector<float> original_thresholds = _thresholds;
  auto original_param = _param;
  set_parameters(parameters); 
  prepare_twotrackks();
  std::vector<std::string> variables =
  {"minTrackPt_piKs", "minTrackIPChi2_piKs", "minComboPt_Ks", "maxEta_Ks",
  "min_combip"};
  
  int last = int(_thresholds.size()) -1;
  for (int i{0}; i < int(_thresholds.size()); i++) {
    std::string end_line = i == last ? "" : ",\n";
    if (spreadsheet_print && (i < last )) end_line = "\n";
    else if (spreadsheet_print && i == last)  end_line = "";
    std::string spacing = spreadsheet_print ? "" : "  ";
    std::string equals = spreadsheet_print ? "&" : "=";
    os << spacing << "TwoTrackKs_" << variables[i] << equals
    << _thresholds[i] << end_line;
  }
  
  _thresholds = original_thresholds;
  _param._value = original_param._value; 
}


bool twotrackks::operator()(
  const float* monitored_quantities
  ) const {
  // pt_pi
  return monitored_quantities[0] > _thresholds[0] &&
  // ipchi2_pi
  monitored_quantities[1] > _thresholds[1] &&
  // pt_ks
  monitored_quantities[2] > _thresholds[2] &&
  // eta_ks
  (monitored_quantities[3] > 2
  && monitored_quantities[3] < _thresholds[3]) &&
  // ip_pi1, ip_pi2, ip_ks (combined_pt)
  (monitored_quantities[4] > _thresholds[4]); 
}

bool trackmva::operator()(
  const float* monitored_quantities
  ) const {
  float pt =
  monitored_quantities[0];
  float ipchi2 =
  monitored_quantities[1];
  float ghost_prob =
  monitored_quantities[2];

  float pt_shift = pt - _alpha; // alpha_transform(_alpha); 
  return ((pt_shift > _max_pt && ipchi2 > _min_ipchi2) ||
  (pt_shift > _min_pt && pt_shift < _max_pt && (logf(ipchi2) >
  ((1.0*_param1/((pt_shift-_param2)*(pt_shift-_param2))) +
  (_param3/_max_pt)*(_max_pt - pt_shift) + logf(_min_ipchi2)))))
  && (ghost_prob < _track_ghost_prob);
}

bool twotrackmva::operator()(
  const float* monitored_quantities
  ) const {

  return monitored_quantities[0] > _minMVA 
  && monitored_quantities[1] < _twotrack_ghost_prob; 
}

