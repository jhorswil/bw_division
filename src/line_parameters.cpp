#include "line_parameters.h"
#include "line_tools.h"

// Input parameters fed to each line

// line_parameters::line_parameters() {
//   for (int i{0}; i < _num_parameters; i++) {
//     _paramgroup[i] = 0.0;
//   }
//   _paramnames = {"alpha_hadron", "alpha_electron", "alpha_muon",
//   "charm_track_ip",
//   "charm_track_pt", "twotrackmva", "twotrack_ks",
//   "highmass_dimuon_pt",
//   "displaced_dielectron_pt", "displaced_dielectron_ipchi2",
//   "displaced_dimuon_pt", "displaced_dimuon_ipchi2",
//   "highmass_diphoton_et",
//   "lambda_detached_track_mipchi2", "lambda_detached_combination_bpvfd",
//   "xi_lll_ipchi2", "track_ghostprob", "twotrack_ghostprob"
//   // , "DownstreamKsToPiPi_minMVA",
//   // , "DownstreamLambdaToPPi_minMVA" 
//   };

// }

line_parameters::line_parameters(const double paramgroup[_num_parameters]) {
  // Array constructor

  for (int i{0}; i < _num_parameters; i++) {
    _paramgroup[i] = paramgroup[i];
  }
}


line_parameters::line_parameters(const std::vector<double> paramgroup) {
  // Vector constructor

  for (int i{0}; i < _num_parameters; i++) {
    _paramgroup[i] = paramgroup.at(i);
  }

}

double& line_parameters::operator[](const int index) {
  return _paramgroup[index];
}


double line_parameters::access(const int index) const {
  // std::cout << "Index, paramgroup size: " << index << ", "
  // << sizeof(_paramgroup) / sizeof(double) << "\n";
  return _paramgroup[index];
}


double read_access(
  const line_parameters params, int index
  ) {
  return params._paramgroup.at(index);
}

unsigned int line_parameters::size() const {
  return _num_parameters;
}

void line_parameters::print(
  const std::vector<bool> active_params
  ) const {
  for (int i{0}; i < _num_parameters; i++) {
  //if (active_params.size() == 0 || active_params[i]) {
    std::cout << i << " " << _paramnames[i] << ": " << _paramgroup[i]
    << " " << std::endl;
  //}
  }
}

std::vector<double> line_parameters::return_vector() {
  return std::vector<double>(_paramgroup.data(), _paramgroup.data()
  + _num_parameters);
}

bool operator==(
  const line_parameters& lhs, const line_parameters& rhs
  ) {

  line_parameters params;
  bool same{true};
  for (unsigned i{0}; i < params.size(); i++) {
    if (lhs.access(i) != rhs.access(i)) {
      same = false;
      break;
    }
  }
  return same;
}

bool line_parameters::operator!=(
  const line_parameters& line_params
  ) const {
  return !(*this == line_params);
}


std::ostream& operator<<(
  std::ostream& os, const line_parameters& parameters
  ) {
  for(unsigned i{0}; i != parameters._num_parameters; ++i)
  os << parameters._paramgroup[i] << " ";
  return os;
} 
