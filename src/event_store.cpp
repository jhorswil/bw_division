#include <iostream>
#include <map>
#include <vector>
#include "event_store.h"
#include "line_tools.h"

event_store::event_store(
  const unsigned long& event_num, const unsigned int& run_num,
  const std::vector<std::vector<float>>& candidates,
  const std::array<int, NUM_LINES>& quantities_per_line,
  const std::array<int, NUM_LINES>& candidates_per_line
  ) : _event_number(event_num), _run_number(run_num),
  _quantities_per_line(quantities_per_line),
  _candidates_per_line(candidates_per_line) {

  _number_of_candidates = (int)candidates.size();
  _offset[0] = 0;

  for (unsigned i{1}; i < _candidates_per_line.size()+1; i++) {
    int previous_offset = _offset[i-1];
    _offset[i] = _candidates_per_line[i-1]
    * _quantities_per_line[i-1] + previous_offset;
  }

  for (int i{0}; i < _number_of_candidates; i++) {
    for (unsigned j{0}; j < candidates[i].size(); j++) {
      _monitored_quantities.push_back(candidates[i][j]);
    }
  }
}

bool event_store::pass_decision(
  const linemask_t& activate_lines, const line_tuple& lines
  ) const {
  // Does event pass decision threshold for all active lines

  return tuple_process::for_each_ref(lines, _monitored_quantities,
  _offset, activate_lines);
} 

bool event_store::pass_decision_specline(
  const int& lineindex,
  const line_tuple& lines
  ) const {
  // Does event pass decision threshold for certain line?

  if (lineindex >= (int)_offset.size() - 1) {
    std::cout << "This line index is out of bounds!\n";
    return false;
  } 

  linemask_t activate_lines;
  for (unsigned i{0}; i < _offset.size() - 1; i++) {
    activate_lines[i] = (int)i == lineindex;
  }

  return tuple_process::for_each_ref(lines, _monitored_quantities,
  _offset, activate_lines);
}


std::vector<int> event_store::postcut_candidates_per_line(
  const linemask_t& activate_lines,
  const line_tuple& lines
  ) const {
  // Counting candidates per line
  std::vector<int> counts = {};
  return tuple_process::candidate_count(lines, _monitored_quantities,
  _offset, activate_lines, counts);
}


void event_store::remove_excess(
  const linemask_t& activate_lines,
  const line_tuple& lines
  ) {
  // Remove candidates that do not pass a line-specific selection at the
  // ratecap
  std::vector<bool> candidate_decisions;  
  candidate_decisions = tuple_process::which_candidates(lines,
  _monitored_quantities, _offset, activate_lines, candidate_decisions);
  int candidate_counter{0};

  std::vector<float> new_monitored_quantities;
  std::array<int, NUM_LINES+1> new_offset;
  std::array<int, NUM_LINES> new_candidates_per_line = _candidates_per_line;
  new_offset[0] = 0; 
  _number_of_candidates = 0;

  for (unsigned line{0}; line < NUM_LINES; line++) {
    new_candidates_per_line[line] = 0;
    for (int j{0}; j < _candidates_per_line[line]; j++) {
      if (candidate_decisions[candidate_counter]) {
        for (int k{0}; k < _quantities_per_line[line]; k++) {
          new_monitored_quantities.push_back(
          _monitored_quantities[
          _offset[line]+j*_quantities_per_line[line]+k]);
        }

        new_candidates_per_line[line]++;
      }

      candidate_counter++; 
    }

    _number_of_candidates += new_candidates_per_line[line];
    new_offset[line+1] = new_monitored_quantities.size();
  }

  _offset = new_offset;
  _candidates_per_line = new_candidates_per_line;
  _monitored_quantities = new_monitored_quantities;
}


linemask_t event_store::which_lines(
  const linemask_t& activate_lines,
  const line_tuple& lines
  ) {
  // Boolean for each line that triggers on this event

  std::vector<int> counts = {};
  counts = tuple_process::candidate_count(lines,
  _monitored_quantities, _offset, activate_lines, counts);
  linemask_t triggered_lines {false}; 
  std::vector<int> full_counts(activate_lines.size(), 0);
  int line_counter{0};
  for (unsigned i{0}; i < full_counts.size(); i++) {
    if (activate_lines[i]) {
      full_counts[i] = counts[line_counter];
      line_counter++;
    }
  }

  for (unsigned i{0}; i < activate_lines.size(); i++) {
    if (full_counts.at(i) > 0) triggered_lines[i] = true;
  }

  return triggered_lines;
}


void event_store::print() {
  std::cout << "Number of candidates: " << _number_of_candidates << "\n";
  std::cout << "Event number: " << _event_number << "\n";
  std::cout << "Run number: " << _run_number << "\n";
  std::cout << "Number of candidates per line: \n";
  for (unsigned i{0}; i < _candidates_per_line.size(); i++) 
    std::cout << _candidates_per_line[i] << " ";
  std::cout << "\nMonitored quantities:\n";
  for( unsigned i{0}; i != _monitored_quantities.size(); ++i ) {
    std::cout << _monitored_quantities[i] << " ";
  }
  std::cout << "\n";
}

void remove_event_by_index(
  std::vector<event_store>& vec, size_t pos
  ) {
  std::vector<event_store>::iterator it = vec.begin();
  std::advance(it, pos);
  vec.erase(it);
}
