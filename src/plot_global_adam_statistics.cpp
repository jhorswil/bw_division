#include "opt_discrete_gradient.h"
#include "init_discrete_gradient.h"
#include "init_save_and_collate.h"
#include "misc_tools.h"
#include "opt_save_and_collate.h"

void total_effs_and_rootsafe_plots(
  std::string canvas_dir, std::vector<io_manager*> tuples,
  line_parameters results, opt_discrete_gradient* global_minimiser,
  int job_num, std::string ratecap_string, std::string recon_string
  ) {

  // ----------- Total efficiencies --------------

  TH1D* maxeff_bars =
  new TH1D("maxeff_bars", "Max", tuples.size(),
  0, tuples.size());
  maxeff_bars->SetYTitle((recon_string + " HLT1 Efficiency").c_str());
  maxeff_bars->SetStats(kFALSE);
  maxeff_bars->SetFillStyle(3345);
  maxeff_bars->SetLineWidth(2);
  maxeff_bars->SetLineColor(kRed);
  maxeff_bars->SetFillColor(kRed);

  TH1D* finaleff_bars =
  new TH1D("finaleff_bars", "Optimal", tuples.size(), 0, tuples.size());
  finaleff_bars->SetYTitle((recon_string + " HLT1 Efficiency").c_str());
  finaleff_bars->SetStats(kFALSE);
  finaleff_bars->SetFillStyle(3354);
  finaleff_bars->SetLineWidth(2);
  finaleff_bars->SetLineColor(kBlue);
  finaleff_bars->SetFillColor(kBlue);

  maxeff_bars->SetMinimum(0.0);
  finaleff_bars->SetMinimum(0.0);
  maxeff_bars->SetMaximum(1.0);
  finaleff_bars->SetMaximum(1.0);

  // Relative Effs Latex Labels
  std::cout << "\n\nMax and Final Absolute Effs:\n\n";
  for (unsigned i{1}; i <= tuples.size(); i++) {
    // Parallelize efficiency calcs for faster plotting?
    tuples[i-1]->_parallelise_events = true;
    std::cout << tuples[i-1]->_name << ": $"
    << tuples[i-1]->_optimal_signal_eff
    << " \\pm " << tuples[i-1]->_optimal_signal_eff_err
    <<  "$ & $" << global_minimiser->_signal_efficiencies[i-1]
    << " \\pm " << global_minimiser->_signal_efficiency_errs[i-1] << "$\n";
    double binerror = tuples[i-1]->_optimal_signal_eff_err > 0.0 ?
    tuples[i-1]->_optimal_signal_eff_err : 1e-8;
    maxeff_bars->SetBinError(
    i, binerror);
    maxeff_bars->SetBinContent(i,
    (double)tuples[i-1]->_optimal_signal_eff);
    maxeff_bars->GetXaxis()->SetBinLabel(i, tuples[i-1]->_name.c_str());

    binerror = global_minimiser->_signal_efficiency_errs[i-1] > 0.0 ?
    global_minimiser->_signal_efficiency_errs[i-1] : 1e-8;
    finaleff_bars->SetBinError(i, binerror);
    finaleff_bars->SetBinContent(
    i, (double)global_minimiser->_signal_efficiencies[i-1]);
    finaleff_bars->GetXaxis()->SetBinLabel(i, tuples[i-1]->_name.c_str());
  }

  std::cout << "\n\nMax and Final Absolute Effs with & delimiter for "
    "Excel spreadsheet:\n\n";
  for (unsigned i{1}; i <= tuples.size(); i++) {
    // Parallelize efficiency calcs for faster plotting?
    std::cout << tuples[i-1]->_name << "&"
    << tuples[i-1]->_optimal_signal_eff
    << "&" << tuples[i-1]->_optimal_signal_eff_err
    <<  "&" << global_minimiser->_signal_efficiencies[i-1]
    << "&" << global_minimiser->_signal_efficiency_errs[i-1] << "\n";
  }

  maxeff_bars->GetXaxis()->LabelsOption("v");
  finaleff_bars->GetXaxis()->LabelsOption("v");

  std::cout << "\n\nTotal Efficiency Plot\n\n";

  TCanvas* total_abseff_canvas = new TCanvas("total_abseff_canvas",
  "Total Absolute Efficiency Plot", 0, 0, 2560, 1440);
  total_abseff_canvas->cd();
  total_abseff_canvas->SetBottomMargin(0.3);

  maxeff_bars->Draw("Hist, E1");
  finaleff_bars->Draw("Hist, E1, SAME");

  gPad->BuildLegend(0.82,0.85,0.9,0.9,"");
  total_abseff_canvas->SetTitle(
  "Optimal and Maximum Absolute Efficiencies");
  total_abseff_canvas->SaveAs((canvas_dir + "total_abseffs_"
  + std::to_string(job_num) + "_" + ratecap_string + ".tex").c_str());
  total_abseff_canvas->SaveAs((canvas_dir + "total_abseffs_"
  + std::to_string(job_num) + "_" + ratecap_string + ".C").c_str());
  total_abseff_canvas->SaveAs((canvas_dir + "total_abseffs_"
  + std::to_string(job_num) + "_" + ratecap_string + ".svg").c_str());

  // Root safe plot
  for (unsigned i{1}; i <= tuples.size(); i++) {
    maxeff_bars->GetXaxis()->SetBinLabel(i,
    tuples[i-1]->_filesafe_name.c_str()); 
    finaleff_bars->GetXaxis()->SetBinLabel(i,
    tuples[i-1]->_filesafe_name.c_str());
  }

  TCanvas* rootfriendly_abseff_canvas =
  new TCanvas("rootfriendly_abseff_canvas",
  "Total Absolute Efficiency Plot", 0, 0, 2560, 1440);
  rootfriendly_abseff_canvas->cd();
  rootfriendly_abseff_canvas->SetBottomMargin(0.65);

  maxeff_bars->Draw("Hist, E1");
  finaleff_bars->Draw("Hist, E1, SAME");

  gPad->BuildLegend(0.82,0.85,0.9,0.9,"");
  rootfriendly_abseff_canvas->SetTitle(
  "Optimal and Maximum Absolute Efficiencies");
  rootfriendly_abseff_canvas->SaveAs((canvas_dir
  + "rootfriendly_total_abseffs_"
  + std::to_string(job_num) + "_" + ratecap_string + ".C").c_str());
}


void plot_inclusive_exclusive_rates_per_line(
  std::string canvas_dir, std::vector<io_manager*> tuples,
  line_parameters results, opt_discrete_gradient* global_minimiser,
  int job_num, double collision_rate, int& line_count,
  std::vector<std::string> inclusive_hadronic_lines
  ) {

  // --------- Inclusive and Exclusive Rates per Line ---------- //

  std::cout << "\n\nInclusive and Exclusive Minbias Rate per "
  << "Inclusive Hadronic Line Plot\n\n";
  line_store minbias_linestore;
  line_parameters minbias_params;
  io_manager* minbias = tuples[0]->_background_io;


  TH1D* inclusive_rate_per_inclusive_hadronic_line =
  new TH1D("inclusive_minbias_rate_per_inclusive_hadronic_line",
  "Incl. Rate",
  inclusive_hadronic_lines.size(), 0, inclusive_hadronic_lines.size());

  inclusive_rate_per_inclusive_hadronic_line->SetYTitle("Rate (MHz)");
  inclusive_rate_per_inclusive_hadronic_line->SetStats(kFALSE);
  inclusive_rate_per_inclusive_hadronic_line->SetLineWidth(2);
  inclusive_rate_per_inclusive_hadronic_line->SetFillStyle(3345);
  inclusive_rate_per_inclusive_hadronic_line->SetLineColor(kRed);
  inclusive_rate_per_inclusive_hadronic_line->SetFillColor(kRed);

  inclusive_rate_per_inclusive_hadronic_line->SetMinimum(0.0);
  inclusive_rate_per_inclusive_hadronic_line->SetMaximum(1.0);

  TH1D* exclusive_rate_per_inclusive_hadronic_line =
  new TH1D("exclusive_minbias_rate_per_inclusive_hadronic_line",
  "Excl. Rate",
  inclusive_hadronic_lines.size(), 0, inclusive_hadronic_lines.size());

  exclusive_rate_per_inclusive_hadronic_line->SetYTitle("Rate (MHz)");
  exclusive_rate_per_inclusive_hadronic_line->SetStats(kFALSE);
  exclusive_rate_per_inclusive_hadronic_line->SetLineWidth(2);
  exclusive_rate_per_inclusive_hadronic_line->SetFillStyle(3354);
  exclusive_rate_per_inclusive_hadronic_line->SetLineColor(kBlue);
  exclusive_rate_per_inclusive_hadronic_line->SetFillColor(kBlue);

  exclusive_rate_per_inclusive_hadronic_line->SetMinimum(0.0);
  exclusive_rate_per_inclusive_hadronic_line->SetMaximum(1.0);

  std::cout << "Optimal inclusive hadronic line rates in MHz"
  " (inclusive rate, exclusive rate):\n";
  line_store linestore;
  for (unsigned i{1}; i <= NUM_LINES; i++) {
    if (std::find(inclusive_hadronic_lines.begin(),
      inclusive_hadronic_lines.end(), linestore._linenames[i-1])
      != inclusive_hadronic_lines.end()) {

      inclusive_rate_per_inclusive_hadronic_line->SetBinContent(
      line_count+1,
      (minbias->_specline_adam->_background_efficiencies[i-1]
        * collision_rate) / 1e6);
      inclusive_rate_per_inclusive_hadronic_line->SetBinError(
      line_count+1,
      (minbias->_specline_adam->_background_efficiency_errs[i-1]
      * collision_rate) / 1e6);
      inclusive_rate_per_inclusive_hadronic_line->GetXaxis()
      ->SetBinLabel(line_count + 1, linestore._linenames[i-1].c_str());

      double efficiency = 
      minbias->_optimal_unique_events_per_line[i-1]
      / (1.0*minbias->_unique_precut_events);
      double rate = efficiency * collision_rate / 1e6;
      exclusive_rate_per_inclusive_hadronic_line->SetBinContent(
      line_count+1, rate);
      double error =
      (efficiency >= 1.0
      || efficiency <= 0.0) ? 1e-8 : sqrt(fabs(efficiency
      * (1 - efficiency)) / (1.0 * minbias->_unique_precut_events));

      exclusive_rate_per_inclusive_hadronic_line->SetBinError(
      line_count+1, error * rate);
      exclusive_rate_per_inclusive_hadronic_line->GetXaxis()
      ->SetBinLabel(line_count+1, linestore._linenames[i-1].c_str());

      std::cout << linestore._linenames[i-1] << ": "
      << inclusive_rate_per_inclusive_hadronic_line->GetBinContent(
      line_count+1) << " +/- "
      << inclusive_rate_per_inclusive_hadronic_line->GetBinError(
      line_count+1) << ", "
      << exclusive_rate_per_inclusive_hadronic_line->GetBinContent(
      line_count+1) << " +/- "
      << exclusive_rate_per_inclusive_hadronic_line->GetBinError(
      line_count+1) << "\n";

      line_count++;
    }
  }

  std::cout << "Optimal inclusive hadronic line rates in MHz"
  " (inclusive rate, exclusive rate) with & delimiter for excel "
  "spreadsheet:\n";
  line_count = 0;
  for (unsigned i{1}; i <= NUM_LINES; i++) {
    if (std::find(inclusive_hadronic_lines.begin(),
      inclusive_hadronic_lines.end(), linestore._linenames[i-1])
      != inclusive_hadronic_lines.end()) {
      std::cout << linestore._linenames[i-1] << "&"
      << inclusive_rate_per_inclusive_hadronic_line->GetBinContent(
      line_count+1) << "&"
      << inclusive_rate_per_inclusive_hadronic_line->GetBinError(
      line_count+1) << "&"
      << exclusive_rate_per_inclusive_hadronic_line->GetBinContent(
      line_count+1) << "&"
      << exclusive_rate_per_inclusive_hadronic_line->GetBinError(
      line_count+1) << "\n";

      line_count++;
    }
  }

  TCanvas* inclusive_exclusive_rate_per_inclusive_hadronic_line_canvas =
  new TCanvas(
  "inclusive_exclusive_minbias_rate_per_inclusive_hadronic_line",
  "Inclusive and Exclusive Minbias Rate per Inclusive Hadronic Line",
  2560, 1440);
  inclusive_exclusive_rate_per_inclusive_hadronic_line_canvas->cd();

  inclusive_rate_per_inclusive_hadronic_line->Draw("Hist, E1");
  exclusive_rate_per_inclusive_hadronic_line->Draw("Hist, E1, SAME");

  gPad->BuildLegend(0.77,0.8,0.9,0.9,"");

  inclusive_exclusive_rate_per_inclusive_hadronic_line_canvas->SaveAs(
  (canvas_dir
  + "inclusive_exclusive_minbias_rate_per_inclusive_hadronic_line_"
  + std::to_string(job_num) + ".tex").c_str());
  inclusive_exclusive_rate_per_inclusive_hadronic_line_canvas->SaveAs(
  (canvas_dir
  + "inclusive_exclusive_minbias_rate_per_inclusive_hadronic_line_"
  + std::to_string(job_num) + ".C").c_str());
  inclusive_exclusive_rate_per_inclusive_hadronic_line_canvas->SaveAs(
  (canvas_dir
  + "inclusive_exclusive_minbias_rate_per_inclusive_hadronic_line_"
  + std::to_string(job_num) + ".pdf").c_str());
  inclusive_exclusive_rate_per_inclusive_hadronic_line_canvas->SaveAs(
  (canvas_dir
  + "inclusive_exclusive_minbias_rate_per_inclusive_hadronic_line_"
  + std::to_string(job_num) + ".svg").c_str());
}


void plot_low_rate_line_rates(
  std::string canvas_dir, std::vector<io_manager*> tuples,
  line_parameters results, opt_discrete_gradient* global_minimiser,
  int job_num, std::vector<std::string> inclusive_hadronic_lines,
  int& line_count, double collision_rate 
  ) {

  // Low rate lines
  io_manager* minbias = tuples[0]->_background_io;

  TH1D* inclusive_rate_per_low_rate_line =
  new TH1D("inclusive_minbias_rate_per_low_rate_line",
  "Incl. Rate",
  minbias->_lines.size()
  - inclusive_hadronic_lines.size(), 0,
  minbias->_lines.size()
  - inclusive_hadronic_lines.size());

  inclusive_rate_per_low_rate_line->SetYTitle("Rate (MHz)");
  inclusive_rate_per_low_rate_line->SetStats(kFALSE);
  inclusive_rate_per_low_rate_line->SetLineWidth(2);
  inclusive_rate_per_low_rate_line->SetFillStyle(3345);
  inclusive_rate_per_low_rate_line->SetLineColor(kRed);
  inclusive_rate_per_low_rate_line->SetFillColor(kRed);

  inclusive_rate_per_low_rate_line->SetMinimum(0.0);
  inclusive_rate_per_low_rate_line->SetMaximum(0.1);

  TH1D* exclusive_rate_per_low_rate_line =
  new TH1D("exclusive_minbias_rate_per_low_rate_line",
  "Excl. Rate",
  minbias->_lines.size() 
  - inclusive_hadronic_lines.size(), 0,
  minbias->_lines.size()
  - inclusive_hadronic_lines.size());

  exclusive_rate_per_low_rate_line->SetYTitle("Rate (MHz)");
  exclusive_rate_per_low_rate_line->SetStats(kFALSE);
  exclusive_rate_per_low_rate_line->SetLineWidth(2);
  exclusive_rate_per_low_rate_line->SetFillStyle(3354);
  exclusive_rate_per_low_rate_line->SetLineColor(kBlue);
  exclusive_rate_per_low_rate_line->SetFillColor(kBlue);

  exclusive_rate_per_low_rate_line->SetMinimum(0.0);
  exclusive_rate_per_low_rate_line->SetMaximum(0.1);

  std::cout << "Optimal low-rate line rates (inclusive rate, "
  "exclusive rate):\n";

  line_count = 0;
  line_store linestore;
  for (unsigned i{1}; i <= NUM_LINES; i++) {
    if (std::find(inclusive_hadronic_lines.begin(),
      inclusive_hadronic_lines.end(), linestore._linenames[i-1])
      == inclusive_hadronic_lines.end()) {
      inclusive_rate_per_low_rate_line->SetBinContent(
      line_count+1,
      (minbias->_specline_adam->_background_efficiencies[i-1]
      * collision_rate) / 1e6);
      inclusive_rate_per_low_rate_line->SetBinError(
      line_count+1,
      (minbias->_specline_adam->_background_efficiency_errs[i-1]
      * collision_rate) / 1e6);
      inclusive_rate_per_low_rate_line->GetXaxis()->SetBinLabel(
      line_count + 1, linestore._linenames[i-1].c_str());

      double efficiency = 
      minbias->_optimal_unique_events_per_line[i-1]
      / (1.0*minbias->_unique_precut_events);
      double rate = efficiency * collision_rate / 1e6;
      exclusive_rate_per_low_rate_line->SetBinContent(
      line_count+1, rate);
      double error =
      (efficiency >= 1.0
      || efficiency <= 0.0) ? 1e-8 : sqrt(fabs(efficiency
      * (1 - efficiency)) / (1.0 * minbias->_unique_precut_events));

      exclusive_rate_per_low_rate_line->SetBinError(
      line_count+1, error * rate);
      exclusive_rate_per_low_rate_line->GetXaxis()
      ->SetBinLabel(line_count+1, linestore._linenames[i-1].c_str());

      std::cout << linestore._linenames[i-1] << ": "
      << inclusive_rate_per_low_rate_line->GetBinContent(
      line_count+1) << " +/- "
      << inclusive_rate_per_low_rate_line->GetBinError(
      line_count+1) << ", "
      << exclusive_rate_per_low_rate_line->GetBinContent(
      line_count+1) << " +/- "
      << exclusive_rate_per_low_rate_line->GetBinError(
      line_count+1) << "\n";

      line_count++;
    }
  }

  std::cout << "Optimal low-rate line rates (inclusive rate, "
  "exclusive rate) with & delimiter for Excel spreadsheet:\n";
  line_count = 0;
  for (unsigned i{1}; i <= NUM_LINES; i++) {
    if (std::find(inclusive_hadronic_lines.begin(),
      inclusive_hadronic_lines.end(), linestore._linenames[i-1])
      == inclusive_hadronic_lines.end()) {
      std::cout << linestore._linenames[i-1] << "&"

      << inclusive_rate_per_low_rate_line->GetBinContent(
      line_count+1) << "&"
      << inclusive_rate_per_low_rate_line->GetBinError(
      line_count+1) << "&"
      << exclusive_rate_per_low_rate_line->GetBinContent(
      line_count+1) << "&"
      << exclusive_rate_per_low_rate_line->GetBinError(
      line_count+1) << "\n";

      line_count++;
    }
  }

  TCanvas* inclusive_exclusive_rate_per_low_rate_line_canvas =
  new TCanvas(
  "inclusive_exclusive_minbias_rate_per_low_rate_line",
  "Inclusive and Exclusive Minbias Rate per Inclusive Hadronic Line",
  2560, 1440);

  inclusive_exclusive_rate_per_low_rate_line_canvas->cd();

  inclusive_rate_per_low_rate_line->Draw("Hist, E1");
  exclusive_rate_per_low_rate_line->Draw("Hist, E1, SAME");

  gPad->BuildLegend(0.77,0.8,0.9,0.9,"");

  inclusive_exclusive_rate_per_low_rate_line_canvas->SaveAs(
  (canvas_dir
  + "inclusive_exclusive_minbias_rate_per_low_rate_line_"
  + std::to_string(job_num) + ".tex").c_str());
  inclusive_exclusive_rate_per_low_rate_line_canvas->SaveAs(
  (canvas_dir
  + "inclusive_exclusive_minbias_rate_per_low_rate_line_"
  + std::to_string(job_num) + ".C").c_str());
  inclusive_exclusive_rate_per_low_rate_line_canvas->SaveAs(
  (canvas_dir
  + "inclusive_exclusive_minbias_rate_per_low_rate_line_"
  + std::to_string(job_num) + ".pdf").c_str());
  inclusive_exclusive_rate_per_low_rate_line_canvas->SaveAs(
  (canvas_dir
  + "inclusive_exclusive_minbias_rate_per_low_rate_line_"
  + std::to_string(job_num) + ".svg").c_str());
}


void plot_inclusive_line_efficiencies_per_sample(
  std::string canvas_dir, std::vector<io_manager*> tuples,
  line_parameters results, opt_discrete_gradient* global_minimiser,
  int job_num, double collision_rate, double ratelimit,
  std::string recon_equation
  ) {

  // --------- Inclusive Line Efficiencies per Sample --------- //

  std::cout << "\n\nInclusive Line Efficiencies per Sample Plots\n\n";
  line_store linestore;
  for (unsigned i{0}; i < tuples.size(); i++) {
    auto temp_active_lines = tuples[i]->_activate_lines;
    line_store linestore;
    line_parameters params;

    init_discrete_gradient* specline_minimiser =
    new init_discrete_gradient(tuples[i], collision_rate, ratelimit, 0.25,
    2); // Scalewidth and accuracy irrelevant here
    for (unsigned j{0}; j < NUM_LINES; ++j ){
      if (temp_active_lines[j]) {
        // std::cout << "Calc for "
        // << tuples[i]->_background_io->_line_names[j] << "\n";
        std::vector<double> specstats =
        specline_minimiser->speceff_minimised_function(results, j);
        specline_minimiser->_signal_efficiencies.push_back(
        specstats[5]);
        specline_minimiser->_signal_efficiency_errs.push_back(
        specstats[6]);
      }
    }

    TH1D* maxeff_per_line =
    new TH1D((tuples[i]->_filesafe_name + "_maxeff_per_line").c_str(),
    "Max",
    specline_minimiser->_signal_efficiencies.size(), 0,
    specline_minimiser->_signal_efficiencies.size());
    maxeff_per_line->SetYTitle(("\\text{Incl. } " + tuples[i]->_name
    + recon_equation + " \\text{ HLT1 Eff.}").c_str());
    maxeff_per_line->GetYaxis()->SetTitleSize(0.03);
    maxeff_per_line->SetStats(kFALSE);
    maxeff_per_line->SetFillStyle(3345);
    maxeff_per_line->SetLineWidth(2);
    maxeff_per_line->SetLineColor(kRed);
    maxeff_per_line->SetFillColor(kRed);

    TH1D* finaleff_per_line =
    new TH1D((tuples[i]->_filesafe_name + "_finaleff_per_line").c_str(),
    "Opt",
    specline_minimiser->_signal_efficiencies.size(), 0,
    specline_minimiser->_signal_efficiencies.size());

    finaleff_per_line->SetYTitle(("\\text{ Incl.} " + tuples[i]->_name
    + recon_equation + " \\text{ HLT1 Eff.}").c_str());
    finaleff_per_line->GetYaxis()->SetTitleSize(0.03);
    finaleff_per_line->SetStats(kFALSE);
    finaleff_per_line->SetFillStyle(3354);
    finaleff_per_line->SetLineWidth(2);
    finaleff_per_line->SetLineColor(kBlue);
    finaleff_per_line->SetFillColor(kBlue);

    maxeff_per_line->SetMinimum(0.0);
    finaleff_per_line->SetMinimum(0.0);
    maxeff_per_line->SetMaximum(1.0);
    finaleff_per_line->SetMaximum(1.0);
    int linecount{0};
    for (unsigned j{1}; j <= NUM_LINES; ++j ){
      if (temp_active_lines[j-1]) {
        maxeff_per_line->SetBinContent(linecount+1,
        tuples[i]->_specline_adam->_signal_efficiencies[linecount]);
        maxeff_per_line->SetBinError(linecount+1,
        tuples[i]->_specline_adam->_signal_efficiency_errs[linecount]);
        maxeff_per_line->GetXaxis()->SetBinLabel(linecount+1,
        linestore._linenames[j-1].c_str());
        finaleff_per_line->SetBinContent(linecount+1,
        specline_minimiser->_signal_efficiencies[linecount]);
        finaleff_per_line->SetBinError(linecount+1,
        specline_minimiser->_signal_efficiency_errs[linecount]);
        finaleff_per_line->GetXaxis()->SetBinLabel(linecount+1,
        linestore._linenames[j-1].c_str());
        linecount++;
      }
    }

    TCanvas* totaleff_perline_canvas =
    new TCanvas((tuples[i]->_filesafe_name + "_totaleff_perline").c_str(),
    ("\\text{Total Efficiency per Line for } " + tuples[i]->_name).c_str(),
    0, 0, 900, 450);
  
    totaleff_perline_canvas->cd();

    maxeff_per_line->SetMinimum(0.0);
    maxeff_per_line->Draw("Hist, E1");

    finaleff_per_line->SetMinimum(0.0);
    finaleff_per_line->Draw("Hist, E1, SAME");

    gPad->BuildLegend(0.79,0.82,0.9,0.9,"");

    totaleff_perline_canvas->SaveAs((canvas_dir
    + tuples[i]->_filesafe_name + "_total_inclusive_effs_perline_"
    + std::to_string(job_num) + ".tex").c_str());
    totaleff_perline_canvas->SaveAs((canvas_dir
    + tuples[i]->_filesafe_name + "_total_inclusive_effs_perline_"
    + std::to_string(job_num) + ".C").c_str());
    totaleff_perline_canvas->SaveAs((canvas_dir
    + tuples[i]->_filesafe_name + "_total_inclusive_effs_perline_"
    + std::to_string(job_num) + ".svg").c_str());

    delete specline_minimiser;
  }

}


void plot_inclusive_sample_efficiencies_per_line(
  std::string canvas_dir, std::vector<io_manager*> tuples,
  line_parameters results, opt_discrete_gradient* global_minimiser,
  int job_num, double collision_rate, double ratelimit,
  std::string recon_short, std::string recon_equation
  ) {
  // --------------- Inclusive Sample Efficiencies per Line ------------- //

  std::cout << "\n\nInclusive Line Efficiencies for Each Sample Grouped "
  "by Line Plots\n\n";
  line_store linestore;
  for (unsigned i{0}; i < NUM_LINES; i++) {
    std::vector<std::string> represented_samples;
    std::vector<double> max_sample_efficiencies;
    std::vector<double> max_sample_efficiency_errs;
    std::vector<double> opt_sample_efficiencies;
    std::vector<double> opt_sample_efficiency_errs;
    std::string name = linestore._linenames[i]; 
    for (io_manager* tuple: tuples) {
      if( tuple->_activate_lines[i] ){ 
        represented_samples.push_back(tuple->_name);
        max_sample_efficiencies.push_back(
        tuple->_specline_adam->_signal_efficiencies.at(i));
        max_sample_efficiency_errs.push_back(
        tuple->_specline_adam->_signal_efficiency_errs.at(i));

        init_discrete_gradient* specline_minimiser =
        new init_discrete_gradient(tuple, collision_rate, ratelimit, 0.25,
        2); // Scalewidth and accuracy irrelevant here
        std::vector<double> specstats =
        specline_minimiser->speceff_minimised_function(results, i);
        opt_sample_efficiencies.push_back(specstats[5]);
        opt_sample_efficiency_errs.push_back(specstats[6]);
      }
    }

    TH1D* max_inclusive_sample_effs_per_line =
    new TH1D(( std::string(linestore._linenames[i])
    + "_max_inclusive_sample_efficiencies").c_str(),
    "Max", represented_samples.size(), 0, represented_samples.size());

    max_inclusive_sample_effs_per_line->SetYTitle(("Incl. "
    + std::string(linestore._linenames[i]) + " " + recon_short
    + " HLT1 Eff.").c_str());

    max_inclusive_sample_effs_per_line->GetYaxis()->SetTitleSize(0.03);
    max_inclusive_sample_effs_per_line->SetStats(kFALSE);
    max_inclusive_sample_effs_per_line->SetFillStyle(3345);
    max_inclusive_sample_effs_per_line->SetLineWidth(2);
    max_inclusive_sample_effs_per_line->SetLineColor(kRed);
    max_inclusive_sample_effs_per_line->SetFillColor(kRed);

    max_inclusive_sample_effs_per_line->SetMinimum(0.0);
    max_inclusive_sample_effs_per_line->SetMaximum(1.0);

    TH1D* opt_inclusive_sample_effs_per_line =
    new TH1D(( std::string(linestore._linenames[i])
    + "_opt_inclusive_sample_efficiencies").c_str(),
    "Opt", represented_samples.size(), 0, represented_samples.size());

    opt_inclusive_sample_effs_per_line->SetYTitle(("Incl. " + name + " " + recon_short
    + " HLT1 Eff.").c_str());
    opt_inclusive_sample_effs_per_line->GetYaxis()->SetTitleSize(0.03);
    opt_inclusive_sample_effs_per_line->SetStats(kFALSE);
    opt_inclusive_sample_effs_per_line->SetFillStyle(3354);
    opt_inclusive_sample_effs_per_line->SetLineWidth(2);
    opt_inclusive_sample_effs_per_line->SetLineColor(kBlue);
    opt_inclusive_sample_effs_per_line->SetFillColor(kBlue);

    opt_inclusive_sample_effs_per_line->SetMinimum(0.0);
    opt_inclusive_sample_effs_per_line->SetMaximum(1.0);

    for (unsigned j{1}; j <= represented_samples.size(); j++) { 
      double binerror = max_sample_efficiency_errs[j-1] > 0.0 ?
      max_sample_efficiency_errs[j-1] : 1e-8;
      max_inclusive_sample_effs_per_line->SetBinError(
      j, binerror);
      max_inclusive_sample_effs_per_line->SetBinContent(j,
      max_sample_efficiencies[j-1]);
      max_inclusive_sample_effs_per_line->GetXaxis()->SetBinLabel(j,
      represented_samples[j-1].c_str());

      binerror = opt_sample_efficiency_errs[j-1] > 0.0 ?
      opt_sample_efficiency_errs[j-1] : 1e-8;
      opt_inclusive_sample_effs_per_line->SetBinError(
      j, binerror);
      opt_inclusive_sample_effs_per_line->SetBinContent(j,
      opt_sample_efficiencies[j-1]);
      opt_inclusive_sample_effs_per_line->GetXaxis()->SetBinLabel(j,
      represented_samples[j-1].c_str());
    }
    max_inclusive_sample_effs_per_line->GetXaxis()->LabelsOption("v");
    opt_inclusive_sample_effs_per_line->GetXaxis()->LabelsOption("v");

    TCanvas* inclusive_sample_effs_per_line_canvas =
    new TCanvas((name + "_inclusive_sample_effs").c_str(),
    (name + " Inclusive Sample Efficiencies").c_str(),
    2560, 1440);

    inclusive_sample_effs_per_line_canvas->cd();
    inclusive_sample_effs_per_line_canvas->SetBottomMargin(0.65);

    max_inclusive_sample_effs_per_line->Draw("Hist, E1");
    opt_inclusive_sample_effs_per_line->Draw("Hist, E1, SAME");

    gPad->BuildLegend(0.78,0.81,0.9,0.9,"");
    inclusive_sample_effs_per_line_canvas->SaveAs((canvas_dir
    + name + "_inclusive_sample_efficiencies_"
    + std::to_string(job_num) + ".tex").c_str());

    inclusive_sample_effs_per_line_canvas->SaveAs((canvas_dir
    + name + "_inclusive_sample_efficiencies_"
    + std::to_string(job_num) + ".C").c_str());
  }
}


void plot_exclusive_line_efficiencies_per_sample(
  std::string canvas_dir, std::vector<io_manager*> tuples,
  line_parameters results, opt_discrete_gradient* global_minimiser,
  int job_num, std::string recon_equation
  ) {

  // ------------------ Exclusive Line Efficiencies per Sample ----------

  std::cout << "\n\nExclusive Line Efficiencies per Sample Plots\n\n";
  line_store linestore;
  for (const auto& t : tuples ){
    auto temp_active_lines = t->_activate_lines;
    line_store linestore;
    line_parameters params;

    // Remember to divide these by precut_events
    auto unique_events_per_line = t->events_unique_to_lines(results, t->_activate_lines);

    int line_size = std::count_if( t->_activate_lines.begin(),
        t->_activate_lines.end(),
        [](auto& active){ return active; } ); 

    TH1D* maxeff_per_line = new TH1D((t->_filesafe_name
    + "_exclusive_maxeff_per_line").c_str(), "Max", line_size, 0, line_size);

    maxeff_per_line->SetYTitle(("\\text{Excl. } " + t->_name
    + recon_equation+ " \\text{ HLT1 Eff.}").c_str());
    maxeff_per_line->GetYaxis()->SetTitleSize(0.03);
    maxeff_per_line->SetStats(kFALSE);
    maxeff_per_line->SetFillStyle(3345);
    maxeff_per_line->SetLineWidth(2);
    maxeff_per_line->SetLineColor(kRed);
    maxeff_per_line->SetFillColor(kRed);


    TH1D* finaleff_per_line = new TH1D((t->_filesafe_name
    + "_exclusive_opteff_per_line").c_str(), "Opt", line_size, 0, line_size);
    finaleff_per_line->SetYTitle(("\\text{Excl. } "
    + t->_name + recon_equation + " \\text{ HLT1 Eff.}").c_str());
    finaleff_per_line->GetYaxis()->SetTitleSize(0.03);
    finaleff_per_line->SetStats(kFALSE);
    finaleff_per_line->SetFillStyle(3354);
    finaleff_per_line->SetLineWidth(2);
    finaleff_per_line->SetLineColor(kBlue);
    finaleff_per_line->SetFillColor(kBlue);

    maxeff_per_line->SetMinimum(0.0);
    finaleff_per_line->SetMinimum(1.0);
    maxeff_per_line->SetMaximum(1.0);
    finaleff_per_line->SetMaximum(1.0);

    int linecount{0};

    if (t->_optimal_unique_events_per_line.size() == 0) {
      std::cout << "No optimal unique events stored for "
      << t->_name << ". Exiting...\n";
      exit(0);
    }

    for (unsigned j{0}; j < NUM_LINES; ++j ){
      if (!temp_active_lines[j]) continue; 
      if (j > t->_optimal_unique_events_per_line.size()) {
        std::cout << "No optimal unique events stored for "
        << linestore._linenames[j] << " in "
        << t->_name << ". Exiting...\n";
        exit(0);
      }
      double efficiency = t->_optimal_unique_events_per_line[j]
      / (1.0*t->_unique_precut_events);
      maxeff_per_line->SetBinContent(linecount+1, efficiency);
      double error = (efficiency >= 1.0
      || efficiency <= 0.0) ? 1e-8 : sqrt(fabs(efficiency
      * (1 - efficiency)) / (1.0*t->_unique_precut_events));

      maxeff_per_line->SetBinError(linecount+1, error);
      maxeff_per_line->GetXaxis()->SetBinLabel(linecount+1,
      linestore._linenames[j].c_str());

      efficiency = unique_events_per_line[j]
      / (1.0*t->_unique_precut_events);
      finaleff_per_line->SetBinContent(linecount+1, efficiency);

      error = (efficiency >= 1.0
      || efficiency <= 0.0) ? 1e-8 : sqrt(fabs(efficiency
      * (1 - efficiency)) / (1.0*t->_unique_precut_events));

      finaleff_per_line->SetBinError(linecount+1, error);
      finaleff_per_line->GetXaxis()->SetBinLabel(linecount+1,
      linestore._linenames[j].c_str());

      linecount++;
    }

    TCanvas* totaleff_perline_canvas =
    new TCanvas((t->_filesafe_name
    + "_total_exclusive_eff_perline").c_str(),
    ("Total Exclusive Efficiency per Line for "
    + t->_filesafe_name).c_str(),
    0, 0, 900, 450);

    totaleff_perline_canvas->cd();

    maxeff_per_line->Draw("Hist, E1");

    finaleff_per_line->Draw("Hist, E1, SAME");

    gPad->BuildLegend(0.78,0.81,0.9,0.9,"");

    totaleff_perline_canvas->SaveAs((canvas_dir
    + t->_filesafe_name + "_total_exclusive_effs_perline_"
    + std::to_string(job_num) + ".tex").c_str());
    totaleff_perline_canvas->SaveAs((canvas_dir
    + t->_filesafe_name + "_total_exclusive_effs_perline_"
    + std::to_string(job_num) + ".C").c_str());
    totaleff_perline_canvas->SaveAs((canvas_dir
    + t->_filesafe_name + "_total_exclusive_effs_perline_"
    + std::to_string(job_num) + ".svg").c_str());
  }
}


void plot_exclusive_sample_efficiencies_per_line(
    std::string canvas_dir, std::vector<io_manager*> tuples,
    line_parameters results, opt_discrete_gradient* global_minimiser,
    int job_num, std::string recon_short
    ) {

  // --------------- Exclusive Sample Efficiencies per Line ------------- //
  return; 
  std::cout << "\n\nExclusive Line Efficiencies for Each Sample Grouped by "
  "Line Plots\n\n";
  line_store linestore;
  for (unsigned i{0}; i < NUM_LINES; ++i ){
    std::vector<std::string> represented_samples;
    std::vector<double> max_sample_efficiencies;
    std::vector<double> max_sample_efficiency_errs;
    std::vector<double> opt_sample_efficiencies;
    std::vector<double> opt_sample_efficiency_errs;
    std::string name = linestore._linenames[i]; 
    for (io_manager* tuple: tuples) {
      if (tuple->_activate_lines[i] ){
        represented_samples.push_back(tuple->_name);

        double efficiency = 
        tuple->_optimal_unique_events_per_line.at(i)
        / (1.0*tuple->_unique_precut_events);

        double error = 
        (efficiency >= 1.0 || efficiency <= 0.0)
        ? 1e-8 : sqrt(fabs(efficiency * (1 - efficiency))
        / (1.0 * tuple->_unique_precut_events));

        max_sample_efficiencies.push_back(efficiency);
        max_sample_efficiency_errs.push_back(error);

        std::vector<int> unique_events_per_line =
        tuple->events_unique_to_lines(results,
        tuple->_activate_lines);

        efficiency = 
        unique_events_per_line.at(i)
        / (1.0*tuple->_unique_precut_events);

        error = 
        (efficiency >= 1.0 || efficiency <= 0.0) ? 1e-8 : sqrt(fabs(efficiency
        * (1 - efficiency))
        / (1.0 * tuple->_unique_precut_events));

        opt_sample_efficiencies.push_back(efficiency);
        opt_sample_efficiency_errs.push_back(error);
        break;
      }
    }

    TH1D* max_exclusive_sample_effs_per_line =
    new TH1D((name + "_max_exclusive_sample_efficiencies").c_str(),
    "Max", represented_samples.size(), 0, represented_samples.size());
    max_exclusive_sample_effs_per_line->SetYTitle(("Excl. "
    + name + " " + recon_short
    + " HLT1 Eff.").c_str());
    max_exclusive_sample_effs_per_line->GetYaxis()->SetTitleSize(0.03);

    max_exclusive_sample_effs_per_line->SetStats(kFALSE);
    max_exclusive_sample_effs_per_line->SetFillStyle(3345);
    max_exclusive_sample_effs_per_line->SetLineWidth(2);
    max_exclusive_sample_effs_per_line->SetLineColor(kRed);
    max_exclusive_sample_effs_per_line->SetFillColor(kRed);

    max_exclusive_sample_effs_per_line->SetMinimum(0.0);
    max_exclusive_sample_effs_per_line->SetMaximum(1.0);

    TH1D* opt_exclusive_sample_effs_per_line =
    new TH1D((name + "_opt_exclusive_sample_efficiencies").c_str(),
    "Opt", represented_samples.size(), 0, represented_samples.size());
    opt_exclusive_sample_effs_per_line->SetYTitle(("Excl. "
    + name + " " + recon_short
    + " HLT1 Eff.").c_str());
    opt_exclusive_sample_effs_per_line->GetYaxis()->SetTitleSize(0.03);

    opt_exclusive_sample_effs_per_line->SetStats(kFALSE);
    opt_exclusive_sample_effs_per_line->SetFillStyle(3354);
    opt_exclusive_sample_effs_per_line->SetLineWidth(2);
    opt_exclusive_sample_effs_per_line->SetLineColor(kBlue);
    opt_exclusive_sample_effs_per_line->SetFillColor(kBlue);

    opt_exclusive_sample_effs_per_line->SetMinimum(0.0);
    opt_exclusive_sample_effs_per_line->SetMaximum(1.0);

    for (unsigned j{1}; j <= represented_samples.size(); j++) { 
      double binerror = max_sample_efficiency_errs[j-1] > 0.0 ?
      max_sample_efficiency_errs[j-1] : 1e-8;
      max_exclusive_sample_effs_per_line->SetBinError(
      j, binerror);
      max_exclusive_sample_effs_per_line->SetBinContent(j,
      max_sample_efficiencies[j-1]);
      max_exclusive_sample_effs_per_line->GetXaxis()->SetBinLabel(j,
      represented_samples[j-1].c_str());

      binerror = opt_sample_efficiency_errs[j-1] > 0.0 ?
      opt_sample_efficiency_errs[j-1] : 1e-8;
      opt_exclusive_sample_effs_per_line->SetBinError(
      j, binerror);
      opt_exclusive_sample_effs_per_line->SetBinContent(j,
      opt_sample_efficiencies[j-1]);
      opt_exclusive_sample_effs_per_line->GetXaxis()->SetBinLabel(j,
      represented_samples[j-1].c_str());
    }
    max_exclusive_sample_effs_per_line->GetXaxis()->LabelsOption("v");
    opt_exclusive_sample_effs_per_line->GetXaxis()->LabelsOption("v");

    TCanvas* exclusive_sample_effs_per_line_canvas =
    new TCanvas((name + "_exclusive_sample_effs").c_str(),
    (name + " Exclusive Sample Efficiencies").c_str(),
    2560, 1440);
    
    exclusive_sample_effs_per_line_canvas->cd();
    exclusive_sample_effs_per_line_canvas->SetBottomMargin(0.65);

    max_exclusive_sample_effs_per_line->Draw("Hist, E1");
    opt_exclusive_sample_effs_per_line->Draw("Hist, E1, SAME");

    gPad->BuildLegend(0.78,0.81,0.9,0.9,"");

    exclusive_sample_effs_per_line_canvas->SaveAs((canvas_dir
    + name + "_exclusive_sample_efficiencies_"
    + std::to_string(job_num) + ".tex").c_str());
    exclusive_sample_effs_per_line_canvas->SaveAs((canvas_dir
    + name + "_exclusive_sample_efficiencies_"
    + std::to_string(job_num) + ".C").c_str());
  }
}


void plot_global_adam_statistics(
  std::string plot_dir, std::vector<io_manager*> tuples,
  double collision_rate, double ratelimit, line_parameters results,
  opt_discrete_gradient* global_minimiser, int job_num
  ) {
  // Plot efficiencies per line, and then bar chart plot comparing maximum
  // and optimised efficiencies

  double ratecap = (tuples[0]->_percentage_ratelimit/100.0)
  * ((tuples[0]->_collision_rate * tuples[0]->_ratelimit) / 1e6);
  std::stringstream ratecap_ss;
  ratecap_ss << std::fixed << std::setprecision(3) << ratecap;
  std::string ratecap_string = ratecap_ss.str() + "_mhz";
  boost::replace_all(ratecap_string, ".", "point");

  bool reconstructable = tuples[0]->_background_io->_reconstructable_only;
  std::string recon_string = reconstructable ? "Reconstructable" : "";
  std::string recon_short = reconstructable ? "Recons." : "";
  std::string recon_equation = reconstructable ? "\\text{ Recons.}" : "";

  std::string canvas_dir = plot_dir + "global_adam_minplots_"
  + ratecap_string + "/";
  std::filesystem::create_directories(canvas_dir.c_str());

  std::vector<std::string> inclusive_hadronic_lines
  = {"Hlt1TrackMVA", "Hlt1TwoTrackMVA", "Hlt1TwoTrackKs"};

  int line_count{0};

  plot_inclusive_exclusive_rates_per_line(canvas_dir, tuples,
  results, global_minimiser, job_num, collision_rate, line_count,
  inclusive_hadronic_lines);

  plot_low_rate_line_rates(canvas_dir, tuples,
  results, global_minimiser, job_num, inclusive_hadronic_lines,
  line_count, collision_rate);

  total_effs_and_rootsafe_plots(canvas_dir, tuples, results,
  global_minimiser, job_num, ratecap_string, recon_string);

  plot_inclusive_line_efficiencies_per_sample(canvas_dir, tuples,
  results, global_minimiser, job_num, collision_rate, ratelimit,
  recon_equation);

  plot_inclusive_sample_efficiencies_per_line(canvas_dir, tuples,
  results, global_minimiser, job_num, collision_rate, ratelimit,
  recon_short, recon_equation);

  plot_exclusive_line_efficiencies_per_sample(canvas_dir, tuples,
  results, global_minimiser, job_num, recon_equation);

  plot_exclusive_sample_efficiencies_per_line(canvas_dir, tuples,
  results, global_minimiser, job_num, recon_short);

}
