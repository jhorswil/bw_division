#include "io_manager.h"
#include "init_discrete_gradient.h"
#include "misc_tools.h"
#include "line_tools.h"

// More io_manager functions

void io_manager::clear() {
  _name = "";
  _background_io = nullptr;
  _ntuple_path = "";
  _lines.clear(); 
  _axes.clear();
  _s_or_b = true;
  _parameter_values.clear();
}


void io_manager::print() {
  std::cout << "\n----------Tuple properties -----------\n";
  std::cout << "Name: " << _name << "\n";
  std::cout << "File safe name: " << _filesafe_name << "\n";
  std::cout << "Background pointer: " << _background_io << "\n";
  std::cout << "Path: " << _ntuple_path << "\n";
  std::cout << "Precut unique events: " << _unique_precut_events << "\n";
  std::cout << "Special: " << _special << "\n";
  if (_s_or_b) {
    std::cout << "Signal tuple.\n";
  } else {
    std::cout << "Zero-bias tuple.\n";
  }

  std::cout << "Line info:\n";
  for (unsigned i{0}; i < _lines.size(); i++) {
    _lines[i].print();
    std::cout << "\n";
  }
  std::cout << "Axes:\n";
  for (std::string axis: _axes) {
    std::cout << axis << ", ";
  }
  std::cout << "\n";
  for (unsigned i{0}; i < _activate_lines.size(); i++) {
    std::cout << "Activate " << _linestore._linenames[i] << ": "
      << _activate_lines[i] << "\n";
  }
  line_parameters params;
  for (auto& [k,v] : _parameters) {
    std::cout << "Activate " << k << ": "
    << "Minimum: " << v._min << " | Maximum: " << v._max << "\n";
  }

  std::cout << "\nFirst event:\n";
  _events[0].print();

  std::cout << "\n -------------- End of tuple properties ------------ \n";
  std::vector<event_store> _events;
}


void io_manager::calc_results(
    const int& num_results,
    const std::vector<std::vector<double>>& input_param_store,
    const double& collision_rate, const double& ratelimit
    ) {
  // Scan all FoM points in parameter space and save the minimum results.

  _maxeff_adam->_signal_efficiencies = std::vector<double>(num_results);
  _maxeff_adam->_signal_efficiency_errs = std::vector<double>(num_results);
  _maxeff_adam->_background_efficiencies = std::vector<double>(num_results);
  _maxeff_adam->_background_efficiency_errs = std::vector<double>(num_results);
  _maxeff_adam->_background_rates = std::vector<double>(num_results);
  _maxeff_adam->_background_rate_errs = std::vector<double>(num_results);
  _maxeff_adam->_ratelim_signal_efficiencies = std::vector<double>(num_results);
  _maxeff_adam->_ratelim_signal_efficiency_errs = std::vector<double>(
  num_results);
  _maxeff_adam->_all_foms = std::vector<double>(num_results);
  _maxeff_adam->_inputs_used = std::vector<line_parameters>(num_results);

  line_parameters paramph;
  std::cout << "Cutting on: " << _filesafe_name << " with variables:\n";
  for (int i{0}; i < num_results; i++) {
    for (unsigned j{0}; j < paramph.size(); j++) {
      if (_activate_params[j]) {
        std::cout << paramph._paramnames[j]
        << " :: " << input_param_store[i][j] << ", ";
      }
    }

    std::cout << "\n";
    // Calculation for each combination of parameters
    _maxeff_adam->_inputs_used[i] = input_param_store[i];
    std::vector<double> stats =
    _maxeff_adam->minimised_function(
    line_parameters(input_param_store[i]));

    _maxeff_adam->_all_foms[i] = stats[0];
    std::cout << "Resulting FoM: " << stats[0] << "\n";
    
    _maxeff_adam->_background_efficiencies[i] = stats[1];
    _maxeff_adam->_background_efficiency_errs[i] = stats[2];
    _maxeff_adam->_background_rates[i] = stats[3];
    _maxeff_adam->_background_rate_errs[i] = stats[4];
    _maxeff_adam->_signal_efficiencies[i] = stats[5];
    _maxeff_adam->_signal_efficiency_errs[i] = stats[6];
    _maxeff_adam->_ratelim_signal_efficiencies[i] = stats[7];
    _maxeff_adam->_ratelim_signal_efficiency_errs[i] = stats[8];

    // Speedup
    // if (_maxeff_adam->_all_foms.back() > 0.2 && num_results > 1e5) {
    //   i+=20;
    // }
  }

  if (_s_or_b) {
    std::cout << "\n\nOptimal statistics for " << _filesafe_name << ":\n";
    int optimal_index =
    std::distance(std::begin(_maxeff_adam->_all_foms),
    std::min_element(std::begin(_maxeff_adam->_all_foms),
    std::end(_maxeff_adam->_all_foms)));

    _optimal_fom = _maxeff_adam->_all_foms[optimal_index];
    std::cout << "Optimal figure of merit: " << _optimal_fom << "\n";
    _optimal_minbias_rate =
    _maxeff_adam->_background_rates[optimal_index];
    std::cout << "Optimal minbias rate: " << _optimal_minbias_rate
    << "\n";
    _optimal_signal_eff =
    _maxeff_adam->_signal_efficiencies[optimal_index];
    std::cout << "Optimal signal efficiency: " << _optimal_signal_eff
    << "\n";
    _optimal_ratelimited_signal_eff =
    _maxeff_adam->_ratelim_signal_efficiencies[optimal_index];
    std::cout << "Optimal ratelimited signal efficiency: "
    << _optimal_ratelimited_signal_eff << "\n";
    for (unsigned i{0}; i < paramph.size(); i++) {
      auto it = _parameters.find( paramph._paramnames[i] ); 
      if (it != _parameters.end()
      && !(_parameters[paramph._paramnames[i]]._fixed
      || _parameters[paramph._paramnames[i]]._off)) {
        auto& parameter = it->second; 
        _optimal_parameter_inputs.push_back(
        input_param_store[optimal_index][i]);
        std::cout << "Optimal parameter value for "
        << paramph[i] << ": "
        << _optimal_parameter_inputs.back() << "\n";

        bool outside_min = sign(_background_io->_parameter_values[i][1]
        - _background_io->_parameter_values[i][0]) >= 0.0
        ? _optimal_parameter_inputs.back() <= parameter._min
        : _optimal_parameter_inputs.back() >= parameter._min;

        bool outside_max = sign(_background_io->_parameter_values[i][1]
        - _background_io->_parameter_values[i][0]) >= 0.0
        ? _optimal_parameter_inputs.back() >= parameter._max 
        : _optimal_parameter_inputs.back() <= parameter._max; 

        if (outside_min) {
          std::cout << "Parameter has hit floor of range!\n";
        } else if (outside_max) {
          std::cout << "Parameter has hit ceiling of range!\n";
        }
      }
    }
  }   
}


bool io_manager::increase_active_params_calculate_and_process_rate(
  std::vector<std::vector<int>>& params_to_activate,
  std::vector<int>& step_ratio,
  int& linecount, io_manager* background, int l,
  int k, int j,
  const linemask_t& temp_activate_lines, double collision_rate,
  line_store& linestore, int& param_index,
  std::vector<std::vector<double>>& param_inputs,
  std::vector<std::vector<double>>& rates, double cutoff,
  line_parameters& params, double ratelimit,
  std::vector<double>& per_line_values, int primary_step_ratio
  ) {
  // Choose rate to increment parameters:
  // Let's say one parameter has 20 steps and another parameter
  // used in the same two-input line has 40 steps. This will increase
  // the first parameter only once for every two steps in the second
  // parameter.
  // This function also determines the rate at a each incremental
  // pair of thresholds and stops when this rate
  // goes underneath the cutoff * ratelimit.
  // Then it interpolates between the previous and current thresholds and
  // chooses the final cutoff value for the jth parameter.
  // k corresponds to the kth line being scanned, for the jth parameter,
  // and l corresponds to the lth (other) parameter active for the kth line
  // active for the jth parameter. This ensures the rate for all parameters
  // are scanned for all lines they are active within, whilst also scanning
  // the other parameters involved in those lines at the same time.
  // I am sorry for complicated.

  line_parameters selection;
  auto paramset = params_to_activate[linecount];

  for (unsigned m{0}; m < paramset.size(); m++) {
    auto p = paramset[m];
    auto parameter = background->_parameters[selection._paramnames[p]];
    if (parameter._fixed || parameter._off) {
      selection[p] = background->_parameters[selection._paramnames[p]]._value;
    } else if ((int)background->_parameter_values[p].size()
    > floor(l/step_ratio[m])) {
      selection[p] = background->_parameter_values[p][floor(l/step_ratio[m])];
    } else {
      selection[p] = background->_parameter_values[p].back();
    }
  }

  auto main_parameter =
  background->_parameters[selection._paramnames[j]];
  // std::cout << "Main parameter being scanned: "
  // << selection._paramnames[j] << "\n";
  if (main_parameter._fixed || main_parameter._off) { 
    return true;
  }
  double rate = 
  (background->calc_unique_events(selection,
  temp_activate_lines) / background->_unique_precut_events)
  * (collision_rate/1e6);

  int step_ratio_index = floor(l / primary_step_ratio);
  int previous_step_ratio_index = (l == 0) ? step_ratio_index :
  floor((l-1) / primary_step_ratio);

  // std::cout << "Size and iterator: \n";
  // std::cout << "param inputs: " << param_inputs[param_index].size()
  // << ", " << step_ratio_index << "\n";
  param_inputs[param_index][step_ratio_index] =
  selection[j];
  rates[param_index][step_ratio_index] = rate;                            

  std::cout << "Rate for " << linestore._linenames[k] << ", ";

  for (auto& p : paramset) {
    std::cout << selection._paramnames[p] << " = " << selection[p] << " | ";
  }

  std::cout << " = " << rate << " MHz\n";

  // Save optimal value for this parameter/line
  // combination
  bool stop_scanning{false};
  if (rate <= cutoff) {
    if (l == 0) {
      std::cout << "Loosest cut less than "
      "cutoff. Choosing loosest cut...\n";
      params[j] = selection[j];
      if (rate <= ratelimit) {
        _per_line_loosest[k] = true;
      }
    } else {
      // Interpolate last and current values to
      // get selection at cutoff MHz
      double param_difference =
      param_inputs[param_index][step_ratio_index] -
      param_inputs[param_index][previous_step_ratio_index];

      if (param_difference == 0) {
        params[j] = param_inputs[param_index][step_ratio_index];
      } else {
        int param_sign = sign(param_difference);

        params[j] = param_inputs[param_index][step_ratio_index]
        - param_sign * fabs((cutoff - rates[param_index][step_ratio_index])
        / (rates[param_index][step_ratio_index]
        - rates[param_index][previous_step_ratio_index]))
        * fabs(param_difference);
      }

      std::cout << "Current and previous rates (MHz): "
      << rates[param_index][step_ratio_index]
      << " | " << rates[param_index][previous_step_ratio_index]
      << "\n";

      std::cout << "Current and previous parameter values: "
      << param_inputs[param_index][step_ratio_index] << " | "
      << param_inputs[param_index][previous_step_ratio_index] << "\n";
      std::cout << "Interpolation: "
      << params[j] << "\n";
    }
    // Save optimal value for this individual line
    per_line_values[linecount] = params[j];

    stop_scanning = true;
  }
  return stop_scanning;
}


void io_manager::manage_parameter_increment_calculate_rates(
  linemask_t& deactivated_lines,
  linemask_t& activate_lines,
  std::vector<std::vector<int>>& params_to_activate,
  int& linecount, io_manager* background, int k, int j,
  double collision_rate,
  line_store& linestore, int& param_index,
  std::vector<std::vector<double>>& param_inputs,
  std::vector<std::vector<double>>& rates, double cutoff,
  line_parameters& params, double ratelimit,
  std::vector<double>& per_line_values
  ) {

  auto temp_activate_lines = deactivated_lines;
  if (activate_lines[k]) {
    // One line at a time
    temp_activate_lines[k] = activate_lines[k];

    int largest_step_count{0};
    std::vector<int> step_ratio(
    params_to_activate[linecount].size(), 0);
    io_manager* background = _s_or_b ? _background_io : this;

    for (unsigned l{0};
      l < params_to_activate[linecount].size(); l++) {
      int step_count =
      (int)background->
      _parameter_values[params_to_activate[linecount][l]].size();
      
      step_ratio[l] = step_count;
      if (largest_step_count <= step_count) {
        largest_step_count = step_count;
      }
    }

    int primary_step_ratio{1};
    for (unsigned l{0}; l < step_ratio.size(); l++) {
      auto parameter = background->_parameters[
      params._paramnames[params_to_activate[linecount][l]]];
      
      if (parameter._fixed || parameter._off) {
        step_ratio[l] = 1.0;
      } else {
        step_ratio[l] =
        ceil((double)largest_step_count/step_ratio[l]);
      }
      if (params_to_activate[linecount][l] == j) {
        primary_step_ratio = step_ratio[l];
      }
    }

    // For all possible values of the jth parameter
    for (int l{0}; l < largest_step_count; l++) {
      bool stop_scanning =
      increase_active_params_calculate_and_process_rate(
      params_to_activate, step_ratio, linecount,
      background, l, k, j, temp_activate_lines,
      collision_rate, linestore, param_index,
      param_inputs, rates, cutoff, params, ratelimit,
      per_line_values, primary_step_ratio);

      if (stop_scanning) break;
    }

    linecount++;
  }
}


void io_manager::activate_lines_calculate_rate_choose_parameter_value(
  int j, io_manager* background, line_parameters& params,
  std::vector<std::vector<double>>& rates, double cutoff,
  double ratelimit, double collision_rate,
  std::vector<std::vector<double>>& param_inputs,
  int& param_index, line_store& linestore,
  std::vector<double> param_differences
  ) {

  // If this tuple uses this parameter for tuning
  if (_activate_params[j]) {
    std::vector<double> per_line_values;
    std::vector<std::vector<int>> params_to_activate;
    std::vector<std::vector<std::string>> assoc_params_store;

    linemask_t activate_lines = {false};
    linemask_t deactivated_lines = {false};

    // Do one line at a time (this will be the case for some
    // samples)
    for (unsigned k{0}; k < _linestore._associated_params.size(); k++) {
      assoc_params_store.emplace_back(linestore._associated_params[k]);

      // For all params input into this line
      bool associated{false};
      for (std::string split_param: assoc_params_store.back()) {
        if(params._paramnames[j].compare(
        split_param) == 0) {
          associated = true;
          per_line_values.push_back(0);
          break;
        }
      }
      // All associated lines active
      activate_lines[k] = associated;
      // All lines deactivated
    }

    // For the case that there are multiple inputs to a line
    int linecount{0};
    for (unsigned k{0}; k < activate_lines.size(); k++) {
      if (activate_lines[k]) {
        params_to_activate.push_back({});

        for (std::string split_param: assoc_params_store[k]) {
          for (unsigned l{0}; l < params.size(); l++) {
            if (split_param.compare(params._paramnames[l])
              == 0) {
              params_to_activate[linecount].push_back((int)l);
            }
          }
        }
        linecount++;
      }
    }

    // If not empty, do several push backs (several params per line...)
    for (unsigned k{0}; k < background->_parameter_values[j].size(); k++) {
      param_inputs[param_index].push_back(0);
      rates[param_index].push_back(0);
    }

    // std::cout << "\nNumber of associated lines for "
    // << params._paramnames[j] << ": "
    // << per_line_values.size() << "\n";

    // For all active lines for this parameter
    linecount = 0;
    for (unsigned k{0}; k < activate_lines.size(); k++) {
      manage_parameter_increment_calculate_rates(
      deactivated_lines, activate_lines, params_to_activate,
      linecount, background, (int)k, j,
      collision_rate, linestore,
      param_index, param_inputs, rates, cutoff, params,
      ratelimit, per_line_values); 
    }

    // Find min or max value of parameter per line depending on
    // sign of parameter increment (should be positive but this is
    // idiot proof)
    int index{0};

    if (sign(param_differences[j]) > 0) {
      index = std::distance(per_line_values.begin(),
      std::min_element(per_line_values.begin(),
      per_line_values.end()));
    } else {
      index = std::distance(per_line_values.begin(),
      std::max_element(per_line_values.begin(),
      per_line_values.end()));
    }

    // Loosest selection out of all lines is chosen
    params[j] = per_line_values[index];
    // std::cout << "Final params value chosen after processing all "
    // "associated lines: " << params[j] << "\n";
    param_index++;
  }
}


void io_manager::remove_excess_candidates(
    const double& collision_rate, const double& ratelimit,
    const double& cutoff, const int& max_events,
    const bool& reconstructable_only
    ) {
  // Find selection for each line where rate is ~cutoff MHz.
  // Delete all candidates that don't pass this selection

  // std::cout << "\n\nRemoving excess candidates for " << _filesafe_name
  //   << "...\n\n";
  io_manager* background = _s_or_b ? _background_io : this; 
  if (background->_parameter_values.empty()) {
    background->fill_parameter_values();
  }

  std::vector<std::vector<double>> rates;
  std::vector<std::string> paramnames;
  std::vector<std::vector<double>> param_inputs;
  line_parameters params;
  line_store linestore;

  // Vectors for each param
  for (unsigned j{0}; j < _activate_params.size(); j++) {
    if (_activate_params[j]) {
      const std::string name = params._paramnames[j]; 
      // std::cout << "switching on parameters: " 
      //           << j << " " 
      //           << name << " " 
      //           << _parameters[name]._min << " "
      //           << _parameters[name]._max  << std::endl; 
      rates.push_back({});
      param_inputs.push_back({});
      paramnames.push_back(name);
      paramnames.back()[0] = toupper(paramnames.back()[0]); 
    }
  }

  if (!_s_or_b) {
    // For each parameter
    int param_index{0};
    std::vector<double> param_differences;
    // std::cout << params.size() << " "
    // << background->_parameter_values.size() << " "
    // << background->_parameter_values[0].size() << std::endl;  
    
    for (unsigned i{0}; i < params.size(); i++) {
      auto parameter = background->_parameters[params._paramnames[i]];
      if (parameter._fixed || parameter._off) {
        param_differences.push_back(0);
      } else {
        param_differences.push_back(background->_parameter_values[i][1]
        - background->_parameter_values[i][0]);
      }
    }
    
    _per_line_loosest = std::vector<bool>(_activate_lines.size(), false);
    for (unsigned j{0}; j < background->_parameter_values.size(); j++) {
      activate_lines_calculate_rate_choose_parameter_value(
      (int)j, background, params, rates, cutoff, ratelimit,
      collision_rate, param_inputs, param_index, linestore,
      param_differences);
    }

    for (unsigned i{0}; i < params.size(); i++) {
      if (_parameters[params._paramnames[i]]._fixed) {
        params[i] = _parameters[params._paramnames[i]]._value;
      }
    }
    std::cout << "Parameter values at cutoff:\n";
    params.print(_activate_params);
    std::cout << "\n";
    _cutoff_params = params;
  } else {
    params = background->_cutoff_params;
  }

  // std::cout << "\n\nPrinting fixed inputs for debug\n";
  // for (int i{0}; i < NUM_LINES; i++) {
  //   std::cout << i << "th line: " << _fixed_inputs[i] << "\n";
  // }  

  line_tuple linetuple =
  linestore.make_lines(_fixed_inputs, _partially_fixed_lines, _off_lines,
  _fixed_line_params);

  auto set_line_parameters =
  [&](auto& line){line.set_parameters(params);}; 

  tuple_process::for_each(linetuple, set_line_parameters); 
  get_element_by_type<twotrackks>(linetuple).prepare_twotrackks();
  
  _events.erase(std::remove_if( _events.begin(), _events.end(), 
  [&](auto& event){
  event.remove_excess(_activate_lines, linetuple);
  return event._number_of_candidates == 0 || 
  (reconstructable_only && ((int)event._num_tracks < _num_signal_tracks));}),
  _events.end());
}
