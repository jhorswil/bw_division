#include "photon_lines.h"
#include "line_tools.h"


bool diphoton_highmass::operator()(
  const float* monitored_quantities
  ) const {
    
  return _minET < monitored_quantities[0];
}

