#include "read_bwconfig.h"
#include "misc_tools.h"
#include "minimise_fom.h"
#include "init_discrete_gradient.h"
#include "init_save_and_collate.h"
#include "opt_discrete_gradient.h"
#include "opt_save_and_collate.h"
#include "line_manager.h"
#include "line_parameters.h"
#include "event_store.h"
#include "line_tools.h"
#include "read_jsons.h"
#include "io_manager.h"


config_map read_bwconfig_file(
  std::string config_filepath, bool print_inputs ) {
  // This function reads the config file into a map of string keys
  // assigned to string vectors that contain the relevant data in
  // string form. The conversion of these strings to the required
  // data types is performed by other task-specific functions.

  std::vector<std::string> read_lines;
  std::string line{};
  std::ifstream config_file(config_filepath);

  // Store non-commented lines in a vector
  while (getline(config_file, line)) {
    char first_char = line[0];
    if (first_char != '#') {
      read_lines.push_back(line);
    }
  }

  config_map input_map;
  std::vector<std::string> delimited_line, data_vector;
  std::string data_key;
  
  // Store lines in a map, sorted by keys generated from the first
  // element of the delimited line string
  for (unsigned i = 0; i < read_lines.size(); i++) {
    line = read_lines[i];
    boost::split(delimited_line, line, boost::is_any_of(","),
    boost::token_compress_on);

    data_key = delimited_line[0];
    data_vector = std::vector<std::string>(delimited_line.begin() + 1,
    delimited_line.end());
    
    input_map[data_key] = data_vector;

    delimited_line.clear();
    data_vector.clear();
  }

  if (print_inputs) {
    std::cout << "\nALL INPUT VARIABLES FROM CONFIG FILE:\n\n";
    for (config_map::const_iterator it = input_map.begin();
    it != input_map.end(); ++it) {
      data_key = it->first;
      data_vector = it->second;
      std::cout << data_key << ": \n\n";
      
      for (std::string data: data_vector) {
        std::cout << data << "\n";
      }

      std::cout << "\n";
    }
  }

  return input_map;
}


bool string_to_bool(
  config_map all_inputs, std::string map_key, int map_index
  ) {
  // Convert map string vector element to bool
  if (all_inputs.count(map_key) == 0) {
    std::cout << map_key << " not found" << std::endl;
  }
  return all_inputs[map_key][map_index] == "true" ? true : false;
}


int string_to_int(
  config_map all_inputs, std::string map_key, int map_index
  ) {
  // Convert map string vector element to int
  if (all_inputs.count(map_key) == 0) {
    std::cout << map_key << " not found" << std::endl;
  }
  
  return std::stoi(all_inputs[map_key][map_index]);
}


double string_to_double(
  config_map all_inputs, std::string map_key, int map_index) {
  // Convert map string vector element to double
  if (all_inputs.count(map_key) == 0) {
    std::cout << map_key << " not found" << std::endl;
  }

  return std::stod(all_inputs[map_key][map_index]);
}

template <typename generic_type>

generic_type string_vector_conversion(
  config_map all_inputs, std::string map_key, int data_type
  ) {
  // Convert map string vector to <generic_type> vector
  
  generic_type return_values;
  
  for (unsigned i = 0; i < all_inputs[map_key].size(); i++) {
    if (data_type == 0) {
      return_values.push_back(string_to_bool(all_inputs, map_key, i));
    } else if (data_type == 1) {
      return_values.push_back(string_to_int(all_inputs, map_key, i));
    } else {
      return_values.push_back(string_to_double(all_inputs, map_key, i));
    }
  }

  return return_values;
}

io_manager* io_manager_json_readin(
  std::string name, std::string event_type,
  io_manager* background_io, std::string input_path,
  const std::vector<line_manager>& line_managers,
  const fixedlines_t& fixed_inputs, const linemask_t& partially_fixed_lines,
  const linemask_t& fully_fixed_lines,
  const linemask_t& off_lines, const parammask_t& fixed_line_params,
  double collision_rate, double ratelimit, double cutoff,
  double scalewidth, int accuracy,
  int max_events, int num_signal_tracks, double weight,
  bool reconstructable_only
  ) {

  return new io_manager(name, event_type, background_io, input_path,
  line_managers, fixed_inputs, partially_fixed_lines, fully_fixed_lines,
  off_lines, fixed_line_params,
  weight, collision_rate, ratelimit, cutoff, scalewidth,
  accuracy, max_events, num_signal_tracks,
  event_type == "30000000" ? false : reconstructable_only);
}


std::vector<double> hyperparam_rescale(
  int alpha_input, int beta1_input, int beta2_input
  ) {
  return {alpha_input/10.0, beta1_input/100.0, beta2_input/1000.0};
}


void config_master(
  std::string config_filepath, int operation_option, int tuple_num,
  int job_num, int percentage_ratelimit, int alpha_input, int beta1_input,
  int beta2_input, int argc, char** argv 
  ) {
  // Read config file and call the relevant operations

  // Copy this to install and also the electron / muon line removals
  // Parallization command
  // Take is NOT thread safe, que palle ... 

  std::cout << "\nReading config file...\n";
  config_map all_inputs = read_bwconfig_file(config_filepath, false
  // hardcoded print_inputs
  );

  std::cout << "Input file read\n";
  line_parameters param_placeholder;
  
  std::string plot_dir = BWDIVROOT + std::string("/input_output/output_plots/");
  double plot_phi = string_to_double(all_inputs, "plot_phi", 0);
  double collision_rate = string_to_double(all_inputs, "collision_rate", 0);
  double ratelimit = string_to_double(all_inputs, "ratelimit", 0);
  bool plot_epoch = string_to_bool(all_inputs, "plot_epoch", 0);
  int niterations = string_to_int(all_inputs, "niterations", 0);

  // This shouldn't coincide with the line due to the line{int} key
  // contingency
  double alpha = string_to_double(all_inputs, "alpha", 0);
  double beta1 = string_to_double(all_inputs, "beta1", 0);
  double beta2 = string_to_double(all_inputs, "beta2", 0);
  double epsilon = string_to_double(all_inputs, "epsilon", 0);
  double scalewidth = string_to_double(all_inputs, "scalewidth", 0);

  if (tuple_num == job_num && tuple_num == -42) {
    operation_option = string_to_int(all_inputs, "operation_option", 0);
    tuple_num = string_to_int(all_inputs, "tuple_num", 0);
    job_num = string_to_int(all_inputs, "job_num", 0);
    percentage_ratelimit = string_to_int(all_inputs, "percentage_ratelimit", 0);
    
    alpha_input = alpha;
    beta1_input = beta1;
    beta2_input = beta2;

    // See std::cout below
    std::cout << "\n------------------------------------------\n"
    << "Config file " << config_filepath << " specified at command line. "
    "Reading operation option, tuple number, job number and percentage "
    "ratelimit from config file rather than command line\n";
    // << "Operation option, percentage ratelimit, tuple number, job number, "
    // << "alpha, beta1, beta2: "
    // << operation_option << " | " << percentage_ratelimit <<  " | "
    // << tuple_num << " | " << job_num <<  " | " << alpha_input
    // << " | " << beta1_input << " | " << beta2_input << "\n"
    // << "------------------------------------------\n";
  }


  std::string outfile_dir = BWDIVROOT + std::string("/input_output/output_files/");
  int nruns = string_to_int(all_inputs, "nruns", 0);
  bool plot_min_paths = true;
  int max_grid_searches = string_to_int(all_inputs, "max_grid_searches", 0);
  double cutoff = string_to_double(all_inputs, "cutoff", 0);
  
  std::string pram_one = all_inputs["pram_one"][0];
  std::string pram_two = all_inputs["pram_two"][0];
  int accuracy = string_to_int(all_inputs, "accuracy", 0);
  int max_events = string_to_int(all_inputs, "max_events", 0);
  bool reconstructable_only = string_to_bool(all_inputs,
  "reconstructable_only", 0);
  std::vector<double> manual_global_inputs =
  string_vector_conversion<std::vector<double>>(all_inputs,
  "manual_global_inputs", 2); 
  line_parameters selection(string_vector_conversion<std::vector<double>>(
  all_inputs, "selection", 2));
  std::string opt_options = all_inputs["opt_options"][0];
  std::string init_options = all_inputs["init_options"][0];
  bool random_start = string_to_bool(all_inputs, "random_start", 0);
  bool skip_grid_search = string_to_bool(all_inputs, "skip_grid_search", 0);
  
  std::vector<double> hyperparams =
  hyperparam_rescale(alpha_input, beta1_input, beta2_input);

  fixedlines_t fixed_inputs;
  linemask_t partially_fixed_lines;
  linemask_t fully_fixed_lines;
  linemask_t off_lines;
  parammask_t fixed_line_params;
  
  line_parameters params;
  for (unsigned i{0}; i < NUM_LINES; i++) {
    fixed_line_params[i] = std::vector<bool>(params._num_parameters, false);
  }
  
  for(int i{1}; i < argc; i++) {
    const std::string this_arg = argv[i];
    // std::cout << i << " " << this_arg << std::endl; 
    if (this_arg.find(".txt") != std::string::npos) continue;
    
    if (this_arg == "--rate") { 
      percentage_ratelimit = atoi(argv[i+1]);
      std::cout << "Setting rate limit from command line: "
      << percentage_ratelimit << std::endl; 
    }
    
    if (this_arg == "--output") {
      outfile_dir = argv[i+1]; 
      plot_dir = argv[i+1];
      std::cout << "Setting output file and plot directory from command "
      "line: " << outfile_dir << "\n";
      system(("mkdir -p " + std::string(plot_dir)).c_str()); 
    }
  }

  line_store linestore;
  auto lines = linestore.get_line_manager();  
  std::cout << "All lines:\n";
  for (auto& line: lines) {
    std::cout << line._name << std::endl;
  }
  std::cout << "\n";

  // Fixing and switching off lines if chosen in config, and parameters
  // preamble
  std::cout << "\nFixing lines if requested by user...\n";
  for (unsigned i{0}; i < linestore._linenames.size(); i++) {
    if (all_inputs.find(std::string(linestore._linenames[i] + "_fix"))
    != all_inputs.end()) {
      std::cout << linestore._linenames[i]
      << " is set to fixed in the config. Fixing ";

      // Fully fixed (all parameters fixed) means it is also partially fixed
      // (some parameters are fixed)
      fully_fixed_lines[i] = true;
      partially_fixed_lines[i] = true;
      std::vector<std::string> fixed_line_params =
      all_inputs[linestore._linenames[i] + "_fix"];
      
      fixed_inputs[i] =
      line_parameters(std::vector<double>(params._num_parameters, 0));

      for (std::string to_split: fixed_line_params) {
        std::vector<std::string> params_to_fix;
        boost::split(params_to_fix, to_split, boost::is_any_of(";"));
        fixed_inputs[i][global_parameter_index(params_to_fix[0])] =
        std::stod(params_to_fix[1]);
        fixed_line_params[i][global_parameter_index(params_to_fix[0])] =
        true;
        
        std::cout << params_to_fix[0] << ": " << params_to_fix[1]
        << " "; 
      }
      std::cout << "for " << linestore._linenames[i] << " only\n";
    } else {
      // std::cout << "Not fixed.\n";
      partially_fixed_lines[i] = false;
      fully_fixed_lines[i] = false;
      fixed_inputs[i] =
      line_parameters(std::vector<double>(params._num_parameters, 0));
    }

    if (all_inputs.find(std::string(linestore._linenames[i] + "_turnoff"))
    != all_inputs.end()) {
      std::cout << linestore._linenames[i]
      << " is turned off in the config. Switching off...\n";

      off_lines[i] = true;
      fully_fixed_lines[i] = true;
      partially_fixed_lines[i] = true;
      fixed_inputs[i] =
      line_parameters(std::vector<double>(params._num_parameters, 0));

      std::vector<std::string> fixed_line_params =
      all_inputs[linestore._linenames[i] + "_turnoff"];  
    } else {
      // std::cout << "Not fixed.\n";
      off_lines[i] = false;
    }
    // std::cout << "Resulting parameters: \n";
    // fixed_inputs[i].print();
  }
  std::cout << "\nDone fixing.\n";
  

  std::cout << "\nPrescaling lines and fixing parameters if requested by "
  "user...\n";
  for(auto& line: lines) {
    if (all_inputs.count(line._name + "_filter") != 0) {
      line._filter = all_inputs[line._name +"_filter"][0];
      std::cout << "Prescaling " << line._name << "...\n";
    }

    for(auto& [name, parameter] : line._parameters) {
      auto it = all_inputs.find(name);
      if (it == all_inputs.end()) {
        std::cout << "Error, requested parameter not found" << std::endl;
      }
      
      if (it->second.at(0) == "fixed") {
        std::cout << name
        << " is set to fixed in the config. Fixing ";
        parameter._value = stod(it->second.at(1));
        parameter._fixed = true;

        for (unsigned i{0}; i < linestore._linenames.size(); i++) {
          if (linestore._linenames[i].compare(line._name) == 0) {
            // std::cout << "Fixing line associated with fixed " << name
            // << ", index " << global_parameter_index(name) << "\n";
            partially_fixed_lines[i] = true;
            fixed_inputs[i][global_parameter_index(name)]
            = parameter._value;
            std::cout << ": " << parameter._value << " ";
            fixed_line_params[i][global_parameter_index(name)]
            = true;
          }
        }
        std::cout << "\n";

      } else {
        auto tokens = it->second; 
        parameter._value  = stod(tokens[0]); 
        parameter.set(stod(tokens[1]), stod(tokens[2]), stoi(tokens[3]),
        stoi(tokens[4])); 
      }
    }

  }
  std::cout << "\nDone Prescaling and fixing parameters.\n";

  std::string json_input_dir =
  BWDIVROOT + std::string("/input_output/input_files/samples/");
  auto sample_dir = all_inputs["sample_dir"];

  // Need collision rate before this
  std::cout << "Reading in samples:\n";
  cutoff = cutoff * ((1.0 * percentage_ratelimit) / 100.0);
  
  // This function prints the index of each tuple for single-sample
  // operations (choose corresponding tuple_num to choose sample)
  std::vector<io_manager*> tuples =
  read_jsons(json_input_dir, sample_dir, lines, collision_rate,
  ratelimit, cutoff, scalewidth, accuracy, operation_option,
  tuple_num, percentage_ratelimit, max_events, reconstructable_only,
  plot_dir, fixed_inputs, partially_fixed_lines, fully_fixed_lines,
  off_lines, fixed_line_params);

  // Generating actual collision rate and efficiency fraction of the
  // ratelimit, which can be reduced by user percentage_ratelimit input
  collision_rate = tuples[0]->_collision_rate;
  ratelimit = tuples[0]->_ratelimit
  * ((1.0*percentage_ratelimit)/100.0);

  std::cout << "\n\nCollision rate (MHz), ratelimit fraction calculated : "
  << collision_rate/1e6 << ", " << ratelimit << "\n\n";
  std::cout << "Ratelimit in MHz: " << (ratelimit * collision_rate)/1e6
  << "\n";

  std::vector<double> fixed_starting_point =
  string_vector_conversion<std::vector<double>>(
  all_inputs, "fixed_starting_point", 2);

  std::cout << "Beginning operations...\n";
  if (operation_option == 0) {
    // Adam approach with multithreaded individual minimisation
    apply_adam_minimiser_globally(tuples, collision_rate, ratelimit,
    niterations, alpha, beta1, beta2, epsilon, scalewidth, accuracy,
    plot_epoch, plot_dir, fixed_starting_point, outfile_dir, false,
    // collate_or_generate is false
    max_grid_searches, job_num, random_start, skip_grid_search);
  } else if (operation_option == 1) {
    // Genetic algorithm approach to global minimisation (serial)
    minimise_all_ntuples(tuples, plot_dir, collision_rate,
    ratelimit, init_options, opt_options); 
  } else if (operation_option == 2) {
    // Immediately run the second global minimisation step,
    // after collating the threshold results
    // from the parallelized individual minimisation root output files
    apply_adam_minimiser_globally(tuples, collision_rate, ratelimit,
    niterations, alpha, beta1, beta2, epsilon, scalewidth, accuracy,
    plot_epoch, plot_dir, fixed_starting_point, outfile_dir, true,
    max_grid_searches, job_num, random_start, skip_grid_search);
  } else if (operation_option == 3) {
    collate_global_min_results(outfile_dir, tuples);
  } else if (operation_option == 4) {
    // Run multiple individual minimisations for a sample associated with
    // tuple_num (print of tuple nums and associated samples are printed
    // at the beginning of running any operation option).
    // This can be parallelized with Condor by specifying job_num
    // (differentiates output filename with job number)
    multiple_minimisation_runs(
    tuples, collision_rate, ratelimit, niterations, alpha, beta1,
    beta2, epsilon, scalewidth, accuracy, plot_min_paths, plot_dir,
    outfile_dir, nruns, job_num, max_grid_searches, random_start,
    skip_grid_search);
  } else if (operation_option == 5) {
    // Collate individual minimisation results for all
    // active samples without running the global minimisation (for assessing
    // convergence rate)
    collate_all_individual_results(tuples, outfile_dir, false);
  } else if (operation_option == 6) {
    // Fixed starting point individual minimisation with fixed hyperparams
    apply_adam_minimiser_individually(tuples[0], collision_rate,
    ratelimit, niterations, alpha, beta1, beta2,
    epsilon, scalewidth, accuracy, plot_min_paths,
    plot_dir, fixed_starting_point, outfile_dir, job_num, max_grid_searches);
  } else if (operation_option == 7) {
    // Scan hyperparams from fixed starting point.
    // See hyperparam_rescale function above for int -> double conversion
     if (random_start || fixed_starting_point.size()
     != param_placeholder.size()) {
       std::cout << "Must allow for and enter correct number of "
       "fixed starting coordinates with this operation. Exiting...\n";
       exit(0);
     }
    apply_adam_minimiser_individually(tuples[0], collision_rate,
    ratelimit, niterations, hyperparams[0], hyperparams[1], hyperparams[2],
    epsilon, scalewidth, accuracy, false, // plot_min_paths
    plot_dir, fixed_starting_point, outfile_dir, job_num, max_grid_searches);
  } else if (operation_option == 8) {
    // Single tuning parameter scans for sample efficiency and minbias rate
    eff_and_rate_vs_individual_cut(tuples, plot_dir,
    collision_rate, ratelimit);
  } else if (operation_option == 9) {
    // collate_or_generate == true
    // Scan parameter space in two dimensions at a time for an
    // individual sample
    calc_and_plot_statistics_vs_cut(tuples, plot_dir, plot_phi,
    collision_rate, ratelimit, pram_one, pram_two);
  } else if (operation_option == 10) {
    // Calculate FoM, rate, sample efficiency, ratelimited signal
    // efficiency and event count for a given selection specicified in
    // bw_config.txt
     check_statistics_at_selection(selection, tuples, collision_rate,
     ratelimit);
     if (random_start || fixed_starting_point.size()
     != param_placeholder.size()) {
       std::cout << "Must allow for and enter correct number of "
       "fixed starting coordinates with this operation. Exiting...\n";
       exit(0);
     }
  } else if (operation_option == 11) {
    // Collate best individual minimisation results and manually input
    // chosen optimal global minimisation parameter thresholds to print
    // and plot results
    manual_param_entry(
    manual_global_inputs, tuples, collision_rate, ratelimit,
    scalewidth, accuracy, plot_epoch, plot_dir, outfile_dir,
    max_grid_searches, job_num);
  } else if (operation_option == 12) {
    double ga_time =
    minimise_all_ntuples(tuples, plot_dir, collision_rate,
    ratelimit, init_options, opt_options); 

    double adam_time =
    apply_adam_minimiser_globally(tuples, collision_rate, ratelimit,
    niterations, alpha, beta1, beta2, epsilon, scalewidth, accuracy,
    plot_epoch, plot_dir, fixed_starting_point, outfile_dir, false,
    // collate_or_generate is false
    max_grid_searches, job_num);
    
    // double ga_time =
    // apply_individual_minimiser(tuples[0], collision_rate, ratelimit,
    // init_options, plot_min_paths, plot_dir);

    // double adam_time =
    // multiple_minimisation_runs(
    // tuples, collision_rate, ratelimit, niterations, alpha, beta1,
    // beta2, epsilon, scalewidth, accuracy, plot_min_paths, plot_dir,
    // outfile_dir, nruns, job_num, max_grid_searches);
    std::cout << "Real time taken for Adam and GA: " << adam_time << ", "
    << ga_time << "\n";
    std::cout << "Ratio of runtime for GA vs. Adam: " << ga_time / adam_time
    << "x\n";
  }
  // Prints tuples:
  // operation_option == 13 exits after the tuple | tuple_num print
}
