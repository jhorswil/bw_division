#include "muon_lines.h"
#include "line_tools.h"

bool trackmuonmva::operator()(
  const float* monitored_quantities
  ) const {

  float pt = monitored_quantities[0];
  float ipchi2 = monitored_quantities[1];
  float muon_chi2 = monitored_quantities[2];
  float pt_shift = pt - _alpha; /// alpha_transform(_alpha); 

  return ((pt_shift > _max_pt && ipchi2 > _min_ipchi2) ||
  (pt_shift > _min_pt && pt_shift < _max_pt && (logf(ipchi2) >
  ((_param1/((pt_shift-_param2)*(pt_shift-_param2))) +
  (_param3/_max_pt)*(_max_pt - pt_shift) + logf(_min_ipchi2)))))
  && (muon_chi2 < _muon_chi2); 
}

bool singlehighptmuon::operator() (
  const float* monitored_quantities
  ) const {

  return _fixed_threshold < *monitored_quantities; 
}

bool dimuon_highmass::operator() (
  const float* monitored_quantities
  ) const { 

  return _minpt < monitored_quantities[0]
  && _muon_chi2 > monitored_quantities[1];
}

bool dimuon_displaced::operator() (
  const float* monitored_quantities
  ) const { 
  return _minpt < monitored_quantities[0]
    && _minipchi2 < monitored_quantities[1]
    && _muon_chi2 > monitored_quantities[2]; 
}

bool singlehighptmuon_nomuid::operator() (
  const float* monitored_quantities
  ) const {
  return _fixed_threshold < *monitored_quantities; 
}

bool dimuon_noip::operator() (const float* monitored_quantities) const {
  return true; 
}
