#include "opt_discrete_gradient.h"
#include "init_discrete_gradient.h"
#include "init_save_and_collate.h"
#include "misc_tools.h"
#include "opt_save_and_collate.h"


std::vector<double> opt_discrete_gradient::minimised_function(
  const line_parameters& input_parameters
  ) const {
  // Global figure of merit function to be minimised.

  double global_fom{0};

  // Calculate the background efficiency for all minbias lines
  double background_efficiency = 
  _tuples[0]->_background_io->calc_unique_events(
  input_parameters, _tuples[0]->_background_io->_activate_lines)
  / (1.0 * _tuples[0]->_background_io->_unique_precut_events);
  // Binomial errors
  double background_efficiency_err =
  sqrt(fabs(background_efficiency * (1.0 - background_efficiency))
  / (1.0 * _tuples[0]->_background_io->_unique_precut_events));

  double ratelimit_factor = ratelimiter(background_efficiency,
  _ratelimit);

  //  std::cout << "Min. Bias: " << background_efficiency
  // * _tuples[0]->_background_io->_unique_precut_events << " denom: " << 
  //    _tuples[0]->_background_io->_unique_precut_events << " "
  // << ratelimit_factor << std::endl; 

  double signal_efficiency{0.0};
  double signal_efficiency_err{0.0};
  double ratelim_signal_efficiency{0.0};
  double ratelim_signal_efficiency_err{0.0};
  // Adding to figure of merit from current-input efficiency and max
  // efficiency from all tuples
  for (io_manager* tuple: _tuples) {
    if (tuple->_special > 0.0) {
      // Calculated beforehand
      double initial_count = tuple->_unique_precut_events;
      double final_count =
      tuple->calc_unique_events(input_parameters,
      tuple->_activate_lines);

      signal_efficiency =
      (initial_count == 0) ? 0 : (final_count / initial_count);
      ratelim_signal_efficiency =
      signal_efficiency * ratelimit_factor;
  
      signal_efficiency_err = sqrt(fabs(signal_efficiency
      * (1.0 - signal_efficiency)) / initial_count);
      ratelim_signal_efficiency_err =
      sqrt(fabs(ratelim_signal_efficiency
      * (1.0 - ratelim_signal_efficiency)) / initial_count);
      double max_eff = tuple->_optimal_ratelimited_signal_eff;

      // Add to figure of merit
      double fomadd =
      tuple->_special * ((1.0 - (ratelim_signal_efficiency / max_eff))
      * (1.0 - (ratelim_signal_efficiency / max_eff)));
      global_fom += fomadd;
    }
  }
  // Adding gradient
  global_fom += 0.0001 * background_efficiency;
  // std::cout << "Global FoM for inputs ";
  // for (unsigned i{0}; i < input_parameters.size(); i++) {
  //   std::cout << input_parameters._paramnames[i]
  //   << ": " << input_parameters.access(i) << " ";
  // }
  // std::cout << " = " << global_fom << ":\n";

  return {global_fom, background_efficiency, background_efficiency_err,
  background_efficiency * _collision_rate,
  background_efficiency_err * _collision_rate, signal_efficiency,
  signal_efficiency_err, ratelim_signal_efficiency,
  ratelim_signal_efficiency_err, 0.0};
}


void opt_discrete_gradient::efficiency_per_channel(
  const line_parameters input_parameters
  ) {
  // Save efficiencies per channel for given selection

  _signal_efficiencies.clear();
  _signal_efficiency_errs.clear();

  for (io_manager* tuple: _tuples) {
    if (tuple->_special > 0.0) {
      double initial_count = tuple->_unique_precut_events;
      double signal_efficiency =
      (tuple->_unique_precut_events == 0) ? 0 :
      tuple->calc_unique_events(
      input_parameters, tuple->_activate_lines)
      / tuple->_unique_precut_events;

      _signal_efficiencies.push_back(signal_efficiency);
      _signal_efficiency_errs.push_back(sqrt(fabs(signal_efficiency
      * (1.0 - signal_efficiency)) / initial_count));
    }
  }
}


void process_save_and_plot_global_statistics(
  std::vector<io_manager*> tuples, line_parameters grid_search_result,
  double collision_rate, double ratelimit, std::string plot_dir,
  std::string outfile_dir, int job_num, bool plot_epoch,
  adam_base* opt_minimiser
  ) {
 
  tuples[0]->_background_io->_optimal_candidate_counts =
  tuples[0]->_background_io->calc_candidates_per_line(grid_search_result,
  tuples[0]->_background_io->_activate_lines);
  
  tuples[0]->_background_io->_optimal_unique_events_per_line =
  tuples[0]->_background_io->events_unique_to_lines(grid_search_result,
  tuples[0]->_background_io->_activate_lines);

  std::cout << "\n\n";
  line_store linestore;
  for (unsigned i{0}; i < NUM_LINES; i++) {
    std::cout << "Optimal minbias candidates passing "
    << linestore._linenames[i] << " out of total "
    "stored (after sample pre-selection): "
    << tuples[0]->_background_io->_optimal_candidate_counts[i]
    // << " / " << tuples[0]->_background_io->_stored_candidates_per_line[i]
    << "\n";
  }

  std::cout << "\n\nTOTAL MINBIAS EVENTS = "
  << tuples[0]->_background_io->_unique_precut_events << "\n\n";

  init_discrete_gradient* minbias_specline_minimiser =
  new init_discrete_gradient(tuples[0], collision_rate, ratelimit,
  0.25, 2); // Scalewidth and accuracy don't matter here
  for (unsigned i{0}; i < NUM_LINES; ++i) {
    std::vector<double> specstats =
    minbias_specline_minimiser->speceff_minimised_function(
    grid_search_result, i); 
    minbias_specline_minimiser->_background_efficiencies.push_back(
    specstats[1]);
    minbias_specline_minimiser->_background_efficiency_errs.push_back(
    specstats[2]);

    
    std::cout << "Optimal number of events passing "
    << linestore._linenames[i]
    << " out of total for minbias: "
    << specstats[1] * tuples[0]->_background_io->_unique_precut_events
    << " / "
    << tuples[0]->_background_io->_logfile_events_per_line[i] << "\n";
  }
  
  for (unsigned i{0}; i < NUM_LINES; ++i) {
    std::cout << "Optimal unique events passing "
    << linestore._linenames[i] << ": "
    << tuples[0]->_background_io->_optimal_unique_events_per_line[i]
    << "\n";
  }


  tuples[0]->_background_io->_specline_adam = minbias_specline_minimiser;

  if (plot_epoch) {
    opt_minimiser->plot_epoch(plot_dir);
  }

  // Saving final efficiencies
  opt_discrete_gradient* opt_discrete_minimiser =
  dynamic_cast<opt_discrete_gradient*>(opt_minimiser);
  opt_discrete_minimiser->efficiency_per_channel(grid_search_result);
  std::cout << "\n\n";

  save_or_edit_global_min_results(tuples, grid_search_result,
  opt_discrete_minimiser, outfile_dir, job_num);

  plot_global_adam_statistics(plot_dir, tuples, collision_rate,
  ratelimit, grid_search_result, opt_discrete_minimiser, job_num);
}


void print_allen_python_output(
  const std::string& filename, const line_parameters& grid_search_results
  ) {
  // Printing thresholds in AllenConf threshold python file format
   
  std::map<std::string, std::string> variable_map =
  {{"alpha_hadron", "TrackMVA_alpha"},
  {"track_ghostprob", "TrackMVA_maxGhostProb"},
  {"alpha_electron", "TrackElectronMVA_alpha"},
  {"alpha_muon", "TrackMuonMVA_alpha"},
  {"charm_track_ip", "D2HH_track_ip"},
  {"charm_track_pt", "D2HH_track_pt"},
  {"single_highpt", "SingleHighPtLepton_pt"},
  {"twotrackmva", "TwoTrackMVA_minMVA"},
  {"twotrack_ghostprob", "TwoTrackMVA_maxGhostProb"},
  {"highmass_dimuon_pt", "DiMuonHighMass_pt"},
  {"displaced_dimuon_pt", "DiMuonDisplaced_pt"},
  {"displaced_dimuon_ipchi2", "DiMuonDisplaced_ipchi2"},
  {"displaced_dielectron_pt", "DiElectronDisplaced_pt"},
  {"displaced_dielectron_ipchi2", "DiElectronDisplaced_ipchi2"},
  {"highmass_diphoton_et", "DiPhotonHighMass_minET"},
  {"lambda_detached_track_mipchi2", "LambdaLLDetachedTrack_track_mipchi2"},
  {"lambda_detached_combination_bpvfd",
  "LambdaLLDetachedTrack_combination_bpvfd"},
  {"xi_lll_ipchi2","XiOmegaLLL_track_ipchi2"}};

  for (std::string param: grid_search_results._paramnames) {
    if (variable_map.count(param) == 0) {
      // std::cout << "Variable " << param << " was not found in param "
      // "dictionary. Adding one to one assignment...\n";
      variable_map[param] = param;
    }
  }

  std::ofstream os(filename +".py");
  
  os << "from AllenConf.thresholds.thresholds import Thresholds\n\n"
  << "threshold_settings = Thresholds(\n"
  << "  TrackMuonMVA_maxCorrChi2=1.8,\n"
  << "  DiMuonDisplaced_maxCorrChi2=1.8,\n"
  << "  DiMuonHighMass_maxCorrChi2=1.8,\n"
  << "  D2HH_ctIPScale=1.,\n"
  << "  SingleHighPtLepton_pt=12500,\n"
  << "  SingleHighPtLepton_pt_noMuonID=12500,\n";
  // << "  TrackMVA_maxGhostProb=0.8,\n"
  // << "  TwoTrackMVA_maxGhostProb=0.8,\n";
  for (int i{0}; i < grid_search_results._num_parameters; i++) {
    std::string end_line = (i == grid_search_results._num_parameters - 1)
    ? ")\n" : ",\n";

    if (grid_search_results._paramnames[i] == "twotrack_ks") {
      twotrackks().print_allenconf_thresholds(os, grid_search_results);
    } else if (grid_search_results._paramnames[i].compare(
    "single_highpt") == 0) {
      os << "  " << variable_map["single_highpt"] << "="
      << grid_search_results.access(i) << ",\n"
      << "  " << variable_map["single_highpt"] << "_noMuonID="
      << grid_search_results.access(i);
    } else {
      double param_val{grid_search_results.access(i)};

      // if (grid_search_results._paramnames[i].find("alpha")
      // != std::string::npos){
      //   param_val = alpha_transform(param_val);
      // }
      os << "  "
      << variable_map[grid_search_results._paramnames[i]]
      << "=" << param_val;
    }
    os << end_line;
  }

  os << "\n\n";
  os.close(); 
  std::ofstream os2(filename + "_spreadsheet");

  os2 << "Threshold print with & delimiter for spreadsheet (use data "
  "tab-> split by line and enter & as delimiter)\n"
  << "TrackMuonMVA_maxCorrChi2&1.8\n"
  << "DiMuonDisplaced_maxCorrChi2&1.8\n"
  << "DiMuonHighMass_maxCorrChi2&1.8\n"
  << "D2HH_ctIPScale&1.\n"
  << "SingleHighPtLepton_pt&12500\n"
  << "SingleHighPtLepton_pt_noMuonID&12500\n";
  // << "TrackMVA_maxGhostProb&0.8\n"
  // << "TwoTrackMVA_maxGhostProb&0.8\n";
  for (int i{0}; i < grid_search_results._num_parameters; i++) {
    std::string end_line = (i == grid_search_results._num_parameters - 1)
    ? ")\n" : "\n";

    if (grid_search_results._paramnames[i] == "twotrack_ks") {
      // Print with & instead of =, and no comma
      twotrackks().print_allenconf_thresholds(os2, grid_search_results, true); 
    } else if (grid_search_results._paramnames[i].compare(
    "single_highpt") == 0) {
      os2 << variable_map["single_highpt"] << "&"
      << grid_search_results.access(i) << "\n"
      << variable_map["single_highpt"] << "_noMuonID&"
      << grid_search_results.access(i);
    } else {
      double param_val{grid_search_results.access(i)};

      // if (grid_search_results._paramnames[i].find("alpha") != std::string::npos){
      //   param_val = alpha_transform(param_val);
      // }
      os2 << variable_map[grid_search_results._paramnames[i]]
      << "&" << param_val;
    }
    os2 << end_line;
  }
  os2.close(); 
}


double apply_adam_minimiser_globally(
  std::vector<io_manager*> tuples, double collision_rate, double ratelimit,
  int niterations, double alpha, double beta1, double beta2,
  double epsilon, double scalewidth, int accuracy, bool plot_epoch,
  std::string plot_dir,
  std::vector<double> fixed_starting_coordinates, std::string outfile_dir,
  bool collate_or_generate, int max_grid_searches, int job_num,
  bool random_start, bool skip_grid_search
  ) {
  // Minimise the global figure of merit across all channels and lines
  
  TStopwatch t;
  t.Start();
  if (collate_or_generate) {
    std::cout << "Collating individual minimisation results...\n";
    collate_all_individual_results(tuples, outfile_dir);
  } else {
    std::cout << "\n\n ------ Beginning Individual Minimisation ------ "
    "\n\n";
    minimise_all_individual_ntuples(tuples, collision_rate, ratelimit,
    niterations, alpha, beta1, beta2, epsilon, scalewidth, accuracy,
    true, // Plot mps
    plot_dir, fixed_starting_coordinates, outfile_dir, max_grid_searches,
    random_start, skip_grid_search);
  }
  
  for (io_manager* tuple: tuples) {
    if (tuple->_optimal_signal_eff <= 0.0) {
      std::cout << "Max Efficiency found for " << tuple->_event_type
      << " was zero. Either minimisation failed or all representing lines are"
      " fixed/switched off.\n";
      tuple->_optimal_signal_eff = 1e-10;
      tuple->_optimal_fom = std::pow((1 - 1e-10), 2);

      // if (max_grid_searches != -1) {
      //   std::cout << "Exiting...\n";
      //   exit(0);
      // }
    }
  }

  adam_base* opt_minimiser =
  new opt_discrete_gradient(tuples, collision_rate, ratelimit, scalewidth,
  accuracy);

  std::cout << "\n\n -------- Beginning Global Minimisation -------- \n\n";
  int grid_search_count{max_grid_searches + 1};
  double optimal_fom;
  line_parameters grid_search_result;
  grid_recursive_minimisation(tuples, collision_rate, ratelimit,
  niterations, alpha, beta1, beta2, epsilon, scalewidth, accuracy,
  fixed_starting_coordinates, optimal_fom, *opt_minimiser, grid_search_count,
  max_grid_searches, grid_search_result, true, // include prints
  random_start, skip_grid_search);

  std::vector<double> opt_stats =
  opt_minimiser->minimised_function(grid_search_result);

  t.Stop();
  std::cout << "\n\nTime taken for full global optimisation: \n";
  t.Print();
  std::cout << "\n\n--- REMINDER ---\n";
  std::cout << "Some parameters may be turned off in the config file, "
  "and appear in the final set of thresholds as their "
  "tightest config value (but really, no events are allowed to pass so they "
  "might need to be tightened even further in the AllenConf file).";
  int off_count{0};
  for (auto [key,param]: tuples[0]->_parameters) {
    if (param._off) {
      if (off_count != 0) {
        std::cout << " Fixed params:\n";
      }
      off_count++;
      std::cout << key << " ";
    }
  }
  std::cout << "\n";

  opt_minimiser->_opt_fom = opt_stats[0];
  opt_minimiser->_opt_backeff = opt_stats[1];
  opt_minimiser->_opt_backeff_err = opt_stats[2];
  opt_minimiser->_opt_rate = opt_stats[3];
  std::cout << "Optimal Global FoM: " << opt_minimiser->_opt_fom << "\n";
  std::cout << "Optimal minbias efficiency: " << opt_minimiser->_opt_backeff
  << " +/- " << opt_minimiser->_opt_backeff_err << "\n";
  std::cout << "Optimal minbias rate: " << opt_minimiser->_opt_rate/1e6
  << " +/- " << opt_stats[4]/1e6 << " MHz\n";

  for (unsigned i{0}; i < grid_search_result.size(); i++) { 
    std::string unit = grid_search_result._paramnames[i].find("alpha") !=
    std::string::npos || grid_search_result._paramnames[i].find("_pt")
    != std::string::npos ? " MeV" : "";
    unit = (grid_search_result._paramnames[i].find("_ip") !=
    std::string::npos && grid_search_result._paramnames[i].find("chi2")
    == std::string::npos) || grid_search_result._paramnames[i].find("bpvfd")
    != std::string::npos ? " mm" : unit;

    std::cout << "Optimal value for parameter "
    << grid_search_result._paramnames[i] << ": "
    << grid_search_result[i] << unit << "\n";

    
    if (grid_search_result._paramnames[i] == "twotrack_ks") {
    twotrackks().print_thresholds(grid_search_result); 
    std::cout << "Untransformed twotrack_ks: "
    << twotrack_ks_transform(grid_search_result[i]) << "\n";
    }

    // else if (grid_search_result._paramnames[i].find("alpha") != std::string::npos) {
    // std::cout
    // std::cout << "Untransformed "
    //   << grid_search_result._paramnames[i] << ": "
    //   << alpha_transform(grid_search_result[i]) << " MeV\n";
    // }
  }

  print_allen_python_output(outfile_dir + "/thresholds_"
  + std::to_string(int(round(0.001*ratelimit*collision_rate))),
  grid_search_result);

  process_save_and_plot_global_statistics(tuples, grid_search_result,
  collision_rate, ratelimit, plot_dir, outfile_dir, job_num, plot_epoch,
  opt_minimiser);

  return t.RealTime();
}
