#include "calibration_lines.h"
#include "line_tools.h"

bool d2kpi_line::operator()(
  const float* monitored_quantities
  ) const {

  float track_pt =
  monitored_quantities[0];
  float track_ip =
  monitored_quantities[1];
  float d0_ct =
  monitored_quantities[2];
  
  return track_pt > _charm_track_pt && track_ip > _charm_track_ip
  && d0_ct > _charm_track_ip; 
}


