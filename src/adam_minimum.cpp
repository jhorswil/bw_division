#include "adam_base.h"
#include "io_manager.h"
#include "opt_discrete_gradient.h"
#include "init_discrete_gradient.h"

void adam_base::plot_epoch(std::string plot_dir, int job_num) {
  // Plot FoM vs. iterations for a given set of hyperparameters

  std::string tuplename = _tuples[0]->_name;
  std::string filesafe_name = _tuples[0]->_filesafe_name;
  std::vector<std::string> cut_variables;
  std::string plot_name, logplot_name, plot_title, rateplot_name,
  rateplot_title;

  double ratecap = (_tuples[0]->_percentage_ratelimit / 100.0)
  * ((_tuples[0]->_collision_rate * _tuples[0]->_ratelimit) / 1e6);
  
  std::stringstream ratecap_ss;
  ratecap_ss << std::fixed << std::setprecision(3) << ratecap;
  std::string ratecap_string = ratecap_ss.str() + "_mhz";
  boost::replace_all(ratecap_string, ".", "point");

  if (_glob_or_indiv) {
    cut_variables = _tuples[0]->_background_io->_axes;
    plot_name = "global_fom_epoch_plot";
    logplot_name = "global_logfom_epoch_plot";
    plot_title = "Discrete Global Figure of Merit Epoch";
    rateplot_name = "minbias_rate_epoch_plot";
    rateplot_title = "Minbias Rate Epoch";
  } else {
    cut_variables = _tuples[0]->_axes;
    plot_name = filesafe_name + "_fom_epoch_plot_job"
    + std::to_string(job_num);
    logplot_name = filesafe_name + "_logfom_epoch_plot"
    + std::to_string(job_num);
    rateplot_name = filesafe_name + "_minbias_rate_epoch_plot"
    + std::to_string(job_num);
    plot_title = "Discrete Figure of Merit Epoch for "
    + filesafe_name;
    rateplot_title = "Minbias Rate Epoch for " + filesafe_name;
  }

  for (std::string var: cut_variables) {
    var[0] = toupper(var[0]);
  }

  std::string tuple_term = _glob_or_indiv ? "" : filesafe_name + "_";
  std::string canvas_dir = plot_dir + tuple_term + ratecap_string
  + "_epoch_plots/";
  std::filesystem::create_directories(canvas_dir.c_str());

  if (_glob_or_indiv || _include_prints) {
    std::cout << "Iterations required: " << _iterations_required << "\n";
    std::cout << "Number of epochs: " << _nearest_grid_foms.size()
    << "\n";
  }

  std::vector<double> nearest_logfoms;
  std::vector<double> mhz_rate;
  std::vector<double> iterations;
  for (unsigned i{0}; i < _nearest_grid_foms.size(); i++) {
    iterations.push_back((double)i);
    nearest_logfoms.push_back(log(_nearest_grid_foms[i]));
    mhz_rate.push_back(_averaged_rates[i]/1e6);
  }

  TGraph* epoch_plot = new TGraph((int)iterations.size(),
  &iterations[0], &_nearest_grid_foms[0]);

  epoch_plot->SetTitle(
  (plot_title + ";Number of Iterations;Discrete FoM").c_str());
  TGraph* epoch_logplot = new TGraph((int)iterations.size(),
  &iterations[0], &nearest_logfoms[0]);
  epoch_logplot->SetTitle(
  (plot_title + ";Number of Iterations;Log(Discrete FoM)").c_str());
  TGraph* rate_epoch_plot = new TGraph((int)iterations.size(),
  &iterations[0], &mhz_rate[0]);
  rate_epoch_plot->SetTitle(
  (rateplot_title + ";Number of Iterations;Rate (MHz)").c_str());

  TCanvas* epoch_canvas = new TCanvas("epoch_canvas",
  "Discrete FoM vs. Iterations Epoch", 0, 0, 1000, 600);
  epoch_canvas->cd();
  epoch_plot->Draw("AC*");
  epoch_canvas->SaveAs((canvas_dir + plot_name + ".tex").c_str());
  epoch_canvas->SaveAs((canvas_dir + plot_name + ".C").c_str());
  epoch_canvas->SaveAs((canvas_dir + plot_name + ".svg").c_str());


  TCanvas* epoch_logcanvas = new TCanvas("epoch_canvas",
  "Log of Discrete FoM vs. Iterations Epoch", 0, 0, 1000, 600);
  epoch_logcanvas->cd();
  epoch_logplot->Draw("AC*");
  epoch_logcanvas->SaveAs((canvas_dir + logplot_name + ".tex").c_str());
  epoch_logcanvas->SaveAs((canvas_dir + logplot_name + ".C").c_str());
  epoch_logcanvas->SaveAs((canvas_dir + logplot_name + ".svg").c_str());

  TCanvas* rate_epoch_canvas = new TCanvas("epoch_canvas",
  "Rate vs. Iterations Epoch", 0, 0, 1000, 600);
  rate_epoch_canvas->cd();
  rate_epoch_plot->Draw("AC*");
  rate_epoch_canvas->SaveAs((canvas_dir + rateplot_name + ".tex").c_str());
  rate_epoch_canvas->SaveAs((canvas_dir + rateplot_name + ".C").c_str());
  rate_epoch_canvas->SaveAs((canvas_dir + rateplot_name + ".svg").c_str());
}



void adam_base::adam_minimum_loop(
  line_parameters& previous_coords, line_parameters& current_coords,
  bool warm_reset, int i, std::vector<double>& first_moment,
  std::vector<double>& second_moment, double alpha, double beta1,
  double beta2, double epsilon, int& start_check, int min_iterations,
  int niterations, int check_depth, std::vector<bool> active_params,
  bool& break_or_not
  ) {


  std::vector<double> gradient_vector;
  if (warm_reset && i == 0) {
    smart_kick(current_coords, first_moment, second_moment,
    alpha, beta1, beta2, epsilon);
    for (unsigned i{0}; i < first_moment.size(); i++) {
      gradient_vector.push_back(1);
    }
    start_check = _averaged_foms.size();
  } else {
    gradient_vector =
    accurate_gradient_vector(current_coords);
  }
  
  int gradient_index{0};
  if (_glob_or_indiv || _include_prints) {
    std::cout << "\n\nIteration: " << i << "\n";
    std::cout << std::setw(3) << "#" << " " << std::left << std::setw(35)
    << "Parameter" << " " << std::setw(15) << "Threshold" << " "
    << std::setw(15) << "Gradient" << " " << std::setw(15) << "First Moment\n"
    << std::setw(83) <<
    " -----------------------------------------------------------------------"
    "----------- " << std::endl;

    for( unsigned i = 0 ; i != current_coords.size(); ++i) {
      if (active_params.at(i)) {
        if (_parameters[i]._fixed) {
          std::cout << std::setw(3) << i << " " << std::left
          << std::setw(35) << current_coords._paramnames.at(i)  << " " 
          << std::setw(15) << " fixed " << " " 
          << std::setw(15) << " fixed " << " " 
          << std::setw(15) << " fixed " << std::endl;
        } else {
          std::cout << std::setw(3) << i << " " << std::left
          << std::setw(35) << current_coords._paramnames.at(i)  << " " 
          << std::setw(15) << current_coords[i] << " " 
          << std::setw(15) << (active_params.at(i)
          ? std::to_string(gradient_vector.at(gradient_index)) : "0") << " " 
          << std::setw(15) << (active_params.at(i)
          ? std::to_string(first_moment.at(gradient_index)) : "0") << std::endl;
        }
        gradient_index++;
      }
    }
  }
  
  // Iterate through input coordinates (covering all minbias parameters)
  // but only calc gradient and change coordinates of active params
  int index_count{0};
  for (unsigned j{0}; j < current_coords.size(); j++) {
    const auto& config = _parameters[j]; 
    if ((_glob_or_indiv || _tuples[0]->_activate_params[j])) {
      if (!(gradient_vector[index_count] == 0) && !_parameters[j]._fixed) {
        if (!warm_reset || i > 0) {
          first_moment[index_count] =
          beta1 * first_moment[index_count] + ((1.0 - beta1)
          * gradient_vector[index_count]);
          second_moment[index_count] =
          (beta2 * second_moment[index_count]) + ((1.0 - beta2)
          * std::pow(gradient_vector[index_count], 2.0));
          // Give a little push to begin with
          if (i == 0) {
            first_moment[index_count] *= 8.0;
            second_moment[index_count] *= 8.0;
          }
        }
        double first_bias_correction = first_moment[index_count]
        / (1.0 - std::pow(beta1, 1.0*(i+1)));
        double second_bias_correction = second_moment[index_count]
        / (1.0 - std::pow(beta2, 1.0*(i+1)));
        double increase =
        ((_parameters[j].resolution() * alpha
        * first_bias_correction)/(sqrt(second_bias_correction)
        + epsilon));
        current_coords[j] = current_coords[j] - increase;
      } 
      
      bool outside_min =
      sign(config._max - config._min) >= 0.0
      ? current_coords.access(j) < config._min
      : current_coords.access(j) > config._min;
        
      bool outside_max =
      sign(config._max - config._min) >= 0.0
      ? current_coords.access(j) > config._max
      : current_coords.access(j) < config._max;
      
      if ((outside_min || outside_max) && !_parameters[j]._fixed) {
        current_coords[j] = outside_min ? config._min : config._max; 
        first_moment[index_count] *= -1.0;
      }

      index_count++;
    } else {
      current_coords[j] = 0;
    }
  }

  previous_coords = current_coords;
  double fom_difference_sum{1.0};
  if (i > 10) {
    fom_difference_sum = 0.0;
    for (int j{0}; j < 5; j++) {
      fom_difference_sum +=
      fabs(_averaged_foms[i-j] - _averaged_foms[i-j-1]);
    }
  }
  if (i > min_iterations || i == niterations - 1
  || fabs(fom_difference_sum) <= 1e-15) {
    int smallest_recent_fom_index =
    std::distance(std::begin(_averaged_foms) + start_check,
    std::min_element(std::begin(_averaged_foms) + start_check,
    std::end(_averaged_foms)));

    if (_glob_or_indiv || _include_prints) {
      std::cout << "Iterations since smallest value (out of "
      << check_depth << " max): "
      << i - smallest_recent_fom_index << "\n";
      std::cout << "Smallest value so far: "
      << _averaged_foms[smallest_recent_fom_index + start_check]
      << "\n";
      std::cout << "Iteration of smallest value: "
      << smallest_recent_fom_index << "\n";
    }

    if ((i - smallest_recent_fom_index) >= check_depth
    || i == niterations - 1) {
      if (_glob_or_indiv || _include_prints) {
        std::cout << "Stopping condition or final iteration "
        "reached.\n";
      }
      _iterations_required = i + 1;
      // Setting final continuous coords to that corresponding
      // to smallest FoM result 
      current_coords =
      _minpath_coords[smallest_recent_fom_index + start_check];
      break_or_not = true;
    }
  }
}


line_parameters adam_base::adam_minimum(
  const int niterations, const double alpha, const double beta1,
  const double beta2, const double epsilon,
  const std::vector<double> fixed_starting_point, const bool warm_reset,
  const bool random_coords
  ) {
  // Find minimum in parameter space using Adam learning rate


  TStopwatch t;
  t.Start();
  for (unsigned i{0}; i < _parameters.size(); i++) {
    const auto& config = _parameters[i]; 
    bool outside_max =
    sign(config._max - config._min) >= 0.0
    ? config._value >= config._max 
    : config._value <= config._max ;

    bool outside_min =
    sign(config._max - config._min) >= 0.0
    ? config._value <= config._min
    : config._value >= config._min;

    if (outside_max) {
      // std::cout << "Value chosen for " << initial_coords._paramnames[i]
      // << " is too tight. Choosing max bound...\n";
      _parameters[i]._value = config._max;
    } else if (outside_min) {
      // std::cout << "Value chosen for " << initial_coords._paramnames[i]
      // << " is too loose. Choosing min bound...\n";
      _parameters[i]._value = config._min;
    }
  }

  // Choosing initial coordinates
  line_parameters initial_coords;
  for( unsigned i{0}; i != _parameters.size(); ++i ){
    initial_coords[i] = _parameters[i]._value;
  }

  if (!fixed_starting_point.empty()) {
    initial_coords = line_parameters(fixed_starting_point);
  } else if (random_coords) {
    if (_glob_or_indiv || _include_prints) {
      std::cout << "Choosing random coordinate...\n\n";
    }
    initial_coords = random_coord_generator();
  }

  _minpath_coords.push_back(initial_coords);
  _averaged_foms.push_back(minimised_function(initial_coords)[0]);
  std::vector<double> first_moment, second_moment;
  for (unsigned i{0}; i < initial_coords.size(); i++) {
    // If global optimisation, or individual and param is active, add to
    // moments
    if (_glob_or_indiv || (_tuples[0]->_activate_params[i])) {
      first_moment.push_back(0);
      second_moment.push_back(0);
      if (_glob_or_indiv || _include_prints) {
        std::cout << "Activating " << initial_coords._paramnames[i]
        << "\n";
      }
    }
  }

  std::vector<bool> active_params = _glob_or_indiv ?
  _tuples[0]->_background_io->_activate_params
  : _tuples[0]->_activate_params;
  line_parameters current_coords = initial_coords;
  line_parameters previous_coords = current_coords;
  
  int min_iterations = 80;
  int check_depth = 20;
  int start_check{0};
  
  bool break_or_not = false;
  for (int i{0}; i < niterations; i++) {
    // std::cout << i << " " << current_coords << std::endl; 
    adam_minimum_loop(previous_coords, current_coords, warm_reset, i,
    first_moment, second_moment, alpha, beta1, beta2,
    epsilon, start_check, min_iterations, niterations,
    check_depth, active_params, break_or_not);
    if (break_or_not) break;
  }

  if (_iterations_required == 0) {
    _iterations_required = niterations;
  }
  t.Stop();
  if (_glob_or_indiv) {
    std::cout << "\nTime taken for all sample FoMs to be minimised "
    "globally by Adam:\n";
    t.Print();
    std::cout << "\n";
  } else if (_include_prints) {
    std::cout << "\nTime taken for " << _tuples[0]->_filesafe_name
    << " FoM to be minimised by Adam:\n";
    t.Print();
    std::cout << "\n";
  }
  return current_coords;
}
