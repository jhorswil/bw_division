#include "opt_save_and_collate.h"


void read_and_edit_global_results(
  std::string global_results_path, std::string treename,
  std::vector<io_manager*> tuples,
  line_parameters grid_search_result, opt_discrete_gradient* opt_minimiser
  ) {
  // Read and append to global results file

  TFile* global_stats_file =
  TFile::Open(global_results_path.c_str(), "UPDATE");
  TTree* global_stats =
  ((TTree*)global_stats_file->Get(treename.c_str()));
  
  std::cout << "\nGlobal stats file: " << global_results_path
  << " is already written. Reading from file...\n";

  double fom, mbr, mbe, mber;
  fom = opt_minimiser->_opt_fom;
  mbr = opt_minimiser->_opt_rate;
  mbe = opt_minimiser->_opt_backeff;
  mber = opt_minimiser->_opt_backeff_err;
  
  global_stats->SetBranchAddress("optimal_fom", &fom);
  global_stats->SetBranchAddress("optimal_minbias_rate", &mbr);
  global_stats->SetBranchAddress("optimal_minbias_efficiency", &mbe);
  global_stats->SetBranchAddress("optimal_minbias_efficiency_err",
  &mber);
  std::vector<double> results = grid_search_result.return_vector();
  for (unsigned i{0}; i < grid_search_result.size(); i++) {
    global_stats->SetBranchAddress(
    grid_search_result._paramnames[i].c_str(), &results[i]);
  }

  std::vector<int> candidate_counts(NUM_LINES);
  line_store linestore;
  for (unsigned i{0}; i < NUM_LINES; ++i){
    candidate_counts[i] =
    tuples[0]->_background_io->_optimal_candidate_counts[i];
    std::string branchname = std::string(linestore._linenames[i])
    + "_candidate_count";
    global_stats->SetBranchAddress(branchname.c_str(), &candidate_counts[i]);
  }

  std::vector<double> maxeffs, maxeff_errs, opteffs, opteff_errs;
  for (unsigned i{0}; i < tuples.size(); i++) {
    maxeffs.push_back(tuples[i]->_optimal_signal_eff);
    maxeff_errs.push_back(tuples[i]->_optimal_signal_eff_err);
    opteffs.push_back(opt_minimiser->_signal_efficiencies[i]);
    opteff_errs.push_back(opt_minimiser->_signal_efficiency_errs[i]);
  }

  for (unsigned i{0}; i < tuples.size(); i++) {
    std::string branchname = tuples[i]->_filesafe_name + "_maxeff";
    global_stats->SetBranchAddress(branchname.c_str(),
    &maxeffs[i]);
    std::string branchname2 = tuples[i]->_filesafe_name + "_maxeff_err";
    global_stats->SetBranchAddress(branchname2.c_str(),
    &maxeff_errs[i]);
    std::string branchname3 = tuples[i]->_filesafe_name + "_opteff";
    global_stats->SetBranchAddress(branchname3.c_str(),
    &opteffs[i]);
    std::string branchname4 = tuples[i]->_filesafe_name + "_opteff_err";
    global_stats->SetBranchAddress(branchname4.c_str(),
    &opteff_errs[i]);
  }

  global_stats->Fill();
  global_stats_file->cd();
  global_stats->Write();
  global_stats->Print();
  global_stats_file->Close();
}


void write_global_results(
  std::string global_results_path, std::string treename,
  std::vector<io_manager*> tuples,
  line_parameters grid_search_result, opt_discrete_gradient* opt_minimiser
  ) {
  // Write to global results file

  std::cout << "\nGlobal Min stats file: " << global_results_path
  << " has not yet been written. Creating file...\n";
  
  TFile* global_stats_file =
  TFile::Open(global_results_path.c_str(), "RECREATE");
  TTree* global_stats = new TTree(treename.c_str(), "Optimal Optfnc Stats");

  double fom, mbr, mbe, mber;
  fom = opt_minimiser->_opt_fom;
  mbr = opt_minimiser->_opt_rate;
  mbe = opt_minimiser->_opt_backeff;
  mber = opt_minimiser->_opt_backeff_err;

  global_stats->Branch("optimal_fom", &fom, "optimal_fom/D");
  global_stats->Branch("optimal_minbias_rate",
  &mbr, "optimal_minbias_rate/D");
  global_stats->Branch("optimal_minbias_efficiency", &mbe,
  "optimal_minbias_efficiency/D");
  global_stats->Branch("optimal_minbias_efficiency_err",
  &mber, "optimal_minbias_efficiency_err/D");

  std::vector<double> results = grid_search_result.return_vector();
  for (unsigned i{0}; i < grid_search_result.size(); i++) {
    global_stats->Branch(grid_search_result._paramnames[i].c_str(),
    &results[i], (grid_search_result._paramnames[i] + "/D").c_str());
  }

  std::vector<int> candidate_counts =
  tuples[0]->_background_io->_optimal_candidate_counts;
  line_store linestore;
  for (unsigned i{0}; i < NUM_LINES; ++i ){
    std::string branchname = linestore._linenames[i] + std::string("_candidate_count");
    global_stats->Branch(branchname.c_str(), &candidate_counts[i],
    (branchname + "/I").c_str());
  }
  
  std::vector<double> maxeffs, maxeff_errs, opteffs, opteff_errs;
  for (unsigned i{0}; i < tuples.size(); i++) {
    maxeffs.push_back(tuples[i]->_optimal_signal_eff);
    maxeff_errs.push_back(tuples[i]->_optimal_signal_eff_err);
    opteffs.push_back(opt_minimiser->_signal_efficiencies[i]);
    opteff_errs.push_back(opt_minimiser->_signal_efficiency_errs[i]);
  }

  for (unsigned i{0}; i < tuples.size(); i++) {
    std::string branchname = tuples[i]->_filesafe_name + "_maxeff";
    global_stats->Branch(branchname.c_str(),
    &maxeffs[i], (branchname + "/D").c_str());
    std::string branchname2 = tuples[i]->_filesafe_name + "_maxeff_err";
    global_stats->Branch(branchname2.c_str(),
    &maxeff_errs[i], (branchname2 + "/D").c_str());
    std::string branchname3 = tuples[i]->_filesafe_name + "_opteff";
    global_stats->Branch(branchname3.c_str(),
    &opteffs[i], (branchname3 + "/D").c_str());
    std::string branchname4 = tuples[i]->_filesafe_name + "_opteff_err";
    global_stats->Branch(branchname4.c_str(),
    &opteff_errs[i], (branchname4 + "/D").c_str());
  }

  global_stats->Fill();
  global_stats_file->cd();
  global_stats->Write();
  global_stats->Print();
  global_stats_file->Close();
}


void save_or_edit_global_min_results(
  std::vector<io_manager*> tuples, line_parameters grid_search_result,
  opt_discrete_gradient* opt_minimiser, std::string outfile_dir,
  int job_num
  ) {

  double ratecap = (tuples[0]->_percentage_ratelimit/100.0)
  * ((tuples[0]->_collision_rate * tuples[0]->_ratelimit) / 1e6);
  std::stringstream ratecap_ss;
  ratecap_ss << std::fixed << std::setprecision(3) << ratecap;
  std::string ratecap_string = ratecap_ss.str() + "_mhz";
  boost::replace_all(ratecap_string, ".", "point");

  std::string output_dir = outfile_dir + "global_min_stats_"
  + ratecap_string + "/";
  std::filesystem::create_directories(output_dir.c_str());
  
  std::string global_stats_path =
  output_dir + "global_min_stats_job" + std::to_string(job_num) + ".root";

  std::string treename = "global_stats_tree";

  if (!(gSystem->AccessPathName(global_stats_path.c_str()))) {
    read_and_edit_global_results(global_stats_path, treename,
    tuples, grid_search_result, opt_minimiser);
  } else {
    write_global_results(global_stats_path, treename, tuples,
    grid_search_result, opt_minimiser);
  }
}


void collate_global_file_info(
  std::string entry_path, std::string treename,
  std::vector<io_manager*> tuples,
  int& filecounter, std::vector<double>& optimal_foms,
  std::vector<double>& optimal_minbias_rates,
  std::vector<double>& optimal_minbias_efficiencies,
  std::vector<double>& optimal_minbias_efficiency_errs,
  std::vector<std::vector<double>>& max_signal_effs,
  std::vector<std::vector<double>>& max_signal_eff_errs,
  std::vector<std::vector<double>>& optimal_signal_effs,
  std::vector<std::vector<double>>& optimal_signal_eff_errs,
  line_parameters paramph,
  std::vector<std::vector<double>>& optimal_parameter_inputs,
  std::vector<std::vector<int>>& optimal_candidate_counts) {

  if (entry_path.find("global_min_stats_job") !=
  std::string::npos
  && entry_path.find("collated") == std::string::npos) {
    filecounter++;
    std::cout << "Reading " << entry_path << "...\n";
    ROOT::RDataFrame global_stats(treename.c_str(),
    entry_path.c_str());

    std::vector<double> new_stats =
    *(global_stats.Take<double>("optimal_fom"));
    optimal_foms.insert(optimal_foms.end(), new_stats.begin(),
    new_stats.end());
    new_stats =
    *(global_stats.Take<double>("optimal_minbias_rate"));
    optimal_minbias_rates.insert(optimal_minbias_rates.end(),
    new_stats.begin(), new_stats.end());
    new_stats =
    *(global_stats.Take<double>("optimal_minbias_efficiency"));
    optimal_minbias_efficiencies.insert(
    optimal_minbias_efficiencies.end(), new_stats.begin(),
    new_stats.end());
    new_stats = *(global_stats.Take<double>(
    "optimal_minbias_efficiency_err"));
    optimal_minbias_efficiency_errs.insert(
    optimal_minbias_efficiency_errs.end(), new_stats.begin(),
    new_stats.end());

    for (unsigned i{0}; i < paramph.size(); i++) {
      new_stats = *(global_stats.Take<double>((
      paramph._paramnames[i]).c_str()));
      optimal_parameter_inputs[i].insert(
      optimal_parameter_inputs[i].end(), new_stats.begin(),
      new_stats.end());
    }

    for (unsigned i{0}; i < tuples.size(); i++) {
      new_stats =
      *(global_stats.Take<double>((tuples[i]->_filesafe_name
      + "_maxeff").c_str()));
      max_signal_effs[i].insert(max_signal_effs[i].end(),
      new_stats.begin(), new_stats.end());
      new_stats =
      *(global_stats.Take<double>((tuples[i]->_filesafe_name
      + "_maxeff_err").c_str()));
      max_signal_eff_errs[i].insert(max_signal_eff_errs[i].end(),
      new_stats.begin(), new_stats.end());
      new_stats =
      *(global_stats.Take<double>((tuples[i]->_filesafe_name
      + "_opteff").c_str()));
      optimal_signal_effs[i].insert(optimal_signal_effs[i].end(),
      new_stats.begin(), new_stats.end());
      new_stats =
      *(global_stats.Take<double>((tuples[i]->_filesafe_name
      + "_opteff_err").c_str()));
      optimal_signal_eff_errs[i].insert(
      optimal_signal_eff_errs[i].end(), new_stats.begin(),
      new_stats.end());
    }
    
    line_store linestore;
    for (unsigned i{0}; i < NUM_LINES; i++) {
      std::vector<int> newcan_stats = *(global_stats.Take<int>(
      (std::string(linestore._linenames[i]) + "_candidate_count").c_str()));
      optimal_candidate_counts[i].insert(
      optimal_candidate_counts[i].end(),
      newcan_stats.begin(), newcan_stats.end()); 
    }
  }
}


void fill_collated_global_results_file(
  std::string output_dir, line_parameters paramph,
  std::vector<io_manager*> tuples,
  std::vector<std::vector<double>> optimal_parameter_inputs,
  std::vector<std::vector<double>> max_signal_effs,
  std::vector<std::vector<double>> max_signal_eff_errs,
  std::vector<std::vector<double>> optimal_signal_effs,
  std::vector<std::vector<double>> optimal_signal_eff_errs,
  std::vector<std::vector<int>> optimal_candidate_counts,
  std::vector<double> optimal_minbias_rates,
  std::vector<double> optimal_minbias_efficiencies,
  std::vector<double> optimal_minbias_efficiency_errs,
  std::vector<double> optimal_foms) {

  TFile* collated_global_results_file =
  TFile::Open((output_dir + "collated_global_results.root").c_str(),
  "RECREATE");
  TTree* collated_global_resultstree =
  new TTree("collated_global_resultstree",
  "Global minimisation result tree");
  double fom, mbr, mbe, mber;
  std::vector<double> optparams( paramph.size(), 0 ); 
  std::vector<double> maxeffs(tuples.size(), 0); 
  std::vector<double> maxefferrs( tuples.size(), 0); 
  std::vector<double> opteffs( tuples.size(), 0); 
  std::vector<double> optefferrs(tuples.size(), 0); 
  std::vector<int> candcounts( NUM_LINES, 0 );

  collated_global_resultstree->Branch("optimal_fom", &fom,
  "optimal_fom/D");
  collated_global_resultstree->Branch("optimal_minbias_rate",
  &mbr, "optimal_minbias_rate/D");
  collated_global_resultstree->Branch("optimal_minbias_efficiency",
  &mbe, "optimal_minbias_efficiency/D");
  collated_global_resultstree->Branch("optimal_minbias_efficiency_err",
  &mber, "optimal_minbias_efficiency_err/D");

  for (unsigned i{0}; i < paramph.size(); i++) {
    std::string branchname = paramph._paramnames[i];
    collated_global_resultstree->Branch(branchname.c_str(),
    &optparams[i], (branchname + "/D").c_str());
  }

  for (unsigned i{0}; i < tuples.size(); i++) {
    std::string branchname = tuples[i]->_filesafe_name + "_maxeff";
    collated_global_resultstree->Branch(branchname.c_str(),
    &maxeffs[i], (branchname + "/D").c_str());
    std::string branchname2 = tuples[i]->_filesafe_name + "_maxeff_err";
    collated_global_resultstree->Branch(branchname2.c_str(),
    &maxefferrs[i], (branchname2 + "/D").c_str());
    std::string branchname3 = tuples[i]->_filesafe_name + "_opteff";
    collated_global_resultstree->Branch(branchname3.c_str(),
    &opteffs[i], (branchname3 + "/D").c_str());
    std::string branchname4 = tuples[i]->_filesafe_name + "_opteff_err";
    collated_global_resultstree->Branch(branchname4.c_str(),
    &optefferrs[i], (branchname4 + "/D").c_str());
  }
  
  line_store linestore;
  for (unsigned i{0}; i < NUM_LINES; i++) {
    std::string branchname = std::string(linestore._linenames[i])
    + "_candidate_count";
    collated_global_resultstree->Branch(branchname.c_str(),
    &candcounts[i], (branchname + "/I").c_str());
  }
  
  for (unsigned i{0}; i < optimal_foms.size(); i++) {
    fom = optimal_foms[i];
    mbr = optimal_minbias_rates[i];
    mbe = optimal_minbias_efficiencies[i];
    mber = optimal_minbias_efficiency_errs[i];
    for (unsigned j{0}; j < paramph.size(); j++) {
      optparams[j] = optimal_parameter_inputs[j][i];
    }
    for (unsigned j{0}; j < tuples.size(); j++) {
      maxeffs[j] = max_signal_effs[j][i];
      maxefferrs[j] = max_signal_eff_errs[j][i];
      opteffs[j] = optimal_signal_effs[j][i];
      optefferrs[j] = optimal_signal_eff_errs[j][i];
    }
    
    for (unsigned j{0}; j < NUM_LINES; j++) {
      candcounts[j] = optimal_candidate_counts[j][i];
    }
    collated_global_resultstree->Fill();
  }
  std::cout << "Finished filling\n";
  
  collated_global_results_file->cd();
  collated_global_resultstree->Write();
  collated_global_results_file->Close(); 
}



void collate_global_min_results(
  std::string outfile_dir, std::vector<io_manager*> tuples
  ) {
  
  double ratecap = (tuples[0]->_percentage_ratelimit/100.0)
  * ((tuples[0]->_collision_rate * tuples[0]->_ratelimit) / 1e6);
  std::stringstream ratecap_ss;
  ratecap_ss << std::fixed << std::setprecision(3) << ratecap;
  std::string ratecap_string = ratecap_ss.str() + "_mhz";
  boost::replace_all(ratecap_string, ".", "point");

  std::string output_dir = outfile_dir + "global_min_stats_"
  + ratecap_string + "/";

  std::filesystem::create_directories(output_dir.c_str());
  std::string treename = "global_stats_tree";

  std::vector<double> optimal_foms, optimal_minbias_rates,
  optimal_minbias_efficiencies,
  optimal_minbias_efficiency_errs;
  // Each sub vector is associated with a certain param/tuple. Each mother
  // vector is associated with a certain minimisation/fom
  std::vector<std::vector<double>> optimal_parameter_inputs,
  max_signal_effs, max_signal_eff_errs, optimal_signal_effs,
  optimal_signal_eff_errs;
  std::vector<std::vector<int>> optimal_candidate_counts;
  line_parameters paramph;

  for (unsigned i{0}; i < paramph.size(); i++) {
    optimal_parameter_inputs.push_back({});
  }
  for (unsigned i{0}; i < tuples.size(); i++) {
    max_signal_effs.push_back({});
    max_signal_eff_errs.push_back({});
    optimal_signal_effs.push_back({});
    optimal_signal_eff_errs.push_back({});
  }
  for (unsigned i{0}; i < NUM_LINES; i++) {
    optimal_candidate_counts.push_back({});
  }

  int filecounter{0};
  for (const auto & entry : std::filesystem::directory_iterator(
  output_dir)) {
    std::string entry_path = entry.path().string();
    collate_global_file_info(entry_path, treename, tuples, filecounter,
    optimal_foms, optimal_minbias_rates, optimal_minbias_efficiencies,
    optimal_minbias_efficiency_errs, max_signal_effs, max_signal_eff_errs,
    optimal_signal_effs, optimal_signal_eff_errs, paramph,
    optimal_parameter_inputs, optimal_candidate_counts); 
  }

  // If no files exist, do not collate
  if (filecounter == 0) {
    std::cout << "No global minimisation files found to collate."
    " Exiting...\n";
    exit(0);
  }
  for (unsigned i{0}; i < optimal_foms.size(); i++) {
    std::cout << i << "th minimisation minimum FoM: "
    << optimal_foms[i] << "\n";
    std::cout << "Associated coordinates:\n";
    for (unsigned j{0}; j < paramph.size(); j++) {
      std::cout << paramph._paramnames[j] << ": "
      << optimal_parameter_inputs[j][i] << " | ";
    }
    std::cout << "\n";
  }
  std::cout << "\n";

  fill_collated_global_results_file(output_dir, paramph, tuples,
  optimal_parameter_inputs, max_signal_effs, max_signal_eff_errs,
  optimal_signal_effs, optimal_signal_eff_errs, optimal_candidate_counts,
  optimal_minbias_rates, optimal_minbias_efficiencies,
  optimal_minbias_efficiency_errs, optimal_foms);

  int minindex =
  std::distance(std::begin(optimal_foms),
  std::min_element(std::begin(optimal_foms), std::end(optimal_foms)));

  double average_percentage_difference{0};
  int average_counter{0};
  for (double fom: optimal_foms) {
    // Remove outliers
    if (fabs(fom - optimal_foms[minindex])
    < 2 * fabs(optimal_foms[minindex])) {
      average_percentage_difference +=
      fabs(((fom - optimal_foms[minindex]) / fom) * 100);
      average_counter++;
    }
  }

  average_percentage_difference /= (average_counter * 1.0);

  std::cout << "\n\n ------- Best collated global results: --------\n\n";
  std::cout << "Best FoM: " << optimal_foms[minindex] << "\n";
  std::cout << "Average percentage difference to minimum FoM: "
  << average_percentage_difference << "\n";
  std::cout << "Best Minbias Rate (MHz): " << optimal_minbias_rates[minindex]/1e6
  << "\n";
  std::cout << "Minbias Rate error (MHz): " << (optimal_minbias_rates[minindex]
  * optimal_minbias_efficiency_errs[minindex])/1e6 << "\n";
  std::cout << "Best Minbias Efficiency: "
  << optimal_minbias_efficiencies[minindex] << "\n";
  std::cout << "Minbias Efficiency error: "
  << optimal_minbias_efficiency_errs[minindex] << "\n";
  std::cout << "Best parameter values:\n";
  for (unsigned i{0}; i < paramph.size(); i++) {
    std::cout << paramph._paramnames[i] << ": "
    << optimal_parameter_inputs[i][minindex] << " ";
  }
  std::cout << "\n";

  for (unsigned i{0}; i < tuples.size(); i++) {
    std::cout << tuples[i]->_filesafe_name << " best max efficiency: "
    << max_signal_effs[i][minindex] << "\n";
    std::cout << tuples[i]->_filesafe_name << " max efficiency err: "
    << max_signal_eff_errs[i][minindex] << "\n";
    std::cout << tuples[i]->_filesafe_name << " best opt efficiency: "
    << optimal_signal_effs[i][minindex] << "\n";
    std::cout << tuples[i]->_filesafe_name << " opt efficiency err: "
    << optimal_signal_eff_errs[i][minindex] << "\n";
  }
  
  line_store linestore;
  for (unsigned i{0}; i < NUM_LINES; i++) {
    std::cout << "Best " << linestore._linenames[i]
    << " candidate count: "
    << optimal_candidate_counts[i][minindex] << "\n";
  }
}
