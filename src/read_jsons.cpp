#include "read_jsons.h"
#include "read_bwconfig.h"
#include <glob.h>

void sample_builder(
  std::string filepath, std::vector<std::string>& lines_not_included,
  const std::vector<std::string>& sample_dirs,
  std::vector<std::string>& input_files,
  std::vector<std::string>& labels, std::vector<std::string>& event_types,
  std::vector<std::string>& signal_track_counts,
  std::vector<std::vector<std::string>>& lines,
  std::vector<std::string>& weights,
  std::vector<std::string> incl_lines
  ) {
  // Read and store event type, decay name, number of signal
  // tracks and active lines for each sample

  using json = nlohmann::json;
  std::ifstream f(filepath);
  std::cout << "\n\nParsing " << filepath << "...\n";
  json data = json::parse(f);
  std::cout << "\n\nParsing complete.\n";

  std::cout << "Samples and associated lines:\n";
  for (unsigned i{0}; i < data["samples"].size(); i++) {
    std::string event_type = data["samples"][i]["EventType"];
    std::string label = data["samples"][i]["Label"];
    std::string num_signal_tracks =
    data["samples"][i]["nSignalTracks"];
    
    std::string weight = data["samples"][i]["Weight"];
    std::vector<std::string> line_group;
    
    for (std::string line: data["samples"][i]["Lines"]) {
      std::string full_line = "Hlt1" + line;
      if (std::find(incl_lines.begin(), incl_lines.end(), full_line)
        != incl_lines.end()) {
        line_group.push_back(full_line);
      } else {
        if (line.compare("*") == 0) {
          for (std::string line: incl_lines) {
            line_group.push_back(line);
          }
        } else {
          if (std::find(lines_not_included.begin(),
            lines_not_included.end(), full_line)
            == lines_not_included.end()) {
            std::cout << "\nIn file " << filepath << " / " << label << " line not found: "
            << full_line << std::endl; 
            lines_not_included.push_back(full_line);
          }
        }
      }
    }

    std::cout << event_type << " : ";
    for(const auto& line : line_group) std::cout << line << " ";
    std::cout << std::endl; 

    bool no_lines = line_group.empty();
    if (no_lines) { 
      std::cout << "\n\n----------------- EVENT TYPE "
      << event_type
      << " HAS NO IMPLEMENTED LINES --------------\n\n";
      continue; 
    }

    bool found = false; 
    for(const auto& sample_dir : sample_dirs) {
      // This seems like a rather archaic way of searching for files 
      auto file_exists = [](const std::string& name) {
        // Find pathnames matching a pattern
        glob_t result;
        // Copies the fill byte (0) into each of the first sizeof(result)
        // characters of result
        memset(&result, 0, sizeof(result));

        // Searching for pathnames matching 'name'
        auto status = glob(name.c_str(), GLOB_TILDE, NULL, &result);
        bool found = (status==0) && (result.gl_pathc!=0);
        
        // Frees the dynamically allocated storage associated with glob()
        globfree(&result);
        return found;
      };
      
      auto possible_files = event_type == "30000000"
      ? std::vector<std::string>{sample_dir,
      sample_dir + event_type + "/allen_output.root",
      sample_dir + event_type +"_MD/allen_output.root"} : 
      std::vector<std::string>{sample_dir + event_type + "/allen_output.root", 
      sample_dir + event_type + "_MD/allen_output.root"}; 

      for(const auto& file : possible_files) {
        if (!file_exists(file) or file.find(".root")
        == std::string::npos) continue;
        
        input_files.push_back(file);
        found = true; 
        break;
      }
      if (found) break; 
    }
    
    if (found) {
      labels.push_back(label);
      event_types.push_back(event_type);
      signal_track_counts.push_back(num_signal_tracks);
      lines.push_back(line_group);
      weights.push_back(weight);
    } else { 
      std::cout << "\n\n------------- EVENT TYPE " << event_type
      << " NOT REPRESENTED IN SAMPLE DIRECTORY --------------"
      "\n\n";
    }
  }
}


void print_lines_samples_associated_with_parameter(
  line_store& linestore, std::string param, unsigned i,
  std::vector<io_manager*> tuples
  ) {

  std::vector<std::string> assoc_params = linestore._associated_params[i];

  // For all params input into this line
  bool associated{false};
  for (std::string split_param: assoc_params) {
    if (param == split_param) {
      associated = true;
      break;
    }
  }

  if (associated) {
    std::cout << "\nLine: " << linestore._linenames[i] << "\n";
    std::cout << "Samples represented by this line:\n";
    for (io_manager* tuple: tuples) {
      if (tuple->_activate_lines[i])  std::cout << tuple->_name << "\n";
    }
    std::cout << "\n";
  }
}


void plot_reconstructability_efficiencies(
  std::string plot_dir, std::vector<io_manager*> tuples
  ) {

  std::cout << "\n\nReconstructability efficiencies:\n\n";

  std::string canvas_dir = plot_dir + "reconstructable_efficiencies/";
  std::filesystem::create_directories(canvas_dir.c_str());

  TH1D* reconstructable_eff_bars =
  new TH1D("reconstructable_eff_bars", "Reconstructable Eff", tuples.size(),
  0, tuples.size());
  reconstructable_eff_bars->SetYTitle("Reconstructability Efficiency");
  reconstructable_eff_bars->SetStats(kFALSE);
  reconstructable_eff_bars->SetFillStyle(3345);
  reconstructable_eff_bars->SetLineWidth(2);
  reconstructable_eff_bars->SetLineColor(kRed);
  reconstructable_eff_bars->SetFillColor(kRed);
  reconstructable_eff_bars->SetMinimum(0.0);
  reconstructable_eff_bars->SetMaximum(1.0);

  for (unsigned i{0}; i < tuples.size(); i++) {
    double efficiency = tuples[i]->_total_reconstructable_events
    / tuples[i]->_total_allen_events;

    double error = (efficiency >= 1.0 || efficiency <= 0.0) ? 1e-8
    : sqrt(fabs(efficiency * (1 - efficiency))
    / tuples[i]->_total_allen_events);

    std::cout << "$" << tuples[i]->_name << "$: " << efficiency << " \\pm "
      << error << "\n";

    reconstructable_eff_bars->SetBinError(i+1, error);
    reconstructable_eff_bars->SetBinContent(i+1, efficiency);
    reconstructable_eff_bars->GetXaxis()->SetBinLabel(i+1,
    tuples[i]->_name.c_str());
  }

  reconstructable_eff_bars->GetXaxis()->LabelsOption("v");

  TCanvas* reconstructable_canvas = new TCanvas("reconstructable_canvas",
  "Reconstructability Efficiencies", 0, 0, 2560, 1440);
  reconstructable_canvas->cd();
  reconstructable_canvas->SetBottomMargin(0.3);

  reconstructable_eff_bars->Draw("Hist, E1");

  reconstructable_canvas->SetTitle("Reconstructable Efficiencies");
  reconstructable_canvas->SaveAs((canvas_dir + "recon_effs"
  ".tex").c_str());
  reconstructable_canvas->SaveAs((canvas_dir + "recon_effs"
  ".C").c_str());
  reconstructable_canvas->SaveAs((canvas_dir + "recon_effs"
  ".svg").c_str());
}


bool alphabetical_sort(std::string a, std::string b) {return a < b;}


std::vector<io_manager*> read_jsons(
  std::string json_input_dir, const std::vector<std::string>& sample_dir,
  const std::vector<line_manager>& line_managers,
  double collision_rate, double ratelimit, double cutoff,
  double scalewidth, int accuracy, int operation_option,
  int tuple_num, int percentage_ratelimit, int max_events,
  bool reconstructable_only, std::string plot_dir,
  const fixedlines_t& fixed_inputs,
  const linemask_t& partially_fixed_lines,
  const linemask_t& fully_fixed_lines,
  const linemask_t& off_lines,
  const parammask_t& fixed_line_params
  ) {
  // Reading input jsons 

  std::vector<std::string> incl_lines;
  for (auto& line: line_managers) incl_lines.push_back(line._name);
  std::vector<std::string> input_files;
  std::vector<std::string> labels;
  std::vector<std::string> event_types;
  std::vector<std::string> signal_track_counts;
  std::vector<std::string> weights;
  std::vector<std::vector<std::string>> lines;
  std::vector<std::string> lines_not_included;
  std::vector<std::string> json_files;

  for (const auto & entry : std::filesystem::directory_iterator(
  json_input_dir)) {
    json_files.push_back(entry.path().string());
  }

  std::sort(json_files.begin(), json_files.end(), alphabetical_sort);

  for (std::string filepath: json_files) {
    sample_builder(filepath, lines_not_included, sample_dir, input_files,
    labels, event_types, signal_track_counts, lines, weights, incl_lines);
  }

  std::cout << "\n\nSamples present (event type, label, weight, "
  "tuple number):\n";

  int tupleindex{0};
  int actual_index{0};
  for (unsigned i{0}; i < input_files.size(); i++) {
    if (event_types[i].compare("30000000") != 0) {
      std::cout << event_types[i] << " | " << labels[i]
        << " | " << weights[i] <<  " | " << tupleindex << "\n";
      if (tuple_num == tupleindex) actual_index = i;
      tupleindex++;
    }
  }

  if (operation_option == 13) exit(0);

  std::cout << "\nLines not implemented within project: \n";
  for (std::string line: lines_not_included) {
    std::cout << line << "\n";
  }
  std::cout << "\n\n";

  if (std::find(event_types.begin(), event_types.end(),
    "30000000") == event_types.end()) {
    std::cout << "Minimum/Zero bias event type (30000000) not found. "
    "Exiting...\n";
    exit(0);
  }

  int minbias_index = std::find(event_types.begin(), event_types.end(),
  "30000000") - event_types.begin(); 

  std::string minbias_name = labels[minbias_index];
  std::string minbias_event_type = event_types[minbias_index];
  std::string minbias_input_file = input_files[minbias_index];
  std::vector<std::string> minbias_lines = lines[minbias_index];
  std::cout << "\nNumber of Minimum/Zero-bias Lines: " << minbias_lines.size()
  << std::endl; 

  io_manager* minbias =
  io_manager_json_readin(minbias_name, minbias_event_type, nullptr,
  minbias_input_file, line_managers, fixed_inputs, partially_fixed_lines,
  fully_fixed_lines, off_lines, fixed_line_params, collision_rate, ratelimit,
  cutoff, scalewidth, accuracy, max_events, 0, 1.0, reconstructable_only);

  minbias->print();

  collision_rate = minbias->_collision_rate;
  ratelimit = minbias->_ratelimit;

  std::vector<int> options = {4, 6, 7, 9, 10};//, 12};
  bool individual_min = std::find(options.begin(), options.end(),
  operation_option) != options.end();
  
  if ((individual_min) && (tuple_num > (int)input_files.size() - 2
  || tuple_num < 0)) {
    std::cout << "Invalid tuple number " << tuple_num
    << " chosen for individual minimisation. Choose number from 0 - "
    << input_files.size() - 2 << ". Exiting...\n";
    exit(0);
  }

  std::vector<io_manager*> tuples;
  std::vector<io_manager*> temp_tuples;

  // Parallelise generation loop. Also could expand the gradient calculation
  // To do it all in one big loop rather than two and parallelise that
  for (unsigned i{0}; i < input_files.size(); i++) {
    temp_tuples.push_back(nullptr);
  }

  for (unsigned i = 0; i < input_files.size(); i++) {
    if ((int)i != minbias_index && !(individual_min
    && (int)i != actual_index)) {
      
      std::vector<line_manager> lines_for_this_sample; 
      for(const auto& line : line_managers) {
        if (std::find(lines[i].begin(), lines[i].end(), line._name)
        != lines[i].end())
          lines_for_this_sample.push_back(line); 
      }

      temp_tuples[i] = io_manager_json_readin(labels[i], event_types[i],
      minbias, input_files[i], lines_for_this_sample, fixed_inputs,
      partially_fixed_lines, fully_fixed_lines, off_lines, fixed_line_params,
      collision_rate,
      ratelimit, cutoff, scalewidth, accuracy, max_events,
      std::stoi(signal_track_counts[i]), std::stod(weights[i]),
      reconstructable_only);

      temp_tuples[i]->_percentage_ratelimit = percentage_ratelimit;
    }
  }


  for (unsigned i{0}; i < temp_tuples.size(); i++) {
    if ((int)i != minbias_index && !(individual_min
    && (int)i != actual_index)) {
      tuples.push_back(temp_tuples[i]);
    }
    if ((int)i != minbias_index) tupleindex++;
  }

  line_parameters params;
  line_store linestore;

  std::cout << "\n";

  for (const auto& param :  params._paramnames) {
    std::cout << "\nLines and samples for " << param << ":\n";
    
    for (unsigned i{0}; i < linestore._linenames.size(); i++) {
      print_lines_samples_associated_with_parameter(linestore,
      std::string(param), i, tuples);
    }
  }

  if (reconstructable_only) {
    plot_reconstructability_efficiencies(plot_dir, tuples);
  }

  return tuples;
}
