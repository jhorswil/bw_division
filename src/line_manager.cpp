#include "line_manager.h"
#include "line_store.h"

// Config line read-in class
line_manager::line_manager(
    std::string name, 
    const std::vector<std::string>& monitored_quantities,
    const std::vector<std::string>& params
    ) {

  _name = name;
  _monitored_quantities = monitored_quantities;
  _event_branch = name + "__evtNo_t";
  _run_branch = name + "__runNo_t";
  _tree = name + "/monitor_tree";
  _fixed = params.empty();
  
  for(auto& param : params) {
    _parameters.emplace_back(param, parameter_config());
    _cut_variables.push_back(param);
  }
}

void line_manager::print() {
  std::cout << "Line Print:\n";
  std::cout << "Name: " << _name << "\n";
  std::cout << "Cut variables, mins, maxs, nsteps, nsteps gradient: \n";

  for (const auto& [name, param] : _parameters){
    std::cout << name  << ", " << param._min
    << ", " << param._max << ", " << param._steps << ", "
    << param._grad_steps << "\n";
  }

  std::cout << "Monitored quantities: \n";

  for (std::string quant: _monitored_quantities) {
    std::cout << quant << " ";
  }

  std::cout << "Event branch: " << _event_branch << "\n";
  std::cout << "Tree: " << _tree << "\n";

  if (_fixed) {
    std::cout << "This line is fixed (non-tunable input "
      "parameter)\n";
  }
}
