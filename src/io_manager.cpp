#include "io_manager.h"
#include "init_discrete_gradient.h"
#include "misc_tools.h"
#include "line_tools.h"
#include <filesystem>
#include <glob.h>

io_manager::io_manager(
  const std::string& name, const std::string& event_type,
  const std::string& ntuple_path, const std::vector<line_manager>& lines, 
  const fixedlines_t& fixed_inputs,
  const linemask_t& partially_fixed_lines,
  const linemask_t& fully_fixed_lines,
  const linemask_t& off_lines,
  const parammask_t& fixed_line_params,
  unsigned num_signal_tracks, bool reconstructable_only
  ) {
  // Basic info, efficiency denominators, monitored quantity/line builder
  // and line mask 
  _name = name;
  _filesafe_name = latex_to_term(name);
  _num_signal_tracks = num_signal_tracks;
  _reconstructable_only = reconstructable_only;
  _lines = lines;
  std::sort(_lines.begin(), _lines.end(), [](auto& line1, auto& line2) {
  return line1._line_number < line2._line_number;});
  _event_type = event_type;
  _s_or_b = _event_type != "30000000"; 
  _ntuple_path = ntuple_path;
  _partially_fixed_lines = partially_fixed_lines;
  _fully_fixed_lines = fully_fixed_lines;
  _fixed_inputs = fixed_inputs;
  _off_lines = off_lines;
  _fixed_line_params = fixed_line_params;

  // The line and event builder has to happen before the _off_lines loop
  using idmap = std::map<std::pair<unsigned long, unsigned int>,
  std::pair<std::vector<std::vector<float>>, std::vector<int>>>;

  line_parameters params;
  line_store linestore;
  for(const auto& line : lines) {
    _activate_lines[line._line_number] = true;
  }
  // The reason to scope this is the precut_idmap is massive in memory,
  // so the quicker can be removed, the better 
  {
    idmap precut_idmap;

    for (const auto& line: _lines) {
      line_builder(line, params, precut_idmap);
    }

    for (unsigned i{0}; i < NUM_LINES; i++) {
      _logfile_events_per_line.push_back(find_absolute_yield_per_line(
      ntuple_path, linestore._linenames[i]));
    }
  
    for (unsigned int i{0}; i < NUM_LINES; i++) {
      if (_off_lines[i]) {
        for (unsigned j{0}; j < _lines.size(); j++) {
          if (linestore._linenames[i].compare(_lines[j]._name) == 0) {
            if (!_s_or_b) {
              std::cout << "Setting " << _lines[j]._name << " parameters "
              "to tightest value (even though no events are passing regardless)"
              "\n";
            }
            for (unsigned k{0}; k < _lines[j]._cut_variables.size(); k++) {
              _fixed_inputs[i][global_parameter_index(
              _lines[j]._cut_variables[k])] =
              _parameters[_lines[j]._cut_variables[k]]._max;
            }
            break;
          }
        }
      }

      if (_partially_fixed_lines[i] && !_s_or_b) {
        std::cout << "\nPartially fixed params for "
        << linestore._linenames[i] << "\n";
        _fixed_inputs[i].print();
        std::cout << " (not all input params are fixed for this line)\n";
      } else if (_fully_fixed_lines[i] && !_s_or_b) {
        std::cout << "\nFixed params for " << linestore._linenames[i]
        << "\n";
        _fixed_inputs[i].print();
        std::cout << " (all input params are fixed for this line)\n";
      }
    }

    _activate_params = std::vector<bool>(params.size(), false);
    for(const auto& [k,v] : _parameters) {
      // if (_s_or_b) {
      //   std::cout << "Activating " << k << "\n";
      // }
      _activate_params[global_parameter_index(k)] = true;
    }

    reconstructable_events_and_axis_builder(precut_idmap,
    event_type == "30000000" ? false : reconstructable_only);
  }
 
  find_absolute_yield(ntuple_path);

  std::vector<std::string> ending_integers =
  {" 2", " 3", " 4", " 5", " 6", " 7", " 8", " 9"};
  for (std::string integer : ending_integers) {
    if (_name.substr(_name.size() - 2, 2) == integer) {
      _name.erase(_name.size() - 2, _name.size() -1);
      break;
    }
  }
} 

io_manager::io_manager(
  std::string name, std::string event_type,
  io_manager* background_io, std::string ntuple_path,
  const std::vector<line_manager>& lines, 
  const fixedlines_t& fixed_inputs,
  const linemask_t& partially_fixed_lines,
  const linemask_t& fully_fixed_lines,
  const linemask_t& off_lines,
  const parammask_t& fixed_line_params,
  double special,
  double collision_rate, double ratelimit, double cutoff,
  double scalewidth, int accuracy, int max_events,
  int num_signal_tracks, bool reconstructable_only
  ) : io_manager(name, event_type, ntuple_path, lines,
  fixed_inputs, partially_fixed_lines, fully_fixed_lines,
  off_lines, fixed_line_params,
  num_signal_tracks, reconstructable_only) {

  // Determine if sample needs minimising (not_loosest threshold),
  // reassign efficiency denominator if reconstructable only,
  // remove excess candidates above ratelimit, and move parameter boundaries
  // accordingly.

  _special = special;
  _parameter_values = {};
  _background_io = background_io; 
  
  _unique_precut_events = reconstructable_only
  ? _total_reconstructable_events : _total_allen_events;
  
  unsigned old_size = _events.size(); 
  _collision_rate = collision_rate;

  bool not_loosest{false};
  if (_s_or_b) {
    for (unsigned i{0}; i < NUM_LINES; i++) {
      if (_activate_lines[i]
          && !(_background_io->_per_line_loosest[i])) {
        not_loosest = true;
        break;
      }
    }
    _ratelimit = ratelimit;
  } else {
    not_loosest = true;
    // Convert ratelimit to fraction rather than rate in hertz
    _ratelimit = ratelimit/collision_rate;
    // Lines where the parameters are fixed
  }

  _loosest_selection = !not_loosest;

  _parallelise_events = true;

  remove_excess_candidates(collision_rate, ratelimit, cutoff, max_events,
  reconstructable_only);

  _parallelise_events = false;

  std::cout << "Number of events for " << _filesafe_name
  << " before excess removed: " 
  << old_size << " |  After excess removed: " << (int)_events.size() << "\n";

  if (!_s_or_b) {
    move_param_boundaries();
  }

  _maxeff_minimiser =
  new init_fom_minimiser(this, collision_rate, ratelimit);

  // This instantiation disables certain lines for the initfnc, and will be
  // reactivated in init_opt_gradient. Make sure this comes after the rest
  // of the constructor operations
  if (_s_or_b) {
    // Placeholder. These pointers will be reassigned if needed.
    _maxeff_adam = new init_discrete_gradient(this, collision_rate,
    ratelimit, scalewidth, accuracy);

    _specline_adam =
    new init_discrete_gradient(this, collision_rate, ratelimit,
    scalewidth, accuracy);
  }
}

void io_manager::line_builder(
  const line_manager& line, line_parameters params,
  std::map<std::pair<unsigned long, unsigned int>,
  std::pair<std::vector<std::vector<float>>,
  std::vector<int>>>& precut_idmap
  ) {
  // Fill maps for candidate monitored quantities (to be input into lines)
  // and event and run number versus monitored quantities and line number.
  // Also fill associated params and their mins, maxs, grid resolution and
  // gradient resolution 

  std::vector<std::vector<float>> quantities(
  line._monitored_quantities.size());

  // Add root:://eoslhcb.cern.ch/ to path
  auto append_mgm_url = [](const std::string& str){
    return str.find("/eos") == 0 ? "root://eoslhcb.cern.ch/" + str : str;
  };
 
  ROOT::EnableImplicitMT(); 
  const unsigned int number_of_slots = ROOT::GetThreadPoolSize();
  std::vector<TRandom3> randomisers(number_of_slots);
  for(unsigned i{0} ; i!= randomisers.size(); ++i) {
    randomisers[i].SetSeed(i);
  }

  // Dataframe containing line quantities
  auto data_node = ROOT::RDF::RNode(ROOT::RDataFrame(line._tree,
  append_mgm_url(_ntuple_path)));

  if (line._filter.find("PRESCALE") != std::string::npos) {
    double value = std::stod(split(line._filter, '=')[1]);
    // Remove some events at random according to the prescale
    data_node = data_node.Filter([value, randomisers](unsigned slot) mutable {
    return randomisers[slot].Uniform() < value;}, {"rdfslot_"});
  } else if (line._filter != "") {
    data_node = data_node.Filter(line._filter);
  }

  // Create vector string from monitored quantities
  std::string argv = "std::vector<float>{";
  for(unsigned i{0}; i != quantities.size(); ++i) {
    argv += line._monitored_quantities[i]
    + (i == quantities.size() - 1 ? "} " : ",");
  }

  using event_t = std::tuple<unsigned long, unsigned, std::vector<float>>; 

  std::vector<std::vector<event_t>> events_per_thread(number_of_slots); 
  unsigned number_of_events = *data_node.Count(); 
  for(unsigned int i = 0 ; i != number_of_slots; ++i) {
    events_per_thread[i].reserve(2 * number_of_events / number_of_slots);
  }

  data_node.Define("args" , argv).ForeachSlot([&](const unsigned slot, 
  const unsigned long& event_number, const unsigned& run_number, 
  const std::vector<float>& args) mutable {
  //std::cout << slot << " " << event_number << " "
  // << run_number << " " << args[0] << std::endl; 
  events_per_thread[slot].emplace_back(event_number, run_number, args); 
  }, { line._name + "__evtNo_t", line._name +"__runNo_t", "args"});

  std::set< std::pair<unsigned long, unsigned>> unique_events; 
  for(const auto& events : events_per_thread){
    for(const auto& [event_num, run_num, candidate] : events){
      const auto id = std::make_pair(event_num, run_num); 
      unique_events.insert(id);
      std::get<0>(precut_idmap[id]).push_back(candidate);
      std::get<1>(precut_idmap[id]).push_back(line._line_number); 
    }
  }

  int num_candidates_per_line = number_of_events; 
  if (!_s_or_b) {
    std::cout << line._name 
    << " -  Number of candidates: " << num_candidates_per_line
    << " | Number of events: " << unique_events.size() << std::endl; 
  }

  for (auto [name, parameter] : line._parameters) {
    // if (!_s_or_b) {
    //   std::cout << "Activating " << name << " for " << line._name << "\n";
    // }
    _parameters[name] = parameter;
  }
} 


void io_manager::reconstructable_events_and_axis_builder(
  std::map<std::pair<unsigned long, unsigned int>,
  std::pair<std::vector<std::vector<float>>, std::vector<int>>>&
  precut_idmap, bool reconstructable_only
  ) {
  // Build vector of event objects, add number of tracks from
  // nReconstructible branch, and build parameter axis labels

  // We have one map with event keys corresponding to a vector of vectors
  // The vector of vectors contain rows of candidates and columns of
  // monitored quantities for that line. The candidates are stored in
  // blocks.
  // Each block pertains to one line in the same order as the _lines member.
  // std::cout << "Number of map events: " << precut_idmap.size() << "\n";

  for (const auto& [key, event] : precut_idmap){
    std::array<int, NUM_LINES> candidates_per_line= {0};
    for (unsigned i{0}; i < std::get<1>(event).size(); i++) {
      candidates_per_line[std::get<1>(event)[i]]++;
    }
    // Event number, run number, monitored quantities
    _events.emplace_back(std::get<0>(key), std::get<1>(key),
    std::get<0>(event), _linestore._quantities_per_line,
    candidates_per_line);
  }

  if (reconstructable_only) {
    TFile* ntuple_file = TFile::Open(_ntuple_path.c_str(), "READ");
    bool reconstructable_dir_exists =
    ntuple_file->GetDirectory("MCReconstructibleInfo");

    if (reconstructable_dir_exists) {
      // Event and run numbers
      using key_t = std::pair<unsigned long, unsigned>;
      std::map<key_t, event_store*> event_map; 
      for(auto& event : _events)  {
        event_map[std::make_pair(event._event_number, event._run_number)]
        = &event;
      }

      ROOT::EnableImplicitMT();
      ROOT::RDataFrame signal_track_rdf("MCReconstructibleInfo/monitor_tree",
      _ntuple_path.c_str());
      
      std::vector<std::set<key_t>> all_events(ROOT::GetThreadPoolSize()); 

      auto signal_track_lambda_exp =
      [this, event_map, &all_events](
      unsigned slot, unsigned long event_num,
      unsigned run_num, unsigned num_reconstructible_events
      ) mutable {
        auto it = event_map.find(std::make_pair(event_num, run_num));
        if (it != event_map.end()) {
          it->second->_num_tracks = num_reconstructible_events;
        }

        if (num_reconstructible_events >=
        unsigned(this->_num_signal_tracks)) {
          all_events[slot].insert(std::make_pair(event_num, run_num));
        }
      };


      signal_track_rdf.ForeachSlot(signal_track_lambda_exp,
      {"eventNumber","runNumber", "nReconstructible"});

      _total_reconstructable_events = std::accumulate(all_events.begin(),
      all_events.end(), 0, [](unsigned v, auto& s){return v + s.size();});
    } else {
      // If no reconstructable event branch, set all tracks to a large
      // number so all events pass the threshold.
      _total_reconstructable_events = _total_allen_events;
      for (unsigned i = 0; i < _events.size(); i++) {
        _events[i]._num_tracks = 100000;
      }
    }
    ROOT::DisableImplicitMT();
  }

  line_parameters params;
  if (!_s_or_b) {
    _parameter_line_counts = std::vector<int>(params._num_parameters);
    _parameter_line_masks = std::vector<linemask_t>(NUM_LINES);
    for (int i{0}; i < params._num_parameters; i++) {
      _parameter_line_counts[i] = 0;
      for (unsigned j{0}; j < _linestore._linenames.size(); j++) {
        _parameter_line_masks[i][j] = false;
      }
    }
  }

  // std::cout << "\nForming axes and counting lines per parameter\n";
  // All non-repeating parameters used to tune the lines
  for (int i{0}; i < params._num_parameters; i++) {
    if (_activate_params[i]) {
      std::string paramname = params._paramnames[i];
      std::string axis = paramname + "_";
      for (unsigned j{0}; j < _linestore._linenames.size(); j++) {
        for (unsigned k{0}; k < _lines.size(); k++) {
          // Make sure line being selected is in same order as they appear in
          // tuple
          if (_linestore._linenames[j].compare(_lines[k]._name) == 0) {
            auto line = _lines[k];
            for (unsigned l{0}; l < line._parameters.size(); l++) {
              if (std::get<0>(line._parameters[l]).compare(paramname) == 0) {
                if (!_s_or_b) {
                  _parameter_line_counts[i]++;
                  _parameter_line_masks[i][j] = true;
                  // std::cout << "Line count for " << paramname << ": "
                  // << _parameter_line_counts[i] << " after " << line._name
                  // << "\n";
                }

                axis += line._name + "_";
                break;
              }
            }
            break;
          }
        }
      }

      axis.pop_back();
      _axes.push_back(axis);
    }
  }

  // Fixing/switching off parameters if all associated lines are
  // fixed/switched off
  if (!_s_or_b) {
    std::cout << "\n";
    for (int i{0}; i < params._num_parameters; i++) {
      int num_fixed_lines{0};
      
      for (unsigned j{0}; j < _linestore._linenames.size(); j++) {
        if (_parameter_line_masks[i][j] && _fully_fixed_lines[j]) {
          num_fixed_lines++;
          if (num_fixed_lines >= _parameter_line_counts[i]) {
            std::cout << "All lines for " << params._paramnames[i]
            << " are fixed. Fixing parameter to \n";
            
            for (unsigned k{0}; k < _lines.size(); k++) {
              if (_linestore._linenames[j].compare(_lines[k]._name) == 0) {
                auto line = _lines[k];
                
                for (unsigned l{0}; l < line._parameters.size(); l++) {
                  if (params._paramnames[i].compare(
                  std::get<0>(line._parameters[l])) == 0) {
                    std::get<1>(line._parameters[l])._fixed = true;
                    _parameters[params._paramnames[i]]._fixed = true;
                    
                    if (_off_lines[j]) {
                      _parameters[params._paramnames[i]]._value =
                      _parameters[params._paramnames[i]]._max;
                      std::get<1>(line._parameters[l])._value =
                      _parameters[params._paramnames[i]]._max;
                      _parameters[params._paramnames[i]]._off = true;
                    } else {
                      _parameters[params._paramnames[i]]._value =
                      _fixed_inputs[j][i];
                      std::get<1>(line._parameters[l])._value =
                      _fixed_inputs[j][i];
                    }
                    std::cout << _parameters[params._paramnames[i]]._value
                    << "\n";
                    break;
                  }
                }
                break;
              }
            }
          } 
        }
      }
      // std::cout << "Number of fixed lines for " << params._paramnames[i]
      // << ": " << num_fixed_lines << "\n";
    }
  }
}


void io_manager::move_param_boundaries() {
  // Move parameter boundaries in zero-bias tuple (which is where they are
  // always read from) to match the new cutoffs from
  // remove_excess_candidates(...) (above which the rate is too high above
  // the ratelimit)

  io_manager* background = _s_or_b ? _background_io : this;
  line_parameters nearest = find_nearest_point(_cutoff_params);
  for (unsigned i{0}; i < nearest.size(); i++) {
    auto parameter = background->_parameters[nearest._paramnames[i]];
    if (parameter._fixed || parameter._off) continue;

    double param_resolution = parameter.resolution(); 
    // std::cout << "Nearest, cutoff before paramchange: "
    // << nearest[i] << ", " << _cutoff_params[i] << "\n";
    if (nearest[i] > background->_cutoff_params[i]) nearest[i] -=
    param_resolution;

    int minus_steps = round(fabs((nearest[i] - parameter._min)
    / param_resolution));
    std::cout << "\nUnique min, grid resolution, nsteps, nsteps gradient"
    " for " << nearest._paramnames[i] << " before: " << parameter._min 
    << ", " << param_resolution << ", "
    << parameter._steps << ", " << parameter._grad_steps << "\n";

    double gradient_to_grid_ratio =
    1.0 * parameter._grad_steps / parameter._steps;

    parameter._min = nearest[i];
    parameter._steps -= minus_steps;
    parameter._grad_steps -= round(gradient_to_grid_ratio * minus_steps);
    param_resolution = parameter.resolution(); 

    std::cout << "\nUnique min, grid resolution, nsteps, nsteps gradient"
    " for " << nearest._paramnames[i] << " after: " << parameter._min 
    << ", " << param_resolution << ", " << parameter._steps << ", "
    << parameter._grad_steps << "\n";

    for (line_manager& input_line: _lines) {
      for (auto& [name,new_param] : input_line._parameters) { 
        if (name == nearest._paramnames[i]) {
          new_param._min = parameter._min;
          new_param._steps = parameter._steps;
          new_param._grad_steps = parameter._grad_steps;
          break;
        }
      }
    }
  }

  _parameter_values.clear();
  fill_parameter_values();
}



line_parameters io_manager::find_nearest_point(
  const line_parameters& input_parameters
  ) const {
  // Find nearest point on discrete grid to continuous coordinates.
  // (For final minimisation point.)

  line_parameters nearest_coordinates;
  const io_manager* background = _s_or_b ? _background_io : this;
  for (unsigned i{0}; i < input_parameters.size(); i++) {
    if (background->_parameters.at(input_parameters._paramnames.at(i))._fixed
    || background->_parameters.at(input_parameters._paramnames.at(i))._off) {
      nearest_coordinates[i] =
      background->_parameters.at(input_parameters._paramnames.at(i))._value;
    } else {
      std::vector<double> possible_values =
      background->_parameter_values.at(i);

      std::vector<double> differences;
      for (unsigned j{0}; j < possible_values.size(); j++) {
        differences.push_back(fabs(input_parameters.access(i)
        - possible_values.at(j)));
      }

      // Smallest difference yields closest point on parameter axis
      auto it =
      std::min_element(std::begin(differences), std::end(differences));

      int minindex = std::distance(std::begin(differences), it);
      nearest_coordinates[i] = possible_values.at(minindex);
    }
    // std::cout << "Rounding " << input_parameters._paramnames.at(i)
    // << " from "
    // << input_parameters.access(i) << " to "
    // << nearest_coordinates.access(i)
    // << "\n";
  }
  return nearest_coordinates;
}

std::vector<std::string> modern_glob(const std::string& pattern) {
  // Finds pathnames matching a certain pattern
  // (like the signal and zero-bias root files)

  std::vector<std::string> return_vector; 
  glob_t result;
  // Copies 0 into the first sizeof(result) characters of result
  memset(&result, 0, sizeof(result));
  auto status = glob(pattern.c_str(), GLOB_TILDE, NULL, &result);
  if (status != 0) return return_vector;

  for(unsigned i = 0 ; i != result.gl_pathc; ++i) {
    return_vector.emplace_back(result.gl_pathv[i]);
  }

  globfree(&result);

  return return_vector;
}

void io_manager::find_absolute_yield(std::string ntuple_path) {
  // For the denominator on the efficiency calculations

  auto path = std::filesystem::path(ntuple_path).parent_path();
  
  // to do -> switch to more efficient json
  auto logs = modern_glob(path.string() + "/*.log");
  for(const auto& logfile : logs) {
    std::string line{""};
    std::ifstream logstream(logfile);
    while (getline(logstream, line)) {
      if (line.find("Inclusive:") == std::string::npos) continue; 

      std::istringstream ss(line);
      std::vector<std::string> line_vec;
      std::copy(std::istream_iterator<std::string>(ss), 
          std::istream_iterator<std::string>(),
          std::back_inserter(line_vec));

      if (!line_vec.empty() && line_vec[0].compare("Inclusive:")==0) {
        std::string denom;
        if (line_vec[1].back() == '/') {
          denom = line_vec[2];
        } else {
          denom = line_vec[1].substr(line_vec[1].find("/") + 1,
              line_vec[1].size() - line_vec[1].find("/") + 1);
        }

        if (denom.back() == ',') {
          denom.erase(denom.size()-1, denom.size());
        }

        _total_allen_events += std::stoi(denom);
        break;
      }
    }
  }
}


int io_manager::find_absolute_yield_per_line(
  std::string ntuple_path, std::string line_name
  ) {
  auto path = std::filesystem::path(ntuple_path).parent_path();     
  // to do -> switch to more efficient json 
  auto logs = modern_glob(path.string() + "/*.log");
  int yield_per_line{0};
  for(const auto& logfile : logs) 
  {
    std::ifstream logstream(logfile);
    std::string line;
    while (getline(logstream, line)) {
      if (line.find(line_name) == std::string::npos) continue; 
      
      std::istringstream ss(line);
      std::vector<std::string> line_vec;
      std::copy(std::istream_iterator<std::string>(ss), 
          std::istream_iterator<std::string>(),
          std::back_inserter(line_vec));

      if (!line_vec.empty() && line_vec[0].compare(line_name + ":") == 0) {
        std::string numerator;
        if (line_vec[1].back() == '/') {
          numerator = line_vec[1];
          numerator.erase(numerator.size() - 1, numerator.size());
        } else {
          numerator = line_vec[1].substr(0, line_vec[1].find("/"));
        }

        yield_per_line += std::stoi(numerator);
      }
    }
  }
  return yield_per_line;
}

void io_manager::fill_parameter_values() {
  // Filling parameter value vector for FoM scan in discrete range

  io_manager* tuple = _s_or_b ? _background_io : this;
  _parameter_values =
  std::vector<std::vector<double>>(tuple->_parameters.size());
  if (!_s_or_b) {
    // std::cout << "Minbias _parameters size: " << tuple->_parameters.size()
    // << "\n";
  }

  // All zero-bias parameters
  for (const auto& [k, parameter] : _parameters) {
    if (!_s_or_b) {
      // std::cout << "Filling parameter values for " << k << "...\n";
    }

    if (!(parameter._fixed || parameter._off)) {
      double param_min = parameter._min;
      double param_max = parameter._max;
      double param_resolution = parameter.resolution(); 
      double paramsign = sign(param_resolution);

      std::vector<double> values = {};
      double param_append{param_min};
      bool less_than_interval =
      abs(param_resolution) < abs(param_max - param_min);

      // std::cout << "param_append: " << param_append << "\n";
      values.push_back(param_min);

      // It doesn't seem to be recognising in some cases that
      // param_append == param_max +/- param_resolution. -/+ 1e-10 makes sure
      // that this is the case.
      bool add_value{true};
      while (add_value && less_than_interval) {
        param_append += param_resolution;
        values.push_back(param_append);
        // std::cout << "param_append: " << param_append << "\n";
        add_value = (paramsign > 0.0) ?
          (param_append < param_max + param_resolution - 1e-8)
          : (param_append > param_max + param_resolution + 1e-8);
      }

      if ((paramsign > 0.0 && values.back() > param_max)
          || ((paramsign < 0.0) && values.back() < param_max)) {
        values.pop_back();
      }
      auto index = global_parameter_index(k); 
      // Only add if the parameter is active in this channel
      _parameter_values[index] = _activate_params[index]
      ? values : std::vector<double>{};
    } else {
      auto index = global_parameter_index(k); 
      // Only add if the parameter is active in this channel
      _parameter_values[index] = std::vector<double>{};

    }
  }
  // std::cout << "\nFinished filling\n";
}


double io_manager::specline_unique_events(
  const line_parameters& input_parameters, const int& lineindex
  ) {
  // Calculating unique sample events for specific line where
  // Ninputs != Nparams in all cases. For individual line efficiency
  // plot with optimal parameters (after minimisation)

  line_tuple linetuple =
  _linestore.make_lines(_fixed_inputs, _partially_fixed_lines, _off_lines,
  _fixed_line_params);
  auto set_line_parameters =
  [&](auto& line){line.set_parameters(input_parameters);}; 
  tuple_process::for_each(linetuple, set_line_parameters);

  tuple_process::for_each(linetuple, set_line_parameters);
  get_element_by_type<twotrackks>(linetuple).prepare_twotrackks();

  int specline_count = 0;
#pragma omp parallel for reduction(+ : specline_count) if (_parallelise_events)
  for (const event_store& event: _events) {
    if (event.pass_decision_specline(lineindex,
    linetuple)) {
      specline_count++;
    }
  }
  return (double)specline_count;
}


std::vector<int> io_manager::events_unique_to_lines(
  const line_parameters& params, const linemask_t& activated_lines
  ) {

  line_tuple linetuple =
  _linestore.make_lines(_fixed_inputs, _partially_fixed_lines, _off_lines,
  _fixed_line_params);
  auto set_line_parameters =
  [&](auto& line){line.set_parameters(params);}; 

  tuple_process::for_each(linetuple, set_line_parameters);
  get_element_by_type<twotrackks>(linetuple).prepare_twotrackks();

  std::vector<int> unique_events_per_line(NUM_LINES, 0);
  for (unsigned i = 0; i < _events.size(); i++) {
    auto which_lines = _events[i].which_lines(activated_lines, linetuple);
    int number_of_triggered_lines{0};
    int which_index{0};

    for (unsigned j{0}; j < which_lines.size(); j++) {
      if (which_lines[j]) {
        number_of_triggered_lines++;
        which_index = j;
      }
    }

    if (number_of_triggered_lines == 1) {
      unique_events_per_line[which_index]++;
    }
  }

  return unique_events_per_line;
}


double io_manager::calc_unique_events(
  const line_parameters& input_parameters,
  const linemask_t& activated_lines
  ) {
  // Calc number of unique events from a certain set of inputs
  
  line_tuple linetuple =
  _linestore.make_lines(_fixed_inputs, _partially_fixed_lines, _off_lines,
  _fixed_line_params);
  auto set_line_parameters =
  [&](auto& line){line.set_parameters(input_parameters);}; 
  tuple_process::for_each(linetuple, set_line_parameters);
  
  get_element_by_type<twotrackks>(linetuple).prepare_twotrackks();

  int unique_count = 0;
#pragma omp parallel for reduction(+ : unique_count) if (_parallelise_events)
  for (unsigned i = 0; i < _events.size(); i++) {
    if (_events[i].pass_decision(activated_lines,
    linetuple)) unique_count++;
  }
  return (double)unique_count;
}


std::vector<int> io_manager::calc_candidates_per_line(
  const line_parameters& input_parameters,
  const linemask_t& activated_lines
  ) {
  // Calc number of candidates passing per line for a set of inputs

  line_tuple linetuple =
  _linestore.make_lines(_fixed_inputs, _partially_fixed_lines, _off_lines,
  _fixed_line_params);
  auto set_line_parameters =
  [&](auto& line){line.set_parameters(input_parameters);}; 

  tuple_process::for_each(linetuple, set_line_parameters);
  get_element_by_type<twotrackks>(linetuple).prepare_twotrackks();

  std::vector<int> candidates_per_line;
  for (bool active: activated_lines) {
    if (active) {
      candidates_per_line.push_back(0);
    }
  }

  for (const event_store& event: _events) {
    std::vector<int> temp_candidates_per_line =
    event.postcut_candidates_per_line(activated_lines,
    linetuple);

    for (unsigned i{0}; i < temp_candidates_per_line.size(); i++) {
      candidates_per_line[i] += temp_candidates_per_line[i];
    }
  }

  return candidates_per_line;
}

