#include "misc_tools.h"
#include "io_manager.h"
#include "line_manager.h"
#include "line_tools.h"
#include "init_discrete_gradient.h"
#include "opt_discrete_gradient.h"
#include "init_save_and_collate.h"
#include "opt_save_and_collate.h"


std::vector<std::vector<double>> generate_input_param_store(
  io_manager* tuple, int num_results, int paramindex0, int paramindex1
  ) {
  // Generating sets of input params to calculate results for N-dimensional
  // parameter space, where N is the number of parameters being tuned.

  std::vector<std::vector<double>> input_param_store;
  line_parameters params;
  input_param_store.reserve(num_results);
  int num_parameters = (int)params.size();
  std::vector<int> parameter_set_indexes(num_parameters, 0);

  std::vector<int> final_values;
  std::vector<std::vector<double>> poss_values;
  auto& min_bias = tuple->_background_io; 
  for (int i{0}; i < num_parameters; i++) {
    std::vector<double> max{
    min_bias->_parameters[params._paramnames[i]]._max};
    std::vector<double> values =
    (i == paramindex0 || i == paramindex1)
    ? min_bias->_parameter_values[i] : max;
    poss_values.push_back((values));
    final_values.push_back(poss_values[i].size() - 1);
  }

  bool stop_cycling{false};
  bool stop_next{false};
  while (!stop_cycling) {
    std::vector<double> input_params;
    for (int i{0}; i < num_parameters; i++) {
      input_params.push_back(
      poss_values[i][parameter_set_indexes[i]]);
    }
    input_param_store.push_back(input_params);
    
    for (int i{num_parameters-1}; i > -1; i--) {
      if (parameter_set_indexes[i] <  final_values[i]) {
        parameter_set_indexes[i]++;
        break;
      } else {
        parameter_set_indexes[i] = 0;
      }
    }
    stop_cycling = stop_next;
    stop_next = true;
    for (int i{0}; i < num_parameters; i++) {
      if (parameter_set_indexes[i] != final_values[i]) {
        stop_next = false;
        break;
      }
    }
  }

  return input_param_store;   
}


void phase_space_scan(
  std::vector<io_manager*> tuples, double collision_rate,
  double ratelimit, int paramindex0, int paramindex1
  ) {
  // Generate efficiencies for all samples, each receiving a range of
  // cuts on variable numbers of parameters

  if (tuples[0]->_background_io->_parameter_values.empty()) {
    tuples[0]->_background_io->fill_parameter_values();
  }
  int num_results = (int)
  tuples[0]->_background_io->_parameter_values[paramindex0].size() *
  tuples[0]->_background_io->_parameter_values[paramindex1].size();

  std::cout << "Num results: " << num_results << "\n";
  tuples[0]->fill_parameter_values();
  std::vector<std::vector<double>> input_param_store =
  generate_input_param_store(tuples[0], num_results, paramindex0,
  paramindex1);
  

  if (tuples[0]->_parameter_values.size() < 1) {
    std::cout << "There are no parameter values to calculate Fom "
    "for!\n";
    return;
  }

  std::cout << "Tuple properties: \n";
  tuples[0]->print();

  tuples[0]->calc_results(num_results, input_param_store,
  collision_rate, ratelimit);
}


double ratelimiter(
  double trigger_efficiency, double ratelimit
  ) {
  // Calc physics deadtime

  return trigger_efficiency < ratelimit ?
  1.0 : ratelimit/trigger_efficiency;
}



void plot_results_vs_lineor_cut(
  std::vector<std::string> plot_strings,
  std::vector<double> first_cut_vector,
  std::vector<double> second_cut_vector, std::vector<double> results,
  double plot_phi, double plot_theta, std::string draw_option
  ) {
  // Plotting the efficiency of an OR cut from two cut vectors.

	std::string ntuple_name, first_cut_variable, second_cut_variable,
  result_axis, plot_dir;

	ntuple_name = plot_strings[0];
  first_cut_variable = plot_strings[1];
  second_cut_variable = plot_strings[2];
  first_cut_variable[0] = toupper(first_cut_variable[0]);
  second_cut_variable[0] = toupper(second_cut_variable[0]);
  result_axis = plot_strings[3];
	plot_dir = plot_strings[4];

  if (first_cut_vector.size() < 3) {
    std::cout << "Insufficient data points to plot data\n";
    return;
  } else {
    std::string result_axis_copy = result_axis;
    std::replace(result_axis_copy.begin(), result_axis_copy.end(), ' ',
    '_');
    std::string plot_name = result_axis_copy + "_vs_cuton_"
    + first_cut_variable + "_" + second_cut_variable + "_for_"
    + ntuple_name;
    std::string plot_title;
    bool topdown{false};
    if (draw_option.compare("surf1") != 0 && !plot_strings[5].empty()) {
      plot_title = plot_strings[5];
      plot_dir = plot_strings[6];
      topdown = true;
    } else {
      plot_title = result_axis + " vs. Cuts on "
      + first_cut_variable + " and " + second_cut_variable + " for "
      + ntuple_name;
    }

    std::string canvas_dir = plot_dir + result_axis_copy
    + "_plots/";
    std::filesystem::create_directories(canvas_dir.c_str());
    TCanvas* canvas = new TCanvas(plot_name.c_str(), plot_title.c_str(),
    0, 0, 600, 400);
    canvas->cd();

    gStyle->SetPalette(1);
    TGraph2D* twod_distribution = new TGraph2D(
    (int)results.size(), &first_cut_vector[0],
    &second_cut_vector[0], &results[0]);
    twod_distribution->SetTitle((plot_title + ";" + first_cut_variable + ";"
    + second_cut_variable + ";" + result_axis).c_str());
    twod_distribution->SetMarkerStyle(20);
    if (topdown) {
      twod_distribution->SetMarkerSize(2);
      twod_distribution->SetMarkerStyle(kFullCircle);
    }

    twod_distribution->Draw(draw_option.c_str());
    // Save to dedicated plot directory
    canvas->SetPhi(plot_phi);
    canvas->SetTheta(plot_theta);
    canvas->SaveAs((canvas_dir + plot_name + ".tex").c_str());
    canvas->SaveAs((canvas_dir + plot_name + ".C").c_str());
    canvas->SaveAs((canvas_dir + plot_name + ".pdf").c_str());
    canvas->SaveAs((canvas_dir + plot_name + ".svg").c_str());
    delete twod_distribution;
    delete canvas;
  }
}


void plot_sigeff_vs_backrate(
  io_manager* tuple, std::string plot_dir, double collision_rate,
  double ratelimit
  ) {
  // Plotting signal efficiency vs. background rate to determine
  // ratelimiter dropoff. Only makes sense if cuts on the signal
  // are the same as the background. Only works if GA minimiser
  // class was used

  if (!tuple->_s_or_b) return;

  std::string tuplename = tuple->_name;
  std::string filesafe_name = tuple->_filesafe_name;
  std::string backname = tuple->_background_io->_name;
  std::string filesafe_backname = tuple->_background_io->_filesafe_name;

  std::string plot_name = filesafe_name + "_sigeff_vs_backrate";
  std::string plot_title = "SigEff For " + tuplename +
  + " vs. BackRate for " + backname + " with Cuts on: ";
  for (const auto& [k,v] : tuple->_parameters ) {
    plot_title += k + ",";
  }
  plot_title.pop_back();
  std::string canvas_dir = plot_dir + tuple->_filesafe_name
  + "_sigeff_vs_backrates/";
  std::filesystem::create_directories(canvas_dir.c_str());
  
  TCanvas* canvas = new TCanvas(plot_name.c_str(), plot_title.c_str(), 0, 0,
  600, 400);
  canvas->cd();
  TGraph* plot = new TGraph(
  (int)tuple->_maxeff_minimiser->_ratelim_signal_efficiencies.size(),
  &tuple->_maxeff_minimiser->_ratelim_signal_efficiencies[0],
  &tuple->_maxeff_minimiser->_background_rates[0]);
  plot->SetTitle((plot_title
  + ";Ratelimited Signal Efficiency;Background Rate").c_str());
  plot->Draw("ap");
  canvas->SaveAs((canvas_dir + plot_name + ".tex").c_str());
  canvas->SaveAs((canvas_dir + plot_name + ".C").c_str());
  canvas->SaveAs((canvas_dir + plot_name + ".pdf").c_str());
  canvas->SaveAs((canvas_dir + plot_name + ".svg").c_str());
}


void calc_minbias_rate_vs_cut(
  unsigned i, unsigned j, io_manager* minbias, line_store& linestore,
  std::vector<std::vector<double>>& param_inputs,
  std::vector<std::vector<double>>& rates,
  std::vector<std::vector<double>>& rate_errs,
  double collision_rate
  ) {
  // Save rate for cut on jth axis value of ith minbias parameter

  line_parameters params;
  if (minbias->_parameter_values[i].empty()) return;
  params[i] = minbias->_parameter_values[i][j];
  linemask_t activate_lines;
  // For all lines
  for (unsigned k{0}; k < linestore._associated_params.size(); k++) {
    std::vector<std::string> assoc_params = linestore._associated_params[k];
    // For all params input into this line
    bool associated =
    std::find(assoc_params.begin(),
    assoc_params.end(),
    params._paramnames[i] ) != assoc_params.end(); 
    if (associated && assoc_params.size() > 1) {
      // Set other input params to loosest value
      for (unsigned l{0}; l < assoc_params.size(); l++) {
        if (params._paramnames[i].compare(assoc_params[l])
        != 0) {
          for (unsigned m{0}; m < params.size(); m++) {
            if (params._paramnames[m].compare(
            assoc_params[l]) == 0) {
              params[m] = minbias->_parameters[params._paramnames[m]]._min;
            }
          } 
        }
      }
    }
    // Activate or deactivate line
    activate_lines[k] = associated;
    // FIX: indexing here seems wrong
  }
  // Convert alpha back
  double back_eff = 
  (minbias->calc_unique_events(params, activate_lines)
  / minbias->_unique_precut_events);
  double rate = back_eff * (collision_rate / 1e6);
  double rate_err =
  sqrt(fabs(back_eff * (1.0 - back_eff))
  / (1.0 * minbias->_unique_precut_events)) * (collision_rate / 1e6);
  if (rate <= 3.0) {
    double param_input =
    params._paramnames[i].find("alpha")
    != std::string::npos ? alpha_transform(params[i])
    : params[i];
    if (params._paramnames[i].compare("twotrack_ks") == 0) {
      twotrackks().print_thresholds(params);
    }
    // std::cout << "Param input and rate: " << param_input << ", " << rate
    // << " MHz\n";
    // Remove duplicate rates
    param_inputs[i].push_back(param_input);
    rates[i].push_back(rate);
    rate_errs[i].push_back(rate_err);
    long unsigned ratesize{rates[i].size()};
    if (ratesize > 1 && rate == rates[i][ratesize - 2]) {
      rates[i].erase(rates[i].begin() + ratesize - 2,
      rates[i].begin() + ratesize);
      rate_errs[i].erase(rate_errs[i].begin() + ratesize - 2,
      rate_errs[i].begin() + ratesize);
      param_inputs[i].erase(param_inputs[i].begin() + ratesize - 2,
      param_inputs[i].begin() + ratesize);
    }
  }
}


void compute_efficiency(
  unsigned i, unsigned j, unsigned k,
  std::vector<io_manager*> tuples,
  std::vector<std::string> paramnames, io_manager* minbias,
  std::vector<std::vector<double>> rates,
  std::vector<std::vector<double>>& spec_param_inputs,
  std::vector<std::vector<double>>& efficiencies,
  std::vector<std::vector<double>>& efficiency_errs,
  line_store& linestore, int param_index
) {
  // calculate efficiency of the ith signal sample for the kth
  // axis value of the jth minbias parameter

  line_parameters params;
  if (minbias->_parameter_values[j].empty()) {
    std::cout << "Parameter scan values vector is empty. Skipping...\n";
    return;
  }
  params[j] =
  tuples[i]->_background_io->_parameter_values[j][k];
  linemask_t activate_lines;
  // For all lines
  for (unsigned l{0};
  l < linestore._associated_params.size(); l++) {
    std::vector<std::string> assoc_params = linestore._associated_params[l];
    // For all params input into this line
    bool associated{false};
    for (std::string split_param: assoc_params) {
      if(params._paramnames[j] == split_param) {
        // std::cout << params._paramnames[j] << " is associated with "
        // << split_param << ", including loosest threshold in scan\n";
        associated = true;
        break;
      }
    }
    if (assoc_params.size() > 1 && associated) {
      // Set other input params to loosest value
      for (unsigned m{0}; m < assoc_params.size(); m++) {
        if (params._paramnames[j] == assoc_params[m]) {
          for (unsigned n{0}; n < params.size(); n++) {
            if (params._paramnames[n] == assoc_params[m]) {
              if (minbias->_parameters[params._paramnames[n]]._fixed) {
                // std::cout << "Refixing " << params._paramnames[n] << "...\n";
                params[n] =
                minbias->_parameters[params._paramnames[n]]._value;
              } else if (params._paramnames[n] != params._paramnames[j]) {
                // std::cout << "Setting " << params._paramnames[n]
                // << " to loosest value...\n";
                params[n] = minbias->_parameters[params._paramnames[n]]._min;
              }
            }
          } 
        }
      }
    }
    // Activate or deactivate line
    activate_lines[l] = associated;
  }
  // Convert alpha back

  if (rates[j][k] <= 3.0) {
    // std::cout << "Rate: " << rates[j][k] << "\n";
    double param_input =
    params._paramnames[j].find("alpha")
    != std::string::npos ? alpha_transform(params[j])
    : params[j];
    double efficiency = 
    tuples[i]->calc_unique_events(params, activate_lines)
    / tuples[i]->_unique_precut_events;
    double efficiency_err =
    sqrt(fabs(efficiency * (1.0 - efficiency))
    / (1.0 * tuples[i]->_unique_precut_events));

    // std::cout << "Efficiency: " << efficiency << " +/- " << efficiency_err
    // << "\n";

    spec_param_inputs[param_index].push_back(param_input);
    efficiencies[param_index].push_back(efficiency);
    efficiency_errs[param_index].push_back(efficiency_err);
    
    long unsigned efficiency_size{efficiencies[param_index].size()};

    if (k > 0 && rates[j][k] == rates[j][k-1]) {
      efficiencies[param_index].erase(efficiencies[param_index].begin()
      + efficiency_size - 2,
      efficiencies[param_index].begin() + efficiency_size);
      
      efficiency_errs[param_index].erase(efficiency_errs[param_index].begin()
      + efficiency_size - 2,
      efficiency_errs[param_index].begin() + efficiency_size);

      spec_param_inputs[param_index].erase(
      spec_param_inputs[param_index].begin() + efficiency_size - 2,
      spec_param_inputs[param_index].begin() + efficiency_size);
    }
  }
}


void calc_eff_vs_cut(
  unsigned i, unsigned j, std::vector<io_manager*> tuples,
  std::vector<std::string>& paramnames, io_manager* minbias,
  std::vector<std::vector<double>> rates,
  std::vector<std::vector<double>>& spec_param_inputs,
  std::vector<std::vector<double>>& efficiencies,
  std::vector<std::vector<double>>& efficiency_errs, int& param_index,
  line_store& linestore, line_parameters paramph
  ) {

  if (j == tuples[i]->_background_io->_parameter_values.size()/2) {
    std::cout << "\nHalfway done.\n";
  }
  if (tuples[i]->_activate_params[j]) {
    if (minbias->_parameters[paramph._paramnames[j]]._fixed) {
      std::cout << "Not plotting " << paramph._paramnames[j]
      << " because fixed.\n";
      return;
    }
    std::cout << "Plotting " << paramph._paramnames[j] << ".\n";
    paramnames.push_back(paramph._paramnames[j]);
    paramnames.back()[0] = toupper(paramnames.back()[0]);
    // For all possible values of the jth parameter
    for (unsigned k{0};
    k < tuples[i]->_background_io->_parameter_values[j].size();
    k++) {
      compute_efficiency(i, j, k, tuples, paramnames, minbias, rates,
      spec_param_inputs, efficiencies, efficiency_errs, linestore,
      param_index);
    }
    param_index++;
  }
}


void eff_and_rate_vs_individual_cut(
  std::vector<io_manager*> tuples, std::string plot_dir,
  double collision_rate, double ratelimit
  ) {
  // Plot efficiency and minbias rate statistics for all samples for cuts on
  // all of their parameters

  io_manager* minbias = tuples[0]->_background_io;
  minbias->_parallelise_events = true;
  std::vector<std::vector<double>> rates, rate_errs;
  std::vector<std::vector<double>> param_inputs;
  line_store linestore;
  line_parameters paramph;
  for (unsigned i{0}; i < tuples[0]->_activate_params.size(); i++) {
    rates.push_back({});
    rate_errs.push_back({});
    param_inputs.push_back({});
  }

  for (unsigned i{0}; i < minbias->_parameter_values.size(); i++) {
    if (minbias->_parameters[paramph._paramnames[i]]._fixed) continue;
    std::cout << "Calculating rates for " << paramph._paramnames[i]
    << "...\n";
    if (i == minbias->_parameter_values.size() / 2) {
      std::cout << "\nHalfway done.\n";
    }
    for (unsigned j{0}; j < minbias->_parameter_values[i].size(); j++) {
      calc_minbias_rate_vs_cut(i, j, minbias, linestore, param_inputs,
      rates, rate_errs, collision_rate);
    }
    
    std::string param_string = paramph._paramnames[i];
    param_string[0] = std::toupper(param_string[0]);
    std::string x_axis = param_string;

    if (paramph._paramnames[i].find("alpha") != std::string::npos
    || paramph._paramnames[i].find("pt") != std::string::npos
    || paramph._paramnames[i].find("_et") != std::string::npos) {
      x_axis += " Cut (MeV);";
    } else if (paramph._paramnames[i].find("ip") != std::string::npos 
    && paramph._paramnames[i].find("chi2") == std::string::npos) {
      x_axis += " Cut (mm);";
    } else {
      x_axis += " Cut;";
    }

    std::string rate_plot_name = paramph._paramnames[i] + "_rates";
    std::string rate_plot_title =
    "HLT1 Output Rate For " + paramph._paramnames[i];
    std::string rate_canvas_dir = plot_dir + paramph._paramnames[i]
    + "_single_param_rates/";
    std::filesystem::create_directories(rate_canvas_dir.c_str());

    std::string style = "AC";
    if (sign(param_inputs[i].back() - param_inputs[i][0]) < 0.0) {
      style += "RX";
    } 
    TCanvas* rate_canvas = new TCanvas(rate_plot_name.c_str(),
    rate_plot_title.c_str(), 0, 0, 600, 400);
    rate_canvas->cd();
    std::vector<double> param_errs(rates[i].size(), 1e-8);
    TGraphErrors* rate_plot = new TGraphErrors(
    (int)rates[i].size(), &param_inputs[i][0], &rates[i][0], &param_errs[0],
    &rate_errs[i][0]);
    rate_plot->SetTitle((rate_plot_title
    + ";" + x_axis + "HLT1 Output Rate (MHz)").c_str());
    rate_plot->Draw(style.c_str());
    rate_canvas->SaveAs((rate_canvas_dir + rate_plot_name
    + ".tex").c_str());
    rate_canvas->SaveAs((rate_canvas_dir + rate_plot_name
    + ".C").c_str());
    rate_canvas->SaveAs((rate_canvas_dir + rate_plot_name
    + ".pdf").c_str());
    rate_canvas->SaveAs((rate_canvas_dir + rate_plot_name
    + ".svg").c_str());
  }

  for (unsigned i{0}; i < tuples.size(); i++) {
    std::cout << "\n\nCalculating efficiencies for "
    << tuples[i]->_filesafe_name << "\n";
    tuples[i]->_parallelise_events = true;
    
    if (tuples[i]->_background_io->_parameter_values.size() < 1) {
      std::cout << "There are no parameter values to calculate Fom "
      "for!\n";
      return;
    }

    std::vector<std::vector<double>> efficiencies, efficiency_errs,
    spec_param_inputs;
    std::vector<std::string> paramnames;
    // Vectors for each param
    for (unsigned j{0}; j < tuples[i]->_activate_params.size(); j++) {
      if (tuples[i]->_activate_params[j]) {
        efficiencies.push_back({});
        efficiency_errs.push_back({});
        spec_param_inputs.push_back({});
        std::cout << "Preparing to scan " << paramph._paramnames[j] << "\n";
      }
    }

    // For all params
    int param_index{0};
    for (unsigned j{0};
    j < tuples[i]->_background_io->_parameter_values.size(); j++) {
      std::cout << "Scanning " << paramph._paramnames[j] << "\n";
      calc_eff_vs_cut(i, j, tuples, paramnames,
      minbias, rates, spec_param_inputs, efficiencies, efficiency_errs,
      param_index, linestore, paramph);
    }
    std::cout << "Efficiencies have been calculated for all parameters"
    ".\n";

    for (unsigned j{0}; j < paramnames.size(); j++) {
      if (minbias->_parameters[paramnames[j]]._fixed) continue;
      std::string tuplename = tuples[i]->_name;
      std::string filesafe_name = tuples[i]->_filesafe_name;
      std::string efficiency_plot_name = filesafe_name + "_"
      + paramnames[j] + "_efficiency";
      std::string efficiency_plot_title =
      "\\text{Efficiency For }" + tuplename + "\\text{ vs. Cut on "
      + paramnames[j] + "}";
      boost::replace_all(efficiency_plot_title, "_", " ");

      std::string efficiency_canvas_dir =
      plot_dir + filesafe_name + "_single_param_efficiencies/";
      std::filesystem::create_directories(
      efficiency_canvas_dir.c_str());
      
      std::string param_string = paramnames[j];
      param_string[0] = std::toupper(param_string[0]);
      std::string x_axis = param_string;

      boost::replace_all(x_axis, "_", " ");

      if (paramnames[j].find("alpha") != std::string::npos
      || paramnames[j].find("pt") != std::string::npos
      || paramnames[j].find("_et") != std::string::npos) {
        x_axis += " Cut (MeV);";
      } else if (paramnames[j].find("ip") != std::string::npos 
      && paramnames[j].find("chi2") == std::string::npos) {
        x_axis += " Cut (mm);";
      } else {
        x_axis += " Cut;";
      }

      TCanvas* efficiency_canvas =
      new TCanvas(efficiency_plot_name.c_str(),
      efficiency_plot_title.c_str(), 0, 0, 600, 400);
      efficiency_canvas->cd();

      std::string style = "AC";
      if (sign(spec_param_inputs[j].back() - spec_param_inputs[j][0])
      < 0.0) {
        style += "RX";
      } 

      std::vector<double> param_errs(efficiencies[j].size(), 1e-12);
      // std::cout << "Efficiencies size: " << efficiencies[j].size() << "\n";
      // std::cout << "Efficiency errs size: " << efficiency_errs[j].size() << "\n";
      // std::cout << "spec_param_inputs size: " << spec_param_inputs[j].size() << "\n";
      // std::cout << "param_errs size: " << param_errs.size() << "\n";
      
      TGraphErrors* efficiency_plot = new TGraphErrors(
      (int)efficiencies[j].size(), &spec_param_inputs[j][0], &efficiencies[j][0],
      &param_errs[0], &efficiency_errs[j][0]);
      efficiency_plot->SetTitle((efficiency_plot_title
      + ";"  + x_axis + tuplename + "\\text{ Signal Efficiency}").c_str());
      efficiency_plot->Draw(style.c_str());
      efficiency_canvas->SaveAs((efficiency_canvas_dir
      + efficiency_plot_name + ".tex").c_str());
      efficiency_canvas->SaveAs((efficiency_canvas_dir
      + efficiency_plot_name + ".C").c_str());
      efficiency_canvas->SaveAs((efficiency_canvas_dir
      + efficiency_plot_name + ".pdf").c_str());
      efficiency_canvas->SaveAs((efficiency_canvas_dir
      + efficiency_plot_name + ".svg").c_str());
    }
    std::cout << "Finished plotting efficiencies for "
    << tuples[i]->_filesafe_name << "\n";
  }
  std::cout << "Finished plotting efficiencies for all samples\n";
}


void calc_and_plot_statistics_vs_cut(
  std::vector<io_manager*> tuples, std::string plot_dir,
  double plot_phi, double collision_rate, double ratelimit,
  std::string param_one, std::string param_two 
  ) { 
  // Calculate the FoM scan across the whole phase space and then plot
  // the statistics for the first two unique parameters of each signal
  // tuple

  bool found_param0{false};
  bool found_param1{false};
  int paramindex0{0};
  int paramindex1{0};
  line_parameters paramph;
  for (unsigned i{0}; i < paramph.size(); i++) {
    if (found_param0 && found_param1) break;

    if (paramph._paramnames[i].compare(param_one) == 0) {
      paramindex0 = i;
      found_param0 = true;
    } else if (paramph._paramnames[i].compare(param_two) == 0) {
      paramindex1 = i;
      found_param1 = true;
    }
  }

  for (std::string param_chosen: {param_one, param_two}) { 
    if (tuples[0]->_parameters.count(param_chosen) == 0 ) {
      std::cout << "Parameter " << param_chosen << " not contained in "
      << tuples[0]->_filesafe_name << "\n";
      
      std::cout << "Available params: ";
      for (auto& [k,v] : tuples[0]->_parameters) {
        std::cout << k << " ";
      }
      std::cout << "\nExiting...\n";
      exit(0);
    }
  }

  if (!(found_param0 && found_param1)) {
    std::cout << "Parameters chosen in config file do not exist. "
    "Exiting...\n";
    exit(0);
  }

  phase_space_scan(tuples, collision_rate, ratelimit, paramindex0,
  paramindex1);
  std::cout << "Plotting statistics for chosen parameter axes:\n";
  // Statistics for first two axes in io_manager line from config file
  // will be plotted here, e.g, alpha(Hlt1TrackMVA),
  // twotrackmva(Hlt1TwoTrackMVA) vs. FoM

  // Need to add all values of certain cut variable to vector
  std::vector<double> first_axis_values, second_axis_values;

  for (unsigned j{0};
  j < tuples[0]->_maxeff_adam->_inputs_used.size(); j++) {
    int both{0};
    for (unsigned k{0}; k < paramph.size(); k++) {
      if ((int)k == paramindex0) {
        first_axis_values.push_back(
        tuples[0]->_maxeff_adam->_inputs_used[j][k]);
        both++;
      } else if ((int)k == paramindex1) {
        second_axis_values.push_back(
        tuples[0]->_maxeff_adam->_inputs_used[j][k]);
        both++;
      }
      if (both > 1) break;
    }
  }

  std::vector<std::string> signal_plot_strings =
  {tuples[0]->_filesafe_name, paramph._paramnames[paramindex0],
  paramph._paramnames[paramindex1], "Efficiency", plot_dir};

  plot_results_vs_lineor_cut(signal_plot_strings,
  first_axis_values, second_axis_values,
  tuples[0]->_maxeff_adam->_signal_efficiencies, plot_phi);

  signal_plot_strings[3] = "Efficiency Error";
  plot_results_vs_lineor_cut(signal_plot_strings,
  first_axis_values, second_axis_values,
  tuples[0]->_maxeff_adam->_signal_efficiency_errs, plot_phi);

  signal_plot_strings[3] = "FoM";
  plot_results_vs_lineor_cut(signal_plot_strings,
  first_axis_values, second_axis_values,
  tuples[0]->_maxeff_adam->_all_foms, plot_phi);

  signal_plot_strings[3] = "Trigger Rate";
  plot_results_vs_lineor_cut(signal_plot_strings,
  first_axis_values, second_axis_values,
  tuples[0]->_maxeff_adam->_background_rates, plot_phi);

  signal_plot_strings[3] = "Rate Error";
  plot_results_vs_lineor_cut(signal_plot_strings,
  first_axis_values, second_axis_values,
  tuples[0]->_maxeff_adam->_background_rate_errs, plot_phi);
  plot_sigeff_vs_backrate(tuples[0], plot_dir);
}


void check_statistics_at_selection(
  line_parameters selection, std::vector<io_manager*> tuples,
  double collision_rate, double ratelimit
  ) {
  
  std::cout << "Cutting on: " << tuples[0]->_filesafe_name
  << " with variables:\n";
  for (unsigned i{0}; i < selection.size(); i++) {
    std::cout << selection._paramnames[i] << ": " << selection[i]
    << " ";
  }
  std::cout << "\n";

  adam_base* minimiser = tuples[0]->_maxeff_adam;
  std::vector<double> stats = minimiser->minimised_function(selection);
  std::cout << "Statistics:\n";

  std::cout << "FoM: " << stats[0] << "\n";
  std::cout << "Minbias rate: " << stats[3] << "+/-" << stats[4] << "Hz\n";
  std::cout << "Signal efficiency: " << stats[5] << "\n";
  std::cout << "Ratelimited signal efficiency: " << stats[7] << "\n";
  std::cout << "Final count: " << stats[9] << "\n";
} 



void manual_param_entry(
  std::vector<double> manual_inputs, std::vector<io_manager*> tuples,
  double collision_rate, double ratelimit,
  double scalewidth, int accuracy, bool plot_epoch,
  std::string plot_dir, std::string outfile_dir,
  int max_grid_searches, int job_num
  ) {
  // Fastpass function if you know the optimal global parameter
  // configuration and want to replot the results

  std::cout << "Collating individual minimisation results...\n";
  collate_all_individual_results(tuples, outfile_dir);
  std::cout << "\n\n";

  for (io_manager* tuple: tuples) {
    if (tuple->_optimal_signal_eff <= 0.0) {
      std::cout << "Max Efficiency found for " << tuple->_event_type
      << " was zero. Minimisation failed.\n";
      tuple->_optimal_fom = 0.999;
      tuple->_optimal_signal_eff = 1e-10;
      if (max_grid_searches != -1) {
        std::cout << "Exiting...\n";
        exit(0);
      }
    }
  }

  adam_base* opt_minimiser =
  new opt_discrete_gradient(tuples, collision_rate, ratelimit, scalewidth,
  accuracy);

  line_parameters chosen_params;
  if (chosen_params.size() == manual_inputs.size()) {
    for (int i{0}; i < chosen_params._num_parameters; i++) {
      chosen_params._paramgroup[i] = manual_inputs.at(i);
    }
  } else {
    std::cout << "Number of manual inputs does not match the required"
    " number (" << chosen_params.size() << "). Exiting...\n";
    exit(0);
  }

  // Not rounding alphas to nearest 100 MeV
  std::vector<double> opt_stats =
  opt_minimiser->minimised_function(chosen_params);

  opt_minimiser->_opt_fom = opt_stats[0];
  opt_minimiser->_opt_backeff = opt_stats[1];
  opt_minimiser->_opt_backeff_err = opt_stats[2];
  opt_minimiser->_opt_rate = opt_stats[3];
  std::cout << "Optimal Global FoM: " << opt_minimiser->_opt_fom << "\n";
  std::cout << "Optimal minbias efficiency: " << opt_minimiser->_opt_backeff
  << " +/- " << opt_minimiser->_opt_backeff_err << "\n";
  std::cout << "Optimal minbias rate: " << opt_minimiser->_opt_rate/1e6
  << " +/- " << opt_stats[4]/1e6 << " MHz\n";

  for (unsigned i{0}; i < chosen_params.size(); i++) {
    std::cout << "Optimal value for parameter "
    << chosen_params._paramnames[i] << ": "
    << chosen_params[i] << "\n";
    // if (i == twotrack_ks_index) {
    //   
    //   twotrackks().print_thresholds(chosen_params[i]); 
    //   std::cout << "Untransformed twotrack_ks: "
    //   << twotrack_ks_transform(chosen_params[i]) << "\n";

    // } else if (i == alpha_hadron_index || i == alpha_electron_index
    // || i == alpha_muon_index) {
    //   std::cout << "Untransformed "
    //   << chosen_params._paramnames[i] << ": "
    //   << alpha_transform(chosen_params[i]) << " MeV\n";
    // }

  }

  print_allen_python_output(outfile_dir, chosen_params);

  process_save_and_plot_global_statistics(tuples, chosen_params,
  collision_rate, ratelimit, plot_dir, outfile_dir, job_num, plot_epoch,
  opt_minimiser);
}


void combine_nobias_log_files(
  std::string log_file_directory, std::string new_file_directory,
  std::vector<line_manager*> lines
  ) {
  // Obtains line yields and total absolute yield from directory of log
  // files and combines into one log file

  int total_allen_events{0};
  std::vector<int> trigger_line_yields(lines.size());
  std::vector<std::string> trigger_line_names(lines.size());
  
  for (unsigned i{0}; i < lines.size(); i++) {
    trigger_line_names[i] = lines[i]->_name;
  } 

  for (const auto & entry : std::filesystem::directory_iterator(
  log_file_directory)) {
    if (entry.path().string().find("log") != std::string::npos) {
      std::ifstream logfile(entry.path().string());
      std::string line;

      while (getline(logfile, line)) {
        std::istringstream ss(line);
        std::vector<std::string> line_vec;

        std::copy(std::istream_iterator<std::string>(ss), 
        std::istream_iterator<std::string>(),
        std::back_inserter(line_vec));

        if (!line_vec.empty() && line_vec[0].compare("Inclusive:")==0) {
          std::string denom;
          if (line_vec[1].back() == '/') {
            denom = line_vec[2];
          } else {
            denom = line_vec[1].substr(line_vec[1].find("/") + 1,
            line_vec[1].size() - line_vec[1].find("/") + 1);
          }
          if (denom.back() == ',') {
            denom.erase(denom.size()-1, denom.size());
          }
          total_allen_events += std::stoi(denom);
        }


        for (unsigned i{0}; i < trigger_line_names.size(); i++) {
          std::string trigger_line = trigger_line_names[i];
          if (!line_vec.empty() && line_vec[0].compare(
          trigger_line + ":") == 0) {
            std::string numerator;
            if (line_vec[1].back() == '/') {
              numerator = line_vec[1];
              numerator.erase(numerator.size() - 1,
              numerator.size());
            } else {
              numerator = line_vec[1].substr(0,
              line_vec[1].find("/"));
            }
            trigger_line_yields[i] += std::stoi(numerator);
          }
        }
      }
    }
  }

  std::ofstream final_log_file(new_file_directory + "allen.log");

  for (unsigned i{0}; i < trigger_line_names.size(); i++) {
    final_log_file << trigger_line_names[i] << ":             "
    "         " << std::to_string(trigger_line_yields[i]) << "/ "
    << total_allen_events << ", (Y +/-  Z) kHz\n";
  }
  
  final_log_file << "Inclusive:                    X/ "
  << std::to_string(total_allen_events) <<  ", (Y +/-  Z) kHz\n";
  final_log_file.close();

  std::cout << new_file_directory + "allen.log has been written. "
  "Exiting...\n";
}


std::vector<std::string> split(
  const std::string& string_to_split, char delimiter, bool ignore_whitespace
  ) {
  std::vector<std::string> elements;
  std::string item;
  std::stringstream ss(string_to_split);
  while (std::getline(ss, item, delimiter)) {
    if (!ignore_whitespace ||
    (item != " " && item != "" && item != "\n" && item != "\t")) {
      elements.push_back(item);
    }
  }

  return elements;
}

std::string latex_to_term(const std::string& name) {
  // Convert latex operations to filename-safe terms

  std::string _filesafe_name = name; 
  boost::replace_all(_filesafe_name, " ", "");
  boost::replace_all(_filesafe_name, "\\", "_");
  boost::replace_all(_filesafe_name, "[", "_");
  boost::replace_all(_filesafe_name, "]", "_");
  boost::replace_all(_filesafe_name, "^+", "plus");
  boost::replace_all(_filesafe_name, "^-", "minus");
  boost::replace_all(_filesafe_name, "^{+}", "plus");
  boost::replace_all(_filesafe_name, "^{++}", "plusplus");
  boost::replace_all(_filesafe_name, "^{-}", "minus");
  boost::replace_all(_filesafe_name, "^{--}", "minusminus");
  boost::replace_all(_filesafe_name, "^{*+}", "starplus");
  boost::replace_all(_filesafe_name, "^{*-}", "starminus");
  boost::replace_all(_filesafe_name, "^{0}", "0");
  boost::replace_all(_filesafe_name, "^0", "0");
  boost::replace_all(_filesafe_name, "^{_ast0}", "star0");
  boost::replace_all(_filesafe_name, "^{*}", "_star");
  boost::replace_all(_filesafe_name, "_{s}", "_s");
  boost::replace_all(_filesafe_name, "_{c}", "_c");
  boost::replace_all(_filesafe_name, "_{cc}", "_cc");
  boost::replace_all(_filesafe_name, "/_", "_");
  boost::replace_all(_filesafe_name, "_to", "_to_");
  boost::replace_all(_filesafe_name, "rightarrow", "_to_");
  boost::replace_all(_filesafe_name, "^_prime", "prime");
  boost::replace_all(_filesafe_name, "(1S)", "_one_s");
  boost::replace_all(_filesafe_name, "{S}", "S");
  boost::replace_all(_filesafe_name, "(S)", "S");
  boost::replace_all(_filesafe_name, "{2S}", "2S");
  boost::replace_all(_filesafe_name, "(2S)", "2S");
  boost::replace_all(_filesafe_name, "text{jj}", "jj");
  boost::replace_all(_filesafe_name, ",", "star0");
  boost::replace_all(_filesafe_name, ">", "_greaterthan_");
  boost::replace_all(_filesafe_name, "<", "_lessthan_");
  boost::replace_all(_filesafe_name, "gevc", "gev_over_c");

  if (_filesafe_name[0] == '_') {
    _filesafe_name.erase(0, 1);
  }
  if (_filesafe_name[_filesafe_name.size() - 1] == '_') {
    _filesafe_name.erase(_filesafe_name.size() - 1, 1);
  }

  return _filesafe_name; 
}


