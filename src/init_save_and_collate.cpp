#include "init_save_and_collate.h"

void read_and_edit_results(
  io_manager* tuple, std::string optimal_stats_path, std::string treename,
  std::vector<std::string> params_used,
  init_discrete_gradient* specline_minimiser
  ) {

  double fom, mbr, mbe, mber, sigeff, sigefferr, rlsigeff, rlsigefferr;
  fom = tuple->_optimal_fom;
  mbr = tuple->_optimal_minbias_rate;
  mbe = tuple->_optimal_minbias_efficiency;
  mber = tuple->_optimal_minbias_efficiency_err;
  sigeff = tuple->_optimal_signal_eff;
  sigefferr = tuple->_optimal_signal_eff_err;
  rlsigeff = tuple->_optimal_ratelimited_signal_eff;
  rlsigefferr = tuple->_optimal_ratelimited_signal_eff_err;
  TFile* optimal_stats_file;
  TTree* optimal_stats;
  std::cout << "\nMin stats file: " << optimal_stats_path
  << " is already written. Reading from file...\n";
  optimal_stats_file =
  TFile::Open(optimal_stats_path.c_str(), "UPDATE");
  optimal_stats =
  ((TTree*)optimal_stats_file->Get(treename.c_str()));
  optimal_stats->Print();
  optimal_stats->SetBranchAddress("optimal_fom", &fom);
  optimal_stats->SetBranchAddress("optimal_minbias_rate", &mbr);
  optimal_stats->SetBranchAddress("optimal_minbias_efficiency", &mbe);
  optimal_stats->SetBranchAddress("optimal_minbias_efficiency_err",
  &mber);
  optimal_stats->SetBranchAddress("optimal_signal_eff", &sigeff);
  optimal_stats->SetBranchAddress("optimal_signal_eff_err", &sigefferr);
  optimal_stats->SetBranchAddress("optimal_ratelimited_signal_eff",
  &rlsigeff);
  optimal_stats->SetBranchAddress("optimal_ratelimited_signal_eff_err",
  &rlsigefferr);

  // Have to do it in this way or it doesn't work (pre-initialisation)
  std::vector<double> optparams;
  for (double param: tuple->_optimal_parameter_inputs) {
    optparams.push_back(param);
  }
  for (unsigned i{0}; i < tuple->_optimal_parameter_inputs.size();
  i++) {
    std::string branchname = "optimal_" + params_used[i]
    + "_value";
    optimal_stats->SetBranchAddress(branchname.c_str(),
    &optparams[i]);
  }

  std::vector<double> optimal_line_efficiencies;
  std::vector<double> optimal_line_efficiency_errs;
  std::vector<int> optimal_candidate_counts;
  std::vector<int> optimal_minbias_candidate_counts;
  for (unsigned i{0};
  i < specline_minimiser->_signal_efficiencies.size(); i++) {
    optimal_line_efficiencies.push_back(
    specline_minimiser->_signal_efficiencies[i]);
    optimal_line_efficiency_errs.push_back(
    specline_minimiser->_signal_efficiency_errs[i]);
    optimal_candidate_counts.push_back(
    tuple->_optimal_candidate_counts[i]);
    optimal_minbias_candidate_counts.push_back(
    tuple->_optimal_minbias_candidate_counts[i]);
  }
  int linecount{0};
  line_store linestore;
  for (unsigned i{0}; i < NUM_LINES; i++) {
    if (!tuple->_activate_lines[i]) continue; 
    std::string name = linestore._linenames[i]; 
    std::string branchname =  "optimal_"+name+"_efficiency";
    std::string branchname2 = "optimal_"+name+"_efficiency_err";
    std::string branchname3 = "optimal_"+name+"_candidate_count";
    std::string branchname4 = "optimal_"+name+"_minbias_candidate_count";
    optimal_stats->SetBranchAddress(branchname.c_str(),
    &optimal_line_efficiencies[linecount]);
    optimal_stats->SetBranchAddress(branchname2.c_str(),
    &optimal_line_efficiency_errs[linecount]);
    optimal_stats->SetBranchAddress(branchname3.c_str(),
    &optimal_candidate_counts[linecount]);
    optimal_stats->SetBranchAddress(branchname4.c_str(),
    &optimal_minbias_candidate_counts[linecount]);
    linecount++;
  }

  std::cout << "Filling and writing new tree to file ...\n";
  optimal_stats->Fill();
  optimal_stats_file->cd();
  optimal_stats->Write();
  optimal_stats_file->Close(); 
}


void write_results(
  io_manager* tuple, std::string optimal_stats_path, std::string treename,
  std::vector<std::string> params_used,
  init_discrete_gradient* specline_minimiser
  ) {
  
  TFile* optimal_stats_file;
  TTree* optimal_stats;
  std::cout << "\nMin stats file: " << optimal_stats_path
  << " has not yet been written. Creating file...\n";
  
  optimal_stats_file =
  TFile::Open(optimal_stats_path.c_str(), "RECREATE");
  optimal_stats = new TTree(treename.c_str(), "Optimal Initfnc Stats");
  optimal_stats->Branch("optimal_fom", &tuple->_optimal_fom,
  "optimal_fom/D");
  optimal_stats->Branch("optimal_minbias_rate",
  &tuple->_optimal_minbias_rate, "optimal_minbias_rate/D");
  optimal_stats->Branch("optimal_minbias_efficiency",
  &tuple->_optimal_minbias_efficiency,
  "optimal_minbias_efficiency/D");
  optimal_stats->Branch("optimal_minbias_efficiency_err",
  &tuple->_optimal_minbias_efficiency_err,
  "optimal_minbias_efficiency_err/D");
  optimal_stats->Branch("optimal_signal_eff",
  &tuple->_optimal_signal_eff, "optimal_signal_eff/D");
  optimal_stats->Branch("optimal_signal_eff_err",
  &tuple->_optimal_signal_eff_err,
  "optimal_signal_eff_err/D");
  optimal_stats->Branch("optimal_ratelimited_signal_eff",
  &tuple->_optimal_ratelimited_signal_eff,
  "optimal_ratelimited_signal_eff/D");
  optimal_stats->Branch("optimal_ratelimited_signal_eff_err",
  &tuple->_optimal_ratelimited_signal_eff_err,
  "optimal_ratelimited_signal_eff_err/D");

  for (unsigned i{0}; i < tuple->_optimal_parameter_inputs.size();
  i++) {
    std::string branchname =
    "optimal_" + params_used[i] + "_value";
    optimal_stats->Branch(branchname.c_str(),
    &tuple->_optimal_parameter_inputs[i],
    (branchname + "/D").c_str());
  }
  
  int linecount{0};
  line_store linestore;
  for (unsigned i{0}; i < NUM_LINES; i++) {
    if (!tuple->_activate_lines[i]) continue; 
    std::string name = linestore._linenames[i]; 
    std::string branchname =  "optimal_"+name+"_efficiency";
    std::string branchname2 = "optimal_"+name+"_efficiency_err";
    std::string branchname3 = "optimal_"+name+"_candidate_count";
    std::string branchname4 = "optimal_"+name+"_minbias_candidate_count";
    optimal_stats->Branch(branchname.c_str(),
    &specline_minimiser->_signal_efficiencies[linecount],
    (branchname + "/D").c_str());
    optimal_stats->Branch(branchname2.c_str(),
    &specline_minimiser->_signal_efficiency_errs[linecount],
    (branchname + "/D").c_str());
    optimal_stats->Branch(branchname3.c_str(),
    &tuple->_optimal_candidate_counts[linecount],
    (branchname3 + "/I").c_str());
    optimal_stats->Branch(branchname4.c_str(),
    &tuple->_optimal_minbias_candidate_counts[linecount],
    (branchname4 + "/I").c_str());
    linecount++;
  }
  
  optimal_stats->Fill();
  optimal_stats_file->cd();
  optimal_stats->Write();
  optimal_stats_file->Close();
}


void save_or_edit_indiv_min_results(
  io_manager* tuple, double alpha, double beta1,
  double beta2, std::vector<double> starting_point,
  std::string outfile_dir, int job_num,
  bool plot_min_paths, std::string plot_dir
  ) {

  double ratecap = (tuple->_percentage_ratelimit/100.0)*
  (tuple->_collision_rate * tuple->_ratelimit)/1e6;
  std::stringstream ratecap_ss;
  ratecap_ss << std::fixed << std::setprecision(3) << ratecap;
  std::string ratecap_string = ratecap_ss.str() + "_mhz";
  boost::replace_all(ratecap_string, ".", "point");

  std::string output_dir;
  std::string starting_coords;
  std::string hyperparams;
  // Input starting point as empty vec if you don't want starting
  // coordinates in name or in specific folder
  if (starting_point.empty()) {
    output_dir = outfile_dir + "indiv_min_stats_" + ratecap_string
    + "/";
    starting_coords = "";
    hyperparams = "";
  } else {
    output_dir = outfile_dir + "indiv_min_stats_"
    + ratecap_string + "/hyperparam_scan/";
    starting_coords = "_starting_coords_";
    for (double point: starting_point) {
      std::ostringstream sp_oss;
      sp_oss << std::fixed;
      sp_oss << std::setprecision(2);
      sp_oss << point;
      starting_coords += sp_oss.str() + "_";
    }
    boost::replace_all(starting_coords, "-", "minus");
    boost::replace_all(starting_coords, ".", "point");
    std::ostringstream param_oss;
    param_oss << std::fixed;
    param_oss << std::setprecision(2);
    param_oss << "alpha" << alpha << "_beta_one" << beta1 << "_beta_two"
    << beta2;
    hyperparams = param_oss.str();
    boost::replace_all(hyperparams, ".", "point");
  }
  std::filesystem::create_directories(output_dir.c_str());

  std::string optimal_stats_path =
  output_dir + tuple->_filesafe_name + "_indiv_min_stats_job"
  + std::to_string(job_num) + starting_coords
  + hyperparams + ".root";
  std::string treename = tuple->_filesafe_name + "_indiv_stats_tree";

  // Plot the minimiser path if desired
  if (plot_min_paths && !tuple->_loosest_selection) {
    tuple->_maxeff_adam->plot_min_path(plot_dir, job_num);
    tuple->_maxeff_adam->plot_epoch(plot_dir, job_num);
  }
  std::vector<std::string> params_used = tuple->_optimal_paramnames;
  init_discrete_gradient* specline_minimiser = tuple->_specline_adam;
  if (!(gSystem->AccessPathName(optimal_stats_path.c_str()))) {
    read_and_edit_results(tuple, optimal_stats_path, treename,
    params_used, specline_minimiser);
  } else {
    write_results(tuple, optimal_stats_path, treename, params_used,
    specline_minimiser);
  }
}


void collate_file_info(
  std::string entry_path, io_manager* tuple,
  int& filecounter,
  std::vector<double>& optimal_foms,
  std::vector<double>& optimal_minbias_rates,
  std::vector<double>& optimal_minbias_efficiencies,
  std::vector<double>& optimal_minbias_efficiency_errs,
  std::vector<double>& optimal_signal_effs,
  std::vector<double>& optimal_signal_eff_errs,
  std::vector<double>& optimal_ratelimited_signal_effs,
  std::vector<double>& optimal_ratelimited_signal_eff_errs,
  line_parameters params,
  std::vector<std::vector<double>>& optimal_parameter_inputs,
  std::vector<std::vector<double>>& optimal_line_efficiencies,
  std::vector<std::vector<double>>& optimal_line_efficiency_errs,
  std::vector<std::vector<int>>& optimal_candidate_counts,
  std::vector<std::vector<int>>& optimal_minbias_candidate_counts,
  std::vector<double>& learning_rates, std::vector<double>& beta_ones,
  std::vector<double>& beta_twos, bool paramscan
  ) {
  // Read individual minimisation output root file and save into
  // result vectors

  // Need to improve this
  std::string indiv_string = entry_path.substr(
  entry_path.find(tuple->_filesafe_name)
  + tuple->_filesafe_name.size(), 16);
  bool correct_file = (entry_path.find(tuple->_filesafe_name) !=
  std::string::npos) && indiv_string.compare("_indiv_min_stats") == 0;

  if (correct_file && entry_path.find("collated")
  == std::string::npos) {
    filecounter++;
    if (paramscan) {
      std::string alpha, betaone, betatwo;
      for (size_t i{entry_path.find("alpha") + 5};
      i < entry_path.size(); i++) {
        if (strcmp(&entry_path[i], "_") != 0) {
          alpha += entry_path[i];
        } else {
          break;
        }
      }
      boost::replace_all(alpha, "point", ".");
      learning_rates.push_back(std::stod(alpha));
      for (size_t i{entry_path.find("beta_one") + 8};
      i < entry_path.size(); i++) {
        if (strcmp(&entry_path[i], "_") != 0) {
          betaone += entry_path[i];
        } else {
          break;
        }
      }
      boost::replace_all(betaone, "point", ".");
      beta_ones.push_back(std::stod(betaone));
      for (size_t i{entry_path.find("beta_two") + 10};
      i < entry_path.size(); i++) {
        if (strcmp(&entry_path[i], "_") != 0) {
          betatwo += entry_path[i];
        } else {
          break;
        }
      }
      boost::replace_all(betatwo, "point", ".");
      beta_twos.push_back(std::stod(betatwo));
    }
    std::string treename = tuple->_filesafe_name
    + "_indiv_stats_tree";
    ROOT::RDataFrame optimal_stats(treename.c_str(),
    entry_path.c_str());

    std::vector<double> new_stats =
    *(optimal_stats.Take<double>("optimal_fom"));
    optimal_foms.insert(optimal_foms.end(), new_stats.begin(),
    new_stats.end());
    new_stats =
    *(optimal_stats.Take<double>("optimal_minbias_rate"));
    optimal_minbias_rates.insert(optimal_minbias_rates.end(),
    new_stats.begin(), new_stats.end());
    new_stats =
    *(optimal_stats.Take<double>("optimal_minbias_efficiency"));
    optimal_minbias_efficiencies.insert(
    optimal_minbias_efficiencies.end(), new_stats.begin(),
    new_stats.end());
    new_stats = *(optimal_stats.Take<double>(
    "optimal_minbias_efficiency_err"));
    optimal_minbias_efficiency_errs.insert(
    optimal_minbias_efficiency_errs.end(), new_stats.begin(),
    new_stats.end());
    new_stats =
    *(optimal_stats.Take<double>("optimal_signal_eff"));
    optimal_signal_effs.insert(optimal_signal_effs.end(),
    new_stats.begin(), new_stats.end());
    new_stats =
    *(optimal_stats.Take<double>("optimal_signal_eff_err"));
    optimal_signal_eff_errs.insert(optimal_signal_eff_errs.end(),
    new_stats.begin(), new_stats.end());
    new_stats = *(optimal_stats.Take<double>(
    "optimal_ratelimited_signal_eff"));
    optimal_ratelimited_signal_effs.insert(
    optimal_ratelimited_signal_effs.end(), new_stats.begin(),
    new_stats.end());
    new_stats = *(optimal_stats.Take<double>(
    "optimal_ratelimited_signal_eff_err"));
    optimal_ratelimited_signal_eff_errs.insert(
    optimal_ratelimited_signal_eff_errs.end(), new_stats.begin(),
    new_stats.end());

    int paramcount{0};
    bool isempty = optimal_parameter_inputs.empty();
    for (unsigned i{0}; i < params.size(); i++) {
      if (tuple->_activate_params[i]) {
        if (isempty) optimal_parameter_inputs.push_back({});
        new_stats = 
        *(optimal_stats.Take<double>(("optimal_"
        + std::string(params._paramnames[i]) + "_value").c_str()));
        optimal_parameter_inputs[paramcount].insert(
        optimal_parameter_inputs[paramcount].end(),
        new_stats.begin(), new_stats.end());
        paramcount++;
      }
    } 

    isempty = optimal_line_efficiencies.empty();
    int linecount{0};
    line_store linestore;
    for (unsigned i{0}; i < NUM_LINES; i++) {
      if (tuple->_activate_lines[i]) {
        if (isempty) {
          optimal_line_efficiencies.push_back({});
          optimal_line_efficiency_errs.push_back({});
          optimal_candidate_counts.push_back({});
          optimal_minbias_candidate_counts.push_back({});
        }
        
        new_stats = *(optimal_stats.Take<double>("optimal_"
        + std::string(linestore._linenames[i])
        + "_efficiency"));
        optimal_line_efficiencies[linecount].insert(
        optimal_line_efficiencies[linecount].end(),
        new_stats.begin(), new_stats.end());
        new_stats = *(optimal_stats.Take<double>(
        "optimal_" + std::string(linestore._linenames[i])
        + "_efficiency_err"));
        optimal_line_efficiency_errs[linecount].insert(
        optimal_line_efficiency_errs[linecount].end(),
        new_stats.begin(), new_stats.end());
        std::vector<int> newcan_stats =
        *(optimal_stats.Take<int>(
        ("optimal_" + std::string(linestore._linenames[i])
        + "_candidate_count").c_str()));
        optimal_candidate_counts[linecount].insert(
        optimal_candidate_counts[linecount].end(),
        newcan_stats.begin(), newcan_stats.end()); 
        newcan_stats =
        *(optimal_stats.Take<int>(
        ("optimal_" + std::string(linestore._linenames[i])
        + "_minbias_candidate_count").c_str()));
        optimal_minbias_candidate_counts[linecount].insert(
        optimal_minbias_candidate_counts[linecount].end(),
        newcan_stats.begin(), newcan_stats.end()); 
        linecount++;
      }
    }
  }
}


void write_collated_results_to_master_file(
  std::string collated_filepath, io_manager* tuple,
  std::vector<double> optimal_foms,
  std::vector<double> optimal_minbias_rates,
  std::vector<double> optimal_minbias_efficiencies,
  std::vector<double> optimal_minbias_efficiency_errs,
  std::vector<double> optimal_signal_effs,
  std::vector<double> optimal_signal_eff_errs,
  std::vector<double> optimal_ratelimited_signal_effs,
  std::vector<double> optimal_ratelimited_signal_eff_errs,
  line_parameters params,
  std::vector<std::vector<double>> optimal_parameter_inputs,
  std::vector<std::vector<double>> optimal_line_efficiencies,
  std::vector<std::vector<double>> optimal_line_efficiency_errs,
  std::vector<std::vector<int>> optimal_candidate_counts,
  std::vector<std::vector<int>> optimal_minbias_candidate_counts,
  std::vector<double> learning_rates, std::vector<double> beta_ones,
  std::vector<double> beta_twos, bool paramscan
) {
  // Write all collated results into master root file for eventually
  // viewing threshold and convergence distributions

  // std::cout << "Opening " << tuple->_filesafe_name << "file...\n";
  TFile* collated_results_file =
  TFile::Open(collated_filepath.c_str(), "RECREATE");
  TTree* collated_resultstree =
  new TTree("collated_resultstree",
  ("Result tree for " + tuple->_filesafe_name).c_str());
  double fom, mbr, mbe, mber, sigeff, sigefferr, rlsigeff,
  rlsigefferr;
  std::cout << "Generating branches...\n";
  collated_resultstree->Branch("optimal_fom", &fom,
  "optimal_fom/D");
  collated_resultstree->Branch("optimal_minbias_rate",
  &mbr, "optimal_minbias_rate/D");
  collated_resultstree->Branch("optimal_minbias_efficiency",
  &mbe, "optimal_minbias_efficiency/D");
  collated_resultstree->Branch("optimal_minbias_efficiency_err",
  &mber, "optimal_minbias_efficiency_err/D");
  collated_resultstree->Branch("optimal_signal_eff",
  &sigeff, "optimal_signal_eff/D");
  collated_resultstree->Branch("optimal_signal_eff_err",
  &sigefferr, "optimal_signal_eff_err/D");
  collated_resultstree->Branch("optimal_ratelimited_signal_eff",
  &rlsigeff, "optimal_ratelimited_signal_eff/D");
  collated_resultstree->Branch("optimal_ratelimited_signal_eff_err",
  &rlsigefferr, "optimal_ratelimited_signal_eff_err/D");

  // std::cout << "Choosing correct parameters...\n";
  std::vector<std::string> params_used;
  for (unsigned i{0}; i < tuple->_activate_params.size(); i++) {
    if (tuple->_activate_params[i]) {
      params_used.emplace_back(params._paramnames[i]);
    }
  }
  
  std::vector<double> optparams;
  for (std::string param: params_used) {
    optparams.push_back(0);
  }
  
  // std::cout << "Generating parameter branches...\n";
  for (unsigned i{0}; i < params_used.size();
  i++) {
    std::string branchname =
    "optimal_" + params_used[i] + "_value";
    collated_resultstree->Branch(branchname.c_str(),
    &optparams[i], (branchname + "/D").c_str());
  }
  
  std::vector<double> optimal_line_efficiencies_phs;
  std::vector<double> optimal_line_efficiency_errs_phs;
  std::vector<int> optimal_candidate_counts_phs;
  std::vector<int> optimal_minbias_candidate_counts_phs;
  for (unsigned i{0}; i < NUM_LINES; i++) {
    if (tuple->_activate_lines[i]) {
      optimal_line_efficiencies_phs.push_back(0);
      optimal_line_efficiency_errs_phs.push_back(0);
      optimal_candidate_counts_phs.push_back(0);
      optimal_minbias_candidate_counts_phs.push_back(0);
    }
  }

  int linecount = 0;
  line_store linestore;
  std::cout << "Generating line branches for "
  << tuple->_filesafe_name << "...\n";

  for (unsigned i{0}; i != NUM_LINES; ++i ){
    if (!tuple->_activate_lines[i]) continue; 
    std::cout << "For " << linestore._linenames[i] << "\n";
    const std::string name = linestore._linenames[i];
    std::string branchname =  "optimal_"+name+"_efficiency";
    std::string branchname2 = "optimal_"+name+"_efficiency_err";
    std::string branchname3 = "optimal_"+name+"_candidate_count";
    std::string branchname4 = "optimal_"+name+"_minbias_candidate_count";
    collated_resultstree->Branch(branchname.c_str(),
    &optimal_line_efficiencies_phs[linecount],
    (branchname + "/D").c_str());
    collated_resultstree->Branch(branchname2.c_str(),
    &optimal_line_efficiency_errs_phs[linecount],
    (branchname2 + "/D").c_str());
    collated_resultstree->Branch(branchname3.c_str(),
    &optimal_candidate_counts_phs[linecount],
    (branchname3 + "/I").c_str());
    collated_resultstree->Branch(branchname4.c_str(),
    &optimal_minbias_candidate_counts_phs[linecount],
    (branchname4 + "/I").c_str());
    linecount++;
  }

  // std::cout << "Generating hyperparameter branches...\n";
  double learning_rateph, beta_oneph, beta_twoph;
  if (paramscan) {
    collated_resultstree->Branch("learning_rate",
    &learning_rateph, "learning_rate/D");
    collated_resultstree->Branch("beta_one", &beta_oneph,
    "beta_one/D");
    collated_resultstree->Branch("beta_two", &beta_twoph,
    "beta_two/D");
  }
  
  // std::cout << "Writing results...\n";
  for (unsigned i{0}; i < optimal_foms.size(); i++) {
    fom = optimal_foms[i];
    mbr = optimal_minbias_rates[i];
    mbe = optimal_minbias_efficiencies[i];
    mber = optimal_minbias_efficiency_errs[i];
    sigeff = optimal_signal_effs[i];
    sigefferr = optimal_signal_eff_errs[i];
    rlsigeff = optimal_ratelimited_signal_effs[i];
    rlsigefferr = optimal_ratelimited_signal_eff_errs[i];
    for (unsigned j{0}; j < params_used.size(); j++) {
      optparams[j] = optimal_parameter_inputs[j][i];
    }
    
    for (unsigned j{0};
    j < optimal_line_efficiencies_phs.size(); j++) {
      optimal_line_efficiencies_phs[j] =
      optimal_line_efficiencies[j][i];
      optimal_line_efficiency_errs_phs[j] =
      optimal_line_efficiency_errs[j][i];
      optimal_candidate_counts_phs[j] =
      optimal_candidate_counts[j][i];
      optimal_minbias_candidate_counts_phs[j] =
      optimal_minbias_candidate_counts[j][i];
    }

    if (paramscan) {
      learning_rateph = learning_rates[i];
      beta_oneph = beta_ones[i];
      beta_twoph = beta_twos[i];
    }

    collated_resultstree->Fill();
  }
  std::cout << "Finished filling\n";
  
  std::cout << "\nWriting to " << collated_filepath << "\n";
  collated_results_file->cd();
  collated_resultstree->Write();
  collated_results_file->Close(); 
}


void collate_all_individual_results(
  std::vector<io_manager*> tuples, std::string outfile_dir,
  bool paramscan
  ) {
  // Read and save results from all individual minimisation output
  // files contained in outfile_dir

  double ratecap = (tuples[0]->_percentage_ratelimit/100.0)
  * ((tuples[0]->_collision_rate * tuples[0]->_ratelimit) / 1e6);
  std::stringstream ratecap_ss;
  ratecap_ss << std::fixed << std::setprecision(3) << ratecap;
  std::string ratecap_string = ratecap_ss.str() + "_mhz";
  boost::replace_all(ratecap_string, ".", "point");


  std::string output_dir = outfile_dir + "indiv_min_stats_"
  + ratecap_string + "/";
  output_dir += paramscan ? "hyperparam_scan/" : "";

  double average_difference_in_efficiency_to_max{0};
  for (io_manager* tuple: tuples) {
    std::string collated_filepath = 
    output_dir + "collated_" + tuple->_filesafe_name + "_results.root";
    // Deactivating certain lines for initfnc collation
    line_store linestore;
    line_parameters params;

    std::vector<double> optimal_foms, optimal_minbias_rates,
    optimal_minbias_efficiencies,
    optimal_minbias_efficiency_errs, optimal_signal_effs,
    optimal_signal_eff_errs, optimal_ratelimited_signal_effs,
    optimal_ratelimited_signal_eff_errs;
    std::vector<std::vector<double>> optimal_parameter_inputs,
    optimal_line_efficiencies, optimal_line_efficiency_errs;
    std::vector<std::vector<int>> optimal_candidate_counts,
    optimal_minbias_candidate_counts;
    std::vector<double> learning_rates;
    std::vector<double> beta_ones;
    std::vector<double> beta_twos;

    tuple->_optimal_parameter_inputs.clear();
    int filecounter{0};
    for (const auto & entry : std::filesystem::directory_iterator(
    output_dir)) {
      std::string entry_path = entry.path().string();
      collate_file_info(
      entry_path, tuple, filecounter, optimal_foms,
      optimal_minbias_rates, optimal_minbias_efficiencies,
      optimal_minbias_efficiency_errs, optimal_signal_effs,
      optimal_signal_eff_errs, optimal_ratelimited_signal_effs,
      optimal_ratelimited_signal_eff_errs, params,
      optimal_parameter_inputs, optimal_line_efficiencies,
      optimal_line_efficiency_errs, optimal_candidate_counts,
      optimal_minbias_candidate_counts, learning_rates, beta_ones,
      beta_twos, paramscan);
    }

    // If no files exist, do not collate
    if (filecounter == 0) {
      std::cout << "\n\nNo files found for " << tuple->_filesafe_name
      << ". Continuing...\n\n";
      continue;
    }

    std::cout << "\nFor " << tuple->_filesafe_name<< ":\n";
    for (unsigned i{0}; i < optimal_foms.size(); i++) {
      std::cout << i << "th minimisation minimum FoM: "
      << optimal_foms[i] << "\n";
      std::cout << "Associated coordinates:\n";
      int paramindex{0};
      for (unsigned j{0}; j < params.size(); j++) {
        if (tuple->_activate_params.at(j)) {
          std::cout << params._paramnames.at(j) << ": "
          << optimal_parameter_inputs.at(paramindex).at(i) << " | ";
          paramindex++;
        }
      }
      std::cout << "\n";
    }
    // std::cout << "Finished scanning coordinates.\n";

    if (gSystem->AccessPathName(collated_filepath.c_str())) {
      write_collated_results_to_master_file(
      collated_filepath, tuple, optimal_foms,
      optimal_minbias_rates, optimal_minbias_efficiencies,
      optimal_minbias_efficiency_errs, optimal_signal_effs,
      optimal_signal_eff_errs, optimal_ratelimited_signal_effs,
      optimal_ratelimited_signal_eff_errs, params,
      optimal_parameter_inputs, optimal_line_efficiencies,
      optimal_line_efficiency_errs, optimal_candidate_counts,
      optimal_minbias_candidate_counts, learning_rates, beta_ones,
      beta_twos, paramscan);
    }

    // std::cout << "Finished writing results to master file.\n";

    int minindex =
    std::distance(std::begin(optimal_foms),
    std::min_element(std::begin(optimal_foms), std::end(optimal_foms)));
    std::cout << "Best FoM was " << minindex << "th out of "
    << optimal_foms.size() << "\n";

    double average_percentage_difference{0};
    for (double eff: optimal_ratelimited_signal_effs) {
      average_percentage_difference +=
      fabs(eff - optimal_ratelimited_signal_effs[minindex]) * 100;
    }

    average_percentage_difference
    /= (optimal_ratelimited_signal_effs.size() * 1.0);

    std::cout << "Average percentage difference for efficiency of "
    << tuple->_name
    << ": " << average_percentage_difference << "\n";

    average_difference_in_efficiency_to_max += average_percentage_difference;

    // Recording and printing the optimal results
    std::cout << "\n Final Collated (Best) Result for "
    << tuple->_filesafe_name << ":\n";
    tuple->_optimal_fom = optimal_foms[minindex];
    std::cout << "Optimal figure of merit: " << tuple->_optimal_fom
    << "\n";
    tuple->_optimal_minbias_rate = optimal_minbias_rates[minindex];
    std::cout << "Optimal minbias rate: " << tuple->_optimal_minbias_rate
    << "\n";
    tuple->_optimal_minbias_efficiency =
    optimal_minbias_efficiencies[minindex];
    std::cout << "Optimal minbias efficiency: "
    << tuple->_optimal_minbias_efficiency << "\n";
    tuple->_optimal_minbias_efficiency_err =
    optimal_minbias_efficiency_errs[minindex];
    std::cout << "Optimal minbias efficiency error: "
    << tuple->_optimal_minbias_efficiency_err << "\n";
    tuple->_optimal_signal_eff = optimal_signal_effs[minindex];
    std::cout << "Optimal signal efficiency: "
    << tuple->_optimal_signal_eff << "\n";
    tuple->_optimal_signal_eff_err =
    optimal_signal_eff_errs[minindex];
    std::cout << "Optimal signal efficiency error: "
    << tuple->_optimal_signal_eff_err << "\n";
    tuple->_optimal_ratelimited_signal_eff =
    optimal_ratelimited_signal_effs[minindex];
    std::cout << "Optimal ratelimited signal efficiency: "
    << tuple->_optimal_ratelimited_signal_eff << "\n";
    tuple->_optimal_ratelimited_signal_eff_err =
    optimal_ratelimited_signal_eff_errs[minindex];
    std::cout << "Optimal ratelimited signal efficiency error: "
    << tuple->_optimal_ratelimited_signal_eff_err << "\n";

    int paramindex{0};
    for (unsigned j{0}; j < params.size(); j++) {
      if (tuple->_activate_params[j]) {
        std::cout << params._paramnames[j] << ": "
        << optimal_parameter_inputs[paramindex][minindex] << " | ";
        paramindex++;
      }
    }
    std::cout << "\n";

    for (unsigned i{0}; i < optimal_line_efficiencies.size(); i++) {
      tuple->_specline_adam->_signal_efficiencies.push_back(
      optimal_line_efficiencies[i][minindex]);
      tuple->_specline_adam->_signal_efficiency_errs.push_back(
      optimal_line_efficiency_errs[i][minindex]);
      tuple->_optimal_candidate_counts.push_back(
      optimal_candidate_counts[i][minindex]);
    }
  }

  average_difference_in_efficiency_to_max /= (tuples.size() * 1.0);
  std::cout << "\nMean percentage difference in ratelimited efficiency "
  "to maximum ratelimited efficiency: "
  << average_difference_in_efficiency_to_max << "\n";
}
