#include "init_discrete_gradient.h"
#include "init_save_and_collate.h"
#include "inclusive_hadron_lines.h"
#include "adam_base.h"
#include "line_parameters.h"
#include "misc_tools.h"


init_discrete_gradient::init_discrete_gradient(
  io_manager* tuple, double collision_rate, double ratelimit,
  double scalewidth, int accuracy, bool include_prints
  ) : adam_base(false, {tuple}, collision_rate, ratelimit,
  scalewidth, accuracy, include_prints) {
  // Constructor for homemade discrete gradient minimiser class
  // glob_or_indiv set to false (individual) 
  _tuple = tuple;
}


std::vector<double> init_discrete_gradient::speceff_minimised_function(
  const line_parameters input_parameters, const unsigned& lineindex
  ) const {
  // Performs a cut only on specified param
  // with input parameters on a tuple (for line-specific efficiencies)

  const line_store linestore;
  double initial_count = _tuple->_unique_precut_events;
  double final_count =
  (double)_tuple->specline_unique_events(input_parameters, lineindex);

  // SEffs
  double signal_efficiency = (initial_count == 0) ? 0 :
  1.0 * final_count / initial_count;

  double signal_efficiency_err = (signal_efficiency >= 1.0
  || signal_efficiency <= 0.0) ? 1e-8 : sqrt(fabs(signal_efficiency
  * (1 - signal_efficiency)) / initial_count);

  // BEffs
  double background_efficiency =
  1.0 * _tuple->_background_io->specline_unique_events(input_parameters,
  lineindex) / _tuple->_background_io->_unique_precut_events;
  double background_efficiency_err = (background_efficiency >= 1.0
  || background_efficiency <= 0.0) ? 1e-8 :
  sqrt(fabs(background_efficiency * (1 - background_efficiency)) /
  _tuple->_background_io->_unique_precut_events);

  // Ratelimited SEffs
  double ratelim_signal_efficiency = 
  signal_efficiency * ratelimiter(background_efficiency,
  _ratelimit);
  
  double ratelim_signal_efficiency_err = signal_efficiency_err
  * ratelimiter(background_efficiency, _ratelimit);
  // We can plot the minimiser path using these
  double fom = (1.0 - ratelim_signal_efficiency)
  * (1.0 - ratelim_signal_efficiency) + 0.0001 * background_efficiency;

  return {fom, background_efficiency, background_efficiency_err,
  background_efficiency * _collision_rate,
  background_efficiency_err * _collision_rate, signal_efficiency,
  signal_efficiency_err, ratelim_signal_efficiency,
  ratelim_signal_efficiency_err, final_count};

}


std::vector<double> init_discrete_gradient::minimised_function(
  const line_parameters& input_parameters
  ) const {
  // FoM calculation function for initial minimiser.
  // Employs signal efficiency ratelimited by minbias in calculation.

  // Calculated beforehand
  double initial_count = _tuple->_unique_precut_events;
  // Cut on all lines in the signal sample
  double final_count =
  (double)_tuple->calc_unique_events(input_parameters,
   _tuple->_activate_lines);

  // SEffs
  double signal_efficiency = (initial_count == 0) ? 0 :
  1.0 * final_count / initial_count;

  double signal_efficiency_err = (signal_efficiency >= 1.0
  || signal_efficiency <= 0.0) ? 1e-8 : sqrt(fabs(signal_efficiency
  * (1 - signal_efficiency)) / initial_count);

  double ratelimit_factor{1.0};
  double background_efficiency{1.0};
  double background_efficiency_err{0.0};
  if (_loose_thresh_lt_cap) {
    if (!_background_efficiencies.empty()) {
      background_efficiency = _background_efficiencies.back();
      background_efficiency_err = _background_efficiency_errs.back();
    }
  } else {
    // BEffs
    background_efficiency =
      1.0 * _tuple->_background_io->calc_unique_events(input_parameters,
      _tuple->_activate_lines)
      / _tuple->_background_io->_unique_precut_events;

    background_efficiency_err = (background_efficiency >= 1.0
    || background_efficiency <= 0.0) ? 1e-8 :
    sqrt(fabs(background_efficiency * (1 - background_efficiency)) /
    _tuple->_background_io->_unique_precut_events);

    ratelimit_factor = ratelimiter(background_efficiency, _ratelimit);
  }

  // Ratelimited SEffs
  double ratelim_signal_efficiency = signal_efficiency * ratelimit_factor;
  double ratelim_signal_efficiency_err = signal_efficiency_err
  * ratelimit_factor;

  double fom = (1.0 - ratelim_signal_efficiency)
  * (1.0 - ratelim_signal_efficiency) + 0.0001 * background_efficiency;

  return {fom, background_efficiency, background_efficiency_err,
  background_efficiency * _collision_rate,
  background_efficiency_err * _collision_rate, signal_efficiency,
  signal_efficiency_err, ratelim_signal_efficiency,
  ratelim_signal_efficiency_err, final_count};
}


void init_discrete_gradient::min_path_loop(
  unsigned i, unsigned j, std::vector<double>& first_axis_values,
  std::vector<double>& second_axis_values,
  std::string cut_variable1, std::string cut_variable2
  ) const {

  int read_count{0};
  for (unsigned k{i}; k < _minpath_coords[j].size(); k++) {
    if (_tuple->_activate_params[k]) {
      if (cut_variable1.compare(_minpath_coords[j]._paramnames[k])
      == 0) {
        first_axis_values.push_back(
        read_access(_minpath_coords[j], k));
        read_count++;
      } else if (cut_variable2.compare(
        _minpath_coords[j]._paramnames[k]) == 0) {
        second_axis_values.push_back(
        read_access(_minpath_coords[j], k));
        read_count++;
      }
      if (read_count > 1) break;
    }
  }
}


void init_discrete_gradient::plot_min_path(
  std::string plot_dir, int job_num
  ) {
  // Plotting all FoMs calculated by the minimiser and the corresponding
  // inputs as a top-down TGraph2D object (technically 1D)

  std::string tuplename = _tuple->_name;
  std::string filesafe_name = _tuple->_filesafe_name;

  line_parameters params;

  std::vector<std::string> temp_variables =
  _tuple->_background_io->_axes;

  std::vector<std::string> cut_variables;
  std::vector<std::string> lc_cut_variables;
  // const auto& parameters= _tuple->_background_io->_parameters; 
  
  int index{0}; 
  for (unsigned i{0}; i < params._paramnames.size(); i++) {
    if (_tuple->_activate_params[i]) {
      cut_variables.push_back(temp_variables[i]);
      lc_cut_variables.push_back(params._paramnames[i]);

      cut_variables.back()[0] = toupper(cut_variables.back()[0]);
      std::cout << "Cut axis and variable: " << cut_variables.back()
      << ", " << lc_cut_variables.back() << "\n";
    }
    index++;
  }

  if (lc_cut_variables.size() != cut_variables.size()) {
    std::cout << "Axis label vector in plot_min_path (src/"
    "init_discrete_gradient.cpp) is not the same size as "
    "the regular label vector. Check file please. Exiting...\n";
    exit(0);
  }


  double ratecap = (_tuple->_percentage_ratelimit / 100.0)
  * ((_tuple->_collision_rate * _tuples[0]->_ratelimit) /1e6);

  std::stringstream ratecap_ss;
  ratecap_ss << std::fixed << std::setprecision(3) << ratecap;
  std::string ratecap_string = ratecap_ss.str() + "_mhz";
  boost::replace_all(ratecap_string, ".", "point");

  std::string plot_title = "Figure of Merit MinPath for " + filesafe_name;

  std::string canvas_dir = plot_dir + filesafe_name + "_"
  + ratecap_string + "_minpath_plots/";
  std::filesystem::create_directories(canvas_dir.c_str());

  std::cout << "Unique params: " << _tuple->_parameters.size() << "\n";
  if (lc_cut_variables.size() > 2) {
    // Plotting several parameter pair FoM paths 
    for (unsigned i{0}; i < cut_variables.size() - 1; i++) {
      std::vector<std::string> plot_strings =
      {filesafe_name + "_job" + std::to_string(job_num) + "_comb"
      + std::to_string(i),
      cut_variables[i],
      cut_variables[i+1], "FoM", plot_dir, plot_title, canvas_dir};
      
      std::vector<double> first_axis_values, second_axis_values;
      for (unsigned j{0}; j < _minpath_coords.size(); j++) {
        min_path_loop(i, j, first_axis_values, second_axis_values,
        lc_cut_variables[i], lc_cut_variables[i+1]);
      }

      std::cout << "X and Y axis vector sizes: " << first_axis_values.size()
      << ", " << second_axis_values.size() << "\n";

      // Topdown view
      std::cout << "Generating plot for combination " << cut_variables[i] << ", "
      << cut_variables[i+1] << " and job: " << job_num << "\n";
      plot_results_vs_lineor_cut(plot_strings, first_axis_values,
      second_axis_values, _averaged_foms, -90, 270, "pcolz line");
    }
    
    // Last and first  
    int initial_index{0};
    int final_index{0};
    for (unsigned i{0}; i < _tuple->_activate_params.size(); i++) {
      if (_tuple->_activate_params[i]) {
        if (final_index == 0) {
          initial_index = i;
        }
        final_index = i;
      }
    }
    
    std::vector<std::string> plot_strings = 
    {filesafe_name + "_job" + std::to_string(job_num) + "_comb"
    + std::to_string(cut_variables.size() - 1),
    cut_variables.back(), cut_variables[0], "FoM", plot_dir, plot_title,
    canvas_dir};

    std::vector<double> first_axis_values, second_axis_values;
    
    for (unsigned j{0}; j < _minpath_coords.size(); j++) {
      first_axis_values.push_back(
      _minpath_coords[j][final_index]);
      second_axis_values.push_back(_minpath_coords[j][initial_index]);
    }

    // Topdown view
    plot_results_vs_lineor_cut(plot_strings, first_axis_values,
    second_axis_values, _averaged_foms, -90, 270, "pcolz line");
  } else if (_tuple->_parameters.size() > 1) {
    std::vector<std::string> plot_strings =
    {filesafe_name + "_job" + std::to_string(job_num) + "_comb0",
    cut_variables[0], cut_variables[1], "FoM", plot_dir,
    plot_title, canvas_dir};
    
    std::vector<double> first_axis_values, second_axis_values;
    for (unsigned i{0}; i < _minpath_coords.size(); i++) {
      min_path_loop(0, i, first_axis_values, second_axis_values,
      lc_cut_variables[0], lc_cut_variables[1]);
    }
    // std::cout << "X and Y axis vector sizes: " << first_axis_values.size()
    // << ", " << second_axis_values.size() << "\n";
    // Topdown view
    plot_results_vs_lineor_cut(plot_strings, first_axis_values,
    second_axis_values, _averaged_foms, -90, 270, "pcolz line");
  }
}

void store_individual_results(
  io_manager* tuple, line_parameters grid_search_result,
  double collision_rate, double ratelimit, adam_base* initial_minimiser
  ) {

  std::vector<std::string> params_used;
  line_parameters paramset;
  tuple->_optimal_parameter_inputs.clear();
  for (unsigned i{0}; i < grid_search_result.size(); i++) {
    if (tuple->_activate_params[i]) {
      params_used.push_back(paramset._paramnames[i]);

      std::string unit = paramset._paramnames[i].find("alpha") !=
      std::string::npos || paramset._paramnames[i].find("_pt")
      != std::string::npos ? " MeV" : "";
      unit = paramset._paramnames[i].find("_ip") !=
      std::string::npos || paramset._paramnames[i].find("bpvfd")
      != std::string::npos ? " mm" : unit;

      std::cout << "Optimal value for parameter "
      << params_used.back() << ": " << grid_search_result[i] << unit << "\n";
        
      if ( paramset._paramnames[i] == "twotrack_ks") {
        twotrackks().print_thresholds(paramset); 
        std::cout << "Untransformed twotrack_ks: "
        << twotrack_ks_transform(grid_search_result[i]) << "\n";
      }

      // else if (paramset._paramnames[i].find("alpha") != std::string::npos) {
      // std::cout
        // std::cout << "Untransformed "
        //   << grid_search_result._paramnames[i] << ": "
        //   << alpha_transform(grid_search_result[i]) << " MeV\n";
      // }

      tuple->_optimal_parameter_inputs.push_back(grid_search_result[i]);
    }
  }

  tuple->_optimal_lps = grid_search_result;
  tuple->_optimal_paramnames = params_used;

  tuple->_optimal_candidate_counts =
  tuple->calc_candidates_per_line(grid_search_result,
  tuple->_activate_lines);
  
  tuple->_optimal_minbias_candidate_counts =
  tuple->_background_io->calc_candidates_per_line(grid_search_result,
  tuple->_activate_lines);
  
  tuple->_optimal_unique_events_per_line =
  tuple->events_unique_to_lines(grid_search_result, tuple->_activate_lines);

  line_store linestore;
  int counter{0};
  for (unsigned i{0}; i < NUM_LINES; i++) {
    if (tuple->_activate_lines[i]) {
      std::cout << "Optimal line candidate count out of total stored "
        "(after preselection) for "
        << linestore._linenames[i] << ": "
        << tuple->_optimal_candidate_counts[counter]
        // << " / " << tuple->_stored_candidates_per_line[counter]
        << "\n";
      counter++;
    }
  }

  std::cout << "\n\nTotal " << tuple->_filesafe_name << " events = "
    << tuple->_unique_precut_events << "\n";

  init_discrete_gradient* specline_minimiser =
  new init_discrete_gradient(tuple, collision_rate, ratelimit,
  0.25, 2); // Scalewidth and accuracy don't matter here

  counter = 0;
  specline_minimiser->_background_efficiencies.resize(NUM_LINES);
  specline_minimiser->_background_efficiency_errs.resize(NUM_LINES);
  specline_minimiser->_signal_efficiencies.resize(NUM_LINES);
  specline_minimiser->_signal_efficiency_errs.resize(NUM_LINES);

  for (unsigned i{0}; i < NUM_LINES; i++) {
    if (!tuple->_activate_lines[i]) continue; 
    std::vector<double> specstats =
    specline_minimiser->speceff_minimised_function(grid_search_result, i);
    
    specline_minimiser->_background_efficiencies[i] =  specstats[1];
    specline_minimiser->_background_efficiency_errs[i] = specstats[2];
    specline_minimiser->_signal_efficiencies[i] = specstats[5];
    specline_minimiser->_signal_efficiency_errs[i] = specstats[6];

    std::cout << "Number of events passing "
    << linestore._linenames[i]
    << " out of total: "
    << specstats[5] * tuple->_unique_precut_events << " / "
    << tuple->_logfile_events_per_line[counter] << "\n";

    std::cout << "Number of minbias events passing "
    << linestore._linenames[i]
    << " out of total: "
    << specstats[1] * tuple->_background_io->_unique_precut_events
    << " / "
    << tuple->_background_io->_logfile_events_per_line[i] << "\n";

    counter++;
  }

  std::cout << "\n";

  for (unsigned i{0}; i < NUM_LINES; i++) {
    if (tuple->_activate_lines[i]) {
      std::cout << "Optimal unique events passing "
      << linestore._linenames[i] << ": "
      << tuple->_optimal_unique_events_per_line[i] << "\n";
    }
  }
  std::cout << "\n";


  tuple->_maxeff_adam =
  dynamic_cast<init_discrete_gradient*>(initial_minimiser);
  tuple->_specline_adam = specline_minimiser;
}

void apply_adam_minimiser_individually(
  io_manager* tuple, const double collision_rate, const double ratelimit,
  const int niterations, const double alpha, const double beta1,
  const double beta2, const double epsilon, const double scalewidth,
  const int accuracy, const bool plot_min_paths,
  const std::string plot_dir, const std::vector<double> fixed_starting_point,
  const std::string outfile_dir, const int job_num,
  const int max_grid_searches, const bool include_prints,
  const bool random_start, const bool skip_grid_search
  ) {
  // Overhead for applying Adam minimisation to an individual sample
  // and then finding the grid coordinate with the smallest FoM in the
  // neighbourhood of the continuous Adam result

  std::cout << "\n\nBeginning Adam Minimisation for "
  + tuple->_filesafe_name + "...\n\n";
  // Deactivating certain lines for initfnc
  line_store linestore;
  line_parameters params;

  double optimal_fom;
  adam_base* initial_minimiser =
  new init_discrete_gradient(tuple, collision_rate, ratelimit, scalewidth,
  accuracy, include_prints);
  line_parameters grid_search_result;

  std::vector<io_manager*> tuples{tuple};
  int grid_search_count{max_grid_searches + 1};
  bool no_tunable_params = tuple->_parameters.size() == 0;
  if (no_tunable_params) {
    std::cout << "No tunable parameters for "
    << tuple->_filesafe_name << ". Skipping minimisation...\n";
    
    initial_minimiser->minimised_function(grid_search_result);
  } else {
    if (tuple->_loosest_selection) {
      std::cout << "Loosest selection produces rate < "
      << (ratelimit * collision_rate)/1e6
      << " MHz. Choosing loosest selection...\n";
      for (unsigned i{0}; i < grid_search_result.size(); i++) {
        if (tuple->_activate_params[i]) {
          grid_search_result[i] =
          tuple->_background_io->_parameters[params._paramnames[i]]._min;
        } else {
          grid_search_result[i] = 0;
        }
      }

    } else {
      grid_recursive_minimisation(tuples, collision_rate, ratelimit,
      niterations, alpha, beta1, beta2, epsilon, scalewidth, accuracy,
      fixed_starting_point, optimal_fom, *initial_minimiser,
      grid_search_count, max_grid_searches, grid_search_result,
      include_prints, random_start, skip_grid_search);
    } 
  }


  std::vector<double> optimal_statistics =
  initial_minimiser->minimised_function(grid_search_result);

  // Recording and printing the optimal results
  std::cout << "\nResults for " << tuple->_filesafe_name << " tuple:\n";
  tuple->_optimal_fom = optimal_statistics[0];

  std::cout << "Optimal figure of merit: " << tuple->_optimal_fom << "\n";
  tuple->_optimal_minbias_efficiency = optimal_statistics[1];
  tuple->_optimal_minbias_efficiency_err = optimal_statistics[2];
  
  std::cout << "Optimal minbias efficiency: "
  << tuple->_optimal_minbias_efficiency << " +/- "
  << tuple->_optimal_minbias_efficiency_err << "\n";
  tuple->_optimal_minbias_rate = optimal_statistics[3];
  tuple->_optimal_minbias_rate_error = optimal_statistics[4];
  
  std::cout << "Optimal minbias rate (MHz): " << tuple->_optimal_minbias_rate/1e6
  << " +/- " <<  tuple->_optimal_minbias_rate_error/1e6 << "\n";
  tuple->_optimal_signal_eff = optimal_statistics[5];
  tuple->_optimal_signal_eff_err = optimal_statistics[6];
  
  std::cout << "Optimal signal efficiency: " << tuple->_optimal_signal_eff
  << " +/- " << tuple->_optimal_signal_eff_err << "\n";
  tuple->_optimal_ratelimited_signal_eff = optimal_statistics[7];
  tuple->_optimal_ratelimited_signal_eff_err = optimal_statistics[8];
  
  std::cout << "Optimal ratelimited signal efficiency: "
  << tuple->_optimal_ratelimited_signal_eff
  << " +/- " << tuple->_optimal_ratelimited_signal_eff_err << "\n";

  store_individual_results(tuple, grid_search_result, collision_rate,
  ratelimit, initial_minimiser);

  // If minimisation print is verbose, then save the minimisation results
  // to root files
  if (include_prints && !no_tunable_params) {
    save_or_edit_indiv_min_results(
    tuple, alpha, beta1, beta2, fixed_starting_point,
    outfile_dir, job_num, plot_min_paths, plot_dir);
  }
}


double multiple_minimisation_runs(
  std::vector<io_manager*> tuples, double collision_rate, double ratelimit,
  int niterations, double alpha, double beta1, double beta2,
  double epsilon, double scalewidth, int accuracy, bool plot_min_paths,
  std::string plot_dir, std::string outfile_dir, int nruns,
  int job_num, int max_grid_searches, bool random_start, bool skip_grid_search
  ) {
  // Run the individual minimisation nruns times to assess if final result
  // is consistent. Return the average time taken to find the solution

  TStopwatch t;
  t.Start();
  for (int i{0}; i < nruns; i++) {
    std::cout << "Current run: " << i << "\n";
    // Include all prints with true as final argument
    apply_adam_minimiser_individually(tuples[0],
    collision_rate, ratelimit, niterations, alpha, beta1,
    beta2, epsilon, scalewidth, accuracy, plot_min_paths, plot_dir,
    {}, outfile_dir, job_num, max_grid_searches, true, random_start,
    skip_grid_search);
  }
  return t.RealTime() / (1.0 * nruns);
}


void minimise_all_individual_ntuples(
  std::vector<io_manager*> tuples, double collision_rate, double ratelimit,
  int niterations, double alpha, double beta1, double beta2,
  double epsilon, double scalewidth, int accuracy, bool plot_min_paths,
  std::string plot_dir, std::vector<double> fixed_starting_point,
  std::string outfile_dir, int max_grid_searches, bool random_start,
  bool skip_grid_search
  ) {
  // Applies Adam minimiser to all ntuples/samples individually

  TStopwatch t;
  t.Start();
  int counter{0};
#pragma omp parallel
  {
#pragma omp for schedule (dynamic) nowait
    for (io_manager* tuple: tuples) {
      // Do not include all prints, or plot (yet)
      TStopwatch tempt;
      tempt.Start();
      
      apply_adam_minimiser_individually(tuple, collision_rate,
      ratelimit, niterations, alpha, beta1, beta2, epsilon, scalewidth,
      accuracy, false, plot_dir, fixed_starting_point, outfile_dir, 0,
      max_grid_searches, false, random_start, skip_grid_search);

      tempt.Stop();

      std::cout << "\nTime taken for " << tuple->_filesafe_name
      << " FoM to be minimised by Adam:\n";

      tempt.Print();
      std::cout << "\n";
      counter++;
      std::cout << "Number of samples minimised: " << counter << "\n";
    }
  } 

  // Writes after to avoid thread safety issues
  for (io_manager* tuple: tuples) {
    save_or_edit_indiv_min_results(tuple, alpha, beta1, beta2,
    fixed_starting_point, outfile_dir, 0, false, plot_dir);
  }

  std::cout << "\nTime taken to individually minimise all " << tuples.size()
    << " tuples in parallel before global minimisation is:\n";
  t.Print();
  std::cout << "\n";
}
