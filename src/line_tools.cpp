#include "line_tools.h"


double alpha_revert(const double alpha) {
  return alpha; // return sign(alpha) * std::pow((fabs(alpha)/100), 1/1.497);
}


double alpha_transform(const double alpha_trans) {
  return alpha_trans ; // 100 * sign(alpha_trans) * std::pow(fabs(alpha_trans), 1.497);
}


double find_nearest_alpha_value(const double alpha) {
  // Finds alpha value in transformed space to the nearest 20 MeV
  
  double mev = alpha_transform(alpha);
  int sign = mev < 0 ? -1 : 1;
  double mev_closest = round(abs(mev)/20.0) * 20.0;
  return alpha_revert(sign*mev_closest);
}


double twotrack_ks_revert(const double twotrack_ks_trans) {
  return log(twotrack_ks_trans);
}


double twotrack_ks_transform(const double twotrack_ks) {
  return exp(twotrack_ks);
}

/*
double ghost_prob_transform(const double ghost_prob) {
  return 1.0 - ghost_prob;
}

double ghost_prob_revert(const double ghost_prob_transformed) {
  return 1.0 - ghost_prob_transformed;
}
*/
