#include "adam_base.h"
#include "io_manager.h"
#include "opt_discrete_gradient.h"
#include "init_discrete_gradient.h"


line_parameters adam_base::adam_recursive_minimisation(
  const int niterations, const double alpha, const double beta1,
  const double beta2, const double epsilon,
  const std::vector<double> fixed_starting_point, bool random_start,
  bool skip_grid_search
  ) {
  // Warm restart of Adam to increase chances of finding global minimum

  std::vector<bool> active_params = _glob_or_indiv ?
  _tuples[0]->_background_io->_activate_params
  : _tuples[0]->_activate_params;

  line_parameters first_solution =
  adam_minimum(niterations, alpha, beta1, beta2,
  epsilon, fixed_starting_point, false, random_start);
  std::vector<double> first_min_stats = minimised_function(first_solution);

  line_parameters second_solution = first_solution;
  if (!_test_setting) {
    if (_glob_or_indiv) {
      std::cout << "\nBeginning warm restart for global minimisation..."
      "\n";
    } else {
      std::cout << "\nBeginning warm restart for "
      << _tuples[0]->_filesafe_name << "\n";
    }
    second_solution =
    adam_minimum(niterations, alpha, beta1, beta2,
    epsilon, first_solution.return_vector(), true, false);
  }
  

  if (_glob_or_indiv || _include_prints) {
    std::cout << " Finding smallest nearest-point grid FoM...\n";
    std::cout << "\n\nSearching nearest grid points:\n\n";
  }
  // Find smallest continuous FoM, and then the associated grid
  // point
  int smallest_index =
  std::distance(std::begin(_all_foms),
  std::min_element(std::begin(_all_foms),
  std::end(_all_foms)));
  // std::cout << "Best fom (" << smallest_index << " / " << _all_foms.size()
  // << "): " << _all_foms[smallest_index] << "\n";
  // std::cout << "Associated solution (" << smallest_index << " / "
  // << _inputs_used.size() << "): " << _inputs_used[smallest_index] << "\n";
  line_parameters associated_point =
  _tuples[0]->find_nearest_point(_inputs_used[smallest_index]);
  
  if (_glob_or_indiv || _include_prints) {
    std::cout << "\nBest continuous solution: \n";
    std::cout << std::setw(3) << "#" << " " << std::left << std::setw(35)
    << "Parameter" << " " << std::setw(15) << "Threshold\n"
    << std::setw(83) <<
    " -----------------------------------------------------------------------"
    "----------- " << std::endl;
    for (unsigned i{0}; i < _inputs_used[smallest_index].size(); i++) {
      if (_glob_or_indiv || active_params[i]) {
        if (_parameters[i]._fixed) {
          std::cout << std::setw(3) << i << " " << std::left
          << std::setw(35) << _inputs_used[smallest_index]._paramnames.at(i)
          << " " << std::setw(15) << " fixed " << std::endl;
        } else {
          std::cout << std::setw(3) << i << " " << std::left
          << std::setw(35) << _inputs_used[smallest_index]._paramnames.at(i)
          << " " << std::setw(15) << _inputs_used[smallest_index].access(i)
          << std::endl;
        }
      }
    }

    std::cout << "\nNearest discrete solution: \n";
    std::cout << std::setw(3) << "#" << " " << std::left << std::setw(35)
    << "Parameter" << " " << std::setw(15) << "Threshold\n"
    << std::setw(83) <<
    " -----------------------------------------------------------------------"
    "----------- " << std::endl;
    for (unsigned i{0}; i < associated_point.size(); i++) {
      if (_glob_or_indiv || active_params[i]) {
        if (_parameters[i]._fixed) {
          std::cout << std::setw(3) << i << " " << std::left
          << std::setw(35) << associated_point._paramnames.at(i)  << " " 
          << std::setw(15) << " fixed " << std::endl;
        } else {
          std::cout << std::setw(3) << i << " " << std::left
          << std::setw(35) << associated_point._paramnames.at(i)  << " " 
          << std::setw(15) << associated_point.access(i) << std::endl; 
        }
      }
    }

  }


  for (unsigned i{0}; i < associated_point.size(); i++) {
    if (_parameters[i]._fixed) associated_point[i] = _parameters[i]._value;
  }

  double associated_grid_fom =
  minimised_function(associated_point)[0];

  if (_test_setting || skip_grid_search) {
    std::cout << "Test setting or grid search skip active. Skipping non-"
    "continuous search...\n";
    return associated_point;
  }

  // Find smallest FoM of nearest-grid-point to central
  // gradient value
  std::vector<line_parameters> calced_points;
  std::vector<double> calced_foms;

  for (unsigned j{0}; j < _minpath_coords.size(); j++) {
    line_parameters nearest =
    _tuples[0]->find_nearest_point(_minpath_coords[j]);
    for (unsigned i{0}; i < nearest.size(); i++) {
      if (_parameters[i]._fixed) nearest[i] = _parameters[i]._value;
    }
    if (std::find(_nearest_grid_coords.begin(),
    _nearest_grid_coords.end(),
    nearest) == _nearest_grid_coords.end()) {
      calced_points.push_back(nearest);
      calced_foms.push_back(0.0);
    }
    _nearest_grid_coords.push_back(nearest);
  }
  
#pragma omp parallel for if (_glob_or_indiv)
  for (unsigned j = 0; j < calced_points.size(); j++) {
    line_parameters rounded = calced_points[j];
    calced_foms[j] = minimised_function(rounded)[0];
  }

  for (unsigned j{0}; j < _nearest_grid_coords.size(); j++) {
    int index{0};
    for (unsigned k{0}; k < calced_points.size(); k++) {
      if (_nearest_grid_coords[j] == calced_points[k]) {
        index = k;
        break;
      }
    }

    _nearest_grid_foms.push_back(calced_foms[index]);
    if (_glob_or_indiv) {
      std::cout << "Nearest grid coord:\n";
      _nearest_grid_coords[j].print(active_params);
      std::cout << "\nAssociated FoM: " << _nearest_grid_foms[j]
      << "\n";
    }
  }

  int smallest_grid_index =
  std::distance(std::begin(calced_foms),
  std::min_element(std::begin(calced_foms),
  std::end(calced_foms)));
  line_parameters smallest_grid_point =
  calced_points[smallest_grid_index];
  double smallest_grid_fom =
  calced_foms[smallest_grid_index];

  int gradient_index{0};
  if (_glob_or_indiv || _include_prints) {
    std::cout << "\nNearest grid point to continuous result: \n";
    std::cout << std::setw(3) << "#" << " " << std::left << std::setw(35)
    << "Parameter" << " " << std::setw(15) << "Threshold\n"
    << std::setw(83) <<
    " -----------------------------------------------------------------------"
    "----------- " << std::endl;

    for( unsigned i{0} ; i < associated_point.size(); i++) {
      if (active_params.at(i)) {
        if (_parameters[i]._fixed) {
          std::cout << std::setw(3) << i << " " << std::left
          << std::setw(35) << associated_point._paramnames.at(i)  << " " 
          << std::setw(15) << " fixed " << std::endl;
        } else {
          std::cout << std::setw(3) << i << " " << std::left
          << std::setw(35) << associated_point._paramnames.at(i)  << " " 
          << std::setw(15) << associated_point[i] << std::endl; 
        }
        gradient_index++;
      }
    }
    std::cout << "\nAssociated FoM: " << associated_grid_fom << "\n";
    std::cout << "Smallest nearest grid fom to path: "
    << smallest_grid_fom << "\n";
    std::cout << "Corresponding coordinates: ";
    std::cout << std::setw(3) << "#" << " " << std::left << std::setw(35)
    << "Parameter" << " " << std::setw(15) << "Threshold\n"
    << std::setw(83) <<
    " -----------------------------------------------------------------------"
    "----------- " << std::endl;

    gradient_index = 0;
    for( unsigned i{0} ; i < smallest_grid_point.size(); i++) {
      if (active_params.at(i)) {
        if (_parameters[i]._fixed) {
          std::cout << std::setw(3) << i << " " << std::left
          << std::setw(35) << smallest_grid_point._paramnames.at(i)  << " " 
          << std::setw(15) << " fixed " << std::endl;
        } else {
          std::cout << std::setw(3) << i << " " << std::left
          << std::setw(35) << smallest_grid_point._paramnames.at(i)  << " "
          << std::setw(15) << smallest_grid_point[i] << std::endl; 
        }
        gradient_index++;
      }
    }
    std::cout << "\n";
  }

  double smallest_fom{0}; 
  line_parameters solution_coords;
  
  if (associated_grid_fom <= smallest_grid_fom) {
    if (_glob_or_indiv || _include_prints) {
      std::cout << "\n\nGrid point closest to smallest continuous "
      "FoM value yielded smallest FoM.\n";
    }
    smallest_fom = associated_grid_fom;
    solution_coords = associated_point;
  } else {
    if (_glob_or_indiv || _include_prints) {
      std::cout << "\n\nNearby grid point to path"
      " was smallest.\n";
    }
    smallest_fom = smallest_grid_fom;
    solution_coords = smallest_grid_point;
  }

  
  if (_glob_or_indiv || _include_prints) {
    std::cout << "Smallest FoM: " << smallest_fom << "\n";
    std::cout << "Associated coordinates: \n";
    gradient_index = 0;
    for( unsigned i{0} ; i < solution_coords.size(); i++) {
      if (active_params.at(i)) {
        if (_parameters[i]._fixed) {
          std::cout << std::setw(3) << i << " " << std::left
          << std::setw(35) << solution_coords._paramnames.at(i)  << " " 
          << std::setw(15) << " fixed " << std::endl;
        } else {
          std::cout << std::setw(3) << i << " " << std::left
          << std::setw(35) << solution_coords._paramnames.at(i)  << " "
          << std::setw(15) << solution_coords[i] << std::endl; 
        }
        gradient_index++;
      }
    }
    std::cout << "\n";
  }

  return solution_coords;
}


void grid_recursive_minimisation(
  std::vector<io_manager*> tuples, double collision_rate,
  double ratelimit, int niterations, double alpha, double beta1,
  double beta2, double epsilon, double scalewidth, int accuracy,
  std::vector<double> fixed_starting_point, double& optimal_fom,
  adam_base& minimiser, int grid_search_count,
  int max_grid_searches, line_parameters& grid_search_result,
  bool include_prints, bool random_start, bool skip_grid_search
  ) {

  if (minimiser._glob_or_indiv) {
    minimiser = opt_discrete_gradient(tuples, collision_rate,
    ratelimit, scalewidth, accuracy);
  } else {
    minimiser =
    init_discrete_gradient(tuples[0], collision_rate, ratelimit,
    scalewidth, accuracy, include_prints);
    if (tuples[0]->_parameters.empty()) {
      minimiser.minimised_function(grid_search_result);
      std::cout << "No tunable parameters for "
      << tuples[0]->_filesafe_name << ". Skipping minimisation...\n";
      return;
    }
  }

  minimiser._test_setting = (max_grid_searches == -1);
  if (minimiser._test_setting) niterations = 1;
  // minimiser._include_prints = minimiser._test_setting;

  line_parameters adam_grid_minimum =
  minimiser.adam_recursive_minimisation(niterations, alpha, beta1,
  beta2, epsilon, fixed_starting_point, random_start, skip_grid_search);

  if (!skip_grid_search) {
    grid_search_result =
    minimiser.recursive_grid_search(adam_grid_minimum,
    optimal_fom, grid_search_count, max_grid_searches);
  } else {
    grid_search_result = adam_grid_minimum;
  }

  for (unsigned i{0}; i < grid_search_result.size(); i++) {
    if (tuples[0]->_background_io->
    _parameters[grid_search_result._paramnames[i]]._fixed) {
      grid_search_result[i] =
      tuples[0]->_background_io->
      _parameters[grid_search_result._paramnames[i]]._value;
    }
  }

  if ((minimiser._glob_or_indiv || minimiser._include_prints)
  && !skip_grid_search) {
    std::cout << "\n\n Finished grid search!\n\n";
  }
}
