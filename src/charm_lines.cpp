#include "charm_lines.h"
#include "line_tools.h"

bool d2pipi_line::operator()(
  const float* monitored_quantities
  ) const {

  float track_pt =
  monitored_quantities[0];

  float track_ip =
  monitored_quantities[1];
  
  float d0_ct =
  monitored_quantities[2];

  return track_pt > _charm_track_pt && track_ip > _charm_track_ip 
  && d0_ct > _charm_track_ip; 
}


bool d2kk_line::operator()(
  const float* monitored_quantities
  ) const {

  float track_pt =
  monitored_quantities[0];
  float track_ip =
  monitored_quantities[1];
  float d0_ct =
  monitored_quantities[2];
  
  return track_pt > _charm_track_pt && track_ip > _charm_track_ip 
     && d0_ct > _charm_track_ip; 
}

bool lambda_ll_detached_track_line::operator()(
  const float* monitored_quantities
  ) const {
 
  return monitored_quantities[0] > _track_mipchi2
  && monitored_quantities[1] > _detached_bpvfd; 
}

bool xi_omega_lll_line::operator()(
  const float* monitored_quantities
  ) const {
  return *monitored_quantities > _ipchi2; 
}


