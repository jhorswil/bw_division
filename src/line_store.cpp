#include "line_store.h"
#include "misc_tools.h" 

template <std::size_t iterator = 0, typename function_type,
typename... line_object>
typename std::enable_if_t<iterator == sizeof...(line_object), void>
for_each_with_counter(const std::tuple<line_object...>&, function_type){}

template <std::size_t iterator = 0, typename function_type,
typename... line_object>
inline typename std::enable_if_t< iterator<sizeof...(line_object), void>
for_each_with_counter(const std::tuple<line_object...>& tuple,
function_type function) {
  function(std::get<iterator>(tuple), iterator);
  for_each_with_counter<iterator + 1, function_type, line_object...>(tuple,
  function);
}

line_store::line_store() {
  // Sort these with corresponding indexes for each line, i.e, Hlt1D2KPi is
  // the same index in _linenames as charm_track_ip;charm_track_pt in
  // _associated_params and 2 in _quantities_per_line.
  // When adding a new line, add to the end of the list so as not to perturb
  // the existing order so indexing remains consistent

  auto lines = make_lines(); 
  for_each_with_counter(lines, [this](const auto& iterator,
  unsigned index) mutable {
    _linenames[index] = std::string(iterator._name).find("DiElectronLowMass")
    != std::string::npos ? "DiElectronLowMass" : iterator._name;
    _quantities_per_line[index] = iterator._num_quantities; 
    _associated_params[index] = iterator.parameter_names(); 
  }); 
}

std::vector<line_manager> line_store::get_line_manager() const {
  auto lines = make_lines(); 
  std::vector<line_manager> line_vector;
  tuple_process::for_each(lines, [&line_vector](
  const auto& iterator) mutable {
    
    auto tokens = split(std::string(iterator._name), ';');
    unsigned line_number = line_vector.size(); 
    for(auto& token : tokens) { 
      std::vector<std::string> monitored_quantities;
      std::vector<std::string> params;
      
      for(auto& quantity : iterator._quantities) 
        monitored_quantities.push_back(token + "__" + std::string(quantity)
        + "_t");

      for(auto& param : iterator._parameters) {
        params.emplace_back(
        param->_name);
      }

      line_vector.emplace_back(token, monitored_quantities, params);
      line_vector.rbegin()->_line_number = line_number; 
      }
    });
  return line_vector; 
}

const line_tuple line_store::make_lines() const { 
  return line_tuple(); 
}

const line_tuple line_store::make_lines(
  const fixedlines_t& fixed_inputs, const linemask_t& fixed_lines,
  const linemask_t& off_lines, const parammask_t& fixed_line_params
  ) const {
  // With fixed line info

  line_tuple linetuple = line_tuple();
  auto fixed_functor =
  [&](auto& line, int iterator
  ) {
    line.register_fixed(fixed_lines.at(iterator));
    line.register_fixed_inputs(fixed_inputs.at(iterator));
    line.register_off(off_lines.at(iterator));
    line.register_param_mask(fixed_line_params.at(iterator));
  };

  tuple_process::for_each_iterated(linetuple, fixed_functor);
  return linetuple;
}