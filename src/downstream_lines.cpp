#include "downstream_lines.h" 
#include "line_tools.h" 

bool DownstreamKsToPiPi::operator()(
  const float* monitored_quantities
  ) const {
  
  return monitored_quantities[0] > _minMVA;
}

bool DownstreamLambdaToPPi::operator()(
  const float* monitored_quantities
  ) const {
  
  return monitored_quantities[0] > _minMVA;
}
