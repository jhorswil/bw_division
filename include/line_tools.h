#ifndef _LINE_TOOLS
#define _LINE_TOOLS
#include <iostream>
#include <vector>
#include <math.h>
#include "electron_lines.h"
#include "muon_lines.h"
#include "inclusive_hadron_lines.h"
#include "charm_lines.h"
#include "calibration_lines.h"
#include "photon_lines.h"
#include "downstream_lines.h"
#include "line_parameters.h"

typedef std::tuple<twotrackmva, trackmva, d2kpi_line, d2pipi_line,
singlehighptelectron, singlehighptmuon, trackelectronmva,
trackmuonmva, twotrackks, d2kk_line, dimuon_highmass, dimuon_noip,
displaced_dielectron, dimuon_displaced, diphoton_highmass, d2kshh_line,
lambda_ll_detached_track_line, xi_omega_lll_line, singlehighptmuon_nomuid,
// DownstreamKsToPiPi,
// DownstreamLambdaToPPi  
dielectron_lowmass> line_tuple;

constexpr unsigned NUM_LINES = std::tuple_size<line_tuple>::value; 
using linemask_t = std::array<bool, NUM_LINES>; 
using fixedlines_t = std::array<line_parameters, NUM_LINES>;
using parammask_t = std::array<std::vector<bool>, NUM_LINES>;

// Same order as above

template <class T> int sign(const T value) {
  if (value >= 0) {
    return 1;
  } else {
    return -1;
  }
}


double alpha_transform(const double alpha);
double alpha_revert(const double alpha);
double find_nearest_alpha_value(const double alpha);
double twotrack_ks_transform(const double twotrack_ks);


#endif
