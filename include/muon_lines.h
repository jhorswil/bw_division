#ifndef _MUON_LINES
#define _MUON_LINES
#include <iostream>
#include <vector>
#include <math.h>
#include "line_parameters.h"
#include "line.h"

struct line_parameters;

struct trackmuonmva : public line_template<trackmuonmva, 3> {
  static constexpr const char* _name = "Hlt1TrackMuonMVA"; 
  static constexpr std::array<const char*, 3> _quantities =
  {"pt","ipchi2","muonchi2"}; 
  proxy _alpha{this, "alpha_muon"}; 
  const float _max_pt{2.6e4};
  const float _min_ipchi2{7.4};
  const float _min_pt{2e3};
  const float _muon_chi2{1.8};
  // For some reason this line works in GeV/GeV^2
  // GeV^2 so multiplying Allen value by 1e6
  const float _param1{1e6};
  // GeV so multiply Allen value by 1e3
  const float _param2{2e3};
  const float _param3{1.248};
  bool operator()(const float* monitored_quantities) const;   
};

struct singlehighptmuon : public line_template<singlehighptmuon, 1> {
  static constexpr const char* _name = "Hlt1SingleHighPtMuon"; 
  static constexpr std::array<const char*, 1> _quantities = {"pt"};
  const float _fixed_threshold{12500};
  bool operator()(const float* monitored_quantities) const;   
};

struct dimuon_highmass : public line_template<dimuon_highmass, 2> {
  static constexpr const char* _name = "Hlt1DiMuonHighMass"; 
  static constexpr std::array<const char*, 2> _quantities =
  {"pt","muonchi2"}; 
  proxy _minpt{this, "highmass_dimuon_pt"}; 
  const float _muon_chi2{1.8};
  bool operator()(const float* monitored_quantities) const;   
};

struct dimuon_displaced : public line_template<dimuon_displaced, 3> {
  static constexpr const char* _name = "Hlt1DiMuonDisplaced"; 
  static constexpr std::array<const char*, 3> _quantities =
  {"pt","ipchi2","muonchi2"}; 
  proxy _minpt    {this, "displaced_dimuon_pt"};
  proxy _minipchi2{this, "displaced_dimuon_ipchi2"};
  const float _muon_chi2{1.8};
  bool operator()(const float* monitored_quantities) const;   
};

struct singlehighptmuon_nomuid : public line_template<singlehighptmuon_nomuid, 1> {
  static constexpr const char* _name = "Hlt1SingleHighPtMuonNoMuID"; 
  static constexpr std::array<const char*, 1> _quantities = {"pt"}; 
  const float _fixed_threshold{12500};
  bool operator()(const float* monitored_quantities) const;   
};

struct dimuon_noip : public line_template<dimuon_noip, 1> {
  static constexpr const char* _name = "Hlt1DiMuonNoIP";
  // ;Hlt1DiMuonNoIP_SS";
  static constexpr std::array<const char*, 1> _quantities = {"pt"}; 
  bool operator()(const float* monitored_quantities) const;
};

#endif
