#ifndef _INCLUSIVE_HADRON_LINES
#define _INCLUSIVE_HADRON_LINES

#include <iostream>
#include <vector>
#include <math.h>
#include "line_parameters.h"
#include "line.h" 

struct twotrackks : public line_template<twotrackks, 5> {
  // Struct for storing twotrackks thresholds and interpolation function
  static constexpr const char* _name = "Hlt1TwoTrackKs"; 
  static constexpr std::array<const char*, 5> _quantities =
  {"min_pt","min_ipchi2","pt_ks","eta_ks", "comb_ip"};
  proxy _param{this, "twotrack_ks"};
  const std::vector<std::vector<float>> _threshold_collection =
  {{1380, 1250, 1100, 800, 520, 470, 450, 425,
  425, 425, 425, 425, 425, 425, 425, 425, 425}, {170, 150, 80, 80, 55,
  50, 50, 50, 45, 40, 35, 30, 25, 20, 15, 10, 6}, {2500, 2500,
  2500, 2500, 2500, 2500, 2450, 2400,
  2044.45, 1788.9, 1533.35, 1277.8, 1022.25, 766.7, 511.15, 255.6, 100},
  {4.2, 4.2, 4.2, 4.2, 4.2, 4.2, 4.2, 4.2, 4.288, 4.376, 4.464, 4.552,
  4.64, 4.728, 4.816, 4.904, 5}, {2.7, 2.5, 2.04, 1.38, 1.38, 0.72, 0.72,
  0.72, 0.64, 0.56, 0.48, 0.4, 0.32, 0.24, 0.16, 0.08, 0.0}};

  const std::vector<int> _inc_or_dec = {1, 1, 1, -1, 1, 1};

  const std::vector<float> _twotrack_ks_steps = {1.0, 1.0, 14.0/15.0,
    13.0/15.0, 12.0/15.0, 11.0/15.0, 10.0/15.0, 9.0/15.0, 8.0/15.0, 7.0/15.0,
    6.0/15.0, 5.0/15.0, 4.0/15.0, 3.0/15.0, 2.0/15.0, 1.0/15.0, 0.0};
  std::vector<float> _thresholds = {};

  void prepare_twotrackks();
  void print_thresholds(const line_parameters&);
  void print_allenconf_thresholds(std::ostream&,
  const line_parameters&, bool= false);

  bool operator()(const float* monitored_quantities) const;
};


struct trackmva : line_template<trackmva, 3> {
  static constexpr const char* _name = "Hlt1TrackMVA"; 
  static constexpr std::array<const char*, 3> _quantities =
  {"pt", "ipchi2", "ghostProb"}; 
  proxy _alpha{this, "alpha_hadron"};
  proxy _track_ghost_prob{this, "track_ghostprob"};
  const float _max_pt{2.6e4};
  const float _min_ipchi2{7.4};
  const float _min_pt{2e3};
  const float _param1{1e6};
  const float _param2{2e3};
  const float _param3{1.248};
  bool operator()(const float* monitored_quantities) const;   

};

struct twotrackmva  : line_template<twotrackmva, 2> {
  static constexpr const char* _name = "Hlt1TwoTrackMVA";
  static constexpr std::array<const char*, 2> _quantities =
  {"mva", "maxChildGhostProb"};
  proxy _minMVA{this, "twotrackmva"}; 
  proxy _twotrack_ghost_prob{this, "twotrack_ghostprob"};
  bool operator()(const float* monitored_quantities) const; 
};


#endif
