#ifndef _CHARM_LINES
#define _CHARM_LINES

#include <iostream>
#include <vector>
#include <math.h>
#include "line_parameters.h"
#include "line.h" 

struct d2pipi_line : public line_template<d2pipi_line, 3> {
  static constexpr const char* _name = "Hlt1D2PiPi";
  static constexpr std::array<const char*, 3> _quantities =
  {"min_pt", "min_ip", "D0_ct"};
  proxy _charm_track_ip{this, "charm_track_ip"};
  proxy _charm_track_pt{this, "charm_track_pt"};
  bool operator()(const float* monitored_quantities) const;
};


struct d2kk_line : public line_template<d2kk_line, 3> {
  static constexpr const char* _name = "Hlt1D2KK"; 
  static constexpr std::array<const char*, 3> _quantities =
  {"min_pt", "min_ip", "D0_ct"};
  proxy _charm_track_ip{this, "charm_track_ip"};
  proxy _charm_track_pt{this, "charm_track_pt"};
  bool operator()(const float* monitored_quantities) const;
};


struct lambda_ll_detached_track_line
: public line_template<lambda_ll_detached_track_line, 2> {
  static constexpr const char* _name = "Hlt1LambdaLLDetachedTrack";
  static constexpr std::array<const char*, 2> _quantities =
  {"t_MIPCHI2", "BPVFD"};
  proxy _track_mipchi2{this, "lambda_detached_track_mipchi2"};
  proxy _detached_bpvfd{this, "lambda_detached_combination_bpvfd"};
  bool operator()(const float* monitored_quantities) const;
};


struct xi_omega_lll_line : public line_template<xi_omega_lll_line, 1> {
  static constexpr const char* _name = "Hlt1XiOmegaLLL"; 
  static constexpr std::array<const char*, 1> _quantities = {"t_MIPCHI2"};
  proxy _ipchi2{this, "xi_lll_ipchi2"};
  bool operator()(const float* monitored_quantities) const;
};

struct d2kshh_line : public line_template<d2kshh_line, 1> {
  static constexpr const char* _name = "Hlt1D2Kshh"; 
  static constexpr std::array<const char*, 1> _quantities = {"ctau"};
  bool operator()(const float* monitored_quantities) const {return true;}
};

#endif
