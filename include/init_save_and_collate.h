#ifndef _INIT_SAVE_AND_COLLATE
#define _INIT_SAVE_AND_COLLATE

#include <iostream>
#include <random>
#include <vector>
#include <sstream>

#include "TLatex.h"

#include "io_manager.h"
#include "adam_base.h"
#include "init_discrete_gradient.h"

struct io_manager;
struct adam_base;
struct init_discrete_gradient;
struct line_parameters;


void save_or_edit_indiv_min_results(
  io_manager* tuple, double alpha, double beta1,
  double beta2, std::vector<double> starting_point,
  std::string outfile_dir, int job_num,
  bool plot_min_paths, std::string plot_dir
  );

void collate_all_individual_results(
  std::vector<io_manager*> tuples, std::string outfile_dir,
  bool paramscan = false
  );

#endif