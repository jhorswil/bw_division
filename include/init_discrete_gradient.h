#ifndef _INIT_DISCRETE_GRADIENT
#define _INIT_DISCRETE_GRADIENT

#include <iostream>
#include <random>
#include <vector>
#include <sstream>

#include "TLatex.h"

#include "io_manager.h"
#include "adam_base.h"

struct io_manager;
struct adam_base;
struct line_parameters;

struct init_discrete_gradient : public adam_base {
  io_manager* _tuple = nullptr;
  bool _include_prints{false};

  init_discrete_gradient(io_manager* tuple,
  double collision_rate, double ratelimit, double scalewidth,
  int accuracy, bool include_prints = false);
  
  ~init_discrete_gradient() {}
  
  std::vector<double> minimised_function(const line_parameters& input_parameters)
  const;
  
  void plot_min_path(std::string plot_dir, int job_num);
  std::vector<double> speceff_minimised_function(
  line_parameters, const unsigned&) const;
  
  void min_path_loop(
  unsigned i, unsigned j, std::vector<double>& first_axis_values,
  std::vector<double>& second_axis_values, std::string cut_variable1,
  std::string cut_variable2) const;
};

void store_individual_results(
  io_manager* tuple, line_parameters grid_search_result,
  std::vector<double> params_used, double collision_rate, double ratelimit,
  adam_base* initial_minimiser
  );

void apply_adam_minimiser_individually(
  io_manager* tuple, double collision_rate, double ratelimit,
  int niterations, double alpha, double beta1, double beta2,
  double epsilon, double scalewidth, int accuracy, bool plot_min_paths,
  std::string plot_dir, std::vector<double> fixed_starting_point,
  std::string outfile_dir, int job_num = 0, int max_grid_searches = 15,
  bool include_prints = false, const bool random_start = false,
  const bool skip_grid_search = false
  );

double multiple_minimisation_runs(
  std::vector<io_manager*> tuples, double collision_rate, double ratelimit,
  int niterations, double alpha, double beta1, double beta2,
  double epsilon, double scalewidth, int accuracy, bool plot_min_paths,
  std::string plot_dir, std::string outfile_dir, int nruns,
  int job_num = 0, int max_grid_searches = 15, bool random_start = false,
  bool skip_grid_search = false
  );

void minimise_all_individual_ntuples(
  std::vector<io_manager*> tuples, double collision_rate, double ratelimit,
  int niterations, double alpha, double beta1, double beta2,
  double epsilon = 1e-8, double scalewidth = 1.0, int accuracy = 2,
  bool plot_min_paths = false,
  std::string plot_dir = "", std::vector<double> fixed_starting_point = {},
  std::string outfile_dir = "", int max_grid_searches = 15,
  bool random_start = false, bool skip_grid_search = false
  );

#endif
