#ifndef _LINE_MANAGER
#define _LINE_MANAGER

#include <iostream>
#include <vector>
#include "parameter_config.h" 

struct line_manager {

  std::string _name{""};
  std::vector<std::string> _cut_variables{};
  std::vector<std::string> _monitored_quantities = {};
  std::string _event_branch{""};
  std::string _run_branch{""};
  std::string _tree{""};
  std::string _filter{""};
  std::vector<std::pair<std::string, parameter_config>> _parameters; 
  int _line_number{0};
  bool _fixed{false};

  line_manager(std::string name, const std::vector<std::string>&,
  const std::vector<std::string>&);
  void print();
};


#endif
