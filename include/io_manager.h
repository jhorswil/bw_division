#ifndef _IO_MANAGER
#define _IO_MANAGER

#include <iostream>

#include <ROOT/RDataFrame.hxx>
#include <TFile.h>
#include <TSpline.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TTree.h>
#include <TStopwatch.h>
#include <TCanvas.h>
#include <TMinuit.h>
#include <TStyle.h>
#include <TRandom3.h>

#include "minimise_fom.h"
#include "line_manager.h"
#include "line_parameters.h"
#include "event_store.h"
#include "line_tools.h"
#include "parameter_config.h" 

// Forward declaration
class init_fom_minimiser;
class opt_fom_minimiser;
class init_discrete_gradient;
class opt_discrete_gradient;
struct adam_base;
struct line_parameters;
struct event_store;
struct unique_id;
struct line_store;

template <typename string_type>
unsigned global_parameter_index(const string_type& name)
{
  // Return the array index of a certain parameter given by name

  auto lp = line_parameters(); 
  for(unsigned i{0}; i != lp.size(); ++i) {
    if (lp._paramnames[i] == name) return i;
  }
  return 999; 
}

struct io_manager {
    // Members
    std::string _name = "";
    std::string _filesafe_name = "";
    std::string _event_type = "";
    std::string _ntuple_path = "";

    std::vector<line_manager> _lines;
    std::map<std::string, parameter_config> _parameters; 
    std::array<bool, NUM_LINES> _activate_lines = {false};
    std::vector<bool> _activate_params = {};
    std::vector<std::string> _axes = {};
    std::vector<int> _stored_candidates_per_line = {};
    std::vector<int> _logfile_events_per_line = {};
    
    std::vector<std::vector<double>> _parameter_values;
    std::vector<event_store> _events;
    double _unique_precut_events = 0;
    double _total_allen_events = 0;
    double _total_reconstructable_events = 0;
    double _collision_rate = 0;
    double _ratelimit = 0;
    int _percentage_ratelimit = 0;
    int _num_signal_tracks = 0;
    
    init_fom_minimiser* _maxeff_minimiser = nullptr;
    init_fom_minimiser* _specline_minimiser = nullptr;
    init_discrete_gradient* _maxeff_adam = nullptr;
    init_discrete_gradient* _specline_adam = nullptr;
    
    double _optimal_fom = 0;
    double _optimal_minbias_rate = 0;
    double _optimal_minbias_rate_error = 0;
    double _optimal_minbias_efficiency = 0;
    double _optimal_minbias_efficiency_err = 0;
    double _optimal_signal_eff = 0;
    double _optimal_signal_eff_err = 0;
    double _optimal_ratelimited_signal_eff = 0;
    double _optimal_ratelimited_signal_eff_err = 0;
    double _special = 0;
    line_parameters _optimal_lps;
    std::vector<std::string> _optimal_paramnames = {};
    std::vector<double> _optimal_parameter_inputs = {};
    std::vector<int> _optimal_candidate_counts = {};
    std::vector<int> _optimal_minbias_candidate_counts = {};
    std::vector<int> _optimal_unique_events_per_line = {};
    std::vector<int> _lines_per_parameter = {};
    
    bool _s_or_b = true;
    bool _reconstructable_only = false;
    io_manager* _background_io = nullptr;
    line_parameters _cutoff_params;
    bool _parallelise_events{false};
    line_store _linestore;

    std::vector<bool> _per_line_loosest;
    bool _loosest_selection{false};
    // For line fixing
    fixedlines_t _fixed_inputs;
    linemask_t _partially_fixed_lines;
    linemask_t _fully_fixed_lines;
    linemask_t _off_lines;
    parammask_t _fixed_line_params;
    std::vector<linemask_t> _parameter_line_masks;
    std::vector<int> _parameter_line_counts;
        
    // Member functions
    io_manager() = default;
    io_manager(const std::string&, const std::string&, 
    const std::string&, const std::vector<line_manager>&, 
    const fixedlines_t&,
    const linemask_t&, const linemask_t&, const linemask_t&,
    const parammask_t&, const unsigned = 0, bool = true
    ); 
    io_manager(std::string name, std::string event_type,
    io_manager* background_io,
    std::string ntuple_path, const std::vector<line_manager>& lines,
    const fixedlines_t& fixed_inputs,
    const linemask_t& partially_fixed_lines,
    const linemask_t& fully_fixed_lines,
    const linemask_t& off_lines,
    const parammask_t& fixed_line_params,
    double special = 1.0, double collision_rate = 3e7,
    double ratelimit = 0.0356879, double cutoff = 1.5,
    double scalewidth = 0.25, int accuracy = 1,
    int max_events = 20000, int reconstructable_tracks = 0,
    bool reconstructable_only = false
    );

    void clear();
    void print();
    void find_absolute_yield(std::string ntuple_path);
    int find_absolute_yield_per_line(
    std::string ntuple_path, std::string line_name);
    line_parameters find_nearest_point(
    const line_parameters& input_parameters
    ) const;
    void fill_parameter_values();
    double calc_unique_events(
    const line_parameters& input_parameters,
    const linemask_t& activate_lines);
    std::vector<int> events_unique_to_lines(
    const line_parameters& params, const linemask_t& activated_lines
    );
    double specline_unique_events(
    const line_parameters& input_parameters, const int& lineindex);
    std::vector<int> calc_candidates_per_line(
    const line_parameters& input_parameters,
    const linemask_t& activate_lines);
    void calc_results(const int& num_results,
    const std::vector<std::vector<double>>& input_param_store,
    const double& collision_rate, const double& ratelimit);
    void remove_excess_candidates(const double& collision_rate,
    const double& ratelimit,
    const double& cutoff, const int& max_events,
    const bool& reconstructable_only);
    void opening_configuration(std::string name, int num_signal_tracks,
    std::string ntuple_path, std::string event_type,
    io_manager* background_io, const std::vector<line_manager>& lines,
    bool reconstructable_only);
    void line_builder(const line_manager& line, line_parameters params,
    std::map< std::pair<unsigned long, unsigned int>,
    std::pair<std::vector<std::vector<float>>, std::vector<int>>>&
    precut_idmap); 
    void reconstructable_events_and_axis_builder(
    std::map<std::pair<unsigned long, unsigned int>,
    std::pair<std::vector<std::vector<float>>, std::vector<int>>>&
    precut_idmap, bool reconstructable_only);
    void move_param_boundaries();
    bool increase_active_params_calculate_and_process_rate(
    std::vector<std::vector<int>>& params_to_activate,
    std::vector<int>& step_ratio,
    int& linecount, io_manager* background, int l,
    int k, int j,
    const linemask_t& temp_activate_lines, double collision_rate,
    line_store& linestore, int& param_index,
    std::vector<std::vector<double>>& param_inputs,
    std::vector<std::vector<double>>& rates, double cutoff,
    line_parameters& params, double ratelimit,
    std::vector<double>& per_line_values, int primary_step_ratio);
    void manage_parameter_increment_calculate_rates(
    linemask_t& deactivated_lines,
    linemask_t& activate_lines,
    std::vector<std::vector<int>>& params_to_activate,
    int& linecount, io_manager* background, int k, int j,
    double collision_rate,
    line_store& linestore, int& param_index,
    std::vector<std::vector<double>>& param_inputs,
    std::vector<std::vector<double>>& rates, double cutoff,
    line_parameters& params, double ratelimit,
    std::vector<double>& per_line_values);
    void activate_lines_calculate_rate_choose_parameter_value(
    int j, io_manager* background, line_parameters& params,
    std::vector<std::vector<double>>& rates, double cutoff,
    double ratelimit, double collision_rate,
    std::vector<std::vector<double>>& param_inputs,
    int& param_index, line_store& linestore,
    std::vector<double> param_differences);
};


#endif
