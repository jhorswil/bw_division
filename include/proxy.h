#pragma once 

template <typename line_t, unsigned num_elements> struct line_template; 
struct proxy {
  std::string _name{""}; 
  float _value{0}; 

  template <typename line_t, unsigned num_elements> 
  proxy(
  line_template<line_t, num_elements>* parent, const std::string& name
  ) : _name(name) { 
    parent->register_parameter(this);
  }
  
  operator float() const {return _value;} 
}; 


