#ifndef _EFFICIENCY_CALC
#define _EFFICIENCY_CALC

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <boost/algorithm/string/replace.hpp>
#include <functional>
#include <filesystem>

#include <TFile.h>
#include <TGraphErrors.h>
#include <TSpline.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TTree.h>
#include <TStopwatch.h>
#include <TCanvas.h>
#include <TMinuit.h>
#include <TStyle.h>
#include <TRandom3.h>
#include <ROOT/RDataFrame.hxx>
#include <TMultiGraph.h>
#include <ROOT/RDataFrame.hxx>
#include "TGraph2D.h"
#include "TSpline.h"
#include <fstream> 

// Forward declaration
struct io_manager;
struct line_parameters;
struct line_manager;


std::vector<std::vector<double>> generate_input_param_store(
  io_manager* tuple, int num_results, int paramindex0, int paramindex1
  );


void phase_space_scan(
  std::vector<io_manager*> tuples, double collision_rate = 3e7,
  double ratelimit = 0.0356879, int paramindex0 = 0, int paramindex1 = 1
  );


void plot_results_vs_lineor_cut(
  std::vector<std::string> plot_strings, std::vector<double> first_cut_vector,
  std::vector<double> second_cut_vector, std::vector<double> results,
  double plot_phi, double plot_theta = 30, std::string draw_option = "surf1"
  );


void calc_and_plot_statistics_vs_cut(
  std::vector<io_manager*> tuples, std::string plot_dir,
  double plot_phi = -60, double collision_rate = 3e7,
  double ratelimit = 0.0356879,
  std::string paramindex0 = "", std::string paramindex1 = ""
  ); 

void plot_sigeff_vs_backrate(
  io_manager* tuple, std::string plot_dir, double collision_rate = 3e7,
  double ratelimit = 0.0356879
  );

double ratelimiter(
  double trigger_efficiency, double ratelimit = 0.0356879
  );


void eff_and_rate_vs_individual_cut(
  std::vector<io_manager*> tuples, std::string plot_dir,
  double collision_rate, double ratelimit
  );


void check_statistics_at_selection(
  line_parameters selection, std::vector<io_manager*> tuples,
  double collision_rate, double ratelimit
  );


void manual_param_entry(
  std::vector<double> manual_inputs, std::vector<io_manager*> tuples,
  double collision_rate, double ratelimit,
  double scalewidth, int accuracy, bool plot_epoch,
  std::string plot_dir, std::string outfile_dir,
  int max_grid_searches, int job_num
  );

void combine_nobias_log_files(
  std::string log_file_directory, std::string new_file_directory,
  std::vector<line_manager*> lines
  );

std::vector<std::string> split(const std::string&, char, bool = true);

// from https://stackoverflow.com/questions/
// 16707645/how-to-get-element-from-stdtuple-by-type

namespace detail {
  template <class line_t, std::size_t num_elements,
  class... line_arguments>
  struct get_number_of_element_from_tuple_by_type_impl
  {
    static constexpr auto value = num_elements;
  };

  template <class line_t, std::size_t num_elements,
  class... line_arguments>
  struct get_number_of_element_from_tuple_by_type_impl<line_t,
  num_elements, line_t, line_arguments...>
  {
    static constexpr auto value = num_elements;
  };

  template <class line_t, std::size_t num_elements, class second_line_t,
  class... line_arguments>
  struct get_number_of_element_from_tuple_by_type_impl<line_t,
  num_elements, second_line_t, line_arguments...>
  {
    static constexpr auto value =
    get_number_of_element_from_tuple_by_type_impl<line_t, num_elements + 1,
    line_arguments...>::value;
  };

} // namespace detail

template <class line_t, class... line_arguments> line_t&
get_element_by_type(std::tuple<line_arguments...>& tuple) {
  return std::get<detail::get_number_of_element_from_tuple_by_type_impl<
  line_t, 0, line_arguments...>::value>(tuple);
}
  
template <typename functor_type>
void process_file(
  const std::string& filename,
  functor_type&& file_processing_functor,
  const char ignore_lines_that_begin_with = '#'
  ) {
    std::string temp_line;
    std::ifstream file_stream(filename.c_str());
    while (file_stream.good()) {
      std::getline(file_stream, temp_line);
      temp_line.erase(std::remove_if(temp_line.begin(), temp_line.end(),
      [](const char character) {return character == '\r';}), temp_line.end());
      if (ignore_lines_that_begin_with != '\0'
      && (temp_line.size() == 0
      || temp_line[0] == ignore_lines_that_begin_with)) continue;
      file_processing_functor(temp_line);
    }
    file_stream.close();
  }
  
template <typename iterator_type, typename functor_type>
std::string vector_to_string(
  iterator_type begin,
  iterator_type end,
  const std::string& delimiter,
  functor_type functor
  ) {
  std::stringstream ss;
  if (begin == end) return "";

  for (auto it = begin; it != end-1; ++it) {
    ss << functor(*it) << delimiter;
  }
  
  ss << functor(*(end-1));
  return ss.str();
}

template <typename container_type,
typename data_type = typename container_type::value_type,
typename functor_type = std::function<data_type(const data_type&)>>
std::string vector_to_string(
  const container_type& container, const std::string& delimiter = "",
  const functor_type& functor = [](const auto& arg){return arg;}
  ) {
  return vector_to_string(std::begin(container), std::end(container),
  delimiter, functor);
}

std::string latex_to_term(const std::string& name); 

#endif
