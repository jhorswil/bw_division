#pragma once  

#include "line_parameters.h" 
#include "proxy.h"
#include <span> 
#include <map> 

template <typename line_t, unsigned num_elements> struct line_template {
  
  static constexpr unsigned _num_quantities = num_elements;
  std::vector<proxy*> _parameters;
  bool _fixed{false};
  bool _off{false};
  line_parameters _fixed_inputs;
  std::vector<bool> _fixed_mask;
  

  bool operator()(const float* monitored_quantities) const {
    if (_off) {
      return false;
    } else {
      return (*static_cast<const line_t*>(this))(
      monitored_quantities); 
    }
  }

  int count_candidates(
    const std::span<const float>& monitored_quantities
    ) const {

    if (_off) {
      return 0;
    } else {
      int num_candidates{0};
      for(unsigned i{0}; i != monitored_quantities.size() / num_elements; ++i) {
        num_candidates +=
        (*this)(monitored_quantities.data() + num_elements * i);
      }
      return num_candidates; 
    }
  }

  bool return_decision(
    const std::span<const float>& monitored_quantities
    ) const {
    if (_off) {
      return false;
    } else {
      for(unsigned i{0}; i != monitored_quantities.size() / num_elements; ++i)
      {
        auto decision =
        (*this)(monitored_quantities.data() + num_elements * i);
        if (decision) return true; 
      }
      return false;
    }
  }

  void candidate_decisions(
    const std::span<const float>& monitored_quantities,
    std::vector<bool>& which
    ) const {
     
    for(unsigned i{0}; i != monitored_quantities.size() / num_elements; ++i) {
      which.push_back(_off ? false : (*this)(monitored_quantities.data()
      + num_elements * i));
    }
  }

  void register_parameter(proxy* prox){_parameters.push_back(prox);}
  void register_fixed(bool fixed){_fixed = fixed;}
  void register_fixed_inputs(line_parameters fixed_inputs){
    _fixed_inputs = fixed_inputs;
  }
  void register_param_mask(std::vector<bool> fixed_mask){
    _fixed_mask = fixed_mask;
  }
  void register_off(bool off){_off = off;}

  void set_parameters(const line_parameters& parameters) {
    line_parameters set_params(parameters);
    if (_fixed) {
      for (unsigned i{0}; i < set_params.size(); i++) {
        if (_fixed_mask[i]) set_params[i] = _fixed_inputs[i];
      }
    } 

    for(auto& param : _parameters) {
      param->_value =
      set_params.get_parameter(param->_name);
    }
  }

  void print() const {
    std::cout << line_t::_name << " "; 
    for(auto& param : _parameters) {
      std::cout << param->_name << " " << param->_value << std::endl;
    }
  }

  const std::string parameter_string() const {
    if (_parameters.size() == 0) return "fixed"; 
    std::string parameter_str = ""; 
    for(unsigned int i = 0; i != _parameters.size(); ++i) {
      parameter_str +=
      _parameters[i]->_name + ((i == _parameters.size() - 1)? "" : ";");
    }
    return parameter_str; 
  }
  
  const std::vector<std::string> parameter_names() const 
  {
    if (_parameters.size() == 0) return {"fixed"};
    std::vector<std::string> param_names; 
    for(unsigned int i = 0; i != _parameters.size(); ++i) {
      param_names.push_back(_parameters[i]->_name);
    }
    return param_names; 
  }
}; 
