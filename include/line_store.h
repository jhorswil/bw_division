#ifndef _LINE_STORE
#define _LINESTORE
#include <iostream>
#include <vector>
#include <span>
#include <tuple>
#include <map>
#include <algorithm>
#include <functional>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>

#include "line_parameters.h"
#include "line_tools.h"
#include "line_manager.h"

namespace tuple_process {
  // enable this function when tuple iterator reaches maximum
  template <std::size_t iterator = 0, typename... line_object>
  inline typename std::enable_if_t<iterator == sizeof...(line_object), bool>
  for_each_ref(const std::tuple<line_object...>&,
  const std::vector<float>& monitored_quantities,
  const std::array<int, NUM_LINES+1>& offset,
  const linemask_t& activate_lines) {return false;}
  // If iterator reach the index of the last element of the tuple, stop
  // recursion (return false).
  // Else, process
  template <std::size_t iterator = 0, typename... line_object> 
  inline typename std::enable_if_t<iterator<sizeof...(line_object), bool>
  for_each_ref(
    const std::tuple<line_object...>& tuple,
    const std::vector<float>& monitored_quantities,
    const std::array<int, NUM_LINES+1>& offset, const linemask_t& activate_lines
    ) {
    // ---------- Event decision function ----------
    // Most important in whole project

    if (activate_lines.at(iterator)
    && std::get<iterator>(tuple).return_decision(
    std::span<const float>(monitored_quantities.data()
    + offset.at(iterator), offset.at(iterator+1) - offset.at(iterator)))) {
      return true;
    } else {
      // Recur
      return for_each_ref<iterator + 1, line_object...>(tuple,
      monitored_quantities, offset, activate_lines);
    }
  }

  // Again, enable this function when tuple iterator reaches maximum
  template <std::size_t iterator = 0, typename... line_object>
  inline typename std::enable_if_t<iterator == sizeof...(line_object),
  std::vector<int>>
  candidate_count(const std::tuple<line_object...>&,
  const std::vector<float>& monitored_quantities,
  const std::array<int, NUM_LINES+1>& offset,
  const linemask_t& activate_lines,
  std::vector<int>& counts){return counts;}

  // Else run recursion
  template <std::size_t iterator = 0, typename... line_object>
  inline typename std::enable_if_t<iterator<sizeof...(line_object),
  std::vector<int>>
  candidate_count(
    // Counts number of candidates passing each line for a given set of
    // thresholds
    const std::tuple<line_object...>& tuple,
    const std::vector<float>& monitored_quantities,
    const std::array<int, NUM_LINES+1>& offset,
    const linemask_t& activate_lines,
    std::vector<int>& counts
    ) {
    if (activate_lines.at(iterator)) {
      counts.push_back(std::get<iterator>(tuple).count_candidates(
      std::span<const float>(monitored_quantities.data()
      + offset.at(iterator), offset.at(iterator+1) - offset.at(iterator))));
    }
    // Recur
    return candidate_count<iterator + 1, line_object...>(tuple,
    monitored_quantities, offset, activate_lines, counts);
  }

  template <std::size_t iterator = 0, typename... line_object>
  inline typename std::enable_if_t<iterator == sizeof...(line_object),
  std::vector<bool>>
  which_candidates(const std::tuple<line_object...>&,
  const std::vector<float>& monitored_quantities,
  const std::array<int, NUM_LINES+1>& offset,
  const linemask_t& activate_lines, std::vector<bool>& which) {
    return which;
  }

  template <std::size_t iterator = 0, typename... line_object>
  inline typename std::enable_if_t<iterator<sizeof...(line_object),
  std::vector<bool>>
  which_candidates(
    const std::tuple<line_object...>& tuple,
    const std::vector<float>& monitored_quantities,
    const std::array<int, NUM_LINES+1>& offset,
    const linemask_t& activate_lines, std::vector<bool>& which
    ) {
    // Return mask showing which lines triggered on a given event
    // (inclusive or exclusive?)
    if (activate_lines.at(iterator)) {
      std::get<iterator>(tuple).candidate_decisions(
      std::span<const float>(monitored_quantities.data()
      + offset.at(iterator), offset.at(iterator+1) - offset.at(iterator)),
      which);
    }

    // Recur
    return which_candidates<iterator + 1, line_object...>(tuple,
    monitored_quantities, offset, activate_lines, which);
  }
  
  template <std::size_t iterator = 0, typename function_type,
  typename... line_object>
  typename std::enable_if_t<iterator == sizeof...(line_object), void>
  for_each(std::tuple<line_object...>&, function_type){}

  template <std::size_t iterator = 0, typename function_type,
  typename... line_object>
  inline typename std::enable_if_t<iterator<sizeof...(line_object), void>
  for_each(std::tuple<line_object...>& tuple,
  function_type function_one_arg) {
    function_one_arg(std::get<iterator>(tuple));
    for_each<iterator + 1, function_type, line_object...>(tuple,
    function_one_arg);
  }
  
  // Adapted version of above with fixed line functionality
  // Stop recurring at end of tuple
  template <std::size_t iterator = 0, typename function_type,
  typename... line_object>
  typename std::enable_if_t<iterator == sizeof...(line_object), void>
  for_each_iterated(std::tuple<line_object...>&, function_type){}

  template <std::size_t iterator = 0, typename function_type,
  typename... line_object>
  inline typename std::enable_if_t<iterator<sizeof...(line_object), void>
  for_each_iterated(std::tuple<line_object...>& tuple,
  function_type function_two_arg) {
    // Lambda function
    function_two_arg(std::get<iterator>(tuple), iterator);
    // Recur
    for_each_iterated<iterator + 1, function_type, line_object...>(tuple,
    function_two_arg);
  }
}


struct line_store {
  // Struct for all lines, line order, and calling lines
  
  std::array<std::string, NUM_LINES> _linenames;
  std::array<std::vector<std::string>, NUM_LINES> _associated_params;
  std::array<int, NUM_LINES> _quantities_per_line;
  line_store();
  const line_tuple make_lines(const fixedlines_t&,
  const linemask_t&, const linemask_t&, const parammask_t&) const;
  const line_tuple make_lines() const;
  std::vector<line_manager> get_line_manager() const; 
};

#endif
