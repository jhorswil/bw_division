#ifndef _READ_JSONS
#define _READ_JSONS

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <map>
#include <string>
#include <functional>
#include <filesystem>
#include <algorithm>

#include <TFile.h>
#include <TROOT.h>
#include <TSystem.h>

#include <nlohmann/json.hpp>
#include "line_manager.h"
#include "io_manager.h"

std::vector<io_manager*> read_jsons(
  std::string json_input_dir, const std::vector<std::string>& sample_dir,
  const std::vector<line_manager>& line_managers,  
  double collision_rate, double ratelimit, double cutoff,
  double scalewidth, int accuracy, int operation_option,
  int tuple_num, int percentage_ratelimit, int max_events,
  bool reconstructable_only, std::string plot_dir,
  const fixedlines_t& fixed_inputs,
  const linemask_t& partially_fixed_lines,
  const linemask_t& fully_fixed_lines,
  const linemask_t& off_lines,
  const parammask_t& fixed_line_params
  );

#endif
