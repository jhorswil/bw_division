#ifndef _ELECTRON_LINES
#define _ELECTRON_LINES

#include <iostream>
#include <vector>
#include <math.h>
#include "line_parameters.h"
#include "line.h" 

struct trackelectronmva : public line_template<trackelectronmva, 2> {
  static constexpr const char* _name{"Hlt1TrackElectronMVA"}; 
  static constexpr std::array<const char*, 2> _quantities =
  {"pt_corrected","ipchi2"}; 
  proxy _alpha{this, "alpha_electron"}; 
  const float _max_pt{2.6e4};
  const float _min_ipchi2{3};
  const float _min_pt{1e3};
  const float _param1{1e6};
  const float _param2{1e3};
  const float _param3{1.248}; 
  bool operator()(const float* monitored_quantities) const;   
};

struct singlehighptelectron : public line_template<singlehighptelectron, 1> {
  static constexpr const char* _name = "Hlt1SingleHighPtElectron"; 
  static constexpr std::array<const char*, 1> _quantities = {"pt_corrected"};
  const float _fixed_threshold{12500};
  bool operator()(const float* monitored_quantities) const;   
};


struct displaced_dielectron : public line_template<displaced_dielectron, 2 > {
  static constexpr const char* _name = "Hlt1DiElectronDisplaced"; 
  static constexpr std::array<const char*, 2> _quantities = {"pt", "ipchi2"};
  proxy _min_pt    {this, "displaced_dielectron_pt"}; 
  proxy _min_ipchi2{this, "displaced_dielectron_ipchi2"}; 
  bool operator()(const float* monitored_quantities) const;   
};


struct dielectron_lowmass : public line_template<dielectron_lowmass, 1> {
  static constexpr const char* _name =
  "Hlt1DiElectronLowMass_massSlice1_prompt;"
  "Hlt1DiElectronLowMass_massSlice2_prompt;"
  "Hlt1DiElectronLowMass_massSlice3_prompt;"
  "Hlt1DiElectronLowMass_massSlice4_prompt;"
  "Hlt1DiElectronLowMass_SS_massSlice1_prompt;"
  "Hlt1DiElectronLowMass_SS_massSlice2_prompt;"
  "Hlt1DiElectronLowMass_SS_massSlice3_prompt;"
  "Hlt1DiElectronLowMass_SS_massSlice4_prompt;"
  "Hlt1DiElectronLowMass_massSlice1_displaced;"
  "Hlt1DiElectronLowMass_massSlice2_displaced;"
  "Hlt1DiElectronLowMass_massSlice3_displaced;"
  "Hlt1DiElectronLowMass_massSlice4_displaced;"
  "Hlt1DiElectronLowMass_SS_massSlice1_displaced;"
  "Hlt1DiElectronLowMass_SS_massSlice2_displaced;"
  "Hlt1DiElectronLowMass_SS_massSlice3_displaced;"
  "Hlt1DiElectronLowMass_SS_massSlice4_displaced";

  static constexpr std::array<const char*, 1> _quantities =
  {"die_pts_bremcorr"};
  bool operator()(const float* monitored_quantities) const;   
};
#endif
