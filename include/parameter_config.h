#pragma once 

#include <math.h>

struct parameter_config {
  double _value; 
  double _min;
  double _max; 
  double _grad_resolution; 
  double _grad_sign; 
  int _steps;
  int _grad_steps; 
  bool _fixed{false};
  bool _off{false};

  int sign(const double arg) const {
    return arg >= 0 ? 1 : -1;
  } 
 
  double resolution() const {
    return fabs(_max - _min) / _steps;
  }

  void set(
    const double min,
    const double max,
    const int steps, 
    const int grad_steps) {

    _min = min;
    _max = max;
    _steps = steps;
    _grad_steps = grad_steps; 
    _grad_resolution = fabs((_max - _min) / _grad_steps); 
    _grad_sign = sign(_max - _min); 
  }

  parameter_config() = default; 
  parameter_config(
    const double value, 
    const double min,
    const double max,
    const int steps,
    const int grad_steps
    ) : _value(value), _min(min), _max(max),
    _steps(steps), _grad_steps(grad_steps) {

    _grad_resolution = fabs((_max - _min) / _grad_steps); 
    _grad_sign = sign(_max - _min); 
  }

  void clip_max(double& x) const {
    bool outside_max = sign(_max - _min) >= 0.0 ? x >= _max : x <= _max; 
    x = outside_max ? _max : x; 
  }
  void clip_min(double& x) const {
    bool outside_min = sign(_max - _min) >= 0.0 ? x <= _min : x >= _min; 
    x = outside_min ? _min : x; 
  }
};



