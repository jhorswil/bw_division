#ifndef _MINIMISE_FOM
#define _MINIMISE_FOM

#include <iostream>
#include <filesystem>

#include <ROOT/RDataFrame.hxx>
#include <TFile.h>
#include <TSpline.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TTree.h>
#include <TStopwatch.h>
#include <TCanvas.h>
#include <TMinuit.h>
#include <TStyle.h>
#include <TRandom3.h>

#include "TMVA/GeneticAlgorithm.h"
#include "TMVA/GeneticFitter.h"
#include "TMVA/IFitterTarget.h"

#include "ROOT/RDataFrame.hxx"
#include "io_manager.h"

using namespace TMVA;

// Forward declare
class opt_fom_minimiser;
struct io_manager;

struct init_fom_minimiser : public IFitterTarget {
  io_manager* _tuple = nullptr;
  int _calc_counter = 0;
  double _collision_rate = 0;
  double _ratelimit = 0;
  std::vector<double> _final_counts = {};
  std::vector<double> _signal_efficiencies = {};
  std::vector<double> _ratelim_signal_efficiencies = {};
  std::vector<double> _signal_efficiency_errs = {};
  std::vector<double> _ratelim_signal_efficiency_errs = {};
  std::vector<double> _background_efficiencies = {};
  std::vector<double> _background_efficiency_errs = {};
  std::vector<double> _background_rates = {};
  std::vector<double> _background_rate_errs = {};
  std::vector<std::vector<double>> _inputs_used = {};
  std::vector<double> _all_foms = {};
  
  init_fom_minimiser(
    io_manager* tuple, double collision_rate, double ratelimit
    ) : IFitterTarget() {
    _tuple = tuple;
    _collision_rate = collision_rate;
    _ratelimit = ratelimit;
  }

  double SpecificFunction(std::vector<double> input_parameters,
  const unsigned);
  void plot_min_path(std::string plot_dir);

  Double_t EstimatorFunction(std::vector<Double_t>& input_parameters);
};


double apply_individual_minimiser(
  io_manager* tuple, double collision_rate, double ratelimit,
  std::string options, bool plot_min_paths = false,
  std::string plot_dir = ""
  );

void minimise_individual_ntuples(
  std::vector<io_manager*> tuples, std::string plot_dir,
  double collision_rate, double ratelimit, std::string options,
  bool plot_min_paths = false
  );

double minimise_all_ntuples(
  std::vector<io_manager*> tuples, std::string plot_dir,
  double collision_rate, double ratelimit,
  std::string init_options, std::string opt_options
  );


struct opt_fom_minimiser : public IFitterTarget {
  std::vector<io_manager*> _tuples = {};
  int _calc_counter = 0;
  double _collision_rate = 0;
  double _ratelimit = 0;
  std::vector<double> _signal_efficiencies = {};
  std::vector<double> _signal_efficiency_errs = {};
  std::vector<double> _ratelim_signal_efficiencies = {};
  std::vector<double> _ratelim_signal_efficiency_errs = {};
  double _background_efficiency = 0;
  double _background_efficiency_err = 0;
  double _background_rate = 0;
  double _background_rate_err = 0;
  std::vector<std::vector<double>> _inputs_used = {};
  std::vector<double> _all_foms = {};

  opt_fom_minimiser(
    std::vector<io_manager*> tuples, double collision_rate, double ratelimit
  ) : IFitterTarget() {
    _tuples = tuples;
    _collision_rate = collision_rate;
    _ratelimit = ratelimit;
  }

  Double_t EstimatorFunction(std::vector<Double_t>& input_parameters);
};

void manual_optparam_entry(
  std::vector<double> manual_inputs, std::vector<io_manager*> tuples,
  std::string plot_dir, double collision_rate,
  double ratelimit, std::string init_options
  );


#endif
