#ifndef _CALIBRATION_LINES
#define _CALIBRATION_LINES

#include <iostream>
#include <vector>
#include <math.h>
#include "line_parameters.h"
#include "line.h" 

struct d2kpi_line : line_template<d2kpi_line, 3> {
  
  static constexpr const char* _name = "Hlt1D2KPi"; 
  static constexpr std::array<const char*, 3> _quantities =
  {"min_pt", "min_ip", "D0_ct"};
   
  proxy _charm_track_ip{this, "charm_track_ip"}; 
  proxy _charm_track_pt{this, "charm_track_pt"}; 

  bool operator()(const float* monitored_quantities) const;
};


#endif
