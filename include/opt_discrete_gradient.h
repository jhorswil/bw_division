#ifndef _OPT_DISCRETE_GRADIENT
#define _OPT_DISCRETE_GRADIENT

#include <iostream>
#include <random>
#include <vector>

#include "io_manager.h"
#include "adam_base.h"

struct opt_discrete_gradient : public adam_base {
  // glob_or_indiv = true
  opt_discrete_gradient(std::vector<io_manager*> tuples,
  double collision_rate, double ratelimit, double scalewidth,
  int accuracy)
  : adam_base(true, tuples, collision_rate, ratelimit,
  scalewidth, accuracy) {}
  ~opt_discrete_gradient() {};
  std::vector<double> minimised_function(
  const line_parameters& input_parameters) const;
  void efficiency_per_channel(const line_parameters input_parameters);
};


void print_allen_python_output(
  const std::string& dir,
  const line_parameters& grid_search_results
  );

void process_save_and_plot_global_statistics(
  std::vector<io_manager*> tuples, line_parameters grid_search_result,
  double collision_rate, double ratelimit, std::string plot_dir,
  std::string outfile_dir, int job_num, bool plot_epoch,
  adam_base* opt_minimiser
  );

void plot_global_adam_statistics(
  std::string plot_dir, std::vector<io_manager*> tuples,
  double collision_rate, double ratelimit, line_parameters results,
  opt_discrete_gradient* global_minimiser, int job_num
  );


double apply_adam_minimiser_globally(
  std::vector<io_manager*>, double, double, int, double, double, double,
  double, double, int, bool, std::string, std::vector<double>, std::string,
  bool = false, int = 15, int = 0, bool = false, bool = false
  );
#endif
