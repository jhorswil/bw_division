#ifndef _ADAM_BASE
#define _ADAM_BASE

#include <iostream>
#include <random>
#include <vector>
#include <algorithm>
#include <bits/stdc++.h>
#include "line_parameters.h"
#include <unordered_set>
#include "parameter_config.h" 
#include "io_manager.h" 

struct opt_discrete_gradient;

static std::vector<line_parameters> empty_vec = {};

struct adam_base {
  bool _glob_or_indiv{true};
  int _iterations_required = 0;
  std::vector<parameter_config> _parameters; 
  int _calc_counter = 0;
  double _collision_rate = 0;
  double _ratelimit = 0;
  double _scalewidth = 1.0;
  int _accuracy = 0;
  std::vector<io_manager*> _tuples;
  std::vector<line_parameters> _minpath_coords = {};
  std::vector<line_parameters> _inputs_used = {};
  std::vector<double> _all_foms = {};
  std::vector<double> _averaged_foms = {};
  std::vector<double> _averaged_rates = {};
  std::vector<line_parameters> _nearest_grid_coords = {};
  std::vector<double> _nearest_grid_foms = {};
  std::vector<double> _final_counts = {};
  std::vector<double> _signal_efficiencies = {};
  std::vector<double> _ratelim_signal_efficiencies = {};
  std::vector<double> _signal_efficiency_errs = {};
  std::vector<double> _ratelim_signal_efficiency_errs = {};
  std::vector<double> _background_efficiencies = {};
  std::vector<double> _background_efficiency_errs = {};
  std::vector<double> _background_rates = {};
  std::vector<double> _background_rate_errs = {};
  double _opt_fom{0};
  double _opt_rate{0};
  double _opt_backeff{0};
  double _opt_backeff_err{0};
  bool _include_prints{false};
  bool _save_path{true};
  bool _test_setting{false};
  bool _loose_thresh_lt_cap{false};

  virtual std::vector<double> minimised_function(const line_parameters&
  input_parameters) const {
    std::cout << "Calling base class function\n";
    return {0.0};
  };

  adam_base(bool glob_or_indiv, std::vector<io_manager*> tuples,
  double collision_rate, double ratelimit, double scalewidth,
  int accuracy, bool include_prints = false);

  virtual ~adam_base(){};

  void plot_epoch(std::string plot_dir, int job_num = 0);
  line_parameters random_coord_generator() const;

  std::vector<line_parameters> list_all_neighbour_coords(
  const line_parameters&) const;

  std::vector<line_parameters> list_orthogonal_coords(const line_parameters&)
  const;

  std::vector<line_parameters> list_orthogonal_diagonal_coords(
  const line_parameters&, const int = 1) const;

  void gradient_loop(
  double& scale_multiple, std::vector<bool>& zero_gradient_params,
  bool& zero_gradients, std::vector<line_parameters>& coords_to_calculate,
  std::vector<std::vector<double>>& all_stats,
  unsigned weight_size, std::vector<double>& gradient_vector,
  line_parameters input_parameters,
  const std::vector<std::vector<int>> weights,
  const std::vector<double> divisions,
  std::vector<std::vector<line_parameters>>& inputs_used,
  std::vector<std::vector<double>>& foms,
  std::vector<std::vector<double>>& background_efficiencies,
  std::vector<std::vector<double>>& background_efficiency_errs,
  std::vector<std::vector<double>>& background_rates,
  std::vector<std::vector<double>>& background_rate_errs,
  std::vector<std::vector<double>>& signal_efficiencies,
  std::vector<std::vector<double>>& signal_efficiency_errs,
  std::vector<std::vector<double>>& ratelim_signal_efficiencies,
  std::vector<std::vector<double>>& ratelim_signal_efficiency_errs,
  std::vector<std::vector<int>>& final_counts);

  std::vector<double> accurate_gradient_vector(
  const line_parameters& input_parameters);

  line_parameters grid_search_neighbourhood(
  const line_parameters input_parameters, double& optimal_fom,
  std::vector<line_parameters>& previous_neighbouring_coords = empty_vec,
  int depth = 1, bool high_recursion = false);

  void reverse_momentum_calc(
  std::vector<double> increase, std::vector<double>& first_moment,
  std::vector<double>& second_moment, double alpha, double beta1,
  double beta2, double epsilon);

  void smart_kick(const line_parameters current_coords,
  std::vector<double>& first_moment,
  std::vector<double>& second_moment, double alpha, double beta1,
  double beta2, double epsilon);

  line_parameters recursive_grid_search(
  const line_parameters& input_parameters,
  double& optimal_fom, int& num_grid_searches, int max_grid_searches);

  line_parameters adam_recursive_minimisation(
  const int niterations, const double alpha, const double beta1,
  const double beta2, const double epsilon,
  const std::vector<double> fixed_starting_point, bool random_start,
  bool skip_grid_search);

  void adam_minimum_loop(
  line_parameters& previous_coords, line_parameters& current_coords,
  bool warm_reset, int i, std::vector<double>& first_moment,
  std::vector<double>& second_moment, double alpha, double beta1,
  double beta2, double epsilon, int& start_check, int min_iterations,
  int ninterations, int check_depth, std::vector<bool> active_params,
  bool& break_or_not
  );

  line_parameters adam_minimum(const int niterations, const double alpha,
  const double beta1, const double beta2, const double epsilon = 1e-8,
  const std::vector<double> fixed_starting_point = {},
  const bool warm_reset = false, const bool random_start = false);
};


void grid_recursive_minimisation(
  std::vector<io_manager*> tuples, double collision_rate,
  double ratelimit, int niterations, double alpha, double beta1,
  double beta2, double epsilon, double scalewidth, int accuracy,
  std::vector<double> fixed_starting_point, double& optimal_fom,
  adam_base& minimiser, int grid_search_count,
  int max_grid_searches, line_parameters& grid_search_result,
  bool include_prints = false, bool random_start = false,
  bool skip_grid_search = false
  );

#endif
