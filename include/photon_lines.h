#ifndef _PHOTON_LINES
#define _PHOTON_LINES

#include <iostream>
#include <vector>
#include <math.h>
#include "line_parameters.h"
#include "line.h" 

struct diphoton_highmass : line_template<diphoton_highmass, 1> {
  static constexpr const char* _name = "Hlt1DiPhotonHighMass";
  static constexpr std::array<const char*, 1> _quantities = {"minet"};
  proxy _minET{this, "highmass_diphoton_et"}; 
  bool operator()(const float* monitored_quantities) const;   
};

#endif
