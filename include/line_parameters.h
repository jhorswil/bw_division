#ifndef _LINE_PARAMETERS
#define _LINE_PARAMETERS
#include <iostream>
#include <string_view>
#include <vector>
#include <array>
#include <math.h>

struct line_parameters {
  // Update this number if adding new parameters
  static const int _num_parameters{18};
  std::array<double, _num_parameters> _paramgroup; 
  std::array<std::string, _num_parameters> _paramnames =
  {"alpha_hadron", "alpha_electron", "alpha_muon", "charm_track_ip",
  "charm_track_pt", "twotrackmva", "twotrack_ks",
  "highmass_dimuon_pt",
  "displaced_dielectron_pt", "displaced_dielectron_ipchi2",
  "displaced_dimuon_pt", "displaced_dimuon_ipchi2",
  "highmass_diphoton_et",
  "lambda_detached_track_mipchi2", "lambda_detached_combination_bpvfd",
  "xi_lll_ipchi2",
  "track_ghostprob", "twotrack_ghostprob"
  // , "DownstreamKsToPiPi_minMVA",
  // , "DownstreamLambdaToPPi_minMVA" 
  };
  line_parameters(){}; 
  line_parameters(const double paramgroup[_num_parameters]);
  line_parameters(const std::vector<double> paramgroup);
  void initiate_members();
  void set(const std::vector<double>& params); 
  friend bool operator==(const line_parameters&, const line_parameters&);

  bool operator!=(const line_parameters& line_params) const;
  double& operator[](const int index);
  double get_parameter(const std::string& name) const {
    for(unsigned int i = 0 ; i != _num_parameters; ++i) {
      if (_paramnames.at(i) == name) return _paramgroup.at(i);
    }
    std::cout << "Parameter: " << name << " not found!" << std::endl;
    return -1; 
  }
  void set_parameter(const std::string& name, double value){
    for(unsigned int i{0} ; i != _num_parameters; ++i) 
      if (_paramnames[i] == name){
        _paramgroup[i] = value; return;
      }
    std::cout << "Error, " << name << " not found" << std::endl;
  }
  unsigned int size() const;
  void print(const std::vector<bool> active_params = {}) const;
  double access(const int) const;
  std::vector<double> return_vector();
};

std::ostream& operator<<(std::ostream& os, const line_parameters& parameters);

double read_access(const line_parameters, int);

struct line_parameters_hash {
  size_t operator()(const line_parameters& line_params) const {
    size_t id{0};
    line_parameters params;
    for (unsigned i{0}; i < params.size(); i++) {
      id += (i + 1) * std::hash<double>()(line_params._paramgroup[i]);
    }
    return id;
  }
};

#endif
