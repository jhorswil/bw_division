#ifndef _OPT_SAVE_AND_COLLATE
#define _OPT_SAVE_AND_COLLATE

#include <iostream>
#include <random>
#include <vector>
#include <sstream>

#include "TLatex.h"

#include "io_manager.h"
#include "adam_base.h"
#include "opt_discrete_gradient.h"

struct opt_discrete_gradient;

void read_and_edit_global_results(
  std::string global_results_path, std::string treename,
  std::vector<io_manager*> tuples,
  line_parameters grid_search_result, opt_discrete_gradient* opt_minimiser
  );

void write_global_results(
  std::string global_results_path, std::string treename,
  std::vector<io_manager*> tuples,
  line_parameters grid_search_result, opt_discrete_gradient* opt_minimiser
  );

void save_or_edit_global_min_results(
  std::vector<io_manager*> tuples,
  line_parameters grid_search_result, opt_discrete_gradient* opt_minimiser,
  std::string outfile_dir, int job_num
  );

void collate_global_min_results(
  std::string outfile_dir, std::vector<io_manager*> tuples
  );
#endif