#ifndef _DOWNSTREAMLINES
#define _DOWNSTREAMLINES

#include <iostream>
#include <vector>
#include <math.h>
#include "line_parameters.h"
#include "line.h" 

struct DownstreamKsToPiPi : line_template<DownstreamKsToPiPi, 1> {
  static constexpr const char* _name = "Hlt1DownstreamKsToPiPi"; 
  static constexpr std::array<const char*, 1> _quantities =
  {"mva_ks_detached"};
  proxy _minMVA {this, "DownstreamKsToPiPi_minMVA"};
  bool operator()(const float* monitored_quantities) const; 
};

struct DownstreamLambdaToPPi : line_template<DownstreamLambdaToPPi, 1> {
  static constexpr const char* _name = "Hlt1DownstreamLambdaToPPi";
  static constexpr std::array<const char*, 1> _quantities =
  {"mva_l0_detached"};
  proxy _minMVA {this, "DownstreamLambdaToPPi_minMVA"};
  bool operator()(const float* monitored_quantities) const;
};

#endif
