#ifndef _READ_BWCONFIG
#define _READ_BWCONFIG

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <fstream>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>

#include "io_manager.h"


typedef std::map<std::string, std::vector<std::string>> config_map;


io_manager* io_manager_json_readin(
  std::string name, std::string event_type,
  io_manager* background_io, std::string input_path,
  const std::vector<line_manager>& lines,
  const fixedlines_t& fixed_inputs,
  const linemask_t& partially_fixed_lines,
  const linemask_t& fully_fixed_lines,
  const linemask_t& off_lines,
  const parammask_t& fixed_line_params,
  double collision_rate,
  double ratelimit, double cutoff, double scalewidth, int accuracy,
  int max_events, int num_signal_tracks, double weight,
  bool reconstructable_only
  );

config_map read_bwconfig_file(
  std::string config_filepath, bool print_inputs
  );

void config_master(
  std::string config_filepath, int operation_option,
  int tuple_no, int job_num, int percentage_ratelimit,
  int alpha_input, int beta1_input,
  int scalewidth_input, int argc, char** argv 
  );
#endif
