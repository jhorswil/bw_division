#ifndef _EVENT_STORE
#define _EVENT_STORE

#include <iostream>
#include <memory>
#include <functional>
#include <array>
#include <algorithm>
#include <string>
#include <vector>

#include "ROOT/RDataFrame.hxx"
#include "misc_tools.h"
#include "line_parameters.h"
#include "line_store.h"

struct unique_id;
struct event_store {
    // Struct to contain all candidates in a single event, and a pass
    // decision mechanism for this event

    unsigned long _event_number{0};
    unsigned int _run_number{0};
    unsigned int _num_tracks{0};
    int _number_of_candidates{0};
    std::array<int, NUM_LINES> _quantities_per_line = {};
    std::array<int, NUM_LINES> _candidates_per_line = {};
    std::array<int, NUM_LINES+1> _offset = {};
    // std::vector<int> _postcut_candidates_per_line = {};
    // Access monitored quantities for a line 
    std::vector<float> _monitored_quantities = {};
    // Access candidates triggered by different lines
    
    event_store(const unsigned long& event_num, const unsigned int& run_num,
    const std::vector<std::vector<float>>& candidates,
    const std::array<int, NUM_LINES>& quantities_per_line,
    const std::array<int, NUM_LINES>& candidates_per_line);
    bool pass_decision(const linemask_t& activate_lines,
    const line_tuple& lines) const;
    bool pass_decision_specline(const int& lineindex,
    const line_tuple& lines) const;
    std::vector<int> postcut_candidates_per_line(
    const linemask_t& activate_lines, const line_tuple& lines) const;
    void remove_excess(const linemask_t& activate_lines,
    const line_tuple& lines);
    linemask_t which_lines(const linemask_t& activate_lines,
    const line_tuple& lines);
    void print();
};

void remove_event_by_index(
  std::vector<event_store>& event_vector,
  size_t index
  );

#endif
