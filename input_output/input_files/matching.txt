# BANDWIDTH DIVISION CONFIGURATION FILE
#
# PARAMETER AND LINE CONFIG SYNTAX:
# Variables separate by ','
# Keep the grid resolution of twotrackmva >= 5e-3 due to small scale local mins
# Keep the gradient resolution of twotrack_ks >= 0.145 due to small scale local mins
# Nico Kleijne and the charm WG asked for charm_track_pt to have a lower bound of 800 MeV (this is the HLT2 cut).
# Due to reconstruction being different between HLT1 and HLT2, it is set to 700 MeV. Do not touch minimum unless this
# has changed
# Charm also want the ip minimum to be 0.06mm.
#
# Example of how to fix a parameter
# alpha_hadron,fixed,600
# Starting points obtained from 1000 kHz thresholds from 2024 patches commit ed4953f22a93e4dff001a03ef446a354fdcf79ac Sept 2024
alpha_hadron,40,-2000,10000,600,600
alpha_electron,620,-2000,10000.0,600,600
alpha_muon,-740,-2000,10000,600,600
twotrackmva,0.965,0.9,1.0,100,100
twotrack_ks,-0.01,-3.0,-0.01,40,40
charm_track_pt,750,700,3500,28,42
charm_track_ip,0.075,0.06,0.4,34,68
highmass_dimuon_pt,550,0.0,3500,35,35
displaced_dielectron_pt,950,200.0,2000,20,40
displaced_dielectron_ipchi2,4.5,2.0,10.0,50,50
displaced_dimuon_pt,550,200.0,2000,20,40
displaced_dimuon_ipchi2,5,2.0,10.0,50,50
highmass_diphoton_et,3200,2000,7000,50,50
lambda_detached_track_mipchi2,19,0,200.0,200,100
lambda_detached_combination_bpvfd,17,0,50.0,50,50
xi_lll_ipchi2,0.0,0.0,50.0,100,100
track_ghostprob,fixed,0.8
twotrack_ghostprob,fixed,0.8
# DownstreamKsToPiPi_minMVA,fixed,0.8
# DownstreamKsToPiPi_minMVA,0.5,1.0,50,50
# DownstreamLambdaToPPi_minMVA,0.5,1.0,50,50
#
# OPERATION OPTION
# For choosing with operation the user wants to run when specifying the config file in the command line
operation_option,0
# TUPLE NUMBER
# For choosing the tuple number for individual minimisation testing when specifying the config file in the command line
tuple_num,0
# JOB NUMBER
# For choosing the job number for an operation to distinguish it from others in the output names when specifying the config file in the command line
job_num,3
# PERCENTAGE RATELIMIT
# For choosing the percentage of the ratelimit (specified below) when specifying the config file in the command line
percentage_ratelimit,100
#
# RANDOM START
# Start with a randomised set of coordinates
random_start,false
# SKIP GRID SEARCH
# If minimising with Adam, skip the search of the nearby discrete thresholds and instead choose the nearest to the continuous thresholds
skip_grid_search,false
# DIRECTORIES CONTAINING ROOT FILES FOR CHANNELS USED FOR EFFICIENCY/RATE CALCULATIONS (INCLUDING MINBIAS)
sample_dir,/eos/lhcb/user/t/tevans/HLT1/hlt1_pp_matching_tuning/300515/*.root,/eos/lhcb/user/t/tevans/HLT1/hlt1_pp_matching_tuning/Beam6800GeV-expected-2024-MagDown-Nu7.6/
#
# GENETIC ALGORITHM OPTION STRINGS 
init_options,PopSize=15:Steps=30:Cycles=3:ConvCrit=0.01:SaveBestCycle=5
opt_options,PopSize=60:Steps=30:Cycles=3:ConvCrit=0.01:SaveBestCycle=5
# 2D INDIVIDUAL SAMPLE PLOT ANGLES
plot_phi,-60
# plot_theta,30
# MAXIMUM RATE ALLOWED FROM MINBIAS CUTS (Hz)
ratelimit,1000000
# COLLISION RATE IN Hz TO BE MULTIPLIED BY POST SELECTION MINBIAS YIELD / NO SELECTION MINBIAS YIELD TO OBTAIN MINBIAS RATE (DEFAULT 30 MHz)
collision_rate,26800000
#
# PLOT MINIMISATION PATH
# Generate Global FoM epoch plot
plot_epoch,true
#
# MANUAL GLOBAL INPUTS
# For adding your own 'optimal' parameter inputs to the global minimisation to observe the results.
manual_global_inputs,296,0,0,0.9569,-0.0505,800,0.06,300,500,5,500,5,2500,9,1.4,9,0.5,0.5
#
# ADAM MINIMISER HYPERPARAMETERS
#
# NUMBER OF ITERATIONS TO SEARCH FOR MINIMUM
niterations,200
# LEARNING RATE (multiple of resolution)
alpha,1.0
# MOMENT PARAMETER 1
beta1,0.8
# MOMENT PARAMETER 2
beta2,0.999
# EPSILON
epsilon,1e-8
# WIDTH SCALE
# Fraction of parameter resolution used for width in discrete gradient calculation
scalewidth,1.0
#
# NUMBER OF MINIMISATION RUNS PER INDIVIDUAL SAMPLE MINIMISATION IN PARALLEL PROCESSING
nruns,1
# MAX NUMBER OF GRID SEARCHES AFTER ADAM OPTIMISATION (SET TO -1 FOR TEST MODE. SHORTEST POSSIBLE RUNTIME)
max_grid_searches,50
#
# RATE CUTOFF
# Multiple of cap designated ratelimit, where any events above this rate are to be filtered from each tuple before minimisation
cutoff,1.1
#
# PARAMS TO SCAN
# Name params to be included in phase space scan. Other params are inactive or set to maximum value
pram_one,alpha_hadron
pram_two,twotrackmva
#
# ACCURACY OF GRADIENT CALCULATION
# Setting that dictates number of widths calculated for discrete gradient calculation (and therefore accuracy). Range 0-3. See adam_base.cpp : accurate_gradient_calculation
accuracy,1
#
# SELECTION STATISTICS CHECK
# Input coordinates of statistics you would like to check
selection,40,620,-740,0.965,-0.01,750,0.075,550,950,4.5,550,5,3200,19,17,0.0,0.8,0.8
#
# FIXED STARTING COORDINATES
# Fixed initial coords for hyperparam scans
fixed_starting_point,40,620,-740,0.965,-0.01,750,0.075,550,950,4.5,550,5,3200,19,17,0.0,0.8,0.8
#
# MAX EVENTS PER SIGNAL SAMPLE
max_events,200000
#
# PERFORM BANDWIDTH DIVISION ONLY ON RECONSTRUCTABLE EVENTS
reconstructable_only,true
#
# LINES TO FIX
# Hlt1DiMuonHighMass_fix,highmass_dimuon_pt;100
# Hlt1TwoTrackMVA_fix,twotrackmva;0.995
# Hlt1TrackMVA_fix,alpha_hadron;600
# Hlt1D2KPi_fix,charm_track_pt;700,charm_track_ip;0.08
#
# LINES TO SWITCH OFF
# Hlt1TwoTrackKs_turnoff
# Hlt1TwoTrackMVA_turnoff
# Hlt1DiElectronDisplaced_turnoff
# Hlt1D2KPi_turnoff
#
# Filters 
Hlt1SingleHighPtMuon_filter,Hlt1SingleHighPtMuon__pt_t>10000
Hlt1SingleHighPtMuonNoMuID_filter,Hlt1SingleHighPtMuonNoMuID__pt_t>10000
Hlt1SingleHighPtElectron_filter,Hlt1SingleHighPtElectron__pt_corrected_t>10000
Hlt1TrackElectronMVA_filter,Hlt1TrackElectronMVA__pt_corrected_t>500&&Hlt1TrackElectronMVA__ipchi2_t>4
Hlt1TrackMVA_filter,Hlt1TrackMVA__pt_t>1000&&Hlt1TrackMVA__ipchi2_t>6
Hlt1TrackMuonMVA_filter,Hlt1TrackMuonMVA__muonchi2_t<1.8&&Hlt1TrackMuonMVA__ipchi2_t>4&&Hlt1TrackMuonMVA__pt_t>500
Hlt1TwoTrackMVA_filter,Hlt1TwoTrackMVA__mva_t>0.9
Hlt1DiMuonDisplaced_filter,Hlt1DiMuonDisplaced__muonchi2_t<1.8&&Hlt1DiMuonDisplaced__pt_t>200&&Hlt1DiMuonDisplaced__ipchi2_t>2
Hlt1DiElectronDisplaced_filter,Hlt1DiElectronDisplaced__pt_t>200&&Hlt1DiElectronDisplaced__ipchi2_t>2
Hlt1DiElectronLowMass_massSlice3_prompt_filter,PRESCALE=0.3
Hlt1DiElectronLowMass_massSlice4_prompt_filter,PRESCALE=0.3
Hlt1DiElectronLowMass_SS_massSlice1_prompt_filter,PRESCALE=0.02
Hlt1DiElectronLowMass_SS_massSlice2_prompt_filter,PRESCALE=0.02
Hlt1DiElectronLowMass_SS_massSlice3_prompt_filter,PRESCALE=0.02
Hlt1DiElectronLowMass_SS_massSlice4_prompt_filter,PRESCALE=0.02
Hlt1DiElectronLowMass_SS_massSlice1_displaced_filter,PRESCALE=0.02
Hlt1DiElectronLowMass_SS_massSlice2_displaced_filter,PRESCALE=0.02
Hlt1DiElectronLowMass_SS_massSlice3_displaced_filter,PRESCALE=0.02
Hlt1DiElectronLowMass_SS_massSlice4_displaced_filter,PRESCALE=0.02
Hlt1DiMuonNoIP_SS,PRESCALE=0.1
