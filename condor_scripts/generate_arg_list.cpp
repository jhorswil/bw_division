#include <iostream>
#include <vector>
#include <fstream>
#include <string>

int main( int argc, char** argv )
{
  std::ofstream output( argv[1] );
  unsigned nC =1;
  std::vector<std::vector<std::string> > args(1);
  std::vector<std::string> flag; 

  for( unsigned int arg = 2; arg != argc; ++ arg )
  {
    if( std::string(argv[arg]) == ":::" ){
      nC *= args.rbegin()->size();   
      args.resize( args.size() + 1);
      continue; 
    }
    args.rbegin()->push_back( argv[arg] );
  }
  nC *= args.rbegin()->size();
  
  for( unsigned i = 0 ; i != nC; ++i )
  {
    unsigned r = i; 
    for( std::vector<std::vector<std::string> >::iterator it = args.begin(); it != args.end(); ++it ){
      std::vector<std::string>& pack = *it; 
      output << pack[ r % pack.size() ] << " "; 
      r = (r - r % pack.size() ) / pack.size(); 
    }
    output << std::endl; 
  } 
  output.close();
}
