#include <iostream>
#include <string>

#include "io_manager.h"
#include "read_bwconfig.h"
#include "misc_tools.h"
#include "line_manager.h"
#include "init_discrete_gradient.h"
#include "line_tools.h"

int main(int argc, char** argv) {
  // Bandwidth division master file
  
  std::cout<< "\n#######################################################\n";
  std::cout<< "########## HLT1 BANDWIDTH DIVISION PROGRAM ############\n";
  std::cout<< "#######################################################\n";
  std::cout << "\n----------- Type ./bin/bw_division help for "
  "list of all commands -------------\n\n";

  // if (argc < 2) {
	// 	std::cout << "\nPlease provide an int argument on the command line"
	// 	" to indicate the operation option.\n";
	// 	exit(0);
	// }

  std::string config_filepath{""};
  if (argc > 1) {
    std::string first_argument(argv[1]);
    if (first_argument.compare("help") == 0) {
      std::cout << "\n-------------- HLT1 BANDWIDTH DIVISION HELP "
      "--------------- \n\n";
      std::cout << "-------------- ALL COMMAND LINES ARGUMENTS ARE "
      "OPTIONAL --------------\n";

      std::cout << "--- OPERATION OPTIONS : \n";
      std::cout << "(First command line argument)\n\n";

      std::cout << "(0): Full global Adam multithreaded minimisation. "
      "All individual samples are minimised in parallel by all available "
      "cores. \nThen in global minimisation, each FoM calculation "
      "involved "
      "in calculating the gradient is performed in parallel.\n\n"
      << "(1): Full global Genetic Algorithm serial minimisation "
      "(no multithreading).\n\n"
      << "(2): Collating best results from parallel condor"
      << " individual minimisation jobs or previous serial individual "
      "minimisations to begin a global multithreaded Adam "
      "minimisation.\n\n"
      << "(3): Collate saved global minimisation results to assess "
      "global minimisation convergence rate.\n\n"
      << "(4): Functionality for parallel (condor batch job) individual"
      " minimisation (for assessing individual sample convergence rate)"
      ".\n\n"
      << "(5): Collate individual minimisation results for all"
      " active samples without running the global minimisation (for "
      "assessing individual sample convergence rate).\n\n"
      << "(6): Run individual minimisation with"
      " fixed hyperparams and specified starting coordinates to test "
      "edge-cases.\n\n"
      << "(7): Assess success rate of Adam hyperparameter configuration"
      "from a specified set of starting coordinates.\n"
      "See below (---ADAM HYPERPARAMETERS) for Adam hyperparameter "
      "instructions.\n\n"
      << "(8): Plot sample efficiency and minbias rate vs. a scan of "
      "each parameter in each tuple/sample.\n\n"
      << "(9): Scan and plot the FoM, efficiency and minbias rate "
      << "in input parameter space, for pairs of active parameters in "
      "each tuple.\n\n"
      << "(10): Statistics check for certain tuple and"
      " specified selection.\n"
      "Calculate FoM, rate, sample efficiency, ratelimited signal "
      "efficiency and event count for a given selection specified in "
      "bw_config.txt.\n\n"
      << "(11): Scan existing saved tuples for individual minimisation "
      "results, then use manual global inputs set in the config file to"
      " skip global minimisation and print and plot results.\n\n"
      // << "(12): Collate and combine absolute total yield and absolute "
      // "line yields from a directory of Allen log files, and write to a "
      // "singular log file\n\n"
      << "(12): Run individual minimisation for both the GA and Adam and "
      << "record the ratio of runtimes.\n"
      << "(13): Print tuples/signal sample and their associated tuple "
      << "numbers and then exit the program\n\n";

      std::cout << "\n--- RATELIMIT : \n";
      std::cout << "(Second command line argument)\n\n";

      std::cout << "This is the percentage of the full rate outlined by "
      "the variable 'ratelimit' in input_output/input_files/bw_config.txt."
      "\nFor example, if the ratelimit is set to 1000000 Hz in the config "
      "file, and the second command line argument is 75\n"
      "(./bin/bw_division 0 75) then the "
      "new ratelimit is 750000 Hz, since this is 75 percent of 1 MHz. "
      "\nThis means the target minbias "
      "efficiency is the ratelimit over the collision rate (also defined "
      "in the config file): 750000 Hz/30000000 Hz = 0.025.\nThis defaults "
      "to 100 percent if not specified\n";

      std::cout << "\n---TUPLE/SAMPLE NUMBER : \n";
      std::cout << "(Third command line argument)\n\n";

      std::cout << "Specify the individual sample which you want to "
      "perform a study on. "
      "This is relevant for operation options 4, 6, 7, 9 and 10.\n"
      "Choose operation option 13 to receive a list of the samples and "
      "their associated tuple numbers.\nThese studies help identify "
      "problems"
      " that may occur in the minimisation process, and also helps to "
      "visualise the processes occurring. \nDefaults to 100 if not "
      "specified. Error message will occur if it is required and not "
      "specified, or the tuple number does not exist.\n";

      std::cout << "\n---JOB NUMBER : \n";
      std::cout << "(Fourth command line argument)\n\n";
      
      std::cout << "Differentiates the naming of a given minimisation's"
      " output files. Useful when running batch jobs in parallel to "
      "collate the results from later. Defaults to 0 if not specified."
      "\n";

      std::cout << "\n---ADAM HYPERPARAMETERS : \n";
      std::cout << "(Fifth, sixth and seventh command line arguments)"
      "\n\n";

      std::cout << "Manually specify learning rate (alpha), beta1 and "
      "beta2"
      ".\nUseful for running batch jobs to assess the success rate of "
      "different configurations of hyperparameters during an individual "
      "minimisation.\nInputs are integer and then divided by "
      "10.0, 100.0, 1000.0 by the"
      " program, e.g, ./bin/bw_division 7 100 0 0 10 80 999 gives "
      "alpha = 1, beta1 = 0.8 and beta2 = 0.999\n\n\n";

      std::cout << "Example commands:\nFor full bandwidth division at "
      "150% ratelimit: ./bin/bw_division 0 150\nFor individual "
      "minimisation at 120% ratelimit, tuple 5, job number 3: "
      "./bin/bw_division 4 120 5 3\n";
      exit(0);
    } else if (first_argument.find(".txt") != std::string::npos
    || first_argument.find("config") != std::string::npos) {
      config_filepath =
      BWDIVROOT + std::string("/input_output/input_files/" + first_argument);
      // Value to be written later
      config_master(config_filepath, 0, -42, -42,
      100, 10, 80, 999, argc, argv);
      exit(0);
    }
  } 


  int operation_option = argc > 1 ? std::stoi(argv[1]) : 0;
  if (operation_option > 13 || operation_option < 0) {
    std::cout << "Invalid operation option (first command line argument).\n";
    std::cout << "\n----------- Type ./bin/bw_division help for "
    "list of all commands -------------\n\n";
    exit(0);
  }

  if (argc == 1) {
    std::cout << "No command line operation option chosen. "
    "Defaulting to full global Adam minimisation...\n";
  }

  int tuple_num, job_num, percentage_ratelimit, alpha_input, beta1_input,
  beta2_input;
  
  if (argc < 3) {

    tuple_num = -1;
    job_num = 0;
    percentage_ratelimit = 100;
    alpha_input = 10;
    beta1_input = 80;
    beta2_input = 999;
    
    if (operation_option == 4 || operation_option == 7) {
      std::cout << "No tuple number/job number argument provided for "
      "parallel individual minimisation, but corresponding operation "
      "option chosen. Exiting...\n";
      exit(0);
    } 
  } else {
    percentage_ratelimit = std::stoi(argv[2]);
    tuple_num = argc < 4 ? 100 : std::stoi(argv[3]);
    job_num = argc < 5 ? 0 : std::stoi(argv[4]);
    alpha_input = argc < 6 ? 10 : std::stoi(argv[5]);
    beta1_input = argc < 6 ? 90 : std::stoi(argv[6]);
    beta2_input = argc < 6 ? 999 : std::stoi(argv[7]);


    // ########## PARALLEL PROCESSING INSTRUCTIONS ###########
    // Generate a shell script (e.g, run_bw_division.sh) that generates
    // the correct environment and then calls bw_division exectuable:
    // /afs/cern.ch/work/j/jhorswil/bw_division/build/bin/bw_division "$@"

    // For operation 4:
    // Enter tuple_num as integer e.g ./run_bw_division.sh 4 100 0 ->
    // Calculate optimal statistics with 100% ratelimit (1 MHz) for the 0th
    // tuple/esample.
    // For all samples, one could do for example:
    // ./condorforEach /path/to/run_bw_division.sh 4 ::: 100 ::: `seq 0 80`
    // for 100% ratelimit (1 MHz) and 81 samples read from the jsons.
    // Since the output result root files store data accumulatively,
    // you could do 80 clusters of iterative parallel calculations:
    // ./condorForEach /path/to/run_bw_division.sh 4 ::: 100 ::: `seq 0 80`
    // ::: `seq 0 5` written to 6 files per sample, the best result of which
    // which would be retrieved for the global minimisation

    // For operation 7:
    // Same as operation 4 but you enter default job_num (0) and your
    // desired tuple (2), then sequences of hyperparameters (only one at a
    // time), e.g:
    // ./condorForEach /path/to/run_bw_division.sh 7 ::: 100 ::: 2 ::: 0 :::
    // `seq 10 200` ::: 90 ::: 999
    // This would test the learning rate between 1 and 20x the
    // parameter resolution
  }

  config_filepath = BWDIVROOT + std::string("/input_output/input_files/bw_config.txt");

  config_master(config_filepath, operation_option, tuple_num, job_num,
  percentage_ratelimit, alpha_input, beta1_input, beta2_input, argc, argv);
}
