#include <functional> 
#include <vector> 
#include <TGraph.h> 
#include <TCanvas.h> 
#include <TFitResult.h> 
#include <TAxis.h>
#include <map>
#include <iostream> 
#include "misc_tools.h" 

using smoothing_function = std::function<std::vector<double>(TGraph*)>; 

template <unsigned order> struct smoother {
  std::vector<double> operator()(TGraph* graph) const {
    auto fit_result =
    graph->Fit(("pol" + std::to_string(order)).c_str(), "qS");
    std::vector<double> values; 
    auto parameters = fit_result->Parameters(); 
    for(int i = 0 ; i != graph->GetN(); ++i) {
      double x{0};
      double y{0};
      graph->GetPoint(i, x, y); 
      double val = 0; 
      for(int j = 0 ; j != order+1; ++j) {
        val += parameters[j] * std::pow(x, j);
      }

      if (order == 0) {
        std::cout << val << " -> rounded: "
        << _granularity * round(val / _granularity) << std::endl;
      }
      values.push_back(_granularity * round(val / _granularity));
    }
    return values; 
  }
  smoother(double gran) : _granularity(gran) {} 
  double _granularity{0}; 
};


std::vector<double> get_y(TGraph* graph) {
  std::vector<double> values; 
    for(int i = 0 ; i != graph->GetN(); ++i) {
      double x{0};
      double y{0};
      graph->GetPoint(i, x, y); 
      values.push_back(y); 
    }
  return values; 
}

int main(int argc, char** argv) 
{
  std::vector<int> output_rate;
  std::map<std::string, TGraph*> graphs; 

  std::map<std::string, smoothing_function> functions; 
  functions["TwoTrackMVA_minMVA"] = smoother<2>(0.001); 
  functions["D2HH_track_ip"]      = smoother<0>(0.005); 
  functions["D2HH_track_pt"]      = smoother<0>(50); 
  functions["DiElectronDisplaced_ipchi2"] = smoother<0>(0.5);
  functions["DiElectronDisplaced_pt"] = smoother<1>(50); 
  functions["DiMuonDisplaced_ipchi2"] = smoother<0>(0.5);
  functions["DiMuonDisplaced_pt"]     = smoother<1>(50); 
  functions["DiMuonHighMass_pt"]      = smoother<2>(50); 
  functions["DiPhotonHighMass_minET"] = smoother<2>(100); 
  functions["DetachedTrack_combination_bpvfd"] = smoother<1>(1); 
  functions["LambdaLLDetachedTrack_track_mipchi2"] = smoother<0>(1); 
  functions["TrackElectronMVA_alpha"] = smoother<2>(20); 
  functions["TrackMuonMVA_alpha"] = smoother<2>(20); 
  functions["TrackMVA_alpha"]     = smoother<2>(20); 
  functions["XiOmegaLLL_track_ipchi2"] = smoother<0>(0.5); 

  for(int i = 700; i <= 1300; i+=50) {
    auto file_functor =
    [&](const std::string& line) { 
      if (line.find("Threshold") == 0) return; // continue;
      auto tokens = split(line, '&');
      std::string name = tokens[0];
      double value = stod(split(tokens[1], ')')[0]); 
      // std::cout << name << " " << value << std::endl; 
      if (graphs.count(name) == 0) graphs[name] = new TGraph(); 
      graphs[name]->SetPoint(graphs[name]->GetN(), i, value); 
    };

    process_file(BWDIVROOT + std::string("/smoothing_output/thresholds_")
    + std::to_string(i) + "_spreadsheet", file_functor);
  }

  std::vector<std::ofstream> threshold_sets; 
  // kHz
  int tightest_ratelimit{700};
  int loosest_ratelimit{1300};
  int ratelimit_increase{50};

  for (int i{tightest_ratelimit}; i <= loosest_ratelimit;
  i += ratelimit_increase) {
    threshold_sets.emplace_back(
    BWDIVROOT + std::string("/smoothing_output/thresholds_smoothed_")
    + std::to_string(i) + ".py");
    threshold_sets.back()
    << "from AllenConf.thresholds.thresholds import Thresholds" << std::endl;
    threshold_sets.back() << "threshold_settings = Thresholds(" << std::endl;
  }

  for(auto& [key, graph] : graphs) {
    TCanvas* c1 = new TCanvas("c1", "", 500,400);
    graph->GetXaxis()->SetTitle("Rate [KHz]"); 
    graph->GetYaxis()->SetTitle(key.c_str());

    std::vector<double> new_thresholds; 
    if (functions.find(key) != functions.end()) {
      new_thresholds = functions[key](graph); 
      std::cout << vector_to_string(new_thresholds, " ") << std::endl; 
    } else {
      new_thresholds = get_y(graph); 
    }

    for(unsigned int i = 0 ; i != threshold_sets.size(); ++i) {
      threshold_sets[i] << key << " = " << new_thresholds[i]
      << (key == "XiOmegaLLL_track_ipchi2" ? ")" : ",") << std::endl; 
    }

    graph->Draw(); 
    c1->SaveAs(("smoothing_output/" + key + "_vs_rate.pdf").c_str());
    delete c1;
  }
}
